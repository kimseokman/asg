<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

$sptid = $_POST["sptid"];//test
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

$start_year = $_POST["start_year"];
$start_month = str_pad($_POST["start_month"], "2", "0", STR_PAD_LEFT);
$start_day = str_pad($_POST["start_day"], "2", "0", STR_PAD_LEFT);

$start_date = $start_year."-".$start_month."-".$start_day;

$end_year = $_POST["end_year"];
$end_month = str_pad($_POST["end_month"], "2", "0", STR_PAD_LEFT);
$end_day = str_pad($_POST["end_day"], "2", "0", STR_PAD_LEFT);

$end_date = $end_year."-".$end_month."-".$end_day;

$reportarray = $asg_obj->HistoryGetReportList($start_date, $end_date);

?>
<div class="guide_search_date">
	<? echo $start_date; ?><span> - </span><? echo $end_date; ?>
</div>
<?
if(count($reportarray) == 0) {
?>
<div class="guide_ment">
	<p>해당 기간 내에 존재하는 원격 지원 기록이 없습니다.</p>
	<p>다른 기간을 선택하신 후 다시 시도하시기 바랍니다.</p>
</div>
<?
}//end of : if(count($reportarray) == 0)
else{
?>
<div class="guide_option">
	<div class="set_date_section">
		<select class="searchbar_select select_report_list" name="reportnum" size='15' onchange="ajax_search_history_report()">
		<?
		while(list($key, $val) = each($reportarray)) {
		?>
			<option value="<? echo $val["reportnum"]; ?>">
				<? echo substr($val["starttime"], 0, 16)."  ".$val["name"]; ?>
			</option>
		<?
		}//end of : while(list($key, $val) = each($reportarray))
		?>
		</select>
	</div>
	<div class="search_side_btn_area">
		<a href="#" class="search_btn search_date_history_export_btn" onclick="search_date_history_export()">검색한 기록 보기 (Excel)</a>
	</div>
</div>
<?
}//end of : if(count($reportarray) != 0)
?>