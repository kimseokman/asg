<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

$sptid = $_POST["sptid"];//test
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

$reportnum = $_POST["report_num"];


$reportrow = $asg_obj->GetReport($reportnum);


function print_operationlog($operationlog_id){
	$operationlog_str = "미체크";

	switch($operationlog_id) {
		case 1:
			$operationlog_str = "해결";
			break;
		case 2:
			$operationlog_str = "보류";
			break;
		case 3:
			$operationlog_str = "직접 지원 필요";
			break;
		case 0:
		default:
			$operationlog_str = "미체크";
			break;
	}

	return $operationlog_str;
}

function print_satisfaction($satisfaction_id){
	$satisfaction_str = "무응답";

	switch($satisfaction_id) {
		case 1:
			$satisfaction_str = "만족";
			break;
		case 2:
			$satisfaction_str = "보통";
			break;
		case 3:
			$satisfaction_str = "불만족";
			break;
		case 0:
		default:
			$satisfaction_str = "무응답";
			break;
	}

	return $satisfaction_str;
}

if($reportrow == NULL) {
	echo "EMPTY";
}//end of : if($reportrow == NULL)
else {
	$operationlog = print_operationlog($reportrow->operationlog);
	$satisfaction = print_satisfaction($reportrow->satisfaction);
?>
<div class="history_title"><span>원격 지원 기록</span></div>
<div class="report_area">
	<table class="history_table">
		<colgroup>
			<col width="130px"/>
			<col width="130px"/>
			<col width="130px"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<td>고객 이름</td>
				<td><? echo  $reportrow->name; ?></td>
				<td>E-mail</td>
				<td><a href='mailto:<? echo $reportrow->email; ?>'><? echo $reportrow->email; ?></a></td>
			</tr>
			<tr>
				<td class="common_table_color04">전화 번호</td>
				<td class="common_table_color04"><? echo  $reportrow->tel; ?></td>
				<td class="common_table_color04">IP</td>
				<td class="common_table_color04"><? echo $reportrow->ip; ?></td>
			</tr>
			<tr>
				<td colspan="2">지원 시작 시각</td>
				<td colspan="2"><? echo $reportrow->starttime; ?></td>
			</tr>
			<tr>
				<td class="common_table_color04" colspan="2">지원 종료 시각</td>
				<td class="common_table_color04" colspan="2"><? echo $reportrow->endtime; ?></td>
			</tr>
			<tr>
				<td>장애 유형</td>
				<td colspan="3">
				<?
				if($reportrow->trbnum == 0) {
					echo "선택 안됨";
				}
				else {
					echo $reportrow->trbname;
				}
				?>	
				</td>
			</tr>
			<tr>
				<td class="common_table_color04">해결 여부</td>
				<td class="common_table_color04"><? echo  $operationlog; ?></td>
				<td class="common_table_color04">고객 만족도</td>
				<td class="common_table_color04"><? echo $satisfaction; ?></td>
			</tr>
			<tr>
				<td>고객 피드백</td>
				<td colspan="3"><? echo $reportrow->usercomment; ?></td>
			</tr>
			<tr>
				<td class="common_table_color04">지원 내용</td>
				<td  class="common_table_color04" colspan="3">
					<textarea class="report_content" rows="3" cols="48" wrap="on" name="report"><? echo $reportrow->report; ?></textarea>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="report_area_btns">
		<a href="#" class="search_btn modified_report_btn" onclick="modify_report(<? echo $reportnum; ?>)">내용 수정</a>	
	</div>
</div>
<?
}//end of : if($reportrow != NULL)