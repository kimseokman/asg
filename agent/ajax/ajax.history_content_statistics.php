<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

$sptid = $_POST["sptid"];//test
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

$start_year = $_POST["start_year"];
$start_month = str_pad($_POST["start_month"], "2", "0", STR_PAD_LEFT);
$start_day = str_pad($_POST["start_day"], "2", "0", STR_PAD_LEFT);

$start_date = $start_year."-".$start_month."-".$start_day;

$end_year = $_POST["end_year"];
$end_month = str_pad($_POST["end_month"], "2", "0", STR_PAD_LEFT);
$end_day = str_pad($_POST["end_day"], "2", "0", STR_PAD_LEFT);

$end_date = $end_year."-".$end_month."-".$end_day;

$statistics_trouble_type_arr = $asg_obj->HistoryGetStatsByTrbType($start_date, $end_date);// 장애 유형별 통계
$statistics_resolve_arr = $asg_obj->HistoryGetStatsByFixing($start_date, $end_date);// 해결 여부별 통계
$statistics_satistaction_arr = $asg_obj->HistoryGetStatsBySatisfaction($start_date, $end_date);// 고객 만족도별 통계

function is_zero($value){
	if($value == 0){
		return TRUE;
	}

	return FALSE;
}

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}
?>
<div class="history_title"><span>원격 지원 기록 통계</span></div>
<div class="statistics_area">
	<table class="history_table">
		<colgroup>
			<col width="*" />
			<col width="190px" />
			<col width="190px" />
		</colgroup>
		<thead>
			<tr>
				<th>장애유형</th>
				<th>발생수</th>
				<th>발생비율</th>
			</tr>
		</thead>
		<tbody>
		<?
		$trouble_type_i = 0;
		while(list($key, $val) = each($statistics_trouble_type_arr["statsarray"])) {
			if(is_zero($statistics_trouble_type_arr["sum"])) {
		?>
			<tr>
				<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["trbname"]; ?></td>
				<td class="<? echo print_table_class($trouble_type_i); ?>">0</td>
				<td class="<? echo print_table_class($trouble_type_i); ?>">0 %</td>
			</tr>	
		<?
			}//end of : if($statistics_trouble_type_arr["sum"] == 0)
			else{
		?>
			<tr>
				<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["trbname"]; ?></td>
				<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["count"]; ?></td>
				<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo ((int)round($val["count"]/$statistics_trouble_type_arr["sum"]*100)); ?> %</td>
		<?	
			}//end of : if($statistics_trouble_type_arr["sum"] != 0)	
		?>
			</tr>
		<?		
			$trouble_type_i++;
		}//end of : while(list($key, $val) = each($statistics_trouble_type_arr["statsarray"]))

		if(is_zero($statistics_trouble_type_arr["sum"])) {
		?>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_trouble_type_arr["sum"]; ?></td>
				<td>0 %</td>
			</tr>	
		<?
		}//end of : if($statistics_trouble_type_arr["sum"] == 0)
		else{
		?>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_trouble_type_arr["sum"]; ?></td>
				<td>100 %</td>
			</tr>	
		<?
		}//end of : if($statistics_trouble_type_arr["sum"] != 0)	
		?>
		</tbody>
	</table>
</div>
<div class="statistics_area">
	<table class="history_table">
		<colgroup>
			<col width="*" />
			<col width="190px" />
			<col width="190px" />
		</colgroup>
		<thead>
			<tr>
				<th>해결 여부</th>
				<th>발생수</th>
				<th>발생비율</th>
			</tr>
		</thead>
		<tbody>	
		<?
		if(is_zero($statistics_resolve_arr["sum"])) {
		?>
			<tr>
				<td class="<? echo print_table_class(0); ?>">해결</td>
				<td class="<? echo print_table_class(0); ?>">0</td>
				<td class="<? echo print_table_class(0); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(1); ?>">보류</td>
				<td class="<? echo print_table_class(1); ?>">0</td>
				<td class="<? echo print_table_class(1); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(2); ?>">직접 지원 필요</td>
				<td class="<? echo print_table_class(2); ?>">0</td>
				<td class="<? echo print_table_class(2); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(3); ?>">미체크</td>
				<td class="<? echo print_table_class(3); ?>">0</td>
				<td class="<? echo print_table_class(3); ?>">0 %</td>
			</tr>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_resolve_arr["sum"]; ?></td>
				<td>0 %</td>
			</tr>
		<?
		}//end of : if($statistics_resolve_arr["sum"] == 0)
		else {
		?>
			<tr>
				<td class="<? echo print_table_class(0); ?>">해결</td>
				<td class="<? echo print_table_class(0); ?>"><? echo $statistics_resolve_arr[1]; ?></td>
				<td class="<? echo print_table_class(0); ?>"><? echo ((int)round($statistics_resolve_arr[1]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(1); ?>">보류</td>
				<td class="<? echo print_table_class(1); ?>"><? echo $statistics_resolve_arr[2]; ?></td>
				<td class="<? echo print_table_class(1); ?>"><? echo ((int)round($statistics_resolve_arr[2]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(2); ?>">직접 지원 필요</td>
				<td class="<? echo print_table_class(2); ?>"><? echo $statistics_resolve_arr[3]; ?></td>
				<td class="<? echo print_table_class(2); ?>"><? echo ((int)round($statistics_resolve_arr[3]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(3); ?>">미체크</td>
				<td class="<? echo print_table_class(3); ?>"><? echo $statistics_resolve_arr[0]; ?></td>
				<td class="<? echo print_table_class(3); ?>"><? echo ((int)round($statistics_resolve_arr[0]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_resolve_arr["sum"]; ?></td>
				<td>100 %</td>
			</tr>
		<?
		}//end of : if($statistics_resolve_arr["sum"] != 0)
		?>
		</tbody>
	</table>
</div>
<div class="statistics_area">
	<table class="history_table">
		<colgroup>
			<col width="*" />
			<col width="190px" />
			<col width="190px" />
		</colgroup>
		<thead>
			<tr>
				<th>고객 만족도</th>
				<th>발생수</th>
				<th>발생비율</th>
			</tr>
		</thead>
		<tbody>	
		<?
		if(is_zero($statistics_satistaction_arr["sum"])) {
		?>	
			<tr>
				<td class="<? echo print_table_class(0); ?>">만족</td>
				<td class="<? echo print_table_class(0); ?>">0</td>
				<td class="<? echo print_table_class(0); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(1); ?>">보통</td>
				<td class="<? echo print_table_class(1); ?>">0</td>
				<td class="<? echo print_table_class(1); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(2); ?>">불만족</td>
				<td class="<? echo print_table_class(2); ?>">0</td>
				<td class="<? echo print_table_class(2); ?>">0 %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(3); ?>">무응답</td>
				<td class="<? echo print_table_class(3); ?>">0</td>
				<td class="<? echo print_table_class(3); ?>">0 %</td>
			</tr>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_satistaction_arr["sum"]; ?></td>
				<td>0 %</td>
			</tr>
		<?
		}//end of : if($statistics_satistaction_arr["sum"] == 0) 
		else{
		?>
			<tr>
				<td class="<? echo print_table_class(0); ?>">만족</td>
				<td class="<? echo print_table_class(0); ?>"><? echo $statistics_satistaction_arr[1]; ?></td>
				<td class="<? echo print_table_class(0); ?>"><? echo ((int)round($statistics_satistaction_arr[1]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(1); ?>">보통</td>
				<td class="<? echo print_table_class(1); ?>"><? echo $statistics_satistaction_arr[2]; ?></td>
				<td class="<? echo print_table_class(1); ?>"><? echo ((int)round($statistics_satistaction_arr[2]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(2); ?>">불만족</td>
				<td class="<? echo print_table_class(2); ?>"><? echo $statistics_satistaction_arr[3]; ?></td>
				<td class="<? echo print_table_class(2); ?>"><? echo ((int)round($statistics_satistaction_arr[3]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr>
				<td class="<? echo print_table_class(3); ?>">무응답</td>
				<td class="<? echo print_table_class(3); ?>"><? echo $statistics_satistaction_arr[0]; ?></td>
				<td class="<? echo print_table_class(3); ?>"><? echo ((int)round($statistics_satistaction_arr[0]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
			</tr>
			<tr class="model_tfoot">
				<td>전체</td>
				<td><? echo $statistics_satistaction_arr["sum"]; ?></td>
				<td>100%</td>
			</tr>
		<?
		}//end of : if($statistics_satistaction_arr["sum"] != 0) 
		?>	
		</tbody>
	</table>
</div>