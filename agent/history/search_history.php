<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

$sptid = $_POST["sptid"];
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

if($web_login_res != "OK"){
	header("Location: ".AGENT_WEB_BAN_URL);
}

$reportnum = $_POST["reportnum"];
/**
 * search_history.php
 * 2015.07.02 | KSM 
 */

$year_arr = $utils_obj->GetYears();
$month_arr = $utils_obj->GetMonths();
$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
$day_arr = $utils_obj->GetDays($this_last_day);

$start_year = date('Y');
if($_POST["start_year"] != NULL){
	$start_year = $_POST["start_year"];	
}

$start_month = date('m');
if($_POST["start_month"] != NULL){
	$start_month = $_POST["start_month"];	
}

$start_day = date('d');
if($_POST["start_day"] != NULL){
	$start_day = $_POST["start_day"];
}

$start_date = $start_year."-".str_pad($start_month, "2", "0", STR_PAD_LEFT)."-".str_pad($start_day, "2", "0", STR_PAD_LEFT);

$end_year = date('Y');
if($_POST["end_year"] != NULL){
	$end_year = $_POST["end_year"];
}

$end_month = date('m');
if($_POST["end_month"] != NULL){
	$end_month = $_POST["end_month"];	
}

$end_day = date('d');
if($_POST["end_day"] != NULL){
	$end_day = $_POST["end_day"];	
}

$end_date = $end_year."-".str_pad($end_month, "2", "0", STR_PAD_LEFT)."-".str_pad($end_day, "2", "0", STR_PAD_LEFT);

$statistics_trouble_type_arr = $asg_obj->HistoryGetStatsByTrbType($start_date, $end_date);// 장애 유형별 통계
$statistics_resolve_arr = $asg_obj->HistoryGetStatsByFixing($start_date, $end_date);// 해결 여부별 통계
$statistics_satistaction_arr = $asg_obj->HistoryGetStatsBySatisfaction($start_date, $end_date);// 고객 만족도별 통계

function is_zero($value){
	if($value == 0){
		return TRUE;
	}

	return FALSE;
}

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

function print_operationlog($operationlog_id){
	$operationlog_str = "미체크";

	switch($operationlog_id) {
		case 1:
			$operationlog_str = "해결";
			break;
		case 2:
			$operationlog_str = "보류";
			break;
		case 3:
			$operationlog_str = "직접 지원 필요";
			break;
		case 0:
		default:
			$operationlog_str = "미체크";
			break;
	}

	return $operationlog_str;
}

function print_satisfaction($satisfaction_id){
	$satisfaction_str = "무응답";

	switch($satisfaction_id) {
		case 1:
			$satisfaction_str = "만족";
			break;
		case 2:
			$satisfaction_str = "보통";
			break;
		case 3:
			$satisfaction_str = "불만족";
			break;
		case 0:
		default:
			$satisfaction_str = "무응답";
			break;
	}

	return $satisfaction_str;
}

include_once("../header.php");
?>
<!-- history_header -->
<input type="hidden" id="sptid" name="sptid" value="<? echo $sptid; ?>" />
<input type="hidden" id="startCurrentYear" name="startCurrentYear" value="<? echo date('Y'); ?>" />
<input type="hidden" id="startCurrentMonth" name="startCurrentMonth" value="<? echo date('m'); ?>" />
<input type="hidden" id="startCurrentDay" name="startCurrentDay" value="<? echo date('d'); ?>" />
<input type="hidden" id="endCurrentYear" name="endCurrentYear" value="<? echo date('Y'); ?>" />
<input type="hidden" id="endCurrentMonth" name="endCurrentMonth" value="<? echo date('m'); ?>" />
<input type="hidden" id="endCurrentDay" name="endCurrentDay" value="<? echo date('d'); ?>" />
<input type="hidden" id="startBaseYear" name="startBaseYear" value="<? echo SEARCH_START_YEAR; ?>" />
<input type="hidden" id="startBaseMonth" name="startBaseMonth" value="<? echo str_pad(SEARCH_START_MONTH, "2", "0", STR_PAD_LEFT); ?>" />
<input type="hidden" id="startBaseDay" name="startBaseDay" value="<? echo str_pad(SEARCH_START_DAY, "2", "0", STR_PAD_LEFT); ?>" />
<input type="hidden" id="endBaseYear" name="endBaseYear" value="<? echo date('Y'); ?>" />
<input type="hidden" id="endBaseMonth" name="endBaseMonth" value="<? echo date('m'); ?>" />
<input type="hidden" id="endBaseDay" name="endBaseDay" value="<? echo date('d'); ?>" />
<div class="history_header">
	<div class="set_date_area">
		<div class="set_date_section">
			<select class="searchbar_select" id="startYear"  name="startYear">
			<?
			foreach($year_arr as $k => $v){//init start year
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($start_year, $k);?>>
					<? echo $v; ?> 년
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>	
			</select>
		</div>	
		<div class="set_date_section">
			<select class="searchbar_select" id="startMonth" name="startMonth">
			<?
			foreach($month_arr as $k => $v){//init start month
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($start_month, $k); ?>>
					<? echo $v; ?> 월
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>
			</select>
		</div>	
		<div class="set_date_section">
			<select class="searchbar_select" id="startDay" name="startDay">
			<?
			foreach($day_arr as $k => $v){//init start day
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($start_day, $k); ?>>
					<? echo $v; ?> 일
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>
			</select>
		</div>
		<div class="set_date_section">
			<p class="set_date_hypen">-</p>
		</div>	
		<div class="set_date_section">
			<select class="searchbar_select" id="endYear" name="endYear">
			<?
			foreach($year_arr as $k => $v){//init end year
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($end_year, $k);?>>
					<? echo $v; ?> 년
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>	
			</select>
		</div>	
		<div class="set_date_section">	
			<select class="searchbar_select" id="endMonth" name="endMonth">
			<?
			foreach($month_arr as $k => $v){//init end month
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($end_month, $k); ?>>
					<? echo $v; ?> 월
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>	
			</select>
		</div>	
		<div class="set_date_section">	
			<select class="searchbar_select" id="endDay" name="endDay">
			<?
			foreach($day_arr as $k => $v){//init end day
			?>
				<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($end_day, $k); ?>>
					<? echo $v; ?> 일
				</option>
			<?
			}//end of : foreach($year_arr as $k => $v)
			?>	
			</select>
		</div>	
	</div>
	<div class="search_btn_area">
		<a href="#" class="search_btn search_history_btn">보기</a>
		<a href="#" class="search_btn search_today_history_btn">당일기록</a>
	</div>
	<div class="export_btns_area">
		<a href="#" class="search_btn search_all_history_export_btn">전체 기록 보기 (Excel)</a>
	</div>
</div>
<!-- //history_header -->
<!-- history_side -->
<div class="history_side ajax_history_side">
	<div class="guide_search_date">
		<? echo $start_date; ?><span> - </span><? echo $end_date; ?>
	</div>
	<? 
	if($reportnum == NULL){
	?>
	<div class="guide_ment">
		<p>전체 원격 지원 기간 통계입니다.</p>
		<p>원하는 기간의 통계를 보시려면 위의 날짜를 선택하신 후 '보기' 버튼을 눌러주세요.</p>
	</div>
	<?
	}//end of : if($report_num == NULL)
	else {
		$reportarray = $asg_obj->HistoryGetReportList($start_date, $end_date);

		if(count($reportarray) == 0) {
	?>
	<div class="guide_ment">
		<p>해당 기간 내에 존재하는 원격 지원 기록이 없습니다.</p>
		<p>다른 기간을 선택하신 후 다시 시도하시기 바랍니다.</p>
	</div>
	<?		
		}
		else{
	?>
	<div class="guide_option">
		<div class="set_date_section">
			<select class="searchbar_select select_report_list" name="reportnum" size='15' onchange="ajax_search_history_report()">
			<?
			while(list($key, $val) = each($reportarray)) {
			?>
				<option value="<? echo $val["reportnum"]; ?>" <? echo $utils_obj->PrintSelected($reportnum, $val["reportnum"]); ?>>
					<? echo substr($val["starttime"], 0, 16)." ".$val["name"]; ?>
				</option>
			<?
			}//end of : while(list($key, $val) = each($reportarray))
			?>
			</select>
		</div>
		<div class="search_side_btn_area">
			<a href="#" class="search_btn search_date_history_export_btn" onclick="search_date_history_export()">검색한 기록 보기 (Excel)</a>
		</div>
	</div>
	<?
		}
	}
	?>
</div>
<!-- //history_side -->
<!-- history_content -->
<div class="history_content ajax_history_content">
	<? 
	if($reportnum == NULL){
	?>
	<div class="history_title"><span>원격 지원 기록 통계</span></div>
	<div class="statistics_area">
		<table class="history_table">
			<colgroup>
				<col width="*" />
				<col width="190px" />
				<col width="190px" />
			</colgroup>
			<thead>
				<tr>
					<th>장애유형</th>
					<th>발생수</th>
					<th>발생비율</th>
				</tr>
			</thead>
			<tbody>
			<?
			$trouble_type_i = 0;
			while(list($key, $val) = each($statistics_trouble_type_arr["statsarray"])) {
				if(is_zero($statistics_trouble_type_arr["sum"])) {
			?>
				<tr>
					<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["trbname"]; ?></td>
					<td class="<? echo print_table_class($trouble_type_i); ?>">0</td>
					<td class="<? echo print_table_class($trouble_type_i); ?>">0 %</td>
				</tr>	
			<?
				}//end of : if($statistics_trouble_type_arr["sum"] == 0)
				else{
			?>
				<tr>
					<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["trbname"]; ?></td>
					<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo $val["count"]; ?></td>
					<td class="<? echo print_table_class($trouble_type_i); ?>"><? echo ((int)round($val["count"]/$statistics_trouble_type_arr["sum"]*100)); ?> %</td>
			<?	
				}//end of : if($statistics_trouble_type_arr["sum"] != 0)	
			?>
				</tr>
			<?		
				$trouble_type_i++;
			}//end of : while(list($key, $val) = each($statistics_trouble_type_arr["statsarray"]))

			if(is_zero($statistics_trouble_type_arr["sum"])) {
			?>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_trouble_type_arr["sum"]; ?></td>
					<td>0 %</td>
				</tr>	
			<?
			}//end of : if($statistics_trouble_type_arr["sum"] == 0)
			else{
			?>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_trouble_type_arr["sum"]; ?></td>
					<td>100 %</td>
				</tr>	
			<?
			}//end of : if($statistics_trouble_type_arr["sum"] != 0)	
			?>
			</tbody>
		</table>
	</div>
	<div class="statistics_area">
		<table class="history_table">
			<colgroup>
				<col width="*" />
				<col width="190px" />
				<col width="190px" />
			</colgroup>
			<thead>
				<tr>
					<th>해결 여부</th>
					<th>발생수</th>
					<th>발생비율</th>
				</tr>
			</thead>
			<tbody>	
			<?
			if(is_zero($statistics_resolve_arr["sum"])) {
			?>
				<tr>
					<td class="<? echo print_table_class(0); ?>">해결</td>
					<td class="<? echo print_table_class(0); ?>">0</td>
					<td class="<? echo print_table_class(0); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(1); ?>">보류</td>
					<td class="<? echo print_table_class(1); ?>">0</td>
					<td class="<? echo print_table_class(1); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(2); ?>">직접 지원 필요</td>
					<td class="<? echo print_table_class(2); ?>">0</td>
					<td class="<? echo print_table_class(2); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(3); ?>">미체크</td>
					<td class="<? echo print_table_class(3); ?>">0</td>
					<td class="<? echo print_table_class(3); ?>">0 %</td>
				</tr>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_resolve_arr["sum"]; ?></td>
					<td>0 %</td>
				</tr>
			<?
			}//end of : if($statistics_resolve_arr["sum"] == 0)
			else {
			?>
				<tr>
					<td class="<? echo print_table_class(0); ?>">해결</td>
					<td class="<? echo print_table_class(0); ?>"><? echo $statistics_resolve_arr[1]; ?></td>
					<td class="<? echo print_table_class(0); ?>"><? echo ((int)round($statistics_resolve_arr[1]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(1); ?>">보류</td>
					<td class="<? echo print_table_class(1); ?>"><? echo $statistics_resolve_arr[2]; ?></td>
					<td class="<? echo print_table_class(1); ?>"><? echo ((int)round($statistics_resolve_arr[2]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(2); ?>">직접 지원 필요</td>
					<td class="<? echo print_table_class(2); ?>"><? echo $statistics_resolve_arr[3]; ?></td>
					<td class="<? echo print_table_class(2); ?>"><? echo ((int)round($statistics_resolve_arr[3]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(3); ?>">미체크</td>
					<td class="<? echo print_table_class(3); ?>"><? echo $statistics_resolve_arr[0]; ?></td>
					<td class="<? echo print_table_class(3); ?>"><? echo ((int)round($statistics_resolve_arr[0]/$statistics_resolve_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_resolve_arr["sum"]; ?></td>
					<td>100 %</td>
				</tr>
			<?
			}//end of : if($statistics_resolve_arr["sum"] != 0)
			?>
			</tbody>
		</table>
	</div>
	<div class="statistics_area">
		<table class="history_table">
			<colgroup>
				<col width="*" />
				<col width="190px" />
				<col width="190px" />
			</colgroup>
			<thead>
				<tr>
					<th>고객 만족도</th>
					<th>발생수</th>
					<th>발생비율</th>
				</tr>
			</thead>
			<tbody>	
			<?
			if(is_zero($statistics_satistaction_arr["sum"])) {
			?>	
				<tr>
					<td class="<? echo print_table_class(0); ?>">만족</td>
					<td class="<? echo print_table_class(0); ?>">0</td>
					<td class="<? echo print_table_class(0); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(1); ?>">보통</td>
					<td class="<? echo print_table_class(1); ?>">0</td>
					<td class="<? echo print_table_class(1); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(2); ?>">불만족</td>
					<td class="<? echo print_table_class(2); ?>">0</td>
					<td class="<? echo print_table_class(2); ?>">0 %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(3); ?>">무응답</td>
					<td class="<? echo print_table_class(3); ?>">0</td>
					<td class="<? echo print_table_class(3); ?>">0 %</td>
				</tr>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_satistaction_arr["sum"]; ?></td>
					<td>0 %</td>
				</tr>
			<?
			}//end of : if($statistics_satistaction_arr["sum"] == 0) 
			else{
			?>
				<tr>
					<td class="<? echo print_table_class(0); ?>">만족</td>
					<td class="<? echo print_table_class(0); ?>"><? echo $statistics_satistaction_arr[1]; ?></td>
					<td class="<? echo print_table_class(0); ?>"><? echo ((int)round($statistics_satistaction_arr[1]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(1); ?>">보통</td>
					<td class="<? echo print_table_class(1); ?>"><? echo $statistics_satistaction_arr[2]; ?></td>
					<td class="<? echo print_table_class(1); ?>"><? echo ((int)round($statistics_satistaction_arr[2]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(2); ?>">불만족</td>
					<td class="<? echo print_table_class(2); ?>"><? echo $statistics_satistaction_arr[3]; ?></td>
					<td class="<? echo print_table_class(2); ?>"><? echo ((int)round($statistics_satistaction_arr[3]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr>
					<td class="<? echo print_table_class(3); ?>">무응답</td>
					<td class="<? echo print_table_class(3); ?>"><? echo $statistics_satistaction_arr[0]; ?></td>
					<td class="<? echo print_table_class(3); ?>"><? echo ((int)round($statistics_satistaction_arr[0]/$statistics_satistaction_arr["sum"]*100)); ?> %</td>
				</tr>
				<tr class="model_tfoot">
					<td>전체</td>
					<td><? echo $statistics_satistaction_arr["sum"]; ?></td>
					<td>100%</td>
				</tr>
			<?
			}//end of : if($statistics_satistaction_arr["sum"] != 0) 
			?>	
			</tbody>
		</table>
	</div>
	<?
	}//end of : if($report_num == NULL)
	else {
		$reportrow = $asg_obj->GetReport($reportnum);

		$operationlog = print_operationlog($reportrow->operationlog);
		$satisfaction = print_satisfaction($reportrow->satisfaction);
	?>
	<div class="history_title"><span>원격 지원 기록</span></div>
	<div class="report_area">
		<table class="history_table">
			<colgroup>
				<col width="130px"/>
				<col width="130px"/>
				<col width="130px"/>
				<col width="*"/>
			</colgroup>
			<tbody>
				<tr>
					<td>고객 이름</td>
					<td><? echo  $reportrow->name; ?></td>
					<td>E-mail</td>
					<td><a href='mailto:<? echo $reportrow->email; ?>'><? echo $reportrow->email; ?></a></td>
				</tr>
				<tr>
					<td class="common_table_color04">전화 번호</td>
					<td class="common_table_color04"><? echo  $reportrow->tel; ?></td>
					<td class="common_table_color04">IP</td>
					<td class="common_table_color04"><? echo $reportrow->ip; ?></td>
				</tr>
				<tr>
					<td colspan="2">지원 시작 시각</td>
					<td colspan="2"><? echo $reportrow->starttime; ?></td>
				</tr>
				<tr>
					<td class="common_table_color04" colspan="2">지원 종료 시각</td>
					<td class="common_table_color04" colspan="2"><? echo $reportrow->endtime; ?></td>
				</tr>
				<tr>
					<td>장애 유형</td>
					<td colspan="3">
					<?
					if($reportrow->trbnum == 0) {
						echo "선택 안됨";
					}
					else {
						echo $reportrow->trbname;
					}
					?>	
					</td>
				</tr>
				<tr>
					<td class="common_table_color04">해결 여부</td>
					<td class="common_table_color04"><? echo  $operationlog; ?></td>
					<td class="common_table_color04">고객 만족도</td>
					<td class="common_table_color04"><? echo $satisfaction; ?></td>
				</tr>
				<tr>
					<td>고객 피드백</td>
					<td colspan="3"><? echo $reportrow->usercomment; ?></td>
				</tr>
				<tr>
					<td class="common_table_color04">지원 내용</td>
					<td  class="common_table_color04" colspan="3">
						<textarea class="report_content" rows="3" cols="48" wrap="on" name="report"><? echo $reportrow->report; ?></textarea>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="report_area_btns">
			<a href="#" class="search_btn modified_report_btn" onclick="modify_report(<? echo $reportnum; ?>)">내용 수정</a>	
		</div>
	</div>
	<?
	}//end of : if($report_num != NULL)
	?>
</div>
<!-- //history_content -->
<?
include_once("../footer.php");
?>