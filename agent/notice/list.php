<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

include_once("../header.php");

$page = 0;//page : 0 -> total
$search_type = "";
$search_val = "";

$notice_column = array(
	"NO" => "No",
	"TITLE" => "Title",
	"WRITER" => "Writer",
	"DATE" => "Date"
);
$notice_arr = $asg_obj->GetNoticeList($page, $search_type, $search_val);

$pagination = $notice_arr['pagination'];
$content = $notice_arr['content'];
$page_num = $notice_arr['page_num'];

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

function print_on($print_flag){
	if($print_flag){
		return "on";
	}else{
		return "";
	}
}

function print_last($print_flag){
	if($print_flag){
		return "last";
	}else{
		return "";
	}
}
?>
<!-- 내용시작 -->
<div class="cc">
	<div class="ajax_notice">
		<table summary="" class="common_table03">
			<colgroup>
				<col width="70px" />
				<col width="*" />
				<col width="140px" />
				<col width="120px" />
			</colgroup>
			<thead>
				<tr>
					<th><? echo $notice_column["NO"]; ?></th>
					<th><? echo $notice_column["TITLE"]; ?></th>
					<th><? echo $notice_column["WRITER"]; ?></th>
					<th><? echo $notice_column["DATE"]; ?></th>
				</tr>
			</thead>
			<tbody>
				<?
				for ($i = 0; $i < sizeof($content); $i++){
				?>
				<tr>
					<td class="<? echo print_table_class($i); ?> sort_middle"><span><? echo $content[$i]['num']; ?></span></td>
					<td class="<? echo print_table_class($i); ?>"><span class="overflow_dot"><a href="/agent/notice/info.php?seq=<? echo $content[$i]['num']; ?>"><? echo $content[$i]['title']; ?></a></span></td>
					<td class="<? echo print_table_class($i); ?> sort_middle"><span><? echo $content[$i]['name']; ?></span></td>
					<td class="<? echo print_table_class($i); ?> sort_middle"><span><? echo $content[$i]['regdate']; ?></span></td>
				</tr>
				<?	
				}//for ($i = 0; $i < sizeof($content); $i++)
				?>
			</tbody>
		</table>
		<?
		$page_prev = $pagination->GetIsPrev();
		$page_list = $pagination->GetPageList();
		$page_next = $pagination->GetIsNext();
		?>
		<ul class="paging report_notice_page_list">
		<?
			if($page_prev['is_older_page']){
		?>	
			<li class="paging_a01">
				<a href="#" onClick="search_notice_pagination(event, <? echo $page_prev['older_page']; ?>)">
					≪ Previous
				</a>
			</li>
		<?
			}//end of : if($page_prev['is_older_page'])

			while(list($k, $v) = each($page_list)){
		?>
			<li class="<? echo print_on($v['is_current']); ?> <? echo print_last($v['is_last']); ?>">
				<a href="#"  onClick="search_notice_pagination(event, <? echo $k; ?>)">
					<? echo $k; ?>
				</a>
			</li>
		<?
			}// end of  : while(list($k, $v) = each($page_list))

			if($page_next['is_newer_page']){
		?>
			<li class="paging_a02">
				<a href="#" onClick="search_notice_pagination(event, <? echo $page_next['newer_page']; ?>)">
					Next ≫
				</a>
			</li>
		<?
			}//end of : if($page_next['is_newer_page'] == TRUE)
		?>	
		</ul>
	</div>	
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>