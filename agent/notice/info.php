<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

include_once("../header.php");

$seq = $_GET['seq'];
$info = $asg_obj->GetNotice($seq);
?>
<!-- 내용시작 -->
<div class="cc">
	<table summary="" class="notice_detail_table">
		<colgroup>
			<col width="100px" />
			<col width="*" />
			<col width="60px" />
			<col width="130px" />
		</colgroup>
		<tbody>

			<tr>
				<th class="notice_orther_color">Title</th>
				<td colspan="3" class="notice_orther_color"><? echo $info['title']; ?></td>
			</tr>

			<tr>
				<th>Writer</th>
				<td><? echo $info['name']; ?></td>
				<td><strong>Date</strong></td>
				<td><? echo $info['regdate']; ?></td>
			</tr>

			<tr>
				<th class="last">Content</th>
				<td colspan="3" class="last">
					<? echo $info['content']; ?>
				</td>
			</tr>

		</tbody>
	</table>
	<p class="notice_btn_p"><a href="/agent/notice/list.php" class="notice_btn">List</a></p>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>