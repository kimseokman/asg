/**
 * notice.js
 * 2015.05.27 | KSM
 */

(function($){
})(jQuery);

function search_notice_pagination(e, page_num){
	e.preventDefault();

	var page = page_num;
	var search_type = "";
	var search_val = "";

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.notice.php',
		data: {page: page, search_type : search_type, search_val: search_val},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_notice").html(res);
		}
	});
}