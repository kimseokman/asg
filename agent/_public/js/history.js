/**
 * history.js
 * 2015.07.02 | KSM
 */

(function($){
	$('.search_history_btn').on('click', ajax_search_history);
	$('.search_today_history_btn').on('click', ajax_search_today_history);

	$('.search_all_history_export_btn').on('click', search_all_history_export);
})(jQuery);

function fillzeros(num, len) {
	var str = "";
	num = num.toString();
	var num_length = num.length;

	if ( num_length < len) {
		for(var i=0; i < len - num_length; i++){
			str += "0";
		}
	}
    	return str + num;
}



function search_all_history_export(e){
	e.preventDefault();

	var sptid = $('#sptid').val();

	var start_year = $('#startBaseYear').val();
	var start_month = $('#startBaseMonth').val();
	var start_day = $('#startBaseDay').val();

	var end_year = $('#endBaseYear').val();
	var end_month = $('#endBaseMonth').val();
	var end_day = $('#endBaseDay').val();

	//html date select box sync
	$('#startYear').val(parseInt(start_year));
	$('#startMonth').val(parseInt(start_month));
	$('#startDay').val(parseInt(start_day));

	$('#endYear').val(parseInt(end_year));
	$('#endMonth').val(parseInt(end_month));
	$('#endDay').val(parseInt(end_day));

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_side.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_side").html(res);
		}
	});

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_content_statistics.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_content").html(res);
		}
	});

	var $form = $('<form></form>');
	$form.attr('action', '/agent/export/export.excel_report.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_sptid = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var input_start_year = $('<input type="hidden" value="'+start_year+'" name="start_year" />');
	var input_start_month = $('<input type="hidden" value="'+start_month+'" name="start_month" />');
	var input_start_day = $('<input type="hidden" value="'+start_day+'" name="start_day" />');
	var input_end_year = $('<input type="hidden" value="'+end_year+'" name="end_year" />');
	var input_end_month = $('<input type="hidden" value="'+end_month+'" name="end_month" />');
	var input_end_day = $('<input type="hidden" value="'+end_day+'" name="end_day" />');

	$form.append(input_sptid).append(input_start_year).append(input_start_month).append(input_start_day).append(input_end_year).append(input_end_month).append(input_end_day);
	
	$form.submit();
}

function search_date_history_export(){
	var sptid = $('#sptid').val();

	var start_year = $('#startYear').val();
	var start_month = $('#startMonth').val();
	var start_day = $('#startDay').val();

	var end_year = $('#endYear').val();
	var end_month = $('#endMonth').val();
	var end_day = $('#endDay').val();

	var $form = $('<form></form>');
	$form.attr('action', '/agent/export/export.excel_report.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_sptid = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var input_start_year = $('<input type="hidden" value="'+start_year+'" name="start_year" />');
	var input_start_month = $('<input type="hidden" value="'+start_month+'" name="start_month" />');
	var input_start_day = $('<input type="hidden" value="'+start_day+'" name="start_day" />');
	var input_end_year = $('<input type="hidden" value="'+end_year+'" name="end_year" />');
	var input_end_month = $('<input type="hidden" value="'+end_month+'" name="end_month" />');
	var input_end_day = $('<input type="hidden" value="'+end_day+'" name="end_day" />');

	$form.append(input_sptid).append(input_start_year).append(input_start_month).append(input_start_day).append(input_end_year).append(input_end_month).append(input_end_day);
	
	$form.submit();
}

function modify_report(report_num){
	var sptid = $('#sptid').val();

	var report_content = $('.report_content').val();

	var start_year = $('#startYear').val();
	var start_month = $('#startMonth').val();
	var start_day = $('#startDay').val();

	var end_year = $('#endYear').val();
	var end_month = $('#endMonth').val();
	var end_day = $('#endDay').val();

	var $form = $('<form></form>');
	$form.attr('action', '/agent/_func/function.edit_report.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_sptid = $('<input type="hidden" value="'+sptid+'" name="sptid" />');
	var input_report_num = $('<input type="hidden" value="'+report_num+'" name="report_num" />');
	var input_report_content = $('<textarea type="hidden" name="report">'+report_content+'</textarea>');
	var input_start_year = $('<input type="hidden" value="'+start_year+'" name="start_year" />');
	var input_start_month = $('<input type="hidden" value="'+start_month+'" name="start_month" />');
	var input_start_day = $('<input type="hidden" value="'+start_day+'" name="start_day" />');
	var input_end_year = $('<input type="hidden" value="'+end_year+'" name="end_year" />');
	var input_end_month = $('<input type="hidden" value="'+end_month+'" name="end_month" />');
	var input_end_day = $('<input type="hidden" value="'+end_day+'" name="end_day" />');
	
	$form.append(input_sptid).append(input_report_num).append(input_report_content).append(input_start_year).append(input_start_month).append(input_start_day).append(input_end_year).append(input_end_month).append(input_end_day);
	
	$form.submit();
}

function ajax_search_history_report(){
	var sptid = $('#sptid').val();

	var report_num = $('.select_report_list').val();

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_content_report.php',
		data: {
			sptid: sptid,
			report_num: report_num
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_content").html(res);
		}
	});
}

function ajax_search_history(e){
	e.preventDefault();

	var sptid = $('#sptid').val();

	var start_year = $('#startYear').val();
	var start_month = $('#startMonth').val();
	var start_day = $('#startDay').val();

	var end_year = $('#endYear').val();
	var end_month = $('#endMonth').val();
	var end_day = $('#endDay').val();

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_side.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_side").html(res);
		}
	});

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_content_statistics.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_content").html(res);
		}
	});
}

function ajax_search_today_history(e){
	e.preventDefault();

	var sptid = $('#sptid').val();

	var start_year = $('#startCurrentYear').val();
	var start_month = $('#startCurrentMonth').val();
	var start_day = $('#startCurrentDay').val();

	var end_year = $('#endCurrentYear').val();
	var end_month = $('#endCurrentMonth').val();
	var end_day = $('#endCurrentDay').val();

	//html date select box sync
	$('#startYear').val(parseInt(start_year));
	$('#startMonth').val(parseInt(start_month));
	$('#startDay').val(parseInt(start_day));

	$('#endYear').val(parseInt(end_year));
	$('#endMonth').val(parseInt(end_month));
	$('#endDay').val(parseInt(end_day));

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_side.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_side").html(res);
		}
	});

	$.ajax({
		type: "POST",
		url:'../ajax/ajax.history_content_statistics.php',
		data: {
			sptid: sptid,
			start_year: start_year, 
			start_month: start_month, 
			start_day: start_day,
			end_year : end_year,
			end_month: end_month,
			end_day: end_day
		},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_history_content").html(res);
		}
	});
}