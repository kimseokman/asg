<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

header( "Content-Type: application/vnd.ms-excel" ); 
header( "Content-Disposition: attachment; filename=Report.xls" ); 
header( "Content-Description: PHP4 Generated Data" ); 

$sptid = $_POST["sptid"];//test
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

$start_year = $_POST["start_year"];
$start_month = $_POST["start_month"];
$start_day = $_POST["start_day"];

$start_date = $start_year."-".$start_month."-".$start_day;

$end_year = $_POST["end_year"];
$end_month = $_POST["end_month"];
$end_day = $_POST["end_day"];

$end_date = $end_year."-".$end_month."-".$end_day;

$reportarray = $asg_obj->HistoryGetReportForExcel($start_date, $end_date);

function print_operationlog($operationlog_id){
	$operationlog_str = "미체크";

	switch($operationlog_id) {
		case 1:
			$operationlog_str = "해결";
			break;
		case 2:
			$operationlog_str = "보류";
			break;
		case 3:
			$operationlog_str = "직접 지원 필요";
			break;
		case 0:
		default:
			$operationlog_str = "미체크";
			break;
	}

	return $operationlog_str;
}

function print_satisfaction($satisfaction_id){
	$satisfaction_str = "무응답";

	switch($satisfaction_id) {
		case 1:
			$satisfaction_str = "만족";
			break;
		case 2:
			$satisfaction_str = "보통";
			break;
		case 3:
			$satisfaction_str = "불만족";
			break;
		case 0:
		default:
			$satisfaction_str = "무응답";
			break;
	}

	return $satisfaction_str;
}
?>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
	body{background-color:#FFF;}
	table .head_line{text-align:center;}
</style>
</head>
<body>
	<table cellspacing="0" cellpadding="2" border="1">
		<tr class="head_line">
			<td>원격 지원 기록</td>
			<td>고객 만족도</td>
		</tr>
		<tr class="head_line">
			<td>상담원 ID</td>
			<td>상담원 이름</td>
			<td>장애 유형</td>
			<td>원격 지원 시간</td>
			<td>접속 시간</td>
			<td>지원 내용</td>
			<td>장애 해결 여부</td>
			<td>고객 이름</td>
			<td>이메일</td>
			<td>전화 번호</td>
			<td>고객 피드백</td>
			<td>만족도</td>
		</tr>
		<?
		while(list($key, $val) = each($reportarray)) {
			$operationlog = print_operationlog($val["operationlog"]);
			$satisfaction = print_satisfaction($val["satisfaction"]);

			// by hchkim(2009.01.31)
	      		// 접속 시간을 표시한다.
	      		$temphour = 3600;
	      		$tempminute = 60;
	      		
	      		if($val["endtime"] != "" && $val["starttime"] != "") {
	      			$temptimestamp = strtotime($val["endtime"]) - strtotime($val["starttime"]);
	      			
	      			if($temptimestamp < $tempminute){
					$connectedtime = date('s초', $temptimestamp);
				}
				else if($temptimestamp < $temphour){
					$connectedtime = date('i분 s초', $temptimestamp);
				}	
	      		}
	      		else {
	      			$connectedtime = "";
	      		}
		?>
		<tr>
			<td><? echo $val["sptid"]; ?></td>
			<td><? echo $val["sptname"]; ?></td>
			<td><? echo $val["trbname"]; ?></td>
			<td><? echo $val["starttime"]." ~ ".$val["endtime"]; ?></td>
			<td><? echo $connectedtime; ?></td>
			<td><? echo $val["report"]; ?></td>
			<td><? echo $operationlog; ?></td>
			<td><? echo $val["name"]; ?></td>
			<td><? echo $val["email"]; ?></td>
			<td><? echo $val["tel"]; ?></td>
			<td><? echo $val["usercomment"]; ?></td>
			<td><? echo $satisfaction; ?></td>
		</tr>
		<?
		}//end of : while(list($key, $val) = each($reportarray))
		?>
	</table>
</body>
</html>