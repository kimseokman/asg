<?php
session_start();
header('Content-Type: text/html; charset=utf-8');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$current_url = $utils_obj->GetCurrentPageURL();