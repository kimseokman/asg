<?php
include_once($_SERVER['DOCUMENT_ROOT']."/agent/_func/function.agent_common.php");

$sptid = $_POST["sptid"];//test
$pw = AGENT_WEB_LOGIN_PW;
$asg_obj->HistoryLogin($sptid, $pw);
$web_login_res = $asg_obj->HistoryConfirmLogin();

$reportnum = $_POST["report_num"];

$start_year = $_POST["start_year"];
$start_month = $_POST["start_month"];
$start_day = $_POST["start_day"];

$end_year = $_POST["end_year"];
$end_month = $_POST["end_month"];
$end_day = $_POST["end_day"];

$page = array(//page define
	"EDIT_FAIL" => "/agent/history/search_history.php",
	"EDIT_SUCCESS"=> "/agent/history/search_history.php"
);

$msg = array(//page define
	"REPORT_LONG" => "내용은 ".AGENT_REPORT_MAX."자를 넘길 수 없습니다. 한글 ".AGENT_REPORT_MAX_KR."자, 영문 ".AGENT_REPORT_MAX_EN."자 이내로 입력해주세요.",
	"REPORT_NOT_EXIST" => "존재하지 않는 기록입니다. 다시 시도해주세요.",
	"EDIT_SUCCESS" => "내용이 성공적으로 수정되었습니다."
);

if(strlen($_POST["report"]) > AGENT_REPORT_MAX) {// 리포트가 5000자를 넘길 때.
	echo "
		<form name='back_report' method='post' action='".$page["EDIT_FAIL"]."'>
			<input type='hidden' name='sptid' value='".$sptid."' />
			<input type='hidden' name='reportnum' value='".$reportnum."' />
			<input type='hidden' name='start_year' value='".$start_year."' />
			<input type='hidden' name='start_month' value='".$start_month."' />
			<input type='hidden' name='start_day' value='".$start_day."' />
			<input type='hidden' name='end_year' value='".$end_year."' />
			<input type='hidden' name='end_month' value='".$end_month."' />
			<input type='hidden' name='end_day' value='".$end_day."' />
		</form>
		<script> 
			alert('".$msg["REPORT_LONG"]."'); 
			document.back_report.submit();
		</script>
	";
	exit(1);
}

$modify_res = $asg_obj->HistoryModifyReportContent($reportnum, $_POST["report"]);

if( $modify_res == "NOREPORT") {
	echo "
		<form name='back_report' method='post' action='".$page["EDIT_FAIL"]."'>
			<input type='hidden' name='sptid' value='".$sptid."' />
			<input type='hidden' name='reportnum' value='".$reportnum."' />
			<input type='hidden' name='start_year' value='".$start_year."' />
			<input type='hidden' name='start_month' value='".$start_month."' />
			<input type='hidden' name='start_day' value='".$start_day."' />
			<input type='hidden' name='end_year' value='".$end_year."' />
			<input type='hidden' name='end_month' value='".$end_month."' />
			<input type='hidden' name='end_day' value='".$end_day."' />
		</form>
		<script> 
			alert('".$msg["REPORT_NOT_EXIST"]."'); 
			document.back_report.submit();
		</script>
	";
	exit(1);
}
else{
	echo "	
		<form name='back_report' method='post' action='".$page["EDIT_SUCCESS"]."'>
			<input type='hidden' name='sptid' value='".$sptid."' />
			<input type='hidden' name='reportnum' value='".$reportnum."' />
			<input type='hidden' name='start_year' value='".$start_year."' />
			<input type='hidden' name='start_month' value='".$start_month."' />
			<input type='hidden' name='start_day' value='".$start_day."' />
			<input type='hidden' name='end_year' value='".$end_year."' />
			<input type='hidden' name='end_month' value='".$end_month."' />
			<input type='hidden' name='end_day' value='".$end_day."' />
		</form>
		<script>
			alert('".$msg["EDIT_SUCCESS"]."');
			document.back_report.submit();
		</script>
	";
}