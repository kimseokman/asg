/**
 * common.js
 * 2015.05.27 | KSM | all page apply
 */

(function($){
	$('.visual_free_trial_btn').on('click', visual_free_trial_btn_click);
	$('input[name=start_free_trial_mail]').on('keydown', visual_free_trial_input_enter);
}(jQuery));

function visual_free_trial_btn_click(e){
	e.preventDefault();

	var mail = $('input[name=start_free_trial_mail]').val();

	var $form = $('<form></form>');
	$form.attr('action', './free/free_form.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_free_trial_mail = $('<input type="hidden" value="'+mail+'" name="free_trial_mail" />');
	
	$form.append(input_free_trial_mail);
	
	$form.submit();
}

function visual_free_trial_input_enter(e){
	if(e.keyCode == 13){//case by 'ENTER'
		e.preventDefault();

		var mail = $('input[name=start_free_trial_mail]').val();

		var $form = $('<form></form>');
		$form.attr('action', './free/free_form.php');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var input_free_trial_mail = $('<input type="hidden" value="'+mail+'" name="free_trial_mail" />');
		
		$form.append(input_free_trial_mail);
		
		$form.submit();
	}	
}