/**
 * login.js
 * 2015.05.27 | KSM | login page
 */

(function($){
	$('input[name=user_select]').on('click', hide_forgot_pw);

	$('#pw').on('keydown', login_trigger);
	$('#pw').on('keyup', ajax_validate_pw);

	$('.sign_in').on('click', sign_in);

	$('.send_reset_mail_btn').on('click', send_reset_mail);
	$('.password_reset_btn').on('click', reset_pw);

	$('input[name=reset_pw]').on('keyup', ajax_reset_pw);
	$('input[name=reset_retype_pw]').on('keyup', ajax_reset_retype_pw);
}(jQuery));

function hide_forgot_pw(e){
	var selected_user_type = $(this).val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.show_forgot_pw.php',
		data: {access_user_type: selected_user_type},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
			var forgot_password_html = "&nbsp;";

			if(res.show_flag){
				forgot_password_html = res.show_msg;
			}

			$(".forgot_password_area").html(forgot_password_html);
		}
	});
}

function reset_pw(e){
	e.preventDefault();

	document.form_reset_pw.submit();//submit
}

function ajax_reset_retype_pw(e){
	var password = $('input[name=reset_pw]').val();
	var retype_password = $('input[name=reset_retype_pw]').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "retype_password", change_pw: password, retype_pw: retype_password},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.error.flag){
				if($('input[name=reset_retype_pw]').hasClass('login_input') || $('input[name=reset_retype_pw]').hasClass('login_input_check')){
					$("input[name=reset_retype_pw]").removeClass('login_input').removeClass('login_input_check').addClass("login_input_error");
				}
			}else{
				if($('input[name=reset_retype_pw]').hasClass('login_input') || $('input[name=reset_retype_pw]').hasClass('login_input_error')){
					$("input[name=reset_retype_pw]").removeClass('login_input').removeClass('login_input_error').addClass("login_input_check");
				}
			}

		}
	});
}

function ajax_reset_pw(e){
	var password = $('input[name=reset_pw]').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "password", length: password.length},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.error.flag){
				if($('input[name=reset_pw]').hasClass('login_input') || $('input[name=reset_pw]').hasClass('login_input_check')){
					$("input[name=reset_pw]").removeClass('login_input').removeClass('login_input_check').addClass("login_input_error");
				}
			}else{
				if($('input[name=reset_pw]').hasClass('login_input') || $('input[name=reset_pw]').hasClass('login_input_error')){
					$("input[name=reset_pw]").removeClass('login_input').removeClass('login_input_error').addClass("login_input_check");
				}
			}
		}
	});
}

function send_reset_mail(e){
	e.preventDefault();

	document.password_reset_email.submit();//submit
}

function login_trigger(e){
	if(e.keyCode == 13){//case by 'ENTER'
		$('.sign_in').trigger('click');
	}
}

function ajax_validate_pw(e){
	var password = $('#pw').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "password", length: password.length},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.error.flag){
				$("#pw").addClass("login_input_error");
			}else{
				$("#pw").removeClass("login_input_error");
			}

			$(".login_error_p").html(res.error.msg);
			$(".guide_msg").html(res.guide.msg);
		}
	});
}

function sign_in(e){
	e.preventDefault();
	
	//check remember me
	var remember_me_isChecked = $('#remember_me').is(":checked");

	$.cookie('remember_me_checked', remember_me_isChecked);//set checked

	if(remember_me_isChecked){//set user id
		$.cookie('remember_id', $("#id").val());
	}else{
		$.cookie('remember_id');
	}

	document.FLogin.submit();//submit
}