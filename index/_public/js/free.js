/**
 * free.js
 * 2015.05.27 | KSM | free page
 */

(function($){
	$('.free_select_product').change(ajax_free_product);
	$('input[name=free_admin_id]').on('keyup', {check_type: 'id'}, ajax_validate_free);
	$('input[name=free_first_name]').on('keyup', {check_type: 'name'}, ajax_validate_free);
	$('input[name=free_last_name]').on('keyup', {check_type: 'name'}, ajax_validate_free);
	$('input[name=free_mail]').on('keyup', {check_type: 'mail'}, ajax_validate_free);
	$('input[name=free_pw]').on('keyup', {check_type: 'password'}, ajax_validate_free);
	$('input[name=free_retype_pw]').on('keyup', {check_type: 'retype_password'}, ajax_validate_free);
	$('input[name=free_agree]').on('change', {check_type: 'check_agree'}, ajax_validate_free);
	
	$('.free_form_next_btn').on('click', send_free_mail);
	
}(jQuery));


function send_free_mail(e){
	e.preventDefault();

	var auth_code_flag = $('.auth_code_flag').val();

	if(auth_code_flag == true){//free admin create
		var admin_id = $('input[name=free_admin_id]').val();
		var first_name = $('input[name=free_first_name]').val();
		var last_name = $('input[name=free_last_name]').val();
		var mail = $('input[name=free_mail]').val();
		var pw = $('input[name=free_pw]').val();
		var retype_pw = $('input[name=free_retype_pw]').val();
		var product = $('.free_select_product').val();
		var technician = $('.free_technician').val();
		var auth_code = $('.auth_code').val();
		var check_agree = $("input:checkbox[id='free_agree']").is(":checked");

		var $form = $('<form></form>');
		$form.attr('action', '/index/_func/function.create_free_admin.php');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var input_admin_id = $('<input type="hidden" value="'+admin_id+'" name="admin_id" />');
		var input_first_name = $('<input type="hidden" value="'+first_name+'" name="first_name" />');
		var input_last_name = $('<input type="hidden" value="'+last_name+'" name="last_name" />');
		var input_mail = $('<input type="hidden" value="'+mail+'" name="mail" />');
		var input_pw = $('<input type="hidden" value="'+pw+'" name="pw" />');
		var input_retype_pw = $('<input type="hidden" value="'+retype_pw+'" name="retype_pw" />');
		var input_product = $('<input type="hidden" value="'+product+'" name="product" />');
		var input_technician = $('<input type="hidden" value="'+technician+'" name="technician" />');
		var input_auth_code = $('<input type="hidden" value="'+auth_code+'" name="auth_code" />');
		var input_check_agree = $('<input type="hidden" value="'+check_agree+'" name="check_agree" />');

		$form.append(input_admin_id).append(input_first_name).append(input_last_name).append(input_mail).append(input_pw).append(input_retype_pw).append(input_product).append(input_technician).append(input_auth_code).append(input_check_agree);
		
		$form.submit();
	}else{//free trial send mail
		var admin_id = $('input[name=free_admin_id]').val();
		var first_name = $('input[name=free_first_name]').val();
		var last_name = $('input[name=free_last_name]').val();
		var mail = $('input[name=free_mail]').val();

		var $form = $('<form></form>');
		$form.attr('action', '/index/_func/function.free_join_auth_send_mail.php');
		$form.attr('method', 'post');
		$form.appendTo('body');

		var input_admin_id = $('<input type="hidden" value="'+admin_id+'" name="admin_id" />');
		var input_first_name = $('<input type="hidden" value="'+first_name+'" name="first_name" />');
		var input_last_name = $('<input type="hidden" value="'+last_name+'" name="last_name" />');
		var input_mail = $('<input type="hidden" value="'+mail+'" name="mail" />');
		var input_send_type = $('<input type="hidden" value="free_join" name="send_type" />');
		
		$form.append(input_admin_id).append(input_first_name).append(input_last_name).append(input_mail).append(input_send_type);
		
		$form.submit();
	}
}

function ajax_free_product(e){
	var product_code = $('.free_select_product').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.myplan_technician.php',
		data: {product_code: product_code},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$('.ajax_free_technician').html(res);
		}
	});

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.myplan_product.php',
		data: {product_code: product_code},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$('.ajax_free_product').html(res);
		}
	});
}

function ajax_validate_free(e){
	var check_type = e.data.check_type;
	var request_data = null;

	if(check_type == 'id'){
		var admin_id = $('input[name=free_admin_id]').val();
		request_data = {check_type: check_type, admin_id: admin_id, length: admin_id.length};
	}		
	else if(check_type == 'name'){
		var first_name = $('input[name=free_first_name]').val();
		var last_name = $('input[name=free_last_name]').val();
		request_data = {check_type: check_type, first_name: first_name, last_name: last_name};
	}
	else if(check_type == 'mail'){
		var mail = $('input[name=free_mail]').val();
		request_data = {check_type: check_type, mail: mail};	
	}
	else if(check_type == 'password'){
		var change_pw = $('input[name=free_pw]').val();
		request_data = {check_type: check_type, change_pw: change_pw, length : change_pw.length};	
	}
	else if(check_type == 'retype_password'){
		var change_pw = $('input[name=free_pw]').val();
		var retype_pw = $('input[name=free_retype_pw]').val();
		request_data = {check_type: check_type, change_pw: change_pw, retype_pw : retype_pw};	
	}
	else if(check_type == 'check_agree'){
		var check_agree = $("input:checkbox[id='free_agree']").is(":checked");
		request_data = {check_type: check_type, check_agree: check_agree};
	}

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate_free_form.php',
		data: request_data,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(check_type == 'id'){
				$('.ajax_free_admin_id').removeClass('circle_check').removeClass('circle_error');
				$('.free_input_p').removeClass('ok').removeClass('error');

				var ajax_free_admin_id_html = "&nbsp;";
				var ajax_guide_html = "&nbsp;";

				if(res.error.flag){
					$('.ajax_free_admin_id').addClass('circle_error');
					ajax_free_admin_id_html = res.error.msg;
				}else{
					$('.ajax_free_admin_id').addClass('circle_check');
					ajax_free_admin_id_html = res.error.msg;
				}

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.ajax_free_admin_id').html(ajax_free_admin_id_html);
				$('.free_input_p').html(ajax_guide_html);
			}
			else if(check_type == 'name'){
				$('.ajax_free_name').removeClass('circle_check').removeClass('circle_error');
				$('.free_input_p').removeClass('ok').removeClass('error');

				var ajax_free_name_html = "&nbsp;";
				var ajax_guide_html = "&nbsp;";

				if(res.error.flag){
					$('.ajax_free_name').addClass('circle_error');
					ajax_free_name_html = res.error.msg;
				}else{
					$('.ajax_free_name').addClass('circle_check');
					ajax_free_name_html = res.error.msg;
				}

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.ajax_free_name').html(ajax_free_name_html);
				$('.free_input_p').html(ajax_guide_html);
			}
			else if(check_type == 'mail'){
				$('.ajax_free_mail').removeClass('circle_check').removeClass('circle_error');
				$('.free_input_p').removeClass('ok').removeClass('error');

				var ajax_free_mail_html = "&nbsp;";
				var ajax_guide_html = "&nbsp;";

				if(res.error.flag){
					$('.ajax_free_mail').addClass('circle_error');
					ajax_free_mail_html = res.error.msg;
				}else{
					$('.ajax_free_mail').addClass('circle_check');
					ajax_free_mail_html = res.error.msg;
				}

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.ajax_free_mail').html(ajax_free_mail_html);
				$('.free_input_p').html(ajax_guide_html);
			}
			else if(check_type == 'password'){
				$('.ajax_free_pw').removeClass('circle_check').removeClass('circle_error');
				$('.free_input_p').removeClass('ok').removeClass('error');

				var ajax_free_pw_html = "&nbsp;";
				var ajax_guide_html = "&nbsp;";

				if(res.error.flag){
					$('.ajax_free_pw').addClass('circle_error');
					ajax_free_pw_html = res.error.msg;
				}else{
					$('.ajax_free_pw').addClass('circle_check');
					ajax_free_pw_html = res.error.msg;
				}

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.ajax_free_pw').html(ajax_free_pw_html);
				$('.free_input_p').html(ajax_guide_html);
			}
			else if(check_type == 'retype_password'){
				$('.ajax_free_retype_pw').removeClass('circle_check').removeClass('circle_error');
				$('.free_input_p').removeClass('ok').removeClass('error');

				var ajax_free_retype_pw_html = "&nbsp;";
				var ajax_guide_html = "&nbsp;";

				if(res.error.flag){
					$('.ajax_free_retype_pw').addClass('circle_error');
					ajax_free_retype_pw_html = res.error.msg;
				}else{
					$('.ajax_free_retype_pw').addClass('circle_check');
					ajax_free_retype_pw_html = res.error.msg;
				}

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.ajax_free_retype_pw').html(ajax_free_retype_pw_html);
				$('.free_input_p').html(ajax_guide_html);
			}
			else if(check_type == 'check_agree'){
				$('.free_input_p').removeClass('ok').removeClass('error');
				var ajax_guide_html = "&nbsp;";

				if(res.guide.flag){
					$('.free_input_p').addClass('error');
					ajax_guide_html = res.guide.msg;
				}else{
					ajax_guide_html = res.guide.msg;
				}

				$('.free_input_p').html(ajax_guide_html);
			}
		}
	});
}