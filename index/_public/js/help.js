/**
 * help.js
 * 2015.05.27 | KSM | help page
 */

(function($){

	$('.contact_send_btn').on('click', send_contact_mail);
	
}(jQuery));

function send_contact_mail(e){
	e.preventDefault();

	document.contact_form.submit();
}
