<?php
/**
 * contact_us_send_mail.php
 * 2015.06.24 | KSM 
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$asg_obj = new ASGMain();

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"SEND_MAIL_OK" => "/index/".$msg_lang."/help/contact_us.php",
	"SEND_MAIL_FAIL" => "/index/".$msg_lang."/help/contact_us.php"
);

//
//	Logic
//

$name = $_POST['contact_first_name']."-".$_POST['contact_last_name'];
$phone = $_POST['contact_phone'];
$customer_mail = $_POST['contact_mail'];
$message = $_POST['contact_msg'];
$company = $_POST['contact_company'];
$department = $_POST['contact_department'];
$status = "Unresolved";

$save_res = $asg_obj->SetContactUsHistory($name, $phone, $customer_mail, $message, $company, $department, $status);//set db

if($save_res == "OK"){

	//mail promise
	$from = $customer_mail;
	$from_name = $_POST['contact_first_name']." ".$_POST['contact_last_name'];
	$to = ASG_MAIL_ADDR; // Recipients email ID
	$to_name = ASG_MAIL_NAME; // Recipient's name

	//mail content
	$customer_name = $_POST['contact_first_name']." ".$_POST['contact_last_name'];
	$customer_phone = $phone;
	$customer_message = $message;
	$customer_company = $company;
	$customer_department = $department;

	//mail setting
	$contact_us_mail = new PHPMailer(); 
	$contact_us_mail->IsSMTP(); // send via SMTP
	$contact_us_mail->Host = ASG_MAIL_HOST;
	$contact_us_mail->Port = ASG_MAIL_PORT;
	$contact_us_mail->Mailer = ASG_MAILER;
	$contact_us_mail->CharSet = ASG_MAIL_CHARSET;
	$contact_us_mail->SMTPAuth = true; // turn on SMTP authentication
	$contact_us_mail->Username = ASG_MAIL_ID; // SMTP username
	$contact_us_mail->Password = ASG_MAIL_PW;// SMTP password

	//mail body
	include ($_SERVER['DOCUMENT_ROOT']."/index/mail_contents/contact_us_mail.php");

	if($msg_lang == "en") {
		$contact_us_mail->Body = $contact_us_mail_content_en;
		$contact_us_mail->Subject = "[ Anysuppor​t ] Customer Q&A";
	}
	else if($msg_lang == "jp") {
		$contact_us_mail->Body = $contact_us_mail_content_jp;
		$contact_us_mail->Subject = "[ Anysuppor​t ] 顧客 Q&A";
	}
	else if($msg_lang == "kr") {
		$contact_us_mail->Body = $contact_us_mail_content_kr;
		$contact_us_mail->Subject = "[ Anysuppor​t ] 고객 Q&A";
	}
	else {
		$contact_us_mail->Body = $contact_us_mail_content_en;
		$contact_us_mail->Subject = "[ Anysuppor​t ] Customer Q&A";
	}

	$contact_us_mail->From = $from;
	$contact_us_mail->FromName = $from_name;
	$contact_us_mail->AddAddress($to, $to_name);
	$contact_us_mail->AddReplyTo($from, $from_name);
	$contact_us_mail->WordWrap = 50; // set word wrap
	$contact_us_mail->IsHTML(TRUE); // send as HTML
	//send mail
	$contact_us_mail_res = $contact_us_mail->Send();

	if(!$contact_us_mail_res){
		echo "
			<form name='request_mail' method='post' action='".$page['SEND_MAIL_FAIL']."'>
				<input type='hidden' name='request_mail_res' value='FAIL' />
			</form>
			<script>
				document.request_mail.submit();
			</script>
		";
	}	
	else{
		echo "
			<form name='request_mail' method='post' action='".$page['SEND_MAIL_OK']."'>
				<input type='hidden' name='request_mail_res' value='SUCCESS' />
				<input type='hidden' name='request_mail_name' value='".$customer_name."' />
			</form>
			<script>
				document.request_mail.submit();
			</script>
		";
	}
}