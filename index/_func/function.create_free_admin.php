<?php
/**
 * create_free_admin.php
 * 2015.06.03 | KSM | code check added
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$id_length_min = ID_MIN_LENGTH;
$id_length_max = ID_MAX_LENGTH;

$pw_length_min = PW_MIN_LENGTH;
$pw_length_max = PW_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"CREATE_ADMIN_FAIL"=> "/index/".$msg_lang."/free/free_form.php",
	"CREATE_ADMIN_OK" => "/index/_func/function.index_login.php",
	"SEND_MAIL_FAIL" => "/index/".$msg_lang."/free/free_form.php"
);

//
//	Msg Define
//
$msg_en = array(
	"INVALID_ID" => "invalid admin ID",
	"INVALID_MAIL" => "invalid mail",
	"INVALID_PASSWORD" => "invalid password",
	"INVALID_RETYPE_PASSWORD" => "invalid retype password",
	"INVALID_CHECK_AGREE" => "invalid check agree",
	"INVALID_AUTHCODE" => "invalid auth code",
	"DUPLECATE_ADID" => "duplicated admin id",
	"DUPLECATE_EMAIL" => "duplicated email",
	"FREE_JOIN_ALARM_SEND_FAIL" => "mail send error by superadmin",
	"FREE_JOIN_COMPLETE_SEND_FAIL" => "mail send error by user",
	"OK" => "ok"
);

$msg_jp = array(
	"INVALID_ID" => "無効な管理者ID",
	"INVALID_MAIL" => "無効なメール",
	"INVALID_PASSWORD" => "無効なパスワード",
	"INVALID_RETYPE_PASSWORD" => "無効な再入力パスワード",
	"INVALID_CHECK_AGREE" => "無効なチェックが同意します",
	"INVALID_AUTHCODE" => "無効な認証コード",
	"DUPLECATE_ADID" => "重複管理者ID",
	"DUPLECATE_EMAIL" => "重複する電子メール",
	"FREE_JOIN_ALARM_SEND_FAIL" => "mail send error by superadmin",
	"FREE_JOIN_COMPLETE_SEND_FAIL" => "mail send error by user",
	"OK" => "成功"
);

$msg_kr = array(
	"INVALID_ID" => "잘못된 관리자 ID 입니다.",
	"INVALID_MAIL" => "잘못된 메일 입니다.",
	"INVALID_PASSWORD" => "잘못된 비밀번호 입니다.",
	"INVALID_RETYPE_PASSWORD" => "비밀번호를 확인해주세요.",
	"INVALID_CHECK_AGREE" => "동의란에 체크 해주세요.",
	"INVALID_AUTHCODE" => "잘못된 인증 코드 입니다.",
	"DUPLECATE_ADID" => "중복된 관리자 ID 입니다.",
	"DUPLECATE_EMAIL" => "중복된 메일 입니다.",
	"FREE_JOIN_ALARM_SEND_FAIL" => "mail send error by superadmin",
	"FREE_JOIN_COMPLETE_SEND_FAIL" => "mail send error by user",
	"OK" => "성공"
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

$check_agree = $_POST['check_agree'];
$email = $_POST['mail'];
$adid = $_POST['admin_id'];
$name = $_POST['first_name']."-".$_POST['last_name'];
$password = $_POST['pw'];
$retype_pw = $_POST['retype_pw'];

$free_duration = FREE_DAYS;//30
$product_code = $_POST['product'];
$technician_volum = $_POST['technician'];

$customer_url = DEFAULT_CUSTOMER_URL;
$code = $_POST['auth_code'];

//validation
if(strlen($adid) == 0){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_ID']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}
else if(strlen($adid) < $id_length_min || strlen($adid) > $id_length_max){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_ID']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}
else if(!preg_match("`^[_0-9a-z-.]{".$id_length_min.",".$id_length_max."}$`i", $adid)){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_ID']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}
else{		
	$spt_res = $asg_obj->DuplicatedCheckID("SUPPORTER", $adid);	//supporter 중복
	$admin_res = $asg_obj->DuplicatedCheckID("ADMIN", $adid);	//admin 중복
	$super_res = $asg_obj->DuplicatedCheckID("SUPERADMIN", $adid);//superadmin 중복

	if($spt_res != "OK" || $admin_res != "OK" || $super_res != "OK"){
		echo "
			<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
				<input type='hidden' name='mail' value='".$email."' />
				<input type='hidden' name='adid' value='".$adid."' />
				<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
				<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
				<input type='hidden' name='auth_code_flag' value='".TRUE."' />
				<input type='hidden' name='auth_code' value='".$code."' />
			</form>
			<script>
				alert('".$msg[$msg_lang]['INVALID_ID']."');
				document.free_create_admin.submit();
			</script>
		";
		exit(1);
	}
}

$check_mail = $utils_obj->IsValidEmail($email);
if(!$check_mail){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_MAIL']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}

if(strlen($password) < $pw_length_min || strlen($password) > $pw_length_max){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_PASSWORD']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}
else{
	$check_pw = $utils_obj->IsValidPassword($password);

	if(!$check_pw){
		echo "
			<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
				<input type='hidden' name='mail' value='".$email."' />
				<input type='hidden' name='adid' value='".$adid."' />
				<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
				<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
				<input type='hidden' name='auth_code_flag' value='".TRUE."' />
				<input type='hidden' name='auth_code' value='".$code."' />
			</form>
			<script>
				alert('".$msg[$msg_lang]['INVALID_PASSWORD']."');
				document.free_create_admin.submit();
			</script>
		";
		exit(1);
	}
}

if(strcmp($password, $retype_pw)){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_RETYPE_PASSWORD']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}

if($check_agree != 'true'){
	echo "
		<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_FAIL']."'>
			<input type='hidden' name='mail' value='".$email."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='first_name' value='".$_POST['first_name']."' />
			<input type='hidden' name='last_name' value='".$_POST['last_name']."' />
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_CHECK_AGREE']."');
			document.free_create_admin.submit();
		</script>
	";
	exit(1);
}	

$create_res = $asg_obj->CreateFreeAdmin($email, $adid, $name, $password, $free_duration, $product_code, $technician_volum, $customer_url, $code);

if($create_res == "INVALID_AUTHCODE"){
	echo "
		<script>
			alert('".$msg[$msg_lang]['INVALID_AUTHCODE']."');
			location.href='".$page['CREATE_ADMIN_FAIL']."';
		</script>
	";
	exit(1);
}
else if($create_res == "DUPLECATE_ADID"){
	echo "
		<script>
			alert('".$msg[$msg_lang]['DUPLECATE_ADID']."');
			location.href='".$page['CREATE_ADMIN_FAIL']."';
		</script>
	";
	exit(1);
}
else if($create_res == "DUPLECATE_EMAIL"){
	echo "
		<script>
			alert('".$msg[$msg_lang]['DUPLECATE_EMAIL']."');
			location.href='".$page['CREATE_ADMIN_FAIL']."';
		</script>
	";
	exit(1);
}
else if($create_res == "OK"){//auto login

	$free_trial_admin = $asg_obj->GetAdminByID($adid);

	//mail promise
	$from = $free_trial_admin->email;
	$from_name = $_POST['first_name']." ".$_POST['last_name'];
	$to = ASG_MAIL_ADDR; // Recipients email ID
	$to_name = ASG_MAIL_NAME; // Recipient's name

	//mail content
	$user_id = $free_trial_admin->adid;
	$user_name = $_POST['first_name']." ".$_POST['last_name'];
	$user_mail = $free_trial_admin->email;
	$user_password = $password;
	$user_company = $free_trial_admin->company;
	$user_service_expired_date = $free_trial_admin->service_expired_date;

	//mail setting
	$free_join_alarm_mail = new PHPMailer(); 
	$free_join_alarm_mail->IsSMTP(); // send via SMTP
	$free_join_alarm_mail->Host = ASG_MAIL_HOST;
	$free_join_alarm_mail->Port = ASG_MAIL_PORT;
	$free_join_alarm_mail->Mailer = ASG_MAILER;
	$free_join_alarm_mail->CharSet = ASG_MAIL_CHARSET;
	$free_join_alarm_mail->SMTPAuth = true; // turn on SMTP authentication
	$free_join_alarm_mail->Username = ASG_MAIL_ID; // SMTP username
	$free_join_alarm_mail->Password = ASG_MAIL_PW;// SMTP password

	//mail body
	include ($_SERVER['DOCUMENT_ROOT']."/index/mail_contents/free_join_alarm_mail.php");
	
	if($msg_lang == "en") {
		$free_join_alarm_mail->Body = $free_join_alarm_mail_content_en;
		$free_join_alarm_mail->Subject = "[ Anysuppor​t ] Free Trial User : ".$adid;
	}
	else if($msg_lang == "jp") {
		$free_join_alarm_mail->Body = $free_join_alarm_mail_content_jp;
		$free_join_alarm_mail->Subject = "[ Anysuppor​t ] 無料トライアルユーザー : ".$adid;
	}
	else if($msg_lang == "kr") {
		$free_join_alarm_mail->Body = $free_join_alarm_mail_content_kr;
		$free_join_alarm_mail->Subject = "[ Anysuppor​t ] 무료 평가판 사용자 : ".$adid;
	}
	else {
		$free_join_alarm_mail->Body = $free_join_alarm_mail_content_en;
		$free_join_alarm_mail->Subject = "[ Anysuppor​t ] Free Trial User : ".$adid;
	}

	$free_join_alarm_mail->From = $from;
	$free_join_alarm_mail->FromName = $from_name;
	$free_join_alarm_mail->AddAddress($to, $to_name);
	$free_join_alarm_mail->AddReplyTo($from, $from_name);
	$free_join_alarm_mail->WordWrap = 50; // set word wrap
	$free_join_alarm_mail->IsHTML(TRUE); // send as HTML
	//send mail
	$free_join_alarm_mail_res = $free_join_alarm_mail->Send();

	if(!$free_join_alarm_mail_res) {
		echo "
			<script>
				alert('".$msg[$msg_lang]['FREE_JOIN_ALARM_SEND_FAIL']."');
			</script>
		";
	}
	else {//case by free join alarm mail send success
		//mail promise
		$from = ASG_MAIL_ADDR;
		$from_name = ASG_MAIL_NAME;
		$to = $free_trial_admin->email; // Recipients email ID
		$to_name = $_POST['first_name']." ".$_POST['last_name']; // Recipient's name

		//mail content
		$contact_url = ASG_URL."/index/".$msg_lang."/help/contact_us.php";

		//mail setting
		$join_complete_mail = new PHPMailer(); 
		$join_complete_mail->IsSMTP(); // send via SMTP
		$join_complete_mail->Host = ASG_MAIL_HOST;
		$join_complete_mail->Port = ASG_MAIL_PORT;
		$join_complete_mail->Mailer = ASG_MAILER;
		$join_complete_mail->CharSet = ASG_MAIL_CHARSET;
		$join_complete_mail->SMTPAuth = true; // turn on SMTP authentication
		$join_complete_mail->Username = ASG_MAIL_ID; // SMTP username
		$join_complete_mail->Password = ASG_MAIL_PW;// SMTP password

		//mail body
		include ($_SERVER['DOCUMENT_ROOT']."/index/mail_contents/join_complete_mail.php");
		
		if($msg_lang == "en") {
			$join_complete_mail->Body = $join_complete_mail_content_en;
			$join_complete_mail->Subject = "[ Anysuppor​t ] Welcome to AnySupport";
		}
		else if($msg_lang == "jp") {
			$join_complete_mail->Body = $join_complete_mail_content_jp;
			$join_complete_mail->Subject = "[ Anysuppor​t ] Welcome to AnySupport";
		}
		else if($msg_lang == "kr") {
			$join_complete_mail->Body = $join_complete_mail_content_kr;
			$join_complete_mail->Subject = "[ Anysuppor​t ] Welcome to AnySupport";
		}
		else {
			$join_complete_mail->Body = $join_complete_mail_content_en;
			$join_complete_mail->Subject = "[ Anysuppor​t ] Welcome to AnySupport";
		}

		$join_complete_mail->From = $from;
		$join_complete_mail->FromName = $from_name;
		$join_complete_mail->AddAddress($to, $to_name);
		$join_complete_mail->AddReplyTo($from, $from_name);
		$join_complete_mail->WordWrap = 50; // set word wrap
		$join_complete_mail->IsHTML(TRUE); // send as HTML
		//send mail
		$join_complete_mail_res = $join_complete_mail->Send();

		if(!$join_complete_mail_res) {
			echo "
				<script>
					alert('".$msg[$msg_lang]['FREE_JOIN_COMPLETE_SEND_FAIL']."');
					location.href='".$page['SEND_MAIL_FAIL']."';
				</script>
			";
		}
		else {
			echo "
				<form name='free_create_admin' method='post' action='".$page['CREATE_ADMIN_OK']."'>
					<input type='hidden' name='user_select' value='manager' />
					<input type='hidden' name='userid' value='".$adid."' />
					<input type='hidden' name='passwd' value='".$password."' />
					<input type='hidden' name='auto_login_flag' value='".TRUE."' />
				</form>
				<script>
					document.free_create_admin.submit();
				</script>
			";
		}
	}
}