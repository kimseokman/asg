<?php
/**
 * check_reset_pw_code.php
 * 2015.06.03 | KSM | code check added
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"CHECK_CODE_FAIL"=> "/index/".$msg_lang."/login/pw_reset_code_check_fail.php",
	"CHECK_CODE_OK" => "/index/".$msg_lang."/login/pw_reset_code_check_success.php"
);

$check_code_flag = $asg_obj ->CheckEmailValidationForResetPassword($_GET['code']);

if($check_code_flag){
	echo "
		<form name='reset_pw' method='post' action='".$page['CHECK_CODE_OK']."'>
			<input type='hidden' name='code' value='".$_GET['code']."' />
		</form>
		<script>
			document.reset_pw.submit();
		</script>
	";
	exit(0);
}
else{
	echo "
		<script>
			location.href='".$page['CHECK_CODE_FAIL']."';
		</script>
	";
	exit(0);
}