<?php
/**
 * func_free_send_mail.php
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$email_max_length = MAIL_ADDR_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"CHECK_MAIL_FAIL"=> "/index/".$msg_lang."/free/free_form.php",
	"SEND_MAIL_OK" => "/index/".$msg_lang."/free/free_form.php",
	"SEND_MAIL_FAIL" => "/index/".$msg_lang."/free/free_form.php"
);

//
//	Msg Define
//
$msg_en = array(
	"INPUT_ADMIN_ID_EMPTY" => "Please enter admin id.",
	"INPUT_FIRST_NAME_EMPTY" => "Please enter your first name.",
	"INPUT_LAST_NAME_EMPTY" => "Please enter your last name.",
	"INPUT_MAIL_EMPTY" => "Please enter your e-mail.",
	"INPUT_MAIL_LENGTH" => "E-mail address length must be ".$email_max_length." characters or less.",
	"DUPLECATE_ADID" => "Duplicated Admin ID.",
	"DUPLECATE_EMAIL" => "Duplicated e-mail.",
	"SEND_MAIL_OK" => "E-mail has been sent successfully.",
	"SEND_MAIL_FAIL" => "E-mail transmission failed."
);

$msg_jp = array(
	"INPUT_ADMIN_ID_EMPTY" => "Please enter admin id.",
	"INPUT_FIRST_NAME_EMPTY" => "Please enter your first name.",
	"INPUT_LAST_NAME_EMPTY" => "Please enter your last name.",
	"INPUT_MAIL_EMPTY" => "Please enter your e-mail.",
	"INPUT_MAIL_LENGTH" => "E-mail address length must be ".$email_max_length." characters or less.",
	"DUPLECATE_ADID" => "Duplicated Admin ID.",
	"DUPLECATE_EMAIL" => "Duplicated e-mail.",
	"SEND_MAIL_OK" => "E-mail has been sent successfully.",
	"SEND_MAIL_FAIL" => "E-mail transmission failed."
);

$msg_kr = array(
	"INPUT_ADMIN_ID_EMPTY" => "Please enter admin id.",
	"INPUT_FIRST_NAME_EMPTY" => "Please enter your first name.",
	"INPUT_LAST_NAME_EMPTY" => "Please enter your last name.",
	"INPUT_MAIL_EMPTY" => "Please enter your e-mail.",
	"INPUT_MAIL_LENGTH" => "E-mail address length must be ".$email_max_length." characters or less.",
	"DUPLECATE_ADID" => "Duplicated Admin ID.",
	"DUPLECATE_EMAIL" => "Duplicated e-mail.",
	"SEND_MAIL_OK" => "E-mail has been sent successfully.",
	"SEND_MAIL_FAIL" => "E-mail transmission failed."
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

$code = null;
$adid = $_POST['admin_id'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$free_mail = $_POST['mail'];

if(strlen($adid) == 0 || $adid == null || $adid == ""){//basic validate
	echo "
		<form name='free_send_mail' method='post' action='".$page['CHECK_MAIL_FAIL']."'>
			<input type='hidden' name='free_trial_mail' value='".$free_mail."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INPUT_ADMIN_ID_EMPTY']."');
			document.free_send_mail.submit();
		</script>
	";
	exit(0);	
}
else if(strlen($first_name) == 0 || $first_name == null || $first_name == ""){//basic validate
	echo "
		<form name='free_send_mail' method='post' action='".$page['CHECK_MAIL_FAIL']."'>
			<input type='hidden' name='free_trial_mail' value='".$free_mail."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INPUT_FIRST_NAME_EMPTY']."');
			document.free_send_mail.submit();
		</script>
	";
	exit(0);	
}
else if(strlen($last_name) == 0 || $last_name == null || $last_name == ""){//basic validate
	echo "
		<form name='free_send_mail' method='post' action='".$page['CHECK_MAIL_FAIL']."'>
			<input type='hidden' name='free_trial_mail' value='".$free_mail."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INPUT_LAST_NAME_EMPTY']."');
			document.free_send_mail.submit();
		</script>
	";
}
else if(strlen($free_mail) == 0 || $free_mail == null || $free_mail == ""){
	echo "
		<script>
			alert('".$msg[$msg_lang]['INPUT_MAIL_EMPTY']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);	
}
else if(strlen($free_mail) >= $email_max_length){
	echo "
		<script>
			alert('".$msg['INPUT_MAIL_LENGTH']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);
}

$mail_check_res = $asg_obj->GenerateAuthCodeForFreeJoin($code, $free_mail, $adid);

if($mail_check_res == "DUPLECATE_ADID"){
	echo "
		<script>
			alert('".$msg[$msg_lang]['DUPLECATE_ADID']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);
}
else if($mail_check_res == "DUPLECATE_EMAIL"){
	echo "
		<script>
			alert('".$msg[$msg_lang]['DUPLECATE_EMAIL']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);
}
else if($mail_check_res == "OK"){

	//mail promise
	$from = ASG_MAIL_ADDR;
	$from_name = ASG_MAIL_NAME;
	$to = $free_mail; // Recipients email ID
	$to_name = $first_name . " " . $last_name; // Recipient's name

	//mail content
	$link_url = ASG_URL."/index/_func/function.check_free_join_code.php?code=".$code."&first_name=".$first_name."&last_name=".$last_name."&adid=".$adid."&mail=".$free_mail;
	$contact_url = ASG_URL."/index/".$msg_lang."/help/contact_us.php";
	$user_name = $first_name . " " . $last_name;

	//mail setting
	$free_join_auth_mail = new PHPMailer(); 
	$free_join_auth_mail->IsSMTP(); // send via SMTP
	$free_join_auth_mail->Host = ASG_MAIL_HOST;
	$free_join_auth_mail->Port = ASG_MAIL_PORT;
	$free_join_auth_mail->Mailer = ASG_MAILER;
	$free_join_auth_mail->CharSet = ASG_MAIL_CHARSET;
	$free_join_auth_mail->SMTPAuth = true; // turn on SMTP authentication
	$free_join_auth_mail->Username = ASG_MAIL_ID; // SMTP username
	$free_join_auth_mail->Password = ASG_MAIL_PW;// SMTP password

	//mail body
	include ($_SERVER['DOCUMENT_ROOT']."/index/mail_contents/free_join_auth_mail.php");

	if($msg_lang == "en") {
		$free_join_auth_mail->Body = $free_join_auth_mail_content_en;
		$free_join_auth_mail->Subject = "[ Anysuppor​t ] Free Trial";
	}
	else if($msg_lang == "jp") {
		$free_join_auth_mail->Body = $free_join_auth_mail_content_jp;
		$free_join_auth_mail->Subject = "[ Anysuppor​t ] 無料体験";
	}
	else if($msg_lang == "kr") {
		$free_join_auth_mail->Body = $free_join_auth_mail_content_kr;
		$free_join_auth_mail->Subject = "[ Anysuppor​t ] 무료 평가판";
	}
	else {
		$free_join_auth_mail->Body = $free_join_auth_mail_content_en;
		$free_join_auth_mail->Subject = "[ Anysuppor​t ] Free Trial";
	}

	$free_join_auth_mail->From = $from;
	$free_join_auth_mail->FromName = $from_name;
	$free_join_auth_mail->AddAddress($to, $to_name);
	$free_join_auth_mail->AddReplyTo($from, $from_name);
	$free_join_auth_mail->WordWrap = 50; // set word wrap
	$free_join_auth_mail->IsHTML(TRUE); // send as HTML
	//send mail
	$free_join_auth_mail_res = $free_join_auth_mail->Send();

	if(!$free_join_auth_mail_res){
		echo "
			<form name='free_send_mail' method='post' action='".$page['SEND_MAIL_FAIL']."'>
				<input type='hidden' name='free_send_mail_res' value='FAIL' />
			</form>
			<script>
				document.free_send_mail.submit();
			</script>
		";
	}else{
		if($_POST['send_type'] == 'free_join'){
			echo "
				<form name='free_send_mail' method='post' action='".$page['SEND_MAIL_OK']."'>
					<input type='hidden' name='free_send_mail_res' value='SUCCESS' />
					<input type='hidden' name='free_send_mail_first_name' value='".$first_name."' />
					<input type='hidden' name='free_send_mail_last_name' value='".$last_name."' />
				</form>
				<script>
					document.free_send_mail.submit();
				</script>
			";
		}
	}
}