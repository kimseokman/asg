<?php
/**
 * function.check_free_join_code.php
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"CHECK_CODE_FAIL"=> "/index/".$msg_lang."/free/free_form.php",
	"CHECK_CODE_OK" => "/index/".$msg_lang."/free/free_form.php"
);

//
//	Msg Define
//
$msg_en = array(
	"INVALID_AUTHCODE" => "invalid auth code"
);

$msg_jp = array(
	"INVALID_AUTHCODE" => "無効な認証コード"
);

$msg_kr = array(
	"INVALID_AUTHCODE" => "잘못된 인증 코드"
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

$code = $_GET['code'];
$first_name = $_GET['first_name'];
$last_name = $_GET['last_name'];
$adid = $_GET['adid'];
$mail = $_GET['mail'];

$check_code_res = $asg_obj ->CheckEmailAuthCodeForFreeJoin($mail , $adid , $code);

if($check_code_res == "INVALID_AUTHCODE"){
  	echo "
		<form name='auth_mail' method='post' action='".$page["CHECK_CODE_FAIL"]."'>
			<input type='hidden' name='auth_code_flag' value='".FALSE."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['INVALID_AUTHCODE']."');
			document.auth_mail.submit();
		</script>
	";
	exit(0);
}
else if($check_code_res == "OK"){
  	echo "
		<form name='auth_mail' method='post' action='".$page["CHECK_CODE_OK"]."'>
			<input type='hidden' name='auth_code_flag' value='".TRUE."' />
			<input type='hidden' name='auth_code' value='".$code."' />
			<input type='hidden' name='first_name' value='".$first_name."' />
			<input type='hidden' name='last_name' value='".$last_name."' />
			<input type='hidden' name='adid' value='".$adid."' />
			<input type='hidden' name='mail' value='".$mail."' />
		</form>
		<script>
			document.auth_mail.submit();
		</script>
	";
	exit(0);
}