<?php
/**
 * index_login.php
 * 2015.05.27 | KSM | login
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"LOGIN_FAIL"=> "/index/".$msg_lang."/login/login.php",
	"LOGIN_SUCCESS_ADMIN"=> ADMIN_INDEX,
	"LOGIN_SUCCESS_SUPPORTER"=> SUPPORTER_INDEX
);

//
//	Msg Define
//
$msg_en = array(
	"ADMIN_BLOCK" => "It is working to update the admin page. Please try later.",
	"NOID" => "Enter your ID.",
	"NOPASSWORD" => "Enter your Password.",
	"DB_ERROR" => "Now it is not available. <br />Please use later.",
	"USER_EXPIRED" => "This is an expired ID.",
	"NO_VALIDATE" => "Invalid login information."
);

$msg_jp = array(
	"ADMIN_BLOCK" => "管理者ページの更新のために作業中です。しばらくして使用してください。",
	"NOID" => "IDを入力してください。",
	"NOPASSWORD" => "パスワードを入力してください。",
	"DB_ERROR" => "今では使用できません。<br />しばらくして利用してください。",
	"USER_EXPIRED" => "有効期限が切れたIDです。",
	"NO_VALIDATE" => "不正なログイン情報です。"
);

$msg_kr = array(
	"ADMIN_BLOCK" => "관리자 페이지 업데이트를 위해 작업 중 입니다. 잠시 후 사용해 주세요.",
	"NOID" => "아이디를 입력 하십시오.",
	"NOPASSWORD" => "비밀번호를 입력 하십시오.",
	"DB_ERROR" => "지금은 사용할 수 없습니다. <br />잠시 후 이용해 주세요.",
	"USER_EXPIRED" => "사용기간이 만료된 아이디 입니다.",
	"NO_VALIDATE" => "잘못된 로그인 정보 입니다."
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

if($_POST['user_select'] == 'manager'){//case by manager
	$logged = $asg_obj->LoginAdmin($_POST["userid"], $_POST["passwd"]);//try to login by admin
}
else if($_POST['user_select'] == 'technician'){//case by technician
	$logged = $asg_obj->LoginSupporter($_POST["userid"], $_POST["passwd"]);//try to login by  supporter
}


if($logged == "ADMIN"){//case by login success admin
	$_SESSION['language'] = $msg_lang;

	echo "
		<form name='auto_login_form' method='post' action='".$page['LOGIN_SUCCESS_ADMIN']."'>
			<input type='hidden' name='auto_login_flag' value='".$_POST['auto_login_flag']."' />
		</form>
		<script>
			document.auto_login_form.submit();
		</script>
	";
}
else if($logged == "ADMIN_BLOCK"){
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['ADMIN_BLOCK']."');
			location.href='".$page['LOGIN_FAIL']."';
		</script>
	";
}
else if($logged == "SUPPORTER") {//case by login success supporter
	$_SESSION['language'] = $msg_lang;

	echo "
		<script>
			location.href='".$page['LOGIN_SUCCESS_SUPPORTER']."';
		</script>
	";
}
else if($logged == "NOID"){
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['NOID']."'); 
			location.href='".$page['LOGIN_FAIL']."'; 
		</script>
	";
}
else if($logged == "NOPASSWORD"){
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['NOPASSWORD']."'); 
			location.href='".$page['LOGIN_FAIL']."'; 
		</script>
	";
}
else if($logged == "DB_ERROR"){
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['DB_ERROR']."'); 
			location.href='".$page['LOGIN_FAIL']."'; 
		</script>
	";
}
else if($logged == "ADMIN_EXPIRED" || $logged == "SUPPORTER_EXPIRED"){
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['USER_EXPIRED']."'); 
			location.href='".$page['LOGIN_FAIL']."'; 
		</script>
	";
}
else{
	echo "
		<script type='text/javascript'>
			alert('".$msg[$msg_lang]['NO_VALIDATE']."'); 
			location.href='".$page['LOGIN_FAIL']."'; 
		</script>
	";
}