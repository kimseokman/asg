<?php
session_start();
/**
 * index_common.php
 * 2015.06.05 | KSM | for session
 */
header('Content-Type: text/html; charset=utf-8');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$current_url = $utils_obj->GetCurrentPageURL();
$current_site_lang = $utils_obj->GetLangFromURL($current_url);

if($current_site_lang == NULL){
	$set_lang_flag = setcookie('client_lang', 'en', 0, '/');//default
}
else{
	$set_lang_flag = setcookie('client_lang', $current_site_lang, 0, '/');
}