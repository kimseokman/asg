<?php
/**
 * pw_reset.php
 * 2015.06.03 | KSM | password reset
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$pw_length_min = PW_MIN_LENGTH;
$pw_length_max = PW_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"PW_RESET_OK"=> "/index/".$msg_lang."/login/pw_reset_success.php",
	"PW_RESET_FAIL" => "/index/".$msg_lang."/login/pw_reset_fail.php"
);

//
//	Msg Define
//
$msg_en = array(
	"CURRENT_PASSWORD_ERROR"=> "The password can only be character or number of ".$pw_length_min."-".$pw_length_max.".",
	"RETYPE_PASSWORD_ERROR"=> "Passwords does not match.",
	"CODE_ERROR"=> "Failed to initialize the password."
);

$msg_jp = array(
	"CURRENT_PASSWORD_ERROR"=> "パスワードは、".$pw_length_min."-".$pw_length_max."の英語または数字のみ可能です。",
	"RETYPE_PASSWORD_ERROR"=> "パスワードが一致しません。",
	"CODE_ERROR"=> "パスワードの初期化に失敗しました。"
);

$msg_kr = array(
	"CURRENT_PASSWORD_ERROR"=> "비밀번호는 ".$pw_length_min."-".$pw_length_max."의 영문 또는 숫자만 가능 합니다.",
	"RETYPE_PASSWORD_ERROR"=> "비밀번호가 일치하지 않습니다.",
	"CODE_ERROR"=> "비밀번호 초기화를 실패하였습니다."
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

if($_POST['reset_pw'] == "" || $_POST['reset_pw'] == NULL){
	echo "
		<script>
			alert('".$msg[$msg_lang]['CURRENT_PASSWORD_ERROR']."');
			location.href='".$page['PW_RESET_FAIL']."';
		</script>
	";
	exit(1);
}else if(strcmp($_POST['reset_pw'], $_POST['reset_retype_pw'])){
	echo "
		<script>
			alert('".$msg[$msg_lang]['RETYPE_PASSWORD_ERROR']."');
			location.href='".$page['PW_RESET_FAIL']."';
		</script>
	";
	exit(1);
}

$reset_pw_flag = $asg_obj->ResetAdminPassword($_POST['code'], $_POST['reset_pw']);

if($reset_pw_flag){//case by success
	echo "
		<script>
			location.href='".$page['PW_RESET_OK']."';
		</script>
	";
	exit(0);
}else{//case by fail
	echo "
		<script>
			alert('".$msg[$msg_lang]['CODE_ERROR']."');
			location.href='".$page['PW_RESET_FAIL']."';
		</script>
	";
	exit(0);
}