<?php
/**
 * forgot_pw_send_mail.php
 * 2015.06.02 | KSM | reset password send mail
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
require($_SERVER['DOCUMENT_ROOT']."/_lib/_inc/phpmailer/class.phpmailer.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$email_max_length = MAIL_ADDR_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"CHECK_MAIL_FAIL"=> "/index/".$msg_lang."/login/forgot_pw_send_mail.php",
	"SEND_MAIL_OK" => "/index/".$msg_lang."/login/forgot_pw_send_mail_success.php",
	"SEND_MAIL_FAIL" => "/index/".$msg_lang."/login/forgot_pw_send_mail_fail.php"
);

//
//	Msg Define
//
$msg_en = array(
	"INPUT_MAIL_EMPTY" => "Please enter your e-mail.",
	"INPUT_MAIL_LENGTH" => "E-mail address length must be ".$email_max_length." characters or less.",
	"NOT_EXIST_MAIL" => "Unregistered email address.",
	"SEND_MAIL_OK" => "E-mail has been sent successfully.",
	"SEND_MAIL_FAIL" => "E-mail transmission failed."
);

$msg_jp = array(
	"INPUT_MAIL_EMPTY" => "メールアドレスを入力してください。",
	"INPUT_MAIL_LENGTH" => "メールアドレスの長さは、".$email_max_length."文字以内にしてください。",
	"NOT_EXIST_MAIL" => "登録されていない電子メールアドレス。",
	"SEND_MAIL_OK" => "電子メールが正常に送信されました。",
	"SEND_MAIL_FAIL" => "電子メールの送信が失敗しました"
);

$msg_kr = array(
	"INPUT_MAIL_EMPTY" => "이메일을 입력하세요.",
	"INPUT_MAIL_LENGTH" => "이메일 주소 길이는 ".$email_max_length."글자 이내여야 합니다.",
	"NOT_EXIST_MAIL" => "등록되지 않은 이메일 주소 입니다.",
	"SEND_MAIL_OK" => "이메일이 정상적으로 발송 되었습니다.",
	"SEND_MAIL_FAIL" => "이메일 전송이 실패하였습니다."
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//

$base_URL = ($_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$base_host = explode(":", $_SERVER['HTTP_HOST']);
$base_URL .= ($_SERVER['SERVER_PORT'] != '80') ? $base_host[0].':'.$_SERVER['SERVER_PORT'] : $base_host[0];

$code = null;
$adid = null;
$pw_reset_email = $_POST['pw_reset_email'];

if(strlen($pw_reset_email) == 0 || $pw_reset_email == null || $pw_reset_email == ""){//basic validate
	echo "
		<script>
			alert('".$msg[$msg_lang]['INPUT_MAIL_EMPTY']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);	
}
else if(strlen($pw_reset_email) >= $email_max_length){
	echo "
		<script>
			alert('".$msg[$msg_lang]['INPUT_MAIL_LENGTH']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);
}

//db validate
$mail_check_flag = $asg_obj->GenerateAuthCodeForResetAdminPassword($code, $adid, $pw_reset_email);

if($mail_check_flag){//case by email exist

	//mail promise
	$from = ASG_MAIL_ADDR;
	$from_name = ASG_MAIL_NAME;
	$to = $pw_reset_email; // Recipients email ID
	$to_name = ""; // Recipient's name

	//mail content
	$link_url = ASG_URL."/index/_func/function.check_reset_pw_code.php?code=".$code;
	$contact_url = ASG_URL."/index/".$msg_lang."/help/contact_us.php";
	$to_adid = $adid;

	//mail setting
	$reset_pw_auth_mail = new PHPMailer(); 
	$reset_pw_auth_mail->IsSMTP(); // send via SMTP
	$reset_pw_auth_mail->Host = ASG_MAIL_HOST;
	$reset_pw_auth_mail->Port = ASG_MAIL_PORT;
	$reset_pw_auth_mail->Mailer = ASG_MAILER;
	$reset_pw_auth_mail->CharSet = ASG_MAIL_CHARSET;
	$reset_pw_auth_mail->SMTPAuth = true; // turn on SMTP authentication
	$reset_pw_auth_mail->Username = ASG_MAIL_ID; // SMTP username
	$reset_pw_auth_mail->Password = ASG_MAIL_PW;// SMTP password

	//mail body
	include ($_SERVER['DOCUMENT_ROOT']."/index/mail_contents/reset_pw_auth_mail.php");
	
	if($msg_lang == "en") {
		$reset_pw_auth_mail->Body = $reset_pw_auth_mail_content_en;
		$reset_pw_auth_mail->Subject = "[ Anysuppor​t ] Password Reset";
	}
	else if($msg_lang == "jp") {
		$reset_pw_auth_mail->Body = $reset_pw_auth_mail_content_jp;
		$reset_pw_auth_mail->Subject = "[ Anysuppor​t ] パスワードの初期化";
	}
	else if($msg_lang == "kr") {
		$reset_pw_auth_mail->Body = $reset_pw_auth_mail_content_kr;
		$reset_pw_auth_mail->Subject = "[ Anysuppor​t ] 비밀번호 초기화";
	}
	else {
		$reset_pw_auth_mail->Body = $reset_pw_auth_mail_content_en;
		$reset_pw_auth_mail->Subject = "[ Anysuppor​t ] Password Reset";
	}

	$reset_pw_auth_mail->From = $from;
	$reset_pw_auth_mail->FromName = $from_name;
	$reset_pw_auth_mail->AddAddress($to, $to_name);
	$reset_pw_auth_mail->AddReplyTo($from, $from_name);
	$reset_pw_auth_mail->WordWrap = 50; // set word wrap
	$reset_pw_auth_mail->IsHTML(TRUE); // send as HTML
	//send mail
	$reset_pw_auth_mail_res = $reset_pw_auth_mail->Send();

	if(!$reset_pw_auth_mail_res){
		echo "
			<script>
				location.href='".$page['SEND_MAIL_FAIL']."';
			</script>
		";
	}
	else{
		if($_POST['send_type'] == 'pw_reset'){
			echo "
				<script>
					location.href='".$page['SEND_MAIL_OK']."';
				</script>
			";
		}
	}
}
else{//case by email does not exist
	echo "
		<script>
			alert('".$msg[$msg_lang]['NOT_EXIST_MAIL']."');
			location.href='".$page['CHECK_MAIL_FAIL']."';
		</script>
	";
	exit(0);
}