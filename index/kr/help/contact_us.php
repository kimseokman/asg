<?
$wz['pid']  = "50";
$wz['gtt']  = "문의";
$wz['gtt02']  = "문의하기";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");

$request_mail_res = "NORMAL"; 
if($_POST['request_mail_res']){
	$request_mail_res = $_POST['request_mail_res'];
	$request_mail_name = $_POST['request_mail_name'];
}
?>
<!-- 내용시작 -->
<dl class="help_top_dl">
	<dt>문의사항이 있으십니까? </dt>
	<dd>
		오류가 발생 하였습니까?  <br />
		빠르고 신속한 해결에 최선을 다하겠습니다
	</dd>
</dl>

<div class="help_content">
	<div class="help_con_cc">
		<form name="contact_form" method="post" action="/index/_func/function.contact_us_send_mail.php">
			<p class="help_con_p">아래의 입력란에 자세하게 기입해 주세요. </p>
			<p class="help_common_txt">1.이메일 및 연락 가능한 정보를 입력해 주세요</p>
			<ul class="help_input_ul">
				<li class="help_ul_half"><input type="text" name="contact_first_name" id="" value="" placeholder="성." class="help_input" /></li>
				<li class="help_ul_half_last"><input type="text" name="contact_last_name" id="" value="" placeholder="이름" class="help_input"/></li>
				<li><input type="text" name="contact_phone" id="" value="" placeholder="연락가능번호 " class="help_input"/></li>
				<li><input type="text" name="contact_mail" id="" value="" placeholder="이메일" class="help_input"/></li>
			</ul>
			<p class="help_common_txt">2.어떤 도움이 필요하십니까?</p>
			<textarea name="contact_msg" id="" class="help_area" placeholder="자세히 입력해 주세요."></textarea>
			<p class="help_common_txt">3.고객님의 회사 정보를 입력해 주세요</p>
			<ul class="help_input_ul">
				<li><input type="text" name="contact_company" id="" value="" placeholder="회사명" class="help_input"/></li>
				<li><input type="text" name="contact_department" id="" value="" placeholder="부서명" class="help_input"/></li>
			</ul>
			<p class="common_blue_btn contact_send_btn"><a href="#">보내기</a></p>
		</form>	
	</div>
</div>
<?
include_once("../help_quick.php");
?>
<?
if($request_mail_res == "SUCCESS"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>감사합니다. <? echo $request_mail_name; ?>!</dt>
		<dd>
			담당자에게 메일이 발송되었습니다.
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/kr/help/contact_us.php" class="pop_link_a02">확인</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "SUCCESS")

if($request_mail_res == "FAIL"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>죄송합니다. <? echo $request_mail_name; ?>!</dt>
		<dd>
			메일 발송에 실패했습니다.
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/kr/help/contact_us.php" class="pop_link_a02">확인</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "FAIL")
?>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>