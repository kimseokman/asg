<?
include_once("./header.php");
?>
	<!-- 내용시작 -->
	<!-- main visual -->
	<script type="text/javascript" src="/index/kr/js/jquery.bxslider.js"></script>
	<div id="slider"> 
		<div class="slide-wrap">
			<div class="visual_search_con">
				<div class="visual_search">
					<dl class="main_visual_search">
						<dt><input type="text" name="start_free_trial_mail" id="" value="" placeholder="이메일입력" /></dt>
						<dd><a href="#" class="visual_free_trial_btn">무료체험 시작</a></dd>
					</dl>
				</div>
			</div>
			<ul class="bxslider">
				<li class="main_visual_li01">
					<div class="main_visual_con_div">
						<dl class="main_visual_dl">
							<dt>고품질 PC원격지원 서비스</dt>
							<dd>
								애니서포트는 빠르고 누구나 쉽게 이용할 수 있는 
								원격지원 서비스<br />입니다. 애니서포트 PC로 언제 어디서든 원격지PC로 접속하여
								쉽고<br />빠르게 문제를 해결하세요.
							</dd>
						</dl>
					</div>
				</li>
				<li class="main_visual_li02">
					<div class="main_visual_con_div">
						<dl class="main_visual_dl">
							<dt>애니서포트 모바일에디션</dt>
							<dd>
							     스마트폰 및 각종 스마트 기기에서 발생한 상황을 모바일 에디션으로<br /> 
								 해결 하세요. 쉬운 다운로드, 다양한 기기에 맞춘 최상의 이용환경을<br />
								 통해 수준 높은 고객 만족 서비스를 실현하세요.
							</dd>
						</dl>
					</div>
				</li>
				<li class="main_visual_li03">
					<div class="main_visual_con_div">
						<dl class="main_visual_dl">
							<dt>애니서포트 비디오 에디션</dt>
							<dd>
								 비디오 에디션으로 언제 어디서나 실시간으로 
								전송되는 고품질의 <br /> 영상과 음성지원으로 차별화된 지원환경을 구축해 보세요. 진보된<br />
								원격 지원서비스를 경험해 보세요.
								
							</dd>
						</dl>
					</div>
				</li>
			</ul>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.bxslider').bxSlider();
			});
		</script>
	</div>
	
	<!-- main block bar -->
	<div class="common_black_bar">
		<div class="black_bar_1080">
			<dl class="black_dl01">
				<dt>애니서포트와 함께  효과적인 원격지원을 지금 바로 시작 하세요!</dt>
				<dd>
					<ul class="black_btn">
						<li class="black_btn_orther"><a href="/index/kr/free/free_form.php">무료체험</a></li>
						<!--li><a href="/index/kr/buy/buy_form.php">바로구매</a></li-->
					</ul>
				</dd>
			</dl>
		</div>
	</div>
	<!-- main block bar -->
	
	<div class="main_underline_div">
		<div id="main_common_content">
			<ul class="main_middle_con">
				<li>
					<div class="main_middle_img">
						<dl class="main_middle_img_dl">
							<dt>
								<img src="/index/kr/img/main_top_img01.png" alt="" />
								<div class="main_middle_hover_div">
									<dl class="middle_label_dl">
										<dt>$45.99</dt>
										<dd>연 결제</dd>
									</dl>
									<p class="middle_hover_p"><img src="/index/kr/img/main_top_right_img.png" alt="hot deal" /></p>
								</div>
							</dt>
							<dd class="main_middle_bg01">애니서포트 1+2 </dd>
						</dl>
					</div>
					<dl class="main_middle_dl">
						<dt><img src="/index/kr/img/main_top_img03.gif" alt="" /></dt>
						<dd>
							<span class="main_middle_dl_span">한 개정 구매시 두 계정 더 드림</span><br /><br />
							애니서포트 한 계정 구매 시 두 계정을 더 드<br />
							립니다. <a href="/index/kr/pricing/pricing.php" class="main_middle_blue_link">가격보기 +</a> 
						</dd>
					</dl>
				</li>
				<li class="last">
					<div class="main_middle_img">
						<dl class="main_middle_img_dl">
							<dt>
								<img src="/index/kr/img/main_top_img02.png" alt="" />
								<div class="main_middle_hover_div">
									<dl class="middle_label_dl">
										<dt>$59.99</dt>
										<dd>연 결제</dd>
									</dl>
									<p class="middle_hover_p"><img src="/index/kr/img/main_top_right_img.png" alt="hot deal" /></p>
								</div>
							</dt>
							<dd class="main_middle_bg02">올인원 패키지</dd>
						</dl>
					</div>
					<dl class="main_middle_dl">
						<dt><img src="/index/kr/img/main_top_img04.gif" alt="" /></dt>
						<dd>
							<span class="main_middle_dl_span">애니서포트 올인원</span><br /><br />
							애니서포트 PC, 모바일, 비디오에디션을 다같<br />
							이 이용 할 수있는 기회 <a href="/index/kr/pricing/pricing.php" class="main_middle_blue_link">가격보기 +</a> 
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<!-- middle visual -->
	<script type="text/javascript" src="/index/kr/js/jquery.bxslider02.js"></script>
	<div id="slider02"> 
		<div class="slide-wrap02">
			<ul class="bxslider02">
				<li>
					<div class="middle_slide_div01">
						<p class="middle_slide_icon"><img src="/index/kr/img/main_middle_visual_icon01.png" alt="" /></p>
						<dl class="middle_visual_dl">
							<dt>
								애니서포트로 <br />
								시간을 절약하세요!
							</dt>
							<dd>
								고객 PC에 문제가 생겼을 때 번거로운 방문<br />
								절차 없이 바로 전문 상담원의 원격 지원으로 <br />
								편리하게 문제를 해결 하세요.
							</dd>
						</dl>
					</div>
					<img src="/index/kr/img/main_middle_visual_01.png" alt="" />
				</li>
				<li>
					<div class="middle_slide_div02">
						<p class="middle_slide_icon"><img src="/index/kr/img/main_middle_visual_icon02.png" alt="" /></p>
						<dl class="middle_visual_dl">
							<dt>
								스마트 기기를 <br />
								원격 지원하세요!
							</dt>
							<dd>
								다양한 종류의 스마트기기와 OS를 지원하는 <br />
								애니서포트 원격지원 서비스로 만족스런 업무 <br />
								환경을 만들어 보세요.
							</dd>
						</dl>
					</div>
					<img src="/index/kr/img/main_middle_visual_02.png" alt="" />
				</li>
				<li>
					<div class="middle_slide_div03">
						<p class="middle_slide_icon"><img src="/index/kr/img/main_middle_visual_icon03.png" alt="" /></p>
						<dl class="middle_visual_dl">
							<dt>
								실시간 화면 전송으로 <br />
								정확한 원격지원!
							</dt>
							<dd>
								핸드폰이나 태블릿 기기의 내장 된 카메라로 <br />
								문제 상황을 음성과 함께 실시간 전송 하여 원<br />
								격 지원을 이용 하는 방법입니다. 다양한 부가기<br />
								능들과 함께 이용해 보세요.
							</dd>
						</dl>
					</div>
					<img src="/index/kr/img/main_middle_visual_03.png" alt="" />
				</li>
			</ul>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.bxslider02').bxSlider02();
		});
	</script>
	<!-- middle visual -->

	<div class="main_gray_div">
		<div class="main_gray_content">
			<dl class="gray_dl">
				<dt>왜 애니서포트인가? </dt>
				<dd>합리적인 가격으로 최상의 서비스와 기술 지원을 제공합니다.</dd>
			</dl>
			<ul class="gray_con_ul">
				<li>다양한 기기를 지원 </li>
				<li>
					 장애 없는 접속 환경 
					<div class="gray_ul_percent">
						99.9%
					</div>
				</li>
				<li>동시에 여러 상담원 지원 가능</li>
				<li>강력한 보안 </li>
				<li>빠르고 효과적인 서비스 환경 제공  <br />
					<span class="normal_gray">
						-신속하고 쉬운 접속<br />
						-멈춤 없는 화면 공유
					</span>
				</li>
				<li>합리적인 가격으로 제공</li>
			</ul>
			<dl class="gray_icon_dl">
				<dt>
					<ul class="gray_icon_ul">
						<li><img src="/index/kr/img/main_middle_icon01.gif" alt="크롬" /></li>
						<li><img src="/index/kr/img/main_middle_icon02.gif" alt="파이어폭스" /></li>
						<li><img src="/index/kr/img/main_middle_icon03.gif" alt="오페라" /></li>
						<li><img src="/index/kr/img/main_middle_icon04.gif" alt="사파리" /></li>
						<li><img src="/index/kr/img/main_middle_icon05.gif" alt="익스플로러" /></li>
						<li class="phone_icon"><img src="/index/kr/img/main_middle_icon06.gif" alt="안드로이드" /></li>
						<li><img src="/index/kr/img/main_middle_icon07.gif" alt="애플" /></li>
					</ul>
				</dt>
				<dd><a href="/index/kr/product/anysupport.php">상세보기+ </a></dd>
			</dl>
		</div>
	</div>

	<div id="main_common_content">
		<div class="main_bottom_content">
			<p class="main_bottom_p">다양한 곳에서 활용 가능 합니다.</p>
			<ul class="main_bottom_ul">
				<li>
					<dl class="main_bottom_dl">
						<dt>Help Desk</dt>
						<dd>
							외부 서비스 제공 회사나 전자 제품 회사의 CS센터에서 전문 기사의 방문 없이도 빠르고 정확한 고객 <br />
							서비스를 제공 할 수 있습니다     
						</dd>
					</dl>
				</li>
				<li>
					<dl class="main_bottom_dl">
						<dt>소프트웨어, 시스템  A/S 및 서버 네트워크 운영 관리 </dt>
						<dd>비용과 시간의 낭비 없이 효율적인 서비스 지원과 운영관리를 할 수 있습니다.   </dd>
					</dl>
				</li>
				<li>
					<dl class="main_bottom_dl">
						<dt>사내 시스템 지원 및 서버 담당 부서  </dt>
						<dd>애니서포트는 앉은 자리에서 각 부서내 시스템 지원 및 보수. 점검을 원격으로 진행 할 수 있습니다.</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<!-- main bottom banner -->
	<div class="bottom_banner">
		<ul class="bottom_banner_ul">
			<li><img src="/index/kr/img/main_bottom_banner_img01.gif" alt="SAMSUNG" /></li>
			<li><img src="/index/kr/img/main_bottom_banner_img02.gif" alt="standard Chartered" /></li>
			<li><img src="/index/kr/img/main_bottom_banner_img03.gif" alt="IBM" /></li>
			<li><img src="/index/kr/img/main_bottom_banner_img04.gif" alt="HYUNDAI" /></li>
			<li><img src="/index/kr/img/main_bottom_banner_img05.gif" alt="LOTTECARD" /></li>
			<li class="last"><img src="/index/kr/img/main_bottom_banner_img06.gif" alt="LG CNS" /></li>
		</ul>
	</div>
	<!-- main bottom banner -->

	<!-- 내용끝 -->

<?
include_once("./footer.php");
?>