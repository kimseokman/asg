<?
$wz['pid']  = "20";
$wz['gtt']  = "제품소개";
$wz['gtt02']  = "Mobile Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>Anysupport All in one</dt>
				<dd>PC, 각종 디바이스, 실시간 영상 원격지원을 하나로!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>지금 바로 신청 하세요.</dt>
				<dd><a href="/index/kr/free/free_form.php">무료체험시작</a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/kr/product/anysupport.php">Anysupport</a></li>
				<li class="product_tab02_on"><a href="/index/kr/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03"><a href="/index/kr/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">모바일 에디션</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">제품특징</a></li>
				<li><a href="#use" class="arctic_scroll">이용방법</a></li>
				<li><a href="#beneficial" class="arctic_scroll">활용예시</a></li>
			</ul>
			<div class="product_top_div02">
				<dl class="product_top_dl01">
					<dt>
						각종 스마트 기기에 최적화된 사용 환경을 제공하여 스마트 기기 사용자를<br />  위한 맞춤형 원격지원 서비스를 제공합니다.
						고객의 스마트 기기에서 발생<br />하는 문제를 전문 상담원의 PC에서 공유, 정확한 원격 지원을 통해 문제 상</br>황을 해결 합니다. 
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드<br />
											모바일에디션 특징점
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드<br />
											모바일에디션 사용 방법
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div02">
		<div class="product_content" id="features">
			<p class="underline_tit">제품 특징</p>
			<dl class="product_center_dl01">
				<dt>
					다양한 스마트 기기를 위한 <br />
					<span class="product_italic_span">쉽고 빠른 원격지원 서비스 </span>
				</dt>
				<dd>
					다양한 스마트기기를 사용하고 있는 고객들을 위한 맞춤형 원격지원<br />
					서비스를 제공합니다. 언제 어디서든 고객의 기기에 어플리케이션을 <br />
					설치 하기만하면 상담원이 PC 에서 원격 접속하여 문제를 빠르게 <br />
					해결 합니다.
					<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
				</dd>
			</dl>
			<p class="center_p_img"><img src="/index/kr/img/mobileedition_con_img02.gif" alt="" /></p>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg03">
				<p class="product_gray_p">고객중심 서비스 환경을 위한</p>
				<dl class="product_con_dl02">
					<dt>스마트 기기와 OS에 최적화된 기술</dt>
					<dd>
						모바일 에디션은 IOS/ Android기반으로 한 다양한 종류의 스마트<br />
						기기에 최적화된 서비스를 구축하고 있습니다. 스마트 기기 사용 <br />
						환경에 적합한 원격지원 서비스로 업무 효율성과 서비스 만족도를 <br />
						높일 수 있습니다.
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험  ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>여러 종류의 기기에 대한 지원이 필요한 각종 CS부서 </li>
					<li>전문적인 원격 지원이 요구되는 IT 모든 분야 </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg04">
				<p class="product_gray_p">고품격 원격지원을 위한</p>
				<dl class="product_con_dl03">
					<dt>다양한 네트워크 이용을 위한 지원</dt>
					<dd>
						모바일 에디션은 WI-FI, 3G, LTE등 다양한 네트워크 연결 방식을<br />
						통해  막힘 없이 효과적인 원격지원을 가능하게 합니다. 
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>다양한 네트워크 환경을 이용한 고객 지원 분야 </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">이용방법</p>
			<div class="product_movie_area">
				<iframe width="1040" height="400" src="https://www.youtube.com/embed/kVwqYuaBJ1I?wmode=opaque" frameborder="0" allowfullscreen></iframe>
			</div>
			<p class="product_gray_p">효과적인 사용을 위한</p>
			<dl class="product_con_dl02">
				<dt>모바일에디션 이용방법</dt>
				<dd>
					고객이 앱스토어나 플레이스토어 에서 모바일에디션 앱을 다<br />
					운로드 한 후 문제상황을 접수 합니다. 이후에 상담원이 고객에<br />
					게 고유 접속번호 전달하며 고객이 앱 을 통해 접속 번호를 입력<br />
					한 후 바로 원격 지원 서비스를 이용할 수 있습니다.
					<a href="/index/kr/free/free_form.php" class="blue_link_a02">무료체험 ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							다운로드<br />
							모바일에디션 이용 방법
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">활용예시</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">스마트 기기 공급자 및<br />통신사 </p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/mobileedition_list_img01.gif" alt="Mobile device Provider images" /></p>
							</div>
						</dt>
						<dd>
							판매하고 있는 단말기에 간단한 어플리케이션 <br />
							설치만으로 고객이 쉽게 원격 지원서비를 받을<br />
							수 있습니다. 또한 다양한 원격지원 업무로 <br />
							활용 가능 합니다. 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">스마트기기 기반<br />서비스 업체</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/mobileedition_list_img02.gif" alt="Mobile Servise Help Desk images" /></p>
							</div>
						</dt>
						<dd>
							원격 지원을  할 수 있기 때문에 기업에서 <br />
							서비스 제공시 발생하는 다양한상황에서 <br />
							직접 신속하게 고객을 지원 할 수 있습니다. 
						</dd>
					</dl>
				</li>
				<!-- <li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Groupware , VPN<br />Supporter</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/mobileedition_list_img03.gif" alt="Groupware , VPN Supporter images" /></p>
							</div>
						</dt>
						<dd>
							An remotely view and control your client's<br />
							computers in order to quickly diagnose<br />
							and solve problems.Service engineers can<br />
							up date applications,back up files and fix<br />
							device problems remot-ely in a simple and<br />
							efficient way.
						</dd>
					</dl>
				</li> -->
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">세부기능</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>모바일 화면 공유</dt>
						<dd>
							고객의 단말기 화면 을 공유하고<br />
							상담원이 작업 할 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>동시 작업 가능 </dt>
						<dd>
							지원 하는 동안 다른 상담사 <br />
							초대 후 동시 작업 가능
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>다양한 네트워크 지원</dt>
						<dd>
							Wi - Fi , 3G, LTE 등 다양한 연결<br />
							방식을 지원 합니다 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>여러 고객에게 동시 지원</dt>
						<dd>
							여러 고객에게 동시 지원1 명의 <br />
							상담원이 동시에 10 명의 고객에<br />
							게 지원이 가능합니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>파일 송수신 </dt>
						<dd>
							작업 중에 패치나 URL을 보내거나<br />
							로그 파일 을 수신 할 수 있습니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>드로잉 기능</dt>
						<dd>
							선과 도형을 직접 화면에 쓰면서<br />
							시각적으로 설명 할 수 있습니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>다양한 기기와 OS지원</dt>
						<dd>
							모바일, 태블릿 PC 등 스마트 <br />
							기기에 최적화 되었습니다 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>지원 화면 녹화 </dt>
						<dd>
							지원 화면을 캡쳐 하거나 녹화 저장<br />
							할 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>채팅기능</dt>
						<dd>
							원격 지원 중에도 채팅을 통해 <br />
							고객과 대화가 가능합니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>편리한 사용자 화면 </dt>
						<dd>
							고객지원 시 쉽고 빠르게 작업이 <br />
							가능합니다.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>