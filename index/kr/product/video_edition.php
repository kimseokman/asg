<?
$wz['pid']  = "20";
$wz['gtt']  = "제품소개";
$wz['gtt02']  = "Video Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>Anysupport All in one</dt>
				<dd>PC, 각종 디바이스, 실시간 영상 원격지원을 하나로!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>지금 바로 신청 하세요.</dt>
				<dd><a href="/index/kr/free/free_form.php">무료체험시작</a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/kr/product/anysupport.php">Anysupport</a></li>
				<li class="product_tab02"><a href="/index/kr/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03_on"><a href="/index/kr/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">비디오에디션</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">제품특징</a></li>
				<li><a href="#use" class="arctic_scroll">이용방법</a></li>
				<li><a href="#beneficial" class="arctic_scroll">활용예시</a></li>
			</ul>
			<div class="product_top_div03">
				<dl class="product_top_dl01">
					<dt>
						모바일 기기에 탑재된 카메라로 촬영한 영상과 음성을 실시간으로<br />
						상담원에게 전송하며 문제상황을 즉각적으로 해결 할 수 있습니다.<br /> 
						각종 사업 분야 및 재난 현장은 물론 실시간 고객지원이 필요한 전<br />
						세계 어디에서나 빠르고 쉬운 비디오 에디션 솔루션을 이용한 전문<br />
						적인 원격 지원 서비스를 이용해 보세요.<br />
						
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드 <br />
											비디오에디션 세부 특징 
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드 <br />
											비디오에디션 이용 방법
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div04">
		<div class="product_content" id="features">
			<div class="videoedition_con_bg">
				<p class="underline_tit">제품 특징 </p>
				<dl class="product_center_dl02">
					<dt>
						 선명한 영상전송으로<br />
						<span class="product_italic_span">다각적인 원격지원을 실현합니다. </span>
					</dt>
					<dd>
						비디오에디션은 직접 방문 하지 않아도 현장에 있는 것처럼 실시간<br />
						으로 전송되는 선명한 영상과 음성을 상담원과 공유하며, 탑재된 다<br />
						양한 편의 기능을 이용하여 더욱 완벽한 원격지원을 가능하게 합니다.
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료 체험 ></a>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg05">
				<p class="product_gray_p">최고의 원격 지원을 위한 </p>
				<dl class="product_con_dl02">
					<dt>끊김 없는 실시간 비디오 화면 전송</dt>
					<dd>
						비디오 에디션의 뛰어난 화면과 음성 전송 기술력을 바탕으로 실제 <br />
						현장에서 문제를 해결 하는 것처럼 정확하게 진단하고 처리 할 수 <br />
						있습니다.
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>하드웨어 제조회사 서비스센터</li>
					<li>보험회사 및 각종 수리 센터</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg06">
				<p class="product_gray_p">업무시간 단축을 위한  </p>
				<dl class="product_con_dl03">
					<dt>효과적인 기능 제공</dt>
					<dd>
						애니서포트 비디오 애디션은 시간과 장비의 제약 없이 빠르고 정확<br />
						하게 고객이 처한 문제 상황을 진단 할 수 있을 뿐 아니라 메시지, <br />
						그리기 기능 등 효과적인 기능을 탑재하여 고객지원 시 보다 수준 <br />
						높은 서비스로 만족감을 드릴 수 있습니다.
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>School and Government Offices IT engineers </li>
					<li>Enterprise Helpdesk </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">이용 방법</p>
			<div class="product_movie_area">
				<img src="/index/kr/img/product_common_movie_img02.gif" alt="" />
			</div>
			<p class="product_gray_p">효과적인 사용을 위한</p>
			<dl class="product_con_dl02">
				<dt>비디오에디션 이용 방법</dt>
				<dd>
					고객이 전송하고 있는 실시간 영상을 보며 동시에 전문인력의 원<br />
					격지원을 이용할 수 있습니다. 먼저 문제 상황을 접수 하고 내장된 <br />
					카메라를 통해 상담원에게 실시간으로 영상을 전송합니다. 영상을 <br />
					통해 보다 정확하게 판단하고 해결 할 수 있습니다.
					<a href="/index/kr/free/free_form.php" class="blue_link_a02">무료체험 ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							다운로드 <br />
							비디오에디션 이용방법
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">활용 예시</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">가전기기 수리 <br />서비스 센터</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/videoedition_list_img01.gif" alt="Home appliance servise center images" /></p>
							</div>
						</dt>
						<dd>
							실시간 영상전송을 통해 휴대가 불편하거나 
							네<br />트워크가 연결되지 않은 각종 가전 기기의 
							고장<br /> 상황을 출장지원 없이엔지니어가 영상을 
							보며<br />정확하게 해결 하도록 합니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">보험회사 및 각종 <br />재난/사고 해결   </p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/videoedition_list_img02.gif" alt="Insurance company images" /></p>
							</div>
						</dt>
						<dd>
							긴급한 사건 사고 및 재난 발생시 보험 회사 및<br/>
							구조 단체 에서 비디오에디션을 이용하여 즉시<br/> 
							현장 상황을 파악 하고 지원 할 수 있습니다. 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">건설현장 관리 및 <br />시설 관리</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/videoedition_list_img03.gif" alt="Construction engineer and site manager images" /></p>
							</div>
						</dt>
						<dd>
							건설현장 및 시설을 관리하는 현장에서 전문가<br />
							가 직접 들어 가거나 방문하기에 어려움이
							있어<br /> 원격으로 지원이 필요한 경우 실시간으
							로 영상<br />을 보며손쉽게 문제를 해결 할 수 있
							게 합니다.  
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">세부기능</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>실시간 영상 공유</dt>
						<dd>
							실시간 영상 화면 공유로
							고화질의 영<br />상과 음성을 공유 합니다.
							문제 상황을<br /> 정확하게 파악할 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>안내 버튼</dt>
						<dd>
							상담원이 촬영 더 필요한 부분을 고객<br />
							의 단말기 화면에 화살표 표시로 알려<br />
							드립니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>메시지</dt>
						<dd>
							업무 과정에서 전달이 필요한 메시지<br />
							를 고객의 기기로 전송이 가능합니다.
							
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>페인트 기능</dt>
						<dd>
							선과 도형을 직접 화면에 쓰며 시각적으<br />
							로 보다 효과적인 설명을 돕습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>자동 초점 조절  </dt>
						<dd>
							영상 촬영 시 카메라의 초점이 
							어긋난<br /> 경우 자동으로 초점이
							맞춰질 수 있습<br />니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>플래시 전환 </dt>
						<dd>
							상담원이 카메라 플래시를 ON / 
							OFF <br />로 전환 할 수 있습
							니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>영상 통화</dt>
						<dd>
							일반 영상 통화 기능처럼 쉽게 
							통화 연<br />결이 가능합니다.
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>스피커폰 전환 </dt>
						<dd>
							다양한 상황을 고려하여 지원 중 스피커폰으로 전환가능하며 핸즈프리
							통화가 가능 합니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>지원 화면 녹화 </dt>
						<dd>
							지원 화면을 캡쳐 하거나 동영상으로<br />
							녹화 저장 하고 차후 참고 자료로 사용<br />할
							수 있습니다
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>영상 조정 기능</dt>
						<dd>
							영상의 화질과 크기도 필요에 따라 변<br />
							경 할 수 있습니다.또한 영상 공유를<br />
							일시 중단할 수 있습니다.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>