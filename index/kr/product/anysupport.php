<?
$wz['pid']  = "20";
$wz['gtt']  = "제품소개";
$wz['gtt02']  = "Anysupport";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>PC, 각종 디바이스, 실시간 영상 원격지원을 하나로!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>지금 바로 신청 하세요.</dt>
				<dd><a href="/index/kr/free/free_form.php">무료체험시작</a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01_on"><a href="/index/kr/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02"><a href="/index/kr/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03"><a href="/index/kr/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">애니서포트</h3>
			<ul class="sub_a_link_ul01">
				<li><a href="#features" class="arctic_scroll">제품특징</a></li>
				<li><a href="#use" class="arctic_scroll">이용방법</a></li>
				<li><a href="#beneficial" class="arctic_scroll">활용예시</a></li>
			</ul>
			<div class="product_top_div">
				<dl class="product_top_dl01">
					<dt>
						애니서포트는 강력한 보안 시스템을 기반으로 내 PC에서 상대방 PC를  <br />
						바로 원격지원 할 수 있는 서비스 입니다. 애니서포트를 이용하여 
						언제<br /> 어디서든 PC만으로 상대방 PC에 접속하여 바로 문제를 진단하고 해결 <br />
						할 수 있습니다.
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드<br />
											애니서포트 특장점
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											다운로드<br />
											애니서포트 이용방법
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div02">
		<div class="product_content" id="features">
			<p class="underline_tit">제품특징</p>
			<dl class="product_center_dl01">
				<dt>
					효율적이고 합리적인 서비스를 위한 <br />
					<span class="product_italic_span">쉽고 빠른 애니서포트 원격지원 </span>
				</dt>
				<dd>
					고객 PC의 문제점을 빠르게 파악하고 해결 하기 위해 애니서포트 <br />
					원격지원 서비스를 이용해 보세요.엔지니어가 직접 방문할 수 없는 <br />
					경우에도 PC만 있다면 쉬운 접속 방식과 단 몇 초간의 원격 연결만<br />
					으로 어디서든 문제를 해결 할 수 있습니다.
					<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
				</dd>
			</dl>
			<p class="center_p_img"><img src="/index/kr/img/anysupport_con_img02.gif" alt="" /></p>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg">
				<p class="product_gray_p">강력한 보안시스템을 기반으로</p>
				<dl class="product_con_dl02">
					<dt>다양한 전문분야를 위한 원격지원</dt>
					<dd>
						검증된 AES와 SSL 보안 시스템을 기반으로한  애니서포트는 다양한 <br />
						IT 분야 에서 탄력적인 원격지원 서비스를 제공합니다. 좀더 전문화<br />
						되고 진보된 원격 서비스를 경험해 보세요.
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>학교와 정부의 시스템 담당 부서 </li>
					<li> 기업의 Help Desk</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg02">
				<p class="product_gray_p">효과적인 서비스 관리를 위한 </p>
				<dl class="product_con_dl03">
					<dt>혁신적인 통계와 관리시스템</dt>
					<dd>
						애니서포트는 다수의 서비스 내역과 상담원을 효과적으로 <br />
						관리할 수 있는 체계적인 관리자 기능을 제공 합니다. 관리자<br />
						페이지에서 제공하는 상담 유형,기간,시간별 내역 등을 각종<br />
						그래프와 통계를 통해 쉽게 업무 관리에 활용할 수 있습니다.<br /> 
						<a href="/index/kr/free/free_form.php" class="blue_link_a">무료체험 ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>다양한 고객과 부서, 대규모 관리가 필요한 모든 부서 </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">이용방법</p>
			<div class="product_movie_area">
			<iframe width="1040" height="400" src="https://www.youtube.com/embed/8Srh1CliXCU?wmode=opaque" frameborder="0" allowfullscreen></iframe>>
			</div>
			<p class="product_gray_p">효과적인 사용을 위한 </p>
			<dl class="product_con_dl02">
				<dt>애니서포트 이용방법</dt>
				<dd>
					전문적인 원격지원 서비스가 필요한 각 사업장과 개인을 위한 이상적인 원격지원 <br />
					서비스를 제공합니다. 고객이 문제 상황을 접수 후 상담원으로 부터 받은 고유 접<br />
					속 번호를 입력 하면 막힘 없이 바로 원격 지원 서비스를 이용할 수 있습니다.<br />
					<a href="/index/kr/free/free_form.php" class="blue_link_a02">무료체험 ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/kr/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							다운로드<br />
							애니서포트 이용방법
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">활용예시</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">IT 기업, 소프트웨어,<br />시스템 개발 회사 </p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/anysupport_list_img01.gif" alt="Software Service Providers images" /></p>
							</div>
						</dt>
						<dd>
							IT관련 전자 제품 회사, 소프트웨어 개발 
							회사의<br />고객 지원 프로그램으로 활용할 수 있습니다.<br />
							애니서포트의 원격지원을 통해 고객 컴퓨터에<br />
							발생한 문제들을 전문 앤지니어가 효율적인 시<br />
							간과 비용으로 서비스를 제공할 수 있습니다.  
						</dd>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">기업 관공서 등의<br />사내 시스템 지원 센터</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/anysupport_list_img02.gif" alt="IT Organization images" /></p>
							</div>
						</dt>
						<dd>
							각 부서 및 기관에서 발생하는 시스템 관련 
							문제<br />및 네트워크 관련 운영과 처리를 애니서포트 원<br />
							격지원으로 간단하게 관리하고 해결할 수 있습니다.    
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">헬프데스크</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/kr/img/anysupport_list_img03.gif" alt="Help Desk images" /></p>
							</div>
						</dt>
						<dd>
							각종 고객 서비스 센터에서 원격 지원 서비스를<br />
							이용한 다양한 서비스를 실현할 수 있습니다.<br />
							전 세계 어느 곳 이든 적은 비용으로, 만족스런<br />서비스를 제공하세요
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">세부기능</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>화면 공유</dt>
						<dd>
							운영자 와 사용자가 PC 화면을 <br />
							공유 하여 작업 할 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>채팅 기능</dt>
						<dd>
							원격 지원 중에도 채팅을 통해<br />
							고객과 대화가 가능합니다..
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>다양한 OS 지원</dt>
						<dd>
							다양한 OS 지원Windows 는 <br />
							물론 Mac 과Linux 도 지원 하고 <br />
							있습니다..
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>여러 고객에게 동시 지원</dt>
						<dd>
							1 명의 운영자가 동시에 10 명의 <br />
							고객에게 지원 이 가능합니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>파일 송수신 </dt>
						<dd>
							작업 중에 패치나 URL을 보내거나 <br />
							로그 파일 을 수신 할 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>API 서비스 </dt>
						<dd>
							고객이 시스템과 연계시켜 <br />
							이용하실 수 있습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>상담원 초대 기능</dt>
						<dd>
							지원하는 동안 다른 상담원을 초대<br />
							하여 여러 명이 동시에 작업 할 수 <br />
							있습니다
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>지원화면 저장/녹화 </dt>
						<dd>
							현재 화면을 캡쳐, 녹화 저장하여<br />
							추후 지원업무 및 참고자료로<br />
							사용가능 합니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>업무 보고</dt>
						<dd>
							상담원의 작업 횟수 및 지원 내용<br />
							등의 정보를 통계화 하여 볼 수 있<br />
							습니다.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>관리자 전용 페이지</dt>
						<dd>
							상담원 정보를 일괄 관리하고 오류<br />
							및 각종 지원 기록을 그래프등 으로 <br />
							시각화한 관리용 페이지 제공
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>