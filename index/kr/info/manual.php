<?
$wz['pid']  = "40";
$wz['lid']  = "20";
$wz['gtt']  = "고객지원";
$wz['gtt02']  = "메뉴얼";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">메뉴얼</h3>
		<ul class="sub_a_link_ul04">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/kr/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/kr/info/manual.php">메뉴얼</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/kr/info/partners.php">고객사</a></li>
		</ul>

		<ul class="manual_list_ul">
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「AnySupport사용 정보 」
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「Mobile Edition사용 정보」 
						</dd>
					</dl>
				</a>
			</li>
			<li class="last">
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「Video Edition사용정」 
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「AnySupport 사용자 설명」
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「AnySupport빠른 시작」
						</dd>
					</dl>
				</a>
			</li>
			<li class="last">
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							다운로드<br />
							「고객가이드」
						</dd>
					</dl>
				</a>
			</li>
		</ul>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>