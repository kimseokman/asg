<?
$wz['pid']  = "40";
$wz['lid']  = "30";
$wz['gtt']  = "고객지원";
$wz['gtt02']  = "고객사";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">고객사</h3>
		<ul class="sub_a_link_ul05">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/kr/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/kr/info/manual.php">메뉴얼</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/kr/info/partners.php">고객사</a></li>
		</ul>
		<p class="partners_txt_p">
			현재 애니서포트의 원격 서비스는 다양한 분야에서 이용되고 있습니다. 약 2000여개 이상의 기업에서 만족도<br />
			높은 고객 서비스를 실현하고 있으며 여러 종류의 최신 플랫폼에 최적화된 서비스를 제공 함으로서 기업의 CS <br />
			부서는 물론 각종 업무에서 시간과 비용 절약을 위한 최상의 솔루션을 제공하고 있습니다.
		</p>

		<div class="partners_con_dl">
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img01.gif" alt="SAMSUNG" /></dt>
				<dd>
					<p class="partners_right_p">SAMSUNG</p>
					<dl class="partners_right_dl">
						<dt>애니서포트</dt>
						<dd>
							전세계 삼성 컴퓨터, 노트북 사용자의 기술적 문제를 원격제어를 통해 해결<br />
							삼성 계열사  IT지원
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img02.gif" alt="IBM" /></dt>
				<dd>
					<p class="partners_right_p">IBM</p>
					<dl class="partners_right_dl">
						<dt>애니서포트</dt>
						<dd>IBM컴퓨터 , 노트북 사용자의 기술적 문제를 원격제어를 통해 해결</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img03.gif" alt="LG CNS" /></dt>
				<dd>
					<p class="partners_right_p">LG CNS</p>
					<dl class="partners_right_dl">
						<dt>비디오에디션</dt>
						<dd>
							Video &Remote Control 기술을 Telemedicine 서비스에 적용.<br />
							북미 , 캐나다 , 베트남 서비스 
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img04.gif" alt="HYUNDAI" /></dt>
				<dd>
					<p class="partners_right_p">HYUNDAI</p>
					<dl class="partners_right_dl">
						<dt>애니서포트</dt>
						<dd>계열사 IT지원</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img05.gif" alt="Standard Charted" /></dt>
				<dd>
					<p class="partners_right_p">Standard Charted</p>
					<dl class="partners_right_dl">
						<dt>비디오, 리모트 쉐어링 ,컨트롤 </dt>
						<dd>Video & Remote Share &Control 기술을 Self-Service Banking 에 적용</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/kr/img/partner_list_img06.gif" alt="Doosan" /></dt>
				<dd>
					<p class="partners_right_p">Doosan</p>
					<dl class="partners_right_dl">
						<dt>애니서포트</dt>
						<dd>계열사 IT지원</dd>
					</dl>
				</dd>
			</dl>
		</div>

		
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>