<?
$wz['pid']  = "40";
$wz['lid']  = "10";
$wz['gtt']  = "고객지원";
$wz['gtt02']  = "FAQs";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">FAQs</h3>
		<ul class="sub_a_link_ul03">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/kr/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/kr/info/manual.php">메뉴얼</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/kr/info/partners.php">고객사</a></li>
		</ul>
		<div class="faq_con_div">
			<div id='cssmenu'>
				<ul>
					<li class='has-sub faq_orther_bg'><a href='#'><span>일반</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. what  is AnySupport?</span></a>
										<p class="toogle_list_con">
											AnySupport is Web-oriented remote support service that enables support agents to solve their customers' issues online using two-way screen sharing, with controlling mouse and keyboard and additional guide tools.<br /><br />
											AnySupport offers technologies for individual support providers or consultants, and any type of corporate. AnySupport solution package is for organizations needing more administrative and collaborative tools.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. How does AnySupport support session work?</span></a>
										<p class="toogle_list_con">
											At the first connection try for customers to your AnySupport support sessions, they download a small size automatic-installing web plug-in. It provides functionalities for you to view <span class="faq_blue">customers</span> desktop, to control their mouse and keyboard and to do various remote support tasks.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. What is benefit when I use AnySupport compare to other remote support services?</span></a>
										<p class="toogle_list_con">
											AnySupport is offering faster, cost-effective performance with rich statistical report capabilities to both individual support providers and group-based support providers. AnySupport provides to all customers not only essential operative features but also administrative and collaborative features, which are usually limitedly offered to corporate version services of other remote support suppliers.
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'><span>4. What is different between AnySupport remote support service and other remote solution package?</span></a>
										<p class="toogle_list_con">
											AnySupport remote support service is designed for individuals or group-based support providers to easily operate their own servers and manage engineering staffs of their own without difficulties. AnySupport provides product updates continuously at no extra charge and does all the IT work of maintaining the service for you. You also have the advantage of our experienced team with strong security derived from end-to-end encryption.<br /><br />
											To learn more, please review the Product section.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>5. Where do I get more information?</span></a>
										<p class="toogle_list_con">
											To talk to sales agent, email us at <a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. What are the system requirements using AnySupport remote support service?</span></a>
										<p class="toogle_list_con">
											<span class="faq_underline">To start AnySupport remote support service session, you need:</span><br />
											<strong>Windows® 2000, Windows® XP, Server 2003,Server2008 , Server2012, Vista, Windows 7 , Windows 8 , Windows 10 or later;<br />
											Internet Explorer®, Netscape® Navigator 4.0 or later, Mozilla® Firefox® 1.0 or later, Google® ChromeÂ® 4.0 or later, Opera® 10.0 or later, or Safari 1.3 or later<br /></strong>
											The ability to make direct outgoing TCP connections or availability of an HTTP proxy or a SOCKS server
											Minimum of Pentium III with 64 MB of RAM (recommended)
											A stable 56k or better Internet connection (recommended)<br /><br />
											<span class="faq_underline">To join AnySupport remote support service session, your customer needs:</span><br />
											<strong>Windows® 2000, Windows® XP, Server 2003, Server 2008 , Server 2012, Vista, Windows 7, Windows 8, Windows 10 or later, Mac , Linux;<br />
											Internet Explorer®, Netscape® Navigator 4.0 or later, Mozilla® Firefox® 1.0 or later, Google® Chrome® 4.0 or later, Opera® 10.0 or later, or Safari 1.3 or later<br /></strong>
											The ability to make direct outgoing TCP connections or availability of an HTTP proxy or a SOCKS server
											A 28.8kbps or better Internet connection (56k recommended) 
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>7. What operating systems does AnySupport remote support service support?</span></a>
										<p class="toogle_list_con">
											<strong>
												AnySupport remote support service supports Windows® 2000, Windows® XP, Server 2003, Server 2008, Server 2012, Vista, Windows 7, Windows 8, Windows 10 , Linux and Mac OS.
											</strong>
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>8. How do I start using AnySupport remote support service?</span></a>
										<p class="toogle_list_con">
											AnySupport remote support service takes a very few steps to set up and needs no training. Sign up for an account online and get your free trial today. After spending a minute or two for an automatic setup, you can immediately start to run support sessions.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>9. Do my customers need to buy AnySupport remote support service?</span></a>
										<p class="toogle_list_con">
											Absolutely not. Your customers join AnySupport service support sessions as your session participants without additional charges. 
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>10. How do I change my password?</span></a>
										<p class="toogle_list_con">
											First, log onto the AnySupport Web site to change your password. After logging in, open My Account page by clicking on the <span class="faq_blue">’Login’</span> link in upper right corner of the website. There, you can change your password and other account information
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>11. What are the system requirements for using AnySupport Solution Package?</span></a>
										<p class="toogle_list_con">
											<span class="faq_underline">To start an AnySupport Solution Package support session, your agents need:</span><br />
											Same as the system requirements using AnySupport remote support service<br /><br />

											<span class="faq_underline">To join an AnySupport support session, your customer needs:</span><br />
											Same as the system requirements using AnySupport remote support service 
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>12. How long does it take to deploy AnySupport Solution Package?</span></a>
										<p class="toogle_list_con">
											It generally takes less than 2 days to provide all agents with the ability to remotely access and support customer desktops. It does not require IT engineering staff to install, set up or configure software or firewalls.
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'><span>13. What kind of support is available to help me deploy AnySupport Solution Package to my organization?</span></a>
										<p class="toogle_list_con">
											Your assigned manager will be available to help you every step of the way
										</p>
										</li>
									<li class='list-sub'><a href='#'><span>14. How long does it take to train agents to use AnySupport Solution Package?</span></a>
										<p class="toogle_list_con">
											It takes roughly an hour to train agents to use the AnySupport Solution Package interface.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>15. Do our customers need to buy or pre-install AnySupport Solution Package?</span></a>
										<p class="toogle_list_con">
											Absolutely not. Your customers join AnySupport Solution Package support sessions as your clients with no additional charge. 
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>16. What kind of reports can I get?</span></a>
										<p class="toogle_list_con">
											AnySupport reports show actual data including customers and agent feedbacks, login and session activity, available many kinds of statistics. Reports can be managed on a daily, weekly or monthly basis or customized to cover a chosen date range. 
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>17. Can agents share AnySupport seats?</span></a>
										<p class="toogle_list_con">
											It is not recommended. This will severely interfere with your management capability to correctly track individual performance using the AnySupport reporting functionalities.
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub'><a href="#"><span>제품 이용</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. How do I start a support session?</span></a>
										<p class="toogle_list_con">
											Open <a href="http://www.anysupport.net" class="faq_a" target="blank">http://www.anysupport.net</a> website and click <span class="faq_blue">‘Login’</span> on upper right corner or click the icon on your desktop after it is installed. A support session will be initiated automatically on clicking the button. Then talk to your customer or send your customer a link to the session using the email. When you talk to your customer, you just let him know the 6-digit Support Key over the phone and tell them to go to http://822.co.kr and type the Support Key into the input box or click  Number.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. Is there any file-size limitation to transfer files between my customer's computer and my own?</span></a>
										<p class="toogle_list_con">
											There is no size limit on transferring files. Also, AnySupport file transfer has a built-in resume function. Therefore, you can continue on transferring a big file, a large folder or multiple folders when there is an interruption.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. Is there a time limit for a session?</span></a>
										<p class="toogle_list_con">
											There is no limit to the duration of a session and it gives you affordable enough time to resolve the problem of your customer.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>4. Is there a limit to the number of sessions that I can run?</span></a>
										<p class="toogle_list_con">
											Even though you can run as many sessions as you need, you can only run concurrent 10 sessions.
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'>
											<span>5. Does AnySupport remote support service place any files or folders on my customer's computer after a session<br />
											&nbsp;&nbsp;&nbsp;&nbsp;is completed?</span>
										</a>
										<p class="toogle_list_con">
											You can choose one of two options: One option is to leave the installed program files on the <strong>customer’s</strong> computer to expedite session connection for future support requests. The other option is for your customer to delete client-side program files at the end of the support session.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. How many agents can collaborate on a single session?</span></a>
										<p class="toogle_list_con">
											Theoretically there is no limit of collaboration of agents.
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub faq_orther_bg'><a href='#'><span>다양한 기기별 문의</span></a>
						<div class="faq_con_txt_div">
							내용없음
						</div>
					</li>
					<li class='has-sub'><a href='#'><span>구입 및 가격</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. I need a service for multiple users. How can I purchase AnySupport?</span></a>
										<p class="toogle_list_con">
											You can sign up online and request more information using <span class="faq_blue">Contact Us</span> button on the bottom right corner of the website.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. I need a product just for myself to use. How do I purchase AnySupport remote support service?</span></a>
										<p class="toogle_list_con">
											You can sign up online for a free trial, or click <strong>Buy now</strong> menu at homepage when you ready to buy it.
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'>
											<span>3. I am currently an AnySupport remote support service customer. What if I want to implement AnySupport<br />
											&nbsp;&nbsp;&nbsp;&nbsp;Solution Package?</span>
										</a>
										<p class="toogle_list_con">
											Current customers who are interested in AnySupport Solution Package should contact our sales agent by emailing us <a href="mailto:sales@anysupport.net" class="faq_a" target="blank"></a>sales@anysupport.net.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>4. What payment types do you use? Can I submit a purchase order?</span></a>
										<p class="toogle_list_con">
											AnySupport remote support service is sold online. We can only accept payment through credit cards at this time. We accept Visa, MasterCard, Discover and American Express.<br /><br />
											AnySupport will support many payment options including submitting a purchase order. For more information, contact our sales team by emailing us <a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net.</a>
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>5. How can I check my subscription plan?</span></a>
										<p class="toogle_list_con">
											Log in to <a href="www.anysupport.net" class="faq_a" target="blank"></a>www.anysupport.net as a manager and view status on the manager homepage. You can also review after generating reports for various account-related activities.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. How can I cancel my free trial?</span></a>
										<p class="toogle_list_con">
											To cancel the subscription online during your free trial, simply log in as a manager and select Manager Home and click the Cancel after Trial link. Otherwise, at the end of your free trial period you will be automatically subscribed to the AnySupport remote support service with a monthly plan basis.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>7. How can I cancel my paid subscription plan</span></a>
										<p class="toogle_list_con">
											To cancel your AnySupport remote support service, log in as a manager and select Manager Home and click the Cancel after Renewal link. AnySupport remote support service is a pre-paid service and the service will continue until the end of your current paid subscription period. At that time, your plan will be discontinued and will not renew again. If you would prefer to stop service immediately and want to request a refund, please speak to a AnySupport sales associate.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>8. What is the reason that I was charged after I canceled my plan?</span></a>
										<p class="toogle_list_con">
											Definitely we do not charge you after you cancel your plan. It comes from the timing gap of your credit card statement. You may see a charge that occurred just before you canceled. Since AnySupport remote support service is a pre-paid service, your service will continue until the end of your current paid subscription period. At that time, your plan will be discontinued and will not renew again. If you would prefer to stop service immediately and want to request a refund, please speak to a AnySupport sales associate.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>9. What if I don't agree with the amount charged to my credit card?</span></a>
										<p class="toogle_list_con">
											If you do not agree with the amount you have been billed, please speak to a AnySupport sales associate.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>10. What if I purchased the wrong plan?</span></a>
										<p class="toogle_list_con">
											If you purchased a plan by accident or made an unwanted change to your existing plan(s), please speak to a AnySupport sales associate so we can adjust your account to reflect the correct plan and any applicable charges or credits.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>11. How can I review the charges billed to my account?</span></a>
										<p class="toogle_list_con">
											You can check your charges on your manager account homepage.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>12. Who can I contact if I have a billing question or related needs of requesting a refund?</span></a>
										<p class="toogle_list_con">
											Please speak to a AnySupport sales associate.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>13. How can I change the credit card or update the billing information on my account?</span></a>
										<p class="toogle_list_con">
											Log in and select Manager Home and click Manager Account link to review or update your contact, your credit card or other billing information for your account.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>14. What charges apply when I change my plan?</span></a>
										<p class="toogle_list_con">
											LWhen you make a change to your plan, your charge will be automatically adjusted and pro-rated against your current account balance. The cost for your new plan is automatically calculated and you will receive a confirmation email receipt after the changes are completed. If your new plan selection decreases your subscription price, any credit remaining on your account will be automatically applied to your purchase. If your new plan selection increases your subscription price, you will be immediately prompted to enter payment. After submitting payment you will receive a receipt via email. You will also be given instant access to any additional features for the new plan.<br /><br />
											Any credit remaining on your account will be automatically applied to future purchases. Your subscription billing date will change to reflect the date of the change in plan. If you have a credit on your account and would like to request a refund, please speak to a AnySupport sales associate.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>15. Will I get a receipt for my purchase?</span></a>
										<p class="toogle_list_con">
											You will receive a receipt via email for your first AnySupport remote support service subscription purchase.<br /><br />
											Please keep our Customer Support email address at sales@AnySupport.net to ensure you receive receipts and other important communications regarding AnySupport remote support service.
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub faq_orther_bg'><a href='#'><span>보안</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. Is AnySupport remote support service secure?</span></a>
										<p class="toogle_list_con">
											Yes, it provides end-to-end security. You and your customers can trust AnySupport because the data transaction has the same security architecture as online bank and credit card services have. The end-to-end Secure Sockets Layer (SSL) and 128-bit Advanced Encryption Standard (AES) encryption protect the integrity of the data and the privacy of the users.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. Is it OK to work on firewall networks?</span></a>
										<p class="toogle_list_con">
											AnySupport uses general HTTP outbound route to enable screen sharing even in the case of strong firewalls of corporations. Mostly, support agents and their customers can be able to use AnySupport without reconfiguring firewall settings.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. Can you catch a virus from downloading the AnySupport software?</span></a>
										<p class="toogle_list_con">
											No. We continuously monitor our development environment for viruses and malware, and all of our downloadable software is digitally signed to prevent tampering by third parties. The warning message customers might see when they install the software is a default message displayed by their browser whenever they download executable files.
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>4. Is there a time-out when no activity occurs on either my computer or my customer's computer?</span></a>
										<p class="toogle_list_con">
											There is no time-out for live support sessions; live sessions will continue until either the support professional or the customer receiving support terminates the session.
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>