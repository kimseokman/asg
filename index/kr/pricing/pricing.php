<?
$wz['pid']  = "30";
$wz['gtt']  = "";
$wz['gtt02']  = "가격";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080_02">
			<p class="sub_visual_p_02"></p>
			<dl class="pricing_visual_txt">
				<dt>
					합리적인 가격!<br />
					애니서포트를 경험해 보세요!
				</dt>
				<dd><a href="/index/kr/free/free_form.php">15일동안 무료체험 바로 가기</a></dd>
			</dl>
		</div>
		<div class="pricing_link_tab">
			<ul class="pricing_link">
				<li class="pricing_tab01"><a href="#anysuport" class="arctic_scroll">AnySuport1+2</a></li>
				<li class="pricing_tab02"><a href="#allinone" class="arctic_scroll">All in One</a></li>
				<li class="pricing_tab03"><a href="#customizing" class="arctic_scroll">Customizing</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="pricing_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">Pricing</h3>
			
			<div class="pricing_con_div01" id="anysuport">
				<p class="pricing_tit01">애니서포트1+2</p>
				<dl class="pricing_absol_dl">
					<dt>연단위 결제 20% 추가할인</dt>
					<dd>보다 경제적인 가격</dd>
				</dl>
				<div class="pricing_common_left01">
					<p class="pricing_common_img"><img src="/index/kr/img/pricing_con_img01.gif" alt="anysuport1+2 images" /></p>
					<dl class="pricing_con_dl01">
						<dt>1계정 구매 시 무료 2 계정 추가 지급</dt>
						<dd>
							한 개의 애니서포트 ID로 무료로 추가 2개 ID 이용<br />
							가능  
						</dd>
					</dl>
				</div>
				<div class="pricing_common_right01">
					<dl class="pricing_right_sel_dl">
						<dt>
							<div class="select_div01">
								<div class="selectbox">
									<ul>  
										<li><a href="javascript:;" data-path="" id="0" >1Technicians</a></li>
										<li><a href="javascript:;" data-path="" id="1" >2Technicians</a></li>
									</ul>
								</div>
							</div>
						</dt>
						<dd>$ 49/yr</dd>
					</dl>
					<ul class="pricing_checkbox_ul">
						<li><input type="checkbox" name="pricing_checkbox1" id="pricing_check1" class="css-checkbox" /><label for="pricing_check1" class="css-label checkGroup1">월별 결제</label></li>
						<li><input type="checkbox" name="pricing_checkbox1" id="pricing_check2" class="css-checkbox" /><label for="pricing_check2" class="css-label checkGroup1">연 단위 결제</label></li>
					</ul>
					<dl class="pricing_total_dl">
						<dt>Total</dt>
						<dd>3,000/ yr</dd>
					</dl>
				</div>
			</div>

			<div class="pricing_btn_div">
				<ul class="black_btn">
					<li class="black_btn_orther"><a href="/index/kr/free/free_form.php">무료체험</a></li>
					<!--li class="last"><a href="/index/kr/buy/buy_form.php">바로 구매</a></li-->
				</ul>
			</div>

		</div>
	</div>

	<div class="pricing_box_div02">
		<div class="product_content">
			<div class="pricing_con_div02" id="allinone">
				<p class="pricing_tit02">올인원</p>
				<dl class="pricing_absol_dl02">
					<dt>연단위 결제 20% 추가할인</dt>
					<dd>보다 경제적인 가격</dd>
				</dl>
				<div class="pricing_common_left01">
					<p class="pricing_common_img"><img src="/index/kr/img/pricing_con_img02.gif" alt="All in One images" /></p>
					<dl class="pricing_con_dl01">
						<dt>올인원 패키지</dt>
						<dd>
							한 개의계정으로 PC는 물론 모바일 에디션, 비디오<br />
							에디션을 자유롭게 이용할 수 있습니다.  
						</dd>
					</dl>
				</div>
				<div class="pricing_common_right01">
					<dl class="pricing_right_sel_dl">
						<dt>
							<div class="select_div01">
								<div class="selectbox">
									<ul>  
										<li><a href="javascript:;" data-path="" id="0">1Technicians</a></li>
										<li><a href="javascript:;" data-path="" id="1">2Technicians</a></li>
									</ul>
								</div>
							</div>
						</dt>
						<dd>$ 59/yr</dd>
					</dl>
					<ul class="pricing_checkbox_ul">
						<li><input type="checkbox" name="pricing_checkbox2" id="pricing_check3" class="css-checkbox" /><label for="pricing_check3" class="css-label checkGroup1">월별 결제</label></li>
						<li><input type="checkbox" name="pricing_checkbox2" id="pricing_check4" class="css-checkbox" /><label for="pricing_check4" class="css-label checkGroup1">연 단위 결제</label></li>
					</ul>
					<dl class="pricing_total_dl">
						<dt>Total</dt>
						<dd>3,000/ yr</dd>
					</dl>
				</div>
			</div>

			<div class="pricing_btn_div">
				<ul class="black_btn">
					<li class="black_btn_orther"><a href="/index/kr/free/free_form.php">무료체험</a></li>
					<!--li class="last"><a href="/index/kr/buy/buy_form.php">바로 구매</a></li-->
				</ul>
			</div>

		</div>
	</div>

	<div class="pricing_box_div01">
		<div class="product_content">
			
			<div class="pricing_con_div03" id="customizing">
				<p class="pricing_tit03">맞춤형 가격</p>
				<div class="pricing_common_div">
					<p class="pricing_common_img"><img src="/index/kr/img/pricing_con_img03.gif" alt="Ask other options images" /></p>
					<dl class="customizing_dl">
						<dt>합리적이고 다양한 종류의 가격 옵션을 제공합니다. </dt>
						<dd><a href="/index/kr/help/contact_us.php">별도의 가격문의 ></a></dd>
					</dl>
				</div>
			</div>

		</div>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>