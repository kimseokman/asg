<?
  include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/index/_func/function.index_common.php");
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=1220,maximum-scale=1.5,user-scalable=yes">
<title>코이노</title>
<link rel="stylesheet" href="/index/kr/css/base.css" type="text/css" />

<script type="text/javascript" src="/_lib/_inc/jquery/1.11.3/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/_lib/_inc/jquery-cookie/1.4.1/jquery.cookie.js"></script>

<script type="text/javascript" src="/index/kr/js/selectbox02.js"></script>
<script type="text/javascript" src="/index/kr/js/placeholders.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js"></script>
<script type="text/javascript">
  WebFont.load({
     // For google fonts
    google: {
      families: ['Droid Sans', 'Droid Serif']
    },
    // For early access or custom font
    custom: {
        families: ['Nanum Gothic'],
        urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
    } 
  });
</script>

</head>

<body>
<!-- 내용시작 -->
<div id="wrap">
	<div id="header03">
		<div class="header_top03">
			<h1 class="logo03"><a href="/index/kr/index.php"><img src="/index/kr/img/different_logo.gif" alt="코이노 로고" /></a></h1>
		</div>
	</div>
<!-- 내용끝 -->
<div id="container">