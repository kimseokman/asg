<?
include_once("../header02.php");
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			죄송합니다. 이메일 전송에 실패하였습니다.  
			<p class="login_dl_small_span02">다시 시도하기</p>
		</dd>
	</dl>
	<div class="login_con_div02">
		<p class="login_img_p"><img src="/index/kr/img/login_message_img.png" alt="login message" /></p>
		<dl class="login_pw_dl01">
			<dt>이메일 전송이 성공적으로 이루어 지지 않았습니다.</dt>
			<dd>
				죄송합니다.이메일 전송에 실패 하였습니다.<br />
				다시 시도해 주시면 감사하겠습니다
			</dd>
		</dl>
		<p class="login_pw_p01">
			올바른 이메일 주소를 입력 하셨는지 다시 확인해 주세요.
		</p>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>