<?
include_once("../header02.php");
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			비밀번호를 잊으셨나요? 
			<p class="login_dl_small_span">
				가입 시 등록 하셨던 이메일 주소를 입력해 주세요.<br />
				새로운 비밀번호 생성을 위한 페이지 링크를 보내  드립니다. 
			</p>
		</dd>
	</dl>
	<div class="login_con_div02">
		<form name="password_reset_email" method="POST" action="/index/_func/function.forgot_pw_send_mail.php">
			<input type="hidden" name="send_type" value="pw_reset" />
			<p class="login_img_p"><img src="/index/en/img/login_lock_img.png" alt="login lock" /></p>
			<ul class="login_input_ul02">
				<li><input type="text" name="pw_reset_email" id="pw_reset_email" value="" placeholder="이메일 주소를 입력해 주세요. " class="login_input"/></li>
			</ul>
			<p class="pw_blue_p"><!--a href="/index/en/help/contact_us.php">더 많은 도움이 필요하세요?</a--></p>
			<div class="login_btn_area">
				<p class="common_blue_btn send_reset_mail_btn"><a href="#">보내기</a></p>
			</div>
		</form>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>