<?
include_once("../header02.php");
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			이메일이 발송 되었습니다.
			<p class="login_dl_small_span02">메일 수신함을 확인해 주세요.</p>
		</dd>
	</dl>
	<div class="login_con_div02">
		<p class="login_img_p"><img src="/index/kr/img/login_message_img.png" alt="login message" /></p>
		<dl class="login_pw_dl01">
			<dt>이메일 수신함을 확인해 주세요. </dt>
			<dd>
				이메일 내에 링크된 주소를 클릭하시고<br />
				새로운 비밀번호를 생성하세요.
			</dd>
		</dl>
		<p class="login_pw_p01">
			이메일이 도착하기 까지  약 몇 분간 시간이 소요 될 수 있습니다.<br />
			많은 양해 바랍니다.
		</p>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>