<?
include_once("../header02.php");

$remember_me_checked = "";

if($_COOKIE['remember_me_checked'] == 'true'){
	$remember_me_checked = "checked";
	$remember_id = $_COOKIE['remember_id'];
}

if($_SESSION['usertype'] == "ADMIN"){
	$utils_obj->RedirectURL(ADMIN_INDEX);
}else if($_SESSION['usertype'] == "SUPPORTER"){
	$utils_obj->RedirectURL(SUPPORTER_INDEX);
}

?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>nySupport</dt>
		<dd>
			언제 어디서나 애니서포트의<br />
			고품격 서비스를 경험해보세요!
		</dd>
	</dl>
	<div class="login_con_div">
		<form name="FLogin" method="POST" action="/index/_func/function.index_login.php">
			<dl class="login_con_top_dl">
				<dt><img src="/index/kr/img/login_picture.gif" alt="" /></dt>
				<dt>
					<ul class="login_user_select">
					<li>
						<input type="radio" name="user_select" id="user_technician" value="technician" checked />
						<label for="user_technician">Technician</label>
					</li>
					<li class="last">
						<input type="radio" name="user_select" id="user_manager" value="manager" />
						<label for="user_manager">Manager</label>
					</li>
					</ul>
				</dt>
				<dd class="guide_msg">&nbsp;</dd>
			</dl>
			<ul class="login_input_ul">
				<li><input type="text" name="userid" id="id" value="<?echo $remember_id ?>" class="login_input"/></li>
				<li class="last"><input type="password" name="passwd" id="pw" value="" class="login_input"/></li>
			</ul>
			<p class="login_error_p">&nbsp;</p>
			<dl class="login_check_dl">
				<dt class="forgot_password_area">&nbsp;</dt>
				<dd>
					<input type="checkbox" name="" id="remember_me" value="" <?echo $remember_me_checked ?> />
					<label for="remember_me">계정정보 기억 하기</label>
				</dd>
			</dl>
			<div class="login_btn_area">
				<p class="common_red_btn">
					<a href="#" class="sign_in">로그인</a>
				</p>
			</div>
		</fom>	
	</div>
	<dl class="login_bottom_dl">
		<dt>애니서포트는 고객님의 성공적인 비지니스를 지원합니다</dt>
		<dd>
			<ul class="login_bottom_ul">
				<li><img src="/index/kr/img/login_icon01.png" alt="크롬" /></li>
				<li><img src="/index/kr/img/login_icon02.png" alt="익스플로러" /></li>
				<li><img src="/index/kr/img/login_icon03.png" alt="사파리" /></li>
				<li><img src="/index/kr/img/login_icon04.png" alt="오페라" /></li>
				<li class="last"><img src="/index/kr/img/login_icon05.png" alt="파이어폭스" /></li>
			</ul>
		</dd>
	</dl>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>