<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<p class="card_top_p01">고객님의 결제정보를 입력하세요.</p>
		<ul class="buy_top_ul">
			<li class="buy_color01">Plan 세부 사항</li>
			<li class="buy_color02">
				Anysupport 1+2 Monthly plan  10 Technicians
				<p class="buy_absol_btn"><a href="/index/kr/buy/buy_edite.php">Edit</a></p>
			</li>
			<li class="buy_color03 last">
				Start date: Feb-05-2015<br />
				End date: Mar-05-2015
				<p class="buy_top_total">Total : 400.00 USD</p>
			</li>
		</ul>
		<p class="buy_top_line"></p>
		<p class="buy_red_p">오류가 있습니다. 다시 체크해 주세요. </p>
		<dl class="buy_con_dl">
			<dt>카드 사용자이름</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="이름" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="성" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>카드번호</dt>
			<dd>
				<ul class="buy_con_ul02">
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">invailed card number</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>사용 만료 날짜</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0" class="atg">월</a></li>
									<li><a href="javascript:;" data-path="" id="1" class="atg">월</a></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="last">
						<div class="select_differ02_div">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="2" class="atg">년</a></li>
									<li><a href="javascript:;" data-path="" id="3" class="atg">년</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>CVC 코드</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="카드뒷면 세자리 숫자" class="common_input"/></li>
					<li class="last"><img src="/index/kr/img/card_icon.gif" alt="" /></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>카드 청구지 주소</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" placeholder="도시" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="주" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>우편번호</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">invailed card number</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>국적</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="4" class="atg">국가</a></li>
									<li><a href="javascript:;" data-path="" id="5" class="atg">국가</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>연락처</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
	</div>
	<div class="freetrial_content_bottom">
		<p class="right_blue_btn"><a href="#">다음</a></p>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>