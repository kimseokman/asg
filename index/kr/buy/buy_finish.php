<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<dl class="buy_txt_dl">
			<dt>
				감사합니다. <br />
				구매가 완료 되었습니다.
			</dt>
			<dd>애니서포트의 더 나은 원격지원 서비스를 이용해보세요.</dd>
		</dl>
		<dl class="buy_txt_bottom">
			<dt><img src="/index/kr/img/buy_finish.png" alt="finish img" /></dt>
			<dd><a href="/admin/index.php" target="_blank">관리자페이지 바로가기</a><img src="/index/kr/img/buy_arrow.gif" alt="buy arrow" /></dd>
		</dl>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>