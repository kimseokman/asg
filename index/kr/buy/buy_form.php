<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<p class="freetrial_tit">애니서포트 가입을 환영합니다.</p>
	<dl class="freetrial_dl01">
		<dt><img src="/index/kr/img/free_trial_circle_img.png" alt="" /></dt>
		<dd>혹시 이미 애니서포트 회원이십니까?<a href="/index/kr/login/login.php" class="free_link_a">로그인</a></dd>
	</dl>
	<div class="freetrial_content">
		<p class="free_sel_lead"><img src="/index/kr/img/freetrial_myplan.png" alt="my plan" /></p>
		<div class="free_sel_div">
			<dl class="common_select_dl02">
				<dt>
					<div class="select_differ_div01">
						<div class="selectbox_differ">
							<ul>  
								<li><a href="javascript:;" data-path="" id="0">Anysupport1+2</a></li>
								<li><a href="javascript:;" data-path="" id="1">Anysupport1+2</a></li>
							</ul>
						</div>
					</div>
				</dt>
				<dd>
					<div class="select_differ_div01">
						<div class="selectbox_differ">
							<ul>  
								<li><a href="javascript:;" data-path="" id="2">1Technicion</a></li>
								<li><a href="javascript:;" data-path="" id="3">2Technicion</a></li>
							</ul>
						</div>
					</div>
				</dd>
			</dl>
			<ul class="common_radio_ul">
				<li><input type="radio" name="buy_radiobutton1" id="buy_radio1" class="css-radio" /><label for="buy_radio1" class="css-label02 radioGroup1">월 이용 결제 </label></li>
				<li class="last"><input type="radio" name="buy_radiobutton1" id="buy_radio2" class="css-radio" /><label for="buy_radio2" class="css-label02 radioGroup2">연 이용 결제 </label></li>
			</ul>
			<dl class="common_sel_dl02">
				<dt>애니서포트  All in One</dt>
				<dd>월이용 결제 10 technician</dd>
			</dl>
			<dl class="common_sel_total">
				<dt>Total:</dt>
				<dd>$59.00 USD/mon</dd>
			</dl>
		</div>
		<div class="free_input_div">
			<p class="free_input_close_p">오류사항을 다시 확인해 주세요. </p>
			<ul class="free_input_ul01">
				<li><input type="text" name="" id="" value="" placeholder="이름" class="common_input"/></li>
				<li><input type="text" name="" id="" value="" placeholder="성" class="common_input"/></li>
				<li class="circle_check">Hello user!</li>
			</ul>
			<ul class="free_input_ul02">
				<li><input type="text" name="" id="" value="" placeholder="이메일 주소 입력" class="common_input"/></li>
				<li class="circle_check">Ok!</li>
			</ul>
			<p class="free_input_txt">이메일 주소는 고객 아이디로 사용됩니다.</p>
			<ul class="free_input_ul02">
				<li><input type="password" name="" id="" value="" placeholder="비밀번호 입력" class="common_input"/></li>
				<li class="circle_check">Looks nice!</li>
			</ul>
			<p class="free_input_txt">It must include 8-12 digit characters and number</p>
			<ul class="free_input_ul02">
				<li><input type="text" name="" id="" value="" placeholder="비밀번호 재입력" class="common_input_error" disabled/></li>
				<li class="circle_error">Error</li>
			</ul>
			<dl class="free_input_dl">
				<dt><input type="checkbox" name="" id="" value="" /></dt>
				<dd>개인정보 이용에 동의 합니다.</dd>
			</dl>
		</div>
	</div>
	<div class="freetrial_content_bottom">
		<p class="right_blue_btn"><a href="#">다음</a></p>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>