<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<dl class="buy_txt_dl">
			<dt>
				죄송합니다.<br />
				결제가 실패하였습니다.
			</dt>
			<dd>
				결제가 정상적으로 이루어 지지 않았습니다.<br />
				확인 후 다시 이용해 주세요.
			</dd>
		</dl>
		<dl class="buy_txt_bottom">
			<dt>
				<img src="/index/kr/img/buy_error.png" alt="error img" /><br />
				<span class="txt_bottom_span">We appologize you.</span>
			</dt>
			<dd><a href="/index/kr/help/contact_us.php">죄송합니다. 혹시 도움이 필요하십니까?</a><img src="/index/kr/img/buy_arrow.gif" alt="buy arrow" /></dd>
		</dl>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>