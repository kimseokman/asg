<?
$wz['pid']  = "10";
$wz['gtt']  = "";
$wz['gtt02']  = "제품개요";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->
	<div id="content">

		<ul class="overview_list_ul">
			<li>
				<a href="#overview_con01" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/kr/img/overview_top_list_box01.gif" alt="" />
						<p class="overview_box_tit">AnySupport</p>
						<dl class="overview_dl">
							<dt>PC to PC</dt>
							<dd>
								원격으로 고객의 PC화면에 접속하여 빠르게<br />
								문제를 해결 할 수 있습니다.
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/kr/img/overview_top_list_box01_hover.png" alt="" />
					</div>
				</a>
			</li>
			<li>
				<a href="#overview_con02" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/kr/img/overview_top_list_box02.gif" alt="" />
						<p class="overview_box_tit">Mobile Edition</p>
						<dl class="overview_dl">
							<dt>PC to Smart devices</dt>
							<dd>
								다양한 스마트 기기 사용시 발생하는 문제를<br />
								상담원PC를 통한 원격지원으로 해결 합니다.
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/kr/img/overview_top_list_box02_hover.png" alt="" />
					</div>
				</a>
			</li>
			<li class="last">
				<a href="#overview_con03" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/kr/img/overview_top_list_box03.gif" alt="" />
						<p class="overview_box_tit">Video Edition</p>
						<dl class="overview_dl">
							<dt>실시간 비디오 전송</dt>
							<dd>
								비디오 에디션을 통해 전송된 비디오화면을 <br />
								공유하며 손쉽게 문제를 해결 할 수 있습니다.
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/kr/img/overview_top_list_box03_hover.png" alt="" />
					</div>
				</a>
			</li>
		</ul>

		<!-- overview content -->


			<div class="overview_common_div" id="overview_con01">
				<p class="overview_bottom_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/kr/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
				<div class="overview_left_div">
					<img src="/index/kr/img/overview_con_img01.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit01">Easy</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_anysupport_circle01.gif" alt="" /></dt>
									<dd>쉬운 접속</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_anysupport_circle02.gif" alt="" /></dt>
									<dd>간단한 설치</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_anysupport_circle03.gif" alt="" /></dt>
									<dd>쉬운 UI</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							애니서포트는 간단한 설치와 세션 연결로 사용자들의 직관적인<br />
							사용을  가능하게 합니다. 애니서포트로 어디서든 <br />
							쉽게  원격지원 하세요.
						</p>
					</div>
				</div>
				<div class="overview_right_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit01">
							<dt>AnySupport</dt>
							<dd>
								고객의 피씨 화면을 공유하면서 언제 어디서든지 <br />
								PC를 통한 원격 지원을 가능하게 합니다.
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>화면공유 원격지원  </li>
							<li>검증된 보안 기술</li>
							<li>파일 송수신</li>
							<li>다양한 기기와  OS지원</li>
							<li>다중 원격지원 </li>
							<li>자동 재 접속 </li>
							<li>원격지원 녹화 가능 </li>
							<li>상담원 초대</li>
							<li>통계 및 관리자 기능 강화 </li>
						</ul>
						<p class="overview_con_link"><a href="/index/kr/product/anysupport.php">자세히 보기</a></p>
					</div>
				</div>
			</div>

			<div class="overview_common_div" id="overview_con02">
				<p class="overview_top_arrow"><a href="#overview_con01" class="arctic_scroll"><img src="/index/kr/img/overview_con_arrow_top.png" alt="top" /></a></p>
				<p class="overview_bottom_arrow"><a href="#overview_con03" class="arctic_scroll"><img src="/index/kr/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
				<div class="overview_left_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit02">
							<dt>Mobile Edition</dt>
							<dd>
									고객의 스마트 디바이스 화면을 공유하여 각 기기에 <br />
									맞는 원격지원 서비스를 지원합니다. 
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>스마트기기 화면 공유</li>
							<li>각종 디바이스 원격 지원</li>
							<li>안정적 네트워크 접속</li>
							<li>Android／iOS 지원</li>
							<li>파일 송수신 가능 </li>
							<li>강력한 보안</li>
						</ul>
						<p class="overview_con_link"><a href="/index/kr/product/mobile_edition.php">자세히 보기</a></p>
					</div>
				</div>
				<div class="overview_right_div">
					<img src="/index/kr/img/overview_con_img02.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit02">Fast</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_mobileedition_circle01.gif" alt="" /></dt>
									<dd> 빠른 설치</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_mobileedition_circle02.gif" alt="" /></dt>
									<dd>빠른 원격연결</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_mobileedition_circle03.gif" alt="" /></dt>
									<dd>빠른 파일 전송</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							모바일 뿐 아니라 각종 스마트 디바이스<br />
							이용 시 겪게 되는 IT관련 문제를 간단한 APP설치만으로 <br />
							전문 상담원의 PC에서 원격 접속을 통해 손쉽고 빠르게<br />
							문제를 해결할 수 있게 합니다.
						</p>
					</div>
				</div>
			</div>

			<div class="overview_common_div" id="overview_con03">
			<p class="overview_top_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/kr/img/overview_con_arrow_top.png" alt="top" /></a></p>
				<div class="overview_left_div">
					<img src="/index/kr/img/overview_con_img03.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit03">Effective</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_videoedition_circle01.gif" alt="" /></dt>
									<dd>실시간 지원가능</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_videoedition_circle02.gif" alt="" /></dt>
									<dd>다양한 기기와 OS 지원</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/kr/img/overview_videoedition_circle03.gif" alt="" /></dt>
									<dd>부가기능</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							비디오 에디션으로 어디서든지 실시간으로 화면과 음성을 <br />
							공유하고다양한 부가 기능들을 이용하여 시간과 비용 절감은 <br />
							물론 정확한 문제 해결을 지원 할 수 있습니다.     
						</p>
					</div>
				</div>
				<div class="overview_right_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit03">
							<dt>Video Edition</dt>
							<dd>
								실시간 영상을 음성과 함께 공유하면서<br />
								보다 빠르고 정확하게 문제 해결을 돕습니다.
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>내장된 카메라를 이용</li>
							<li>포커스 자동 조절 </li>
							<li>플래쉬ON/OFF</li>
							<li>화면 메세지 전달 </li>
							<li> 통화기능</li>
							<li>원격지원녹화 가능</li>
						</ul>
						<p class="overview_con_link"><a href="/index/kr/product/video_edition.php">자세히 보기</a></p>
					</div>
				</div>

			</div>


		<!-- overview content -->
		<?
		include_once("../allinone.php");
		?>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>