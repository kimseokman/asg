<?
$wz['pid']  = "40";
$wz['lid']  = "30";
$wz['gtt']  = "サポート";
$wz['gtt02']  = "導入事例";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">導入事例</h3>
		<ul class="sub_a_link_ul05">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/jp/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/jp/info/manual.php">マニュアル</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/jp/info/partners.php">導入事例</a></li>
		</ul>
		<p class="partners_txt_p">
			AnySupportをはじめとする弊社の製品は、アジアにて官公庁、金融機関、大手企業様をはじめとする約<br />
			2500社の企業様と10万人の個人ユーザー様にご利用いただいており、官公庁や大手企業へのソリュー<br />
			ション提供・開発事例の実績が多数ございます。
		</p>

		<div class="partners_con_dl">
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img01.gif" alt="SAMSUNG" /></dt>
				<dd>
					<p class="partners_right_p">サムスン電子</p>
					<dl class="partners_right_dl">
						<dt>AnySupport</dt>
						<dd>
							デスクトップパソコンやノートパソコンに関する技術的なお問合せに対するサポ<br />
							ート業務にAnySupportのリモートサポートを導入いただきました。
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img02.gif" alt="IBM" /></dt>
				<dd>
					<p class="partners_right_p">IBM</p>
					<dl class="partners_right_dl">
						<dt>AnySupport</dt>
						<dd>
							IBM製品をご利用のお客様へのカスタマーサポートに、AnySupportのリモートサ<br />
							ポートをご利用いただいております。
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img03.gif" alt="LG CNS" /></dt>
				<dd>
					<p class="partners_right_p">LG CNS</p>
					<dl class="partners_right_dl">
						<dt>Video Edition</dt>
						<dd>
							アメリカ、カナダ、ベトナムで展開中のヘルスケアサービスに、AnySupport Video<br />
							Editionの技術が導入されました。
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img04.gif" alt="HYUNDAI" /></dt>
				<dd>
					<p class="partners_right_p">現代自動車</p>
					<dl class="partners_right_dl">
						<dt>AnySupport</dt>
						<dd>
							子会社へのシステムサポート業務にAnySupportのリモートサポートを導入いただ<br />
							きました。
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img05.gif" alt="Standard Charted" /></dt>
				<dd>
					<p class="partners_right_p">スタンダードチャータード銀行</p>
					<dl class="partners_right_dl">
						<dt>ビデオトーク技術</dt>
						<dd>
							スタンダードチャータード銀行の無人店舗にご訪問のお客様のための高画<br />
							質なビデオトークによる相談ソリューションをご提供いたしました。
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="partners_dl">
				<dt><img src="/index/jp/img/partner_list_img06.gif" alt="Doosan" /></dt>
				<dd>
					<p class="partners_right_p">斗山グループ</p>
					<dl class="partners_right_dl">
						<dt>AnySupport</dt>
						<dd>
							関連会社へのシステムサポート業務にAnySupportのリモートサポートをご利用い<br />
							ただいております。
						</dd>
					</dl>
				</dd>
			</dl>
		</div>

		
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>