<?
$wz['pid']  = "40";
$wz['lid']  = "20";
$wz['gtt']  = "サポート";
$wz['gtt02']  = "マニュアル";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">マニュアル</h3>
		<ul class="sub_a_link_ul04">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/jp/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/jp/info/manual.php">マニュアル</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/jp/info/partners.php">導入事例</a></li>
		</ul>

		<ul class="manual_list_ul">
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「AnySupportのご紹介」
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「Mobile Edition使用説明書」 
						</dd>
					</dl>
				</a>
			</li>
			<li class="last">
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「Video Edition使用説明書」 
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「AnySupport 使用説明書」
						</dd>
					</dl>
				</a>
			</li>
			<li>
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「AnySupportスタートガイド」
						</dd>
					</dl>
				</a>
			</li>
			<li class="last">
				<a href="#">
					<dl class="manual_dl">
						<dt></dt>
						<dd>
							Download<br />
							「管理者サイト使用説明書」
						</dd>
					</dl>
				</a>
			</li>
		</ul>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>