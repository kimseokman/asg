<?
$wz['pid']  = "40";
$wz['lid']  = "10";
$wz['gtt']  = "サポート";
$wz['gtt02']  = "FAQs";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->

	<div class="product_content02">
		<h3 class="sub_common_h3">FAQs</h3>
		<ul class="sub_a_link_ul03">
			<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/jp/info/faq.php">FAQ</a></li>
			<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/jp/info/manual.php">マニュアル</a></li>
			<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/jp/info/partners.php">導入事例</a></li>
		</ul>
		<div class="faq_con_div">
			<div id='cssmenu'>
				<ul>
					<li class='has-sub faq_orther_bg'><a href='#'><span>概要</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. AnySupportとは？</span></a>
										<p class="toogle_list_con">
											AnySupportは自社開発のデータ転送による遠隔制御技術を利用し、インターネットを通してオペレーターがお客様とPC画面を共有し、直接お客様のPCをリモート操作して、トラブル解決や設定などのサービスを提供できるリモートサポートサービスです。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. AnySupportの使い方は？</span></a>
										<p class="toogle_list_con">
											お客様からの電話やメールでのお問合せを受け、オペレーターはAnySupportのプログラムにログインしてお客様用の接続認証コードを発行します。お客様にはリモートサポート接続用のページ(<a href="http://822.co.kr" class="faq_a" target="blank">http://822.co.kr</a>)または専用アプリをダウンロードし、オペレーターがお伝えした接続番号を入力していただくだけで、すぐにリモートサポートが可能です。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. 他社と比較して、AnySupportが優れている点は？</span></a>
										<p class="toogle_list_con">
											AnySupportは高品質なサービスを他社よりもリーズナブルな価格でご提供しています。<br />
											通常リモートサポートは双方のPC間の操作において大きなタイムラグが出やすいものでしたが、自社開発のデータ転送による 遠隔制御技術 により、 AnySupport は他社よりも高速でリアルタイムの動作が可能となりました。また、単にお客様のPCをリモート操作するだけではなく、企業のカスタマーサポート業務にご利用いただけるようにペイント機能やファイル転送機能などの業務に役立つ機能も多数搭載されています。
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'><span>4. AnySupport(SaaS型)と AnySupport(サーバーインストール型)の違いは？</span></a>
										<p class="toogle_list_con">
											SaaS型はインターネットを経由してアプリケーションを利用するSaaS/ASPタイプのプランで、初期費用がかかりません。サーバー等の設置は不要です。ご利用する期間のみ費用が発生し、お支払いは年払いと月払いから選択できます<br />
											また、サーバーインストール型は、アプリケーションを購入してサーバーにインストールするプランとなり、初期費用が発生します。サーバーインストール型の設置の際はサーバーが必要となります。<br /><br />
											詳しくは「製品」のページをご覧ください。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>5. AnySupportをすぐに業務に導入したい場合は？</span></a>
										<p class="toogle_list_con">
											弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. AnySupport(SaaS型)の動作環境は？</span></a>
										<p class="toogle_list_con">
											<span class="faq_underline">リモートサポート オペレーター(サポートをする側)：</span><br />
											<OS><br />
											Windows® 2000、Windows® XP、Server 2003、Server 2008 、Server 2012、Vista、Windows 7、Windows  8、Windows 10および以降のバージョン<br /><br />
											<span class="faq_underline">お客様（サポートを受ける側）：</span><br />
											<OS><br />
											Vista、Windows 7、 Windows 8、 Windows 10 および以降のバージョン、Mac、Linux<br /><br />
											<ブラウザ><br />
											Internet Explorer®、 Netscape® Navigator 4.0および以降のバージョン、 Mozilla® Firefox® 1.0および以降のバージョン、 Google® Chrome® 4.0および以降のバージョン、Opera® 10.0および以降のバージョン、Safari 1.3および以降のバージョン<br /><br />
											<マシンスペック><br />
											Intel® Pentium３®／８００MHz以上<br /><br />
											<接続方式><br />
											TCP／IPによるネットワーク接続環境<br /><br />
											<回線速度><br />６４kbps以上
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>7.  AnySupport(SaaS型)が推奨するOSとブラウザは？</span></a>
										<p class="toogle_list_con">
											<strong>
												Windows® 2000, Windows® XP, Server 2003, Server 2008, Server 2012, Vista, Windows 7, Windows 8, Windows 10, Linux, Mac OS
											</strong>
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>8.  AnySupport(SaaS型)のサービスを利用するには？</span></a>
										<p class="toogle_list_con">
											AnySupportのご利用はシンプルで簡単、面倒なインストール作業や複雑な設定なしで、簡単にリモートサポートを始めることができます。「無料トライアル」をお申込みいただければ、すぐに AnySupportのご利用が可能となります。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>9. AnySupport(SaaS型)利用時に、リモートサポートを受ける側に料金などの負担は？</span></a>
										<p class="toogle_list_con">
											リモートサポートを受ける側に料金が請求されることはありません。また AnySupport プログラムはインストールなしでご利用いただけるため、リモートサポートを受ける側のPCに負荷はかかりません。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>10. パスワードを変更するには？</span></a>
										<p class="toogle_list_con">
											AnySupport のウェブサイト右上の<span class="faq_blue">「Log in」</span>ボタンから管理者ＩＤでログインし、「アカウントサービス 」＞「 ログイン情報」にてパスワードをご変更いただけます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>11. AnySupport(サーバーインストール型)の動作環境は？</span></a>
										<p class="toogle_list_con">
											<span class="faq_underline">リモートサポート オペレーター(サポートをする側)：</span><br />
											動作環境は AnySupport(SaaS型)と同じです。「6. AnySupportの動作環境は？」をご覧ください。<br /><br />

											<span class="faq_underline">お客様（サポートを受ける側）：</span><br />
											動作環境は AnySupport(SaaS型)と同じです。「6. AnySupportの動作環境は？」をご覧ください。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>12. AnySupport(サーバーインストール型)の利用開始までにかかる時間は？</span></a>
										<p class="toogle_list_con">
											２日以内で全オペレーターがリモートサポート可能な状態になります。また、貴社にシステム担当者がいない場合でも、インストール作業やソフトウェア、ファイヤーウォールの設置が可能です。
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'><span>13. AnySupport(サーバーインストール型)の設置の際は？</span></a>
										<p class="toogle_list_con">
											弊社の貴社担当スタッフが設置作業のお手伝いをいたします。
										</p>
										</li>
									<li class='list-sub'><a href='#'><span>14. AnySupport(サーバーインストール型)をオペレーターが使いこなすまでにかかる時間は？</span></a>
										<p class="toogle_list_con">
											約１時間ほどあれば、AnySupportプログラムおよびオペレーター画面を使いこなしていただけます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>15. AnySupport(サーバーインストール型) 利用時に、リモートサポートを受ける側に料金などの負担は？</span></a>
										<p class="toogle_list_con">
											リモートサポートを受ける側に料金が請求されることはありません。また AnySupport プログラムはインストールなしでご利用いただけるため、リモートサポートを受ける側のPCに負荷はかかりません。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>16. 管理者ページにて提供されるサポートデータとは？</span></a>
										<p class="toogle_list_con">
											AnySupportは複数名のオペレーター情報の一括管理やサポート状況などの統計がデータで確認できるサポートデータ満載の管理者ページをご用意しました。ページ内ではオペレーターのサポート作業の回数や時間、またサポート内容などの情報を統計化したデータをご覧いただけるので、後日のサポート業務の見直しなどにご利用いただけます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>17. １ライセンス(=1ID)を複数名の相談員が使うことは可能？</span></a>
										<p class="toogle_list_con">
											１ライセンス(=1ID)では複数オペレーターが同時にログインすることはできません。また、サポートデータは１ライセンス(=1ID) に対して１つしか提供されないため、１ライセンス(=1ID)では複数名のオペレーターの業務状況をそれぞれ把握することはできません。
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub'><a href="#"><span>環境、設定、その他</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. AnySupportでリモートサポートを開始するには？</span></a>
										<p class="toogle_list_con">
											AnySupport のウェブサイト右上の<span class="faq_blue">「Log in」</span>ボタンからオペレーターＩＤでログインし AnySupportプログラムを「実行」、またはAnySupportプログラムをPCにダウンロードしてオペレーターＩＤでログインしてください。<br />
											お客様からの電話やメールでのお問合せを受け、オペレーターはAnySupportのプログラムにログインしてお客様用の接続認証コードを発行します。お客様はリモートサポート接続用のページ(<a href="http://822.co.kr" class="faq_a" target="blank">http://822.co.kr</a>)または専用アプリをダウンロードし、オペレーターがお伝えした接続番号を入力していただくだけで、すぐにリモートサポートが可能です。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. ファイルを転送する場合、転送可能なファイルの大きさに上限はありますか？</span></a>
										<p class="toogle_list_con">
											転送可能なファイルの大きさに上限 はありません。また、AnySupportには レジューム機能があるため、大容量ファイルの転送時に途中で動作が中断されても、再度、転送を続行できます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. リモートサポートを行う場合、サポート時間に制限はありますか？</span></a>
										<p class="toogle_list_con">
											サポート時間に制限はございません。いつでも、何時間でも、ご自由にリモートサポートをご利用いただけます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>4. １ライセンス(=1ID)で同時に何人までリモートサポートが可能ですか？</span></a>
										<p class="toogle_list_con">
											１ライセンス(=1ID)あたり、10人までのお客様(サポートを受ける側)を同時にリモートサポートすることができます。
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'>
											<span>5. リモートサポート後も、次回のためにお客様(サポートを受ける側)のPCに接続用プログラムを設置して<br />
											&nbsp;&nbsp;&nbsp;&nbsp;おくことは可能ですか？</span>
										</a>
										<p class="toogle_list_con">
											リモートサポート接続用のページ(<a href="http://822.co.kr" class="faq_a" target="blank">http://822.co.kr</a>)にて「接続プログラムのダウンロード」を選択していただければ、お客様(サポートを受ける側)のPCに接続用プログラムを設置していただくことができます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. １人のお客様(サポートを受ける側)に対し、同時に何人のオペレーターが共同でサポートできますか？</span></a>
										<p class="toogle_list_con">
											基本的には、上限はありません。複数名のオペレーターが共同で１人のお客様(サポートを受ける側)をサポートすることが可能です。
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub faq_orther_bg'><a href='#'><span>様々なデバイス</span></a>
						<div class="faq_con_txt_div">
							내용없음
						</div>
					</li>
					<li class='has-sub'><a href='#'><span>料金 </span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. AnySupport のウェブサイト上にないプランで契約をする場合は？</span></a>
										<p class="toogle_list_con">
											AnySupport のウェブサイト上「お問合せ」ページ、または 弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. AnySupport のウェブサイト上のプランで契約をする場合は？</span></a>
										<p class="toogle_list_con">
											AnySupport
											のウェブサイト上の「今すぐ購入」ボタンをクリックし、プランをご選択の上、購入手続きをしてください。
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'>
											<span>3. 現在SaaS型 AnySupport()を利用しているのですが、<br />
											&nbsp;&nbsp;&nbsp;&nbsp;AnySupport(サーバーインストール型)に切り替えることはできますか？</span>
										</a>
										<p class="toogle_list_con">
											弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>4. どのような支払い方法がありますか？</span></a>
										<p class="toogle_list_con">
											基本的には、オンラインによるクレジットカード決済を受け付けております。<br />
											(ご利用いただけるクレジットカードの種類： <strong>Visa、MasterCard、Discover、American Express</strong>)<br /><br />
											その他のお支払い方法をご希望の方は 弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>5. 現在 の契約中のプランを確認するには？</span></a>
										<p class="toogle_list_con">
											AnySupport のウェブサイト右上の「Log in」ボタンから管理者ＩＤでログインし、「アカウントサービス」＞「ご契約内容とお支払い情報」にてご確認いただけます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>6. 「無料トライアル」期間中にキャンセルするには？</span></a>
										<p class="toogle_list_con">
											「無料トライアル」期間終了後に自動的に AnySupport月払いプランの購入手続きページに進みますので、そちらのページでキャンセルが可能です。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>7. お支払い済みのサービス契約をキャンセルするには？</span></a>
										<p class="toogle_list_con">
											AnySupport は前払い方式となっており、契約期間が過ぎると自動的に契約が更新され、 ご契約されるサービスの利用料金がお客様のカードから自動的に引き落とされます。<br />
											早急にサービスの解約および払い戻しをご希望の方は、弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>8. 解約したにも関わらず、サービスの利用料金が引き落とされているが？</span></a>
										<p class="toogle_list_con">
											サービスを解約されたにも関わらず、意図的に弊社から利用用金が引き落とされることはありません。 AnySupport <br />
											のご契約日とクレジットカード会社の支払日とのタイミングのずれによるものとも考えられます。再度ご確認の上、ご不明な点がありましたら、弊社の営業担当までご連絡ください。<br />
											(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>9. 現在、契約中のサービス利用料金と実際の引き落とし金額が異なっているが？</span></a>
										<p class="toogle_list_con">
											管理者ページ内の「アカウントサービス」＞「ご契約内容とお支払い情報」にて現在のご契約中のプランを再度ご確認の上、ご不明な点がありましたら、弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>10. 意図していないプランを選択し、購入してしまったら？</span></a>
										<p class="toogle_list_con">
											速やかに弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)<br />
											弊社にてご契約社様のご希望のプランに変更後、必要に応じて払い戻しの手続きを行います。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>11. 引き落とされたサービス利用料金の金額を確認するには？</span></a>
										<p class="toogle_list_con">
											管理者ページ内の「アカウントサービス」＞「ご契約内容とお支払い情報」にて、現在のご契約中のプランおよびサービス利用料金を確認することができます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>12. サービス利用料金の引き落としや払い戻しについての問合せ先は？</span></a>
										<p class="toogle_list_con">
											弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>13. クレジットカード情報やお支払いに関する個人情報を変更する場合は？</span></a>
										<p class="toogle_list_con">
											管理者ページ内の「アカウントサービス」＞「ご契約内容とお支払い情報」にて、クレジットカード情報やお支払いに関する個人情報を変更することができます。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>14. 契約期間中に契約プランを変更した場合は？</span></a>
										<p class="toogle_list_con">
											契約期間中に契約プランを変更すると、お支払い料金が必要に応じて日割りで自動的に計算されます。契約プランを変更すると、変更後のお支払い料金を記載したメールが送信されますのでご確認ください。<br /><br />
											変更後のお支払い料金が変更前のサービス利用料金よりも少なくなる場合、差額分はそのまま次回に繰り越され、次回の自動契約更新時に差額を引いた金額が引き落とされます。払い戻しをご希望の方は、 弊社の営業担当までご連絡ください。(メールアドレス：<a href="mailto:sales@anysupport.net" class="faq_a" target="blank">sales@anysupport.net</a>)<br /><br />
											また、変更後のお支払い料金が変更前のサービス利用料金よりも多くなる場合は、差額のお支払いが必要となります。差額のお支払い完了後、確認メールをお送りしますのでご確認ください。また、その段階で変更したプランのご利用が可能となります。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>15. 領収書の発行は？</span></a>
										<p class="toogle_list_con">
											契約プランのお支払い時にをメールにて領収書をお送りいたします。<br />
											弊社のカスタマーサポート(sales@anysupport.net)からは、領収書をはじめ AnySupportのサービスに関する重要な内容が送信されますので、保管しておいていただきますようお願い申し上げます。
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class='has-sub faq_orther_bg'><a href='#'><span>セキュリティ</span></a>
						<div class="faq_con_txt_div">
							<div id='toogle_list'>
								<ul>
									<li class='list-sub'><a href='#'><span>1. セキュリティ面の心配は？</span></a>
										<p class="toogle_list_con">
											AnySupportは強力なセキュリティのもと、安全なサポート環境をご提供しています。<br />
											すべてのデータは国際標準暗号化技術AESとSSLを利用しており、すべてのデータが暗号化されますので安全です。また、接続時の認証コードは一回だけの使い捨てのため、同じ認証コードを再使用できません。 ウイルスやハッカーによる情報流出や不正な遠隔操作の被害の心配は全くありません。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>2. ルーターやファイアーウォールなどのセキュリティが導入されているが、AnySupportの使用は可能？</span></a>
										<p class="toogle_list_con">
											インターネット環境さえあれば、ほとんどの場合可能です。また、無料体験をご用意しておりますので、事前にテストしていただくこともできます。セキュリティ環境により、そのままで利用できない場合は、社内のネットワーク管理者または弊社へご相談ください。
										</p>
									</li>
									<li class='list-sub'><a href='#'><span>3. AnySupportのプログラムをダウンロードする過程でウイルスに感染する恐れは？</span></a>
										<p class="toogle_list_con">
											ありません。 <br />
											ウイルスやマルウェアに対し、弊社は定期的に監視を行っています。また、第三者による改ざんを防ぐため、ダウンロード可能なソフトウェアはすべてデジタル署名されています。<br /><br />
											AnySupportプログラムをダウンロードまたは実行するとアラートが表示されますが、これはブラウザによってダウンロードを行うとデフォルトで表示されるもので、ウイルスの危険を知らせるアラートではありません。
										</p>
									</li>
									<li class='list-sub'>
										<a href='#'>
											<span>4. リモートサポート中にオペレーターやお客様(サポートを受ける側)のPCで何の作業も行わなかった場合、<br />
											&nbsp;&nbsp;&nbsp;&nbsp;自動的にタイムアウトする？
											</span>
										</a>
										<p class="toogle_list_con">
											リモートサポート中に自動的にタイムアウトすることはありません。リモートサポートはオペレーターもしくはお客様(サポートを受ける側)が「リモートサポートの終了」を選択した時点でのみ終了となります。
										</p>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>