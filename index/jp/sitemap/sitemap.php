<?

include_once("../header.php");
include_once("../black_bar.php");
?>
<!-- 내용시작 -->
<div class="sitemap_div">
	<dl class="sitemap_dl">
		<dt>AnySupport サイトマップ</dt>
		<dd><a href="/index/jp/index.php">AnySupport ホーム <span class="sitemap_dl_span">＞</span></a></dd>
	</dl>
	<ul class="sitemap_ul">
		<li>
			<dl class="sitemap_con_dl">
				<dt>製品概要</dt>
				<dd>
					<ul class="sitemap_ul02">
						<li><a href="/index/jp/overview/overview.php">製品概要</a></li>
					</ul>
				</dd>
			</dl>
		</li>
		<li>
			<dl class="sitemap_con_dl">
				<dt>機能</dt>
				<dd>
					<ul class="sitemap_ul02">
						<li><a href="/index/jp/product/anysupport.php">AnySupport</a></li>
						<li><a href="/index/jp/product/mobile_edition.php">Mobile Edition</a></li>
						<li><a href="/index/jp/product/video_edition.php">Video Edition</a></li>
					</ul>
				</dd>
			</dl>
		</li>
		<li>
			<dl class="sitemap_con_dl">
				<dt>価格</dt>
				<dd>
					<ul class="sitemap_ul02">
						<li><a href="/index/jp/pricing/pricing.php#anysuport">Anysupport 1+2</a></li>
						<li><a href="/index/jp/pricing/pricing.php#allinone">ALL in One</a></li>
						<li><a href="/index/jp/pricing/pricing.php#customizing">オプション</a></li>
					</ul>
				</dd>
			</dl>
		</li>
		<li>
			<dl class="sitemap_con_dl">
				<dt>サポート</dt>
				<dd>
					<ul class="sitemap_ul02">
						<li><a href="/index/jp/info/faq.php">FAQ</a></li>
						<li><a href="/index/jp/info/manual.php">マニュアル </a></li>
						<li><a href="/index/jp/info/partners.php">導入事例</a></li>
					</ul>
				</dd>
			</dl>
		</li>
		<li>
			<dl class="sitemap_con_dl">
				<dt>お問合せ</dt>
				<dd>
					<ul class="sitemap_ul02">
						<li><a href="/index/jp/help/contact_us.php">お問合せ</a></li>
					</ul>
				</dd>
			</dl>
		</li>
	</ul>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>