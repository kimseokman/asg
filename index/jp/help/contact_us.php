<?
$wz['pid']  = "50";
$wz['gtt']  = "お問合せ";
$wz['gtt02']  = "お問合せ";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");

$request_mail_res = "NORMAL"; 
if($_POST['request_mail_res']){
	$request_mail_res = $_POST['request_mail_res'];
	$request_mail_name = $_POST['request_mail_name'];
}
?>
<!-- 내용시작 -->
<dl class="help_top_dl">
	<dt>お問合せフォーム</dt>
	<dd>
		弊社の製品についてのご質問がありましたら<br />
		お気軽にお問い合わせください。
	</dd>
</dl>

<div class="help_content">
	<div class="help_con_cc">
		<form name="contact_form" method="post" action="/index/_func/function.contact_us_send_mail.php">
			<p class="help_con_p">下記の項目をご記入ください。</p>
			<p class="help_common_txt">１．ご連絡先</p>
			<ul class="help_input_ul">
				<li class="help_ul_half"><input type="text" name="contact_first_name" id="" value="" placeholder="姓" class="help_input" /></li>
				<li class="help_ul_half_last"><input type="text" name="contact_last_name" id="" value="" placeholder="名" class="help_input"/></li>
				<li><input type="text" name="contact_phone" id="" value="" placeholder="電話番号" class="help_input"/></li>
				<li><input type="text" name="contact_mail" id="" value="" placeholder="メールアドレス" class="help_input"/></li>
			</ul>
			<p class="help_common_txt">２．お問合せの内容</p>
			<textarea name="contact_msg" id="" class="help_area" placeholder="できるだけ詳しくお問合せの内容をご記入ください。"></textarea>
			<p class="help_common_txt">３．貴社名および部署名</p>
			<ul class="help_input_ul">
				<li><input type="text" name="contact_company" id="" value="" placeholder="貴社名" class="help_input"/></li>
				<li><input type="text" name="contact_department" id="" value="" placeholder="部署名" class="help_input"/></li>
			</ul>
			<p class="common_blue_btn contact_send_btn"><a href="#">送信</a></p>
		</form>	
	</div>
</div>
<?
include_once("../help_quick.php");
?>
<?
if($request_mail_res == "SUCCESS"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>感謝, <? echo $request_mail_name; ?>!</dt>
		<dd>
			担当者にメールが送信されました。
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/jp/help/contact_us.php" class="pop_link_a02">確認</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "SUCCESS")

if($request_mail_res == "FAIL"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>ごめんなさい, <? echo $request_mail_name; ?>!</dt>
		<dd>
			メールの送信に失敗しました。
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/jp/help/contact_us.php" class="pop_link_a02">確認</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "FAIL")
?>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>