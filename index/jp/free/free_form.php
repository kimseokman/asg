<?
include_once("../header03.php");

$free_trial_first_name = "";
$free_trial_last_name = "";
$free_trial_adid = "";
$free_trial_mail = "";
$pw_length_min = PW_MIN_LENGTH;
$pw_length_max = PW_MAX_LENGTH;
$default_code = CODE_PC;
$free_send_mail_res = "NORMAL";
$free_trial_next_btn_str = "送信";

if($_POST['free_trial_mail']){
	$free_trial_mail = $_POST['free_trial_mail'];
}

if($_POST['free_send_mail_res']){
	$free_send_mail_res = $_POST['free_send_mail_res'];
	$modal_name = $_POST['free_send_mail_first_name'] . " " . $_POST['free_send_mail_last_name'];
}

$auth_code_flag = $_POST['auth_code_flag'];
$auth_code = $_POST['auth_code'];

if($auth_code_flag == TRUE){
	$free_trial_first_name = $_POST['first_name'];
	$free_trial_last_name = $_POST['last_name'];
	$free_trial_adid = $_POST['adid'];
	$free_trial_mail = $_POST['mail'];
	$free_trial_next_btn_str = "終わりました";
}

$technician = $utils_obj->GetCodeTechnician($default_code);
$product = $utils_obj->GetCodeProduct($default_code);

?>
<!-- 내용시작 -->
<p class="freetrial_tit">Welcome to AnySupport! </p>
<dl class="freetrial_dl01">
	<dt><img src="/index/jp/img/free_trial_circle_img.png" alt="" /></dt>
	<dd>アカウントをお持ちの方はこちら<a href="/index/jp/login/login.php" class="free_link_a">ログイン</a></dd>
</dl>
<div class="freetrial_content">
	<input type="hidden" class="auth_code_flag" value="<? echo $auth_code_flag; ?>" />
	<input type="hidden" class="auth_code" value="<? echo $auth_code; ?>" />

	<p class="free_sel_lead"><img src="/index/jp/img/freetrial_myplan.png" alt="my plan" /></p>
	<div class="free_sel_div">
		<dl class="common_select_dl">
			<dt>
				<div class="select_differ_div01">
					<div class="selectbox_differ">
						<select name="free_select_product" class="free_select_product">
							<option value="<? echo CODE_PC; ?>" <? echo $utils_obj->PrintSelected($default_code, CODE_PC); ?>><? echo $utils_obj->GetCodeProduct(CODE_PC); ?></option>
							<option value="<? echo CODE_ALL_IN_ONE; ?>" <? echo $utils_obj->PrintSelected($default_code, CODE_ALL_IN_ONE); ?>><? echo $utils_obj->GetCodeProduct(CODE_ALL_IN_ONE); ?></option>
						</select>
					</div>
				</div>
			</dt>
			<dd>
				<div class="select_differ_div01">
					<div class="selectbox_differ ajax_free_technician">
						<select name="free_technician" class="free_technician">
							<option value="<? echo $technician; ?>"><? echo $technician; ?> Technician</option>
						</select>
					</div>
				</div>
			</dd>
		</dl>
		<dl class="common_sel_dl">
			<dt>無料トライアル</dt>
			<dd>
				<span class="ajax_free_product"><? echo $product; ?></span><br />
				<? echo FREE_DAYS; ?> 日 無料!
			</dd>
		</dl>
		<dl class="common_sel_total">
			<dt>合計:</dt>
			<dd>$00.00 円/１ヶ月</dd>
		</dl>
	</div>
	<div class="free_input_div">
		<p class="free_input_p">&nbsp;</p>
		<ul class="free_input_ul02">
			<li><input type="text" name="free_admin_id" id="" value="<? echo $free_trial_adid; ?>" placeholder="管理者 ID" class="common_input" <? echo $utils_obj->PrintDisabled($auth_code_flag); ?> /></li>
			<li class="normal ajax_free_admin_id">&nbsp;</li>
		</ul>
		<ul class="free_input_ul01">
			<li><input type="text" name="free_first_name" id="" value="<? echo $free_trial_first_name; ?>" placeholder="姓" class="common_input" <? echo $utils_obj->PrintDisabled($auth_code_flag); ?> /></li>
			<li><input type="text" name="free_last_name" id="" value="<? echo $free_trial_last_name; ?>" placeholder="名" class="common_input" <? echo $utils_obj->PrintDisabled($auth_code_flag); ?> /></li>
			<li class="normal ajax_free_name">&nbsp;</li>
		</ul>
		<ul class="free_input_ul02">
			<li><input type="text" name="free_mail" id="" value="<? echo $free_trial_mail; ?>" placeholder="メールアドレス" class="common_input"/></li>
			<li class="normal ajax_free_mail">&nbsp;</li>
		</ul>
		<p class="free_input_txt">ご記入のメールアドレスがIDとなります</p>
		<?
		if($auth_code_flag == TRUE){
		?>
		<ul class="free_input_ul02">
			<li><input type="password" name="free_pw" id="" value="" placeholder="パスワード" class="common_input" /></li>
			<li class="normal ajax_free_pw">&nbsp;</li>
		</ul>
		<p class="free_input_txt">パスワードは<? echo $pw_length_min; ?>~<? echo $pw_length_max; ?>ケタの英数字でご記入ください。</p>
		<ul class="free_input_ul02">
			<li><input type="password" name="free_retype_pw" id="" value="" placeholder="パスワードの再入力" class="common_input" /></li>
			<li class="normal ajax_free_retype_pw">&nbsp;</li>
		</ul>
		<dl class="free_input_dl">
			<dt><input type="checkbox" name="free_agree" id="free_agree" value="" /></dt>
			<dd><label for="free_agree">「個人情報の取扱いについて」・「利用規約」に同意の上、お申し込みください。</label></dd>
		</dl>
		<?
		}//end of : if($auth_code_flag == 'TRUE')
		?>
	</div>
</div>
<div class="freetrial_content_bottom">
	<p class="right_blue_btn free_form_next_btn"><a href="#"><? echo $free_trial_next_btn_str; ?></a></p>
</div>
<?
	include_once("../common_quick.php");
?>
<?
if($free_send_mail_res == "SUCCESS"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>ようこそ, <? echo $modal_name; ?>!</dt>
		<dd>
			メールを確認してください。
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/jp/" class="pop_link_a02">確認</a></li>
	</ul>
</div>
<?
}// end of : if($free_send_mail_res == "SUCCESS")

if($free_send_mail_res == "FAIL"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>申し訳ありません, <? echo $modal_name; ?>!</dt>
		<dd>
			メール転送に失敗しました。
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/jp/" class="pop_link_a02">確認</a></li>
	</ul>
</div>
<?
}// end of : if($free_send_mail_res == "FAIL")
?>
<!-- 내용끝 -->
<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>