<?
	include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/index/_func/function.index_common.php");
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=1220,maximum-scale=1.5,user-scalable=yes">
<title>코이노</title>
<link rel="stylesheet" href="/index/jp/css/base.css" type="text/css" />

<script type="text/javascript" src="/_lib/_inc/jquery/1.11.3/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/_lib/_inc/jquery-cookie/1.4.1/jquery.cookie.js"></script>

<script type="text/javascript" src="/index/jp/js/selectbox.js"></script>
<script type="text/javascript" src="/index/jp/js/placeholders.min.js"></script>
<script type="text/javascript" src="/index/jp/js/acodian.js"></script>
<script type="text/javascript" src="/index/jp/js/toogle.js"></script>

<script type="text/javascript">
	var is_fixed = false;
	$( document ).ready( function() {
		var jbOffset = $( '.common_black_bar' ).offset();
		$(window).scroll(function(){
			if ($(window).scrollTop() >= jbOffset.top) {
				is_fixed = true;
			   $('.common_black_bar').addClass('blackFixed');
			}
			else {
				is_fixed = false;
			   $('.common_black_bar').removeClass('blackFixed');
			}
		});

		$(".arctic_scroll").click(function(event){            
                		event.preventDefault();
	                	$('html,body').animate({scrollTop:$(this.hash).offset().top-(is_fixed ? 100:200)}, 500);
	       	});
	});
</script>

</head>

<body>
<!-- 내용시작 -->
<div id="wrap">
	<div id="header">
		<div class="header_top">
			<div class="header_util">
				<dl class="util_right_dl">
					<dt><a href="/index/jp/login/login.php">Log in</a></dt>
					<dd>
						<select onchange="if(this.value) location.href=(this.value);" class="header_select">
							<option value="">Languages</option>
							<?
							while(list($k, $v) = each($lang_list)){
							?>
							<option value="/index/<? echo $k; ?>/index.php" <? echo $utils_obj->PrintSelectedLang($current_site_lang, $k); ?>><? echo $v; ?></option>
							<?
							}// end of : while(list($k, $v) = each($lang_list))
							?>
						</select>
					</dd>
				</dl>
			</div>
			<h1 class="logo"><a href="/index/jp/index.php"><img src="/index/jp/img/logo.gif" alt="코이노 로고" /></a></h1>
			<div id="gnb">
				<ul class="gnb">
					<li <?if($wz['pid']=="10"){?>class="on"<?}?>><a href="/index/jp/overview/overview.php">製品概要</a></li>
					<li <?if($wz['pid']=="20"){?>class="on"<?}?>><a href="/index/jp/product/anysupport.php">機能</a></li>
					<li <?if($wz['pid']=="30"){?>class="on"<?}?>><a href="/index/jp/pricing/pricing.php">価格</a></li>
					<li <?if($wz['pid']=="40"){?>class="on"<?}?>><a href="/index/jp/info/faq.php">サポート</a></li>
					<li <?if($wz['pid']=="50"){?>class="on"<?}?>><a href="/index/jp/help/contact_us.php">お問合せ</a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- 내용끝 -->
<div id="container">