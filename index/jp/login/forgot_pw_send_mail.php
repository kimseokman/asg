<?
include_once("../header02.php");
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			パスワードをお忘れの方
			<p class="login_dl_small_span">
				下記にメールアドレスを入力し、送信ボタンを押してください。<br />
				パスワード再設定用のメールをお送りいたします。
			</p>
		</dd>
	</dl>
	<div class="login_con_div02">
		<form name="password_reset_email" method="POST" action="/index/_func/function.forgot_pw_send_mail.php">
			<input type="hidden" name="send_type" value="pw_reset" />
			<p class="login_img_p"><img src="/index/en/img/login_lock_img.png" alt="login lock" /></p>
			<ul class="login_input_ul02">
				<li><input type="text" name="pw_reset_email" id="pw_reset_email" value="" placeholder="メールアドレスをご記入ください" class="login_input"/></li>
			</ul>
			<p class="pw_blue_p"><!--a href="/index/en/help/contact_us.php">Do you nedd help? We can assist you</a--></p>
			<div class="login_btn_area">
				<p class="common_blue_btn send_reset_mail_btn"><a href="#">送信</a></p>
			</div>
		</form>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>