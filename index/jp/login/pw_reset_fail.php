<?
include_once("../header02.php");
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			パスワード再設定用のメールが送信できませんでした。
			<!-- <p class="login_dl_small_span02">Plese try again.</p> -->
		</dd>
	</dl>
	<div class="login_con_div02">
		<p class="login_img_p"><img src="/index/jp/img/login_message_img.png" alt="login message" /></p>
		<dl class="login_pw_dl01">
			<dt>弊社からのメールが送信できませんでした。</dt>
			<dd>
				恐れ入りますが、メールアドレスをご確認の上、<br />
				もう一度お試しください。
			</dd>
		</dl>
		<p class="login_pw_p01">
			メールが届かない場合は、恐れ入りますがもう一度お試しください。<br />
			また、メールが届くまでに数分かかる場合もございます。
		</p>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer02.php");
?>