<?
$wz['pid']  = "30";
$wz['gtt']  = "";
$wz['gtt02']  = "価格";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080_02">
			<p class="sub_visual_p_02"></p>
			<dl class="pricing_visual_txt">
				<dt>
					リーズナブルな価格で<br />
					トップクラスのリモートサポートを  
				</dt>
				<dd><a href="/index/jp/free/free_form.php">まずは無料トライアルでお試しを！</a></dd>
			</dl>
		</div>
		<div class="pricing_link_tab">
			<ul class="pricing_link">
				<li class="pricing_tab01"><a href="#anysuport" class="arctic_scroll">AnySuport1+2</a></li>
				<li class="pricing_tab02"><a href="#allinone" class="arctic_scroll">All in One</a></li>
				<li class="pricing_tab03"><a href="#customizing" class="arctic_scroll">Customizing</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="pricing_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">価格</h3>
			
			<div class="pricing_con_div01" id="anysuport">
				<p class="pricing_tit01">AnySuport1+2</p>
				<dl class="pricing_absol_dl">
					<dt> 年払いでさらに割引！</dt>
					<!-- <dd>Select Annual Payment</dd> -->
				</dl>
				<div class="pricing_common_left01">
					<p class="pricing_common_img"><img src="/index/jp/img/pricing_con_img01.gif" alt="anysuport1+2 images" /></p>
					<dl class="pricing_con_dl01">
						<dt>AnySupport １ID </dt>
						<dd>
							いまなら１ID購入でさらに２IDをプレゼント。つまり、<br />
							１ID分の価格で３人のオペレーターが同時サポート<br />
							できるのです！
						</dd>
					</dl>
				</div>
				<div class="pricing_common_right01">
					<dl class="pricing_right_sel_dl">
						<dt>
							<div class="select_div01">
								<div class="selectbox">
									<ul>  
										<li><a href="javascript:;" data-path="" id="0" >１ID(オペレーター１人)</a></li>
										<li><a href="javascript:;" data-path="" id="1" >2ID(オペレーター１人)</a></li>
									</ul>
								</div>
							</div>
						</dt>
						<dd>$ 49/yr</dd>
					</dl>
					<ul class="pricing_checkbox_ul">
						<li><input type="checkbox" name="pricing_checkbox1" id="pricing_check1" class="css-checkbox" /><label for="pricing_check1" class="css-label checkGroup1">月払い</label></li>
						<li><input type="checkbox" name="pricing_checkbox1" id="pricing_check2" class="css-checkbox" /><label for="pricing_check2" class="css-label checkGroup1">年払い</label></li>
					</ul>
					<dl class="pricing_total_dl">
						<dt>合計</dt>
						<dd>3,000/ yr</dd>
					</dl>
				</div>
			</div>

			<div class="pricing_btn_div">
				<ul class="black_btn">
					<li class="black_btn_orther"><a href="/index/jp/free/free_form.php">無料トライアル</a></li>
					<!--li class="last"><a href="/index/jp/buy/buy_form.php">製品の購入</a></li-->
				</ul>
			</div>

		</div>
	</div>

	<div class="pricing_box_div02">
		<div class="product_content">
			<div class="pricing_con_div02" id="allinone">
				<p class="pricing_tit02">All in One</p>
				<dl class="pricing_absol_dl02">
					<dt> 年払いでさらに割引！</dt>
					<!-- <dd>Select Annual Payment</dd> -->
				</dl>
				<div class="pricing_common_left01">
					<p class="pricing_common_img"><img src="/index/jp/img/pricing_con_img02.gif" alt="All in One images" /></p>
					<dl class="pricing_con_dl01">
						<dt>オールインワン</dt>
						<dd>
							AnySupportプログラム１つで、すべての製品の<br />
							利用が可能なパッケージです
						</dd>
					</dl>
				</div>
				<div class="pricing_common_right01">
					<dl class="pricing_right_sel_dl">
						<dt>
							<div class="select_div01">
								<div class="selectbox">
									<ul>  
										<li><a href="javascript:;" data-path="" id="0">１ID(オペレーター１人)</a></li>
										<li><a href="javascript:;" data-path="" id="1">2ID(オペレーター１人)</a></li>
									</ul>
								</div>
							</div>
						</dt>
						<dd>$ 59/yr</dd>
					</dl>
					<ul class="pricing_checkbox_ul">
						<li><input type="checkbox" name="pricing_checkbox2" id="pricing_check3" class="css-checkbox" /><label for="pricing_check3" class="css-label checkGroup1">月払い</label></li>
						<li><input type="checkbox" name="pricing_checkbox2" id="pricing_check4" class="css-checkbox" /><label for="pricing_check4" class="css-label checkGroup1">年払い</label></li>
					</ul>
					<dl class="pricing_total_dl">
						<dt>合計</dt>
						<dd>3,000/ yr</dd>
					</dl>
				</div>
			</div>

			<div class="pricing_btn_div">
				<ul class="black_btn">
					<li class="black_btn_orther"><a href="/index/jp/free/free_form.php">無料トライアル</a></li>
					<!--li class="last"><a href="/index/jp/buy/buy_form.php">製品の購入</a></li-->
				</ul>
			</div>

		</div>
	</div>

	<div class="pricing_box_div01">
		<div class="product_content">
			
			<div class="pricing_con_div03" id="customizing">
				<p class="pricing_tit03">オプション</p>
				<div class="pricing_common_div">
					<p class="pricing_common_img"><img src="/index/jp/img/pricing_con_img03.gif" alt="Ask other options images" /></p>
					<dl class="customizing_dl">
						<dt>カスタマイズなどをご希望のお客様は弊社まで</dt>
						<dd><a href="/index/jp/help/contact_us.php">お問合せ ></a></dd>
					</dl>
				</div>
			</div>

		</div>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>