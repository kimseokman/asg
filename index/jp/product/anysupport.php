<?
$wz['pid']  = "20";
$wz['gtt']  = "機能";
$wz['gtt02']  = "Anysupport";

include_once("../header.php");
include_once("../black_bar.php");
?>
<!-- 내용시작 -->
<div class="sub_visual">
	<div class="sub_visual_1080">
		<p class="sub_visual_p"></p>
		<dl class="product_visual_txt01">
			<dt>AnySupport All in one</dt>
			<dd>AnySupport１つですべてのリモートサポートが可能！</dd>
		</dl>
		<dl class="product_visual_txt02">
			<dt>是非、この機会にお試しを！</dt>
			<dd><a href="/index/jp/free/free_form.php">無料トライアル</a></dd>
		</dl>
	</div>
	<div class="product_link_tab">
		<ul class="product_link">
			<li class="product_tab01_on"><a href="/index/jp/product/anysupport.php">AnySupport</a></li>
			<li class="product_tab02"><a href="/index/jp/product/mobile_edition.php">Mobile Edition</a></li>
			<li class="product_tab03"><a href="/index/jp/product/video_edition.php">Video Edition</a></li>
		</ul>
	</div>
</div>
<?
include_once("../location.php");
?>
<div class="product_box_div01">
	<div class="product_content">
		<h3 class="sub_common_h3">AnySupport</h3>
		<ul class="sub_a_link_ul01">
			<li><a href="#features" class="arctic_scroll">製品の特長</a></li>
			<li><a href="#use" class="arctic_scroll">ご利用方法</a></li>
			<li><a href="#beneficial" class="arctic_scroll">活用シーン</a></li>
		</ul>
		<div class="product_top_div">
			<dl class="product_top_dl01">
				<dt>
					AnySupportでは、オペレーターとお客様がインターネ<br />
					ットを通してパソコンやモバイルなどの画面を共有する<br />
					仕組みなので、トラブルや設定変更などのサポートが<br />
					直接その場で解決できるのです。
				</dt>
				<dd>
					<ul class="product_top_ul">
						<li>
							<a href="#" target="_blank">
								<dl class="product_top_dl02">
									<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
									<dd>
										Download<br />
										「AnySupportのご紹介」
									</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="#" target="_blank">
								<dl class="product_top_dl02">
									<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
									<dd>
										Download<br />
										「AnySupport使用説明書」
									</dd>
								</dl>
							</a>
						</li>
					</ul>
				</dd>
			</dl>
		</div>
	</div>
</div>

<div class="product_box_div02">
	<div class="product_content" id="features">
		<p class="underline_tit">製品の特長</p>
		<dl class="product_center_dl01">
			<dt>
				プロフェッショナルな顧客サポートに<br />
				<span class="product_italic_span">簡単＆スピーディーなサポートツール</span>
			</dt>
			<dd>
				お客様のパソコン上のお悩みを迅速にサポートするならば、是非<br />
				AnySpportのリモートサポートを。目の前のパソコンからすぐにお<br />
				客様のパソコンに接続。簡単な操作で効率的なトラブル解決やメン<br />
				テナンスが可能です。
				<a href="/index/jp/free/free_form.php" class="blue_link_a"> 無料トライアル ></a>
			</dd>
		</dl>
		<p class="center_p_img"><img src="/index/jp/img/anysupport_con_img02.gif" alt="" /></p>
	</div>
</div>

<div class="product_box_div01">
	<div class="product_content">
		<div class="support_div_bg">
			<p class="product_gray_p">サポート業務にかかる時間を短縮して効率的に</p>
			<dl class="product_con_dl02">
				<dt>企業の顧客サポートに最適なリモートサポート</dt>
				<dd>
					AES-128bitの公認アルゴリズムとインターネット標準のSSL技術を使用し<br />
					た強力なセキュリティのもと、目の前の端末から効率的にサポート業務を<br />
					行えるので、他の業務に割り当てられる時間も増加し、業務の効率化に<br />
					つながります。
					<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
				</dd>
			</dl>
			<ul class="product_dot_ul">
				<li>公的機関や教育機関のシステム担当部門に</li>
				<li> 企業のヘルプデスクに</li>
			</ul>
		</div>
	</div>
</div>

<div class="product_box_div03">
	<div class="product_content">
		<div class="support_div_bg02">
			<p class="product_gray_p">トップクラスのリモートサポートツール</p>
			<dl class="product_con_dl03">
				<dt>サポートデータ満載の管理者専用ページ</dt>
				<dd>
					AnySupportはオペレーターのためのサポート用画面だけではなく、<br />
					複数名のオペレーターの一括管理や、サポート状況などがデータ<br />
					で把握できる管理者ページをご用意しました。オペレーターのサポ<br />
					ート回数や時間、サポート内容などのデータは、後日のサポート業<br />
					務の見直しなどにご利用いただけます。
					<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
				</dd>
			</dl>
			<ul class="product_dot_ul">
				<li>複数のサポート担当オペレーターを抱える企業様へ</li>
			</ul>
		</div>
	</div>
</div>

<div class="product_box_div01">
	<div class="product_content" id="use">
		<p class="underline_tit">ご利用方法</p>
		<div class="product_movie_area">
			<iframe width="1040" height="400" src="https://www.youtube.com/embed/8Srh1CliXCU?wmode=opaque" frameborder="0" allowfullscreen></iframe>
		</div>
		<p class="product_gray_p">製品チュートリアル</p>
		<dl class="product_con_dl02">
			<dt>AnySupportのご利用方法</dt>
			<dd>
				お客様からお問合せを受け、オペレーターはAnySupportのプログラムにログインしてお<br />
				客様用の接続認証コードを発行。お客様はリモートサポート接続サイトにて接続認証コ<br />
				ードを入力していただくだけで、すぐにリモートサポートが可能です。<br />
				<a href="/index/jp/free/free_form.php" class="blue_link_a02">無料トライアル ></a>
			</dd>
		</dl>
		<div class="product_absol_pdf">
			<a href="#" target="_blank">
				<dl class="absol_pdf">
					<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
					<dd>
						Download<br />
						「AnySupport使用説明書」
					</dd>
				</dl>
			</a>
		</div>
	</div>
</div>

<div class="product_box_div03">
	<div class="product_content" id="beneficial">
		<p class="underline_tit">活用シーン</p>
		<ul class="product_bottom_img_list">
			<li>
				<dl class="img_list_dl">
					<dt>
						<div class="img_list_nor">
							<p class="img_list_tit">製品納入後の<br />アフターケアに</p>
							<div class="img_list_hover_dim"></div>
							<p class="list_nor_img"><img src="/index/jp/img/anysupport_list_img01.gif" alt="Software Service Providers images" /></p>
						</div>
					</dt>
					<dd>
						メンテナンスやアフターケアが欠かせない<br />
						ソフトウェア開発会社やサーバー運用管理<br />
						会社などでは、AnySupportのリモートサ<br />
						ポートが大活躍。低コストで効率的なアフ<br />
						ターケアサービスをご提供いただけます。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="img_list_dl">
					<dt>
						<div class="img_list_nor">
							<p class="img_list_tit">社内のシステム対応や<br />メンテナンスに</p>
							<div class="img_list_hover_dim"></div>
							<p class="list_nor_img"><img src="/index/jp/img/anysupport_list_img02.gif" alt="IT Organization images" /></p>
						</div>
					</dt>
					<dd>
						AnySupportは自分の席を離れずとも、その<br />
						場でシステム対応やメンテナンスなどをリモ<br />
						ートサポートできるので、社内のシステムやサ<br />
						ーバールーム担当者様にとっても 非常に有<br />
						益なツールです。
					</dd>
				</dl>
			</li>
			<li class="last">
				<dl class="img_list_dl">
					<dt>
						<div class="img_list_nor">
							<p class="img_list_tit">ヘルプデスクなど<br />の顧客サポートに</p>
							<div class="img_list_hover_dim"></div>
							<p class="list_nor_img"><img src="/index/jp/img/anysupport_list_img03.gif" alt="Help Desk images" /></p>
						</div>
					</dt>
					<dd>
						Webサービス提供会社や電子機器メーカー<br />
						などでの顧客サポートにAnysupportのリモ<br />
						ートサポートを導入いただくことで、よりスピ<br />
						ーディーで質の高い顧客サポートを実現い<br />
						ただけます。
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>

<div class="product_box_div01">
	<div class="product_content">
		<p class="underline_tit">製品の機能</p>
		<ul class="product_function_ul">
			<li>
				<dl class="product_function_dl">
					<dt>画面の共有</dt>
					<dd>
						オペレーターとお客様間で、互いの<br />
						端末の画面を共有しての作業が可<br />
						能です。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt> チャット機能</dt>
					<dd>
						リモートサポート中も、チャットを通<br />
						して、お客様との会話が可能です。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>多様なOSに対応</dt>
					<dd>
						Windowsはもちろん、MacとLinuxに<br />
						も対応しています 
					</dd>
				</dl>
			</li>
			<li class="last">
				<dl class="product_function_dl">
					<dt>複数顧客への同時サポート</dt>
					<dd>
						１人のオペレーターに対して、同時<br />
						に10名のお客様へのサポートが可<br />
						能です。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>ファイル送受信</dt>
					<dd>
						作業中に修正プログラムやURL<br />
						を送信したり、ログファイルを<br />
						受信したりすることが可能です。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>APIサービス </dt>
					<dd>
						お客様がお使いのシステムと連携<br />
						させて、ご利用いただけます。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>複数オペレーター共同作業</dt>
					<dd>
						サポート中に、他のオペレーターを<br />
						招待し、複数名のオペレーターで同<br />
						時に共同作業ができます。
					</dd>
				</dl>
			</li>
			<li class="last">
				<dl class="product_function_dl">
					<dt>サポート画面の保存・録画 </dt>
					<dd>
						作業中の画面のキャプチャーや、<br />
						作業過程を録画して映像で保存で<br />
						きます。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>業務レポート機能</dt>
					<dd>
						オペレーターのサポート回数や<br />
						時間、サポート内容などを統計<br />
						データでご覧いただけます。
					</dd>
				</dl>
			</li>
			<li>
				<dl class="product_function_dl">
					<dt>管理者専用ページ </dt>
					<dd>
						複数名のオペレーターの一括管理<br />
						や、サポート状況などがで把握でき<br />
						る管理者用の画面を用意しました。
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>
<?
include_once("../allinone.php");
?>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>