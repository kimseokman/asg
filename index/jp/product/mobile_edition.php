<?
$wz['pid']  = "20";
$wz['gtt']  = "機能";
$wz['gtt02']  = "Mobile Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>AnySupport１つですべてのリモートサポートが可能！</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>是非、この機会にお試しを！</dt>
				<dd><a href="/index/jp/free/free_form.php">無料トライアル</a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/jp/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02_on"><a href="/index/jp/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03"><a href="/index/jp/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">Mobile Edition</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">製品の特長</a></li>
				<li><a href="#use" class="arctic_scroll">ご利用方法</a></li>
				<li><a href="#beneficial" class="arctic_scroll">活用シーン</a></li>
			</ul>
			<div class="product_top_div02">
				<dl class="product_top_dl01">
					<dt>
						Mobile Editionでは、お客様のモバイル端末を直接リモ<br />
						ート操作できるので、トラブル対応や設定変更などの<br />
						サポート業務がスピーディー＆スムーズに行えます。
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											「 Mobile Edition使用説明書」
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											「Mobile Edition使用説明書」 
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div02">
		<div class="product_content" id="features">
			<p class="underline_tit">製品の特長</p>
			<dl class="product_center_dl01">
				<dt>
					プロフェッショナルな顧客サポートに<br />
					<span class="product_italic_span">面倒な作業は不要、すぐにリモートサポート</span>
				</dt>
				<dd>
					お客様のモバイル端末上のお悩みを迅速にサポートするならば、<br />
					AnySupport Mobile Editionを。AnySupportプログラム１つでモバイル<br />
					エディションのご利用も可能、お客様は専用アプリをダウンロードする<br />
					だけで簡単にリモートサポートが開始できるのです。
					<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
				</dd>
			</dl>
			<p class="center_p_img"><img src="/index/jp/img/mobileedition_con_img02.gif" alt="" /></p>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg03">
				<p class="product_gray_p">サポート業務にかかる時間を短縮して効率的に</p>
				<dl class="product_con_dl02">
					<dt>多様なデバイスとOSに対応</dt>
					<dd>
						AnySupport Mobile Editionは, モバイルやタブレットPCといったモバイル<br />
						端末用に最適化された形で作られており、現在はAndroid端末のみの対<br />
						ですが、iOS端末への対応も予定されています。
						<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>企業のヘルプデスクに</li>
					<li>公的機関や教育機関のシステム担当部門に</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg04">
				<p class="product_gray_p">トップクラスのリモートサポートツール</p>
				<dl class="product_con_dl03">
					<dt>多様な接続方式に対応</dt>
					<dd>
						AnySupport Mobile Editionでは、Wi-Fi、3G、LTEといった様々なネット<br />
						ワークへの接続方式を通して、リモートサポートを受けることができます。
						<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>多様なネットワーク環境下のお客様へのサポート業務に</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">ご利用方法</p>
			<div class="product_movie_area">
				<iframe width="1040" height="400" src="https://www.youtube.com/embed/kVwqYuaBJ1I?wmode=opaque" frameborder="0" allowfullscreen></iframe>
			</div>
			<p class="product_gray_p">製品チュートリアル</p>
			<dl class="product_con_dl02">
				<dt>AnySupport Mobile Editionのご利用方法</dt>
				<dd>
					お客様からお問合せを受け、オペレーターはAnySupportのプログラムにログインしてお客<br />
					様用の接続認証コードを発行。お客様はAnySupport専用アプリにて接続認証コードを入<br />
					力していただくだけです。
					<a href="/index/jp/free/free_form.php" class="blue_link_a02">無料トライアル ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							Download<br />
							「 Mobile Edition 使用説明書」
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">活用シーン</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">モバイル端末を扱う<br />各種販店様へ</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/mobileedition_list_img01.gif" alt="Mobile device Provider images" /></p>
							</div>
						</dt>
						<dd>
							販売されるモバイル端末にAnySupport Mo-<br />
							bile Editionのアプリケーションをダウンロード<br />
							していただければ、ヘルプデスクへのお問<br />
							合せ時にも、お客様は迷うことなくすぐにリ<br />
							モートサポートをご利用頂けます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">モバイルサービス<br />提供企業様へ</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/mobileedition_list_img02.gif" alt="Mobile Servise Help Desk images" /></p>
							</div>
						</dt>
						<dd>
							AnySupport Mobile Editionは直接お客様の端<br />
							末をリモートサポートできるので、モバイル決<br />
							済サービスやモバイルサイト上でサービスを<br />
							ご提供されている企業様にとって、スピーデ<br />
							ィーで効率的な顧客サポートを実現できる最<br />
							適な顧客サポートツールとなります    
						</dd>
					</dl>
				</li>
				<!-- <li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Groupware , VPN<br />Supporter</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/mobileedition_list_img03.gif" alt="Groupware , VPN Supporter images" /></p>
							</div>
						</dt>
						<dd>
							An remotely view and control your client's<br />
							computers in order to quickly diagnose<br />
							and solve problems.Service engineers can<br />
							up date applications,back up files and fix<br />
							device problems remot-ely in a simple and<br />
							efficient way.
						</dd>
					</dl>
				</li> -->
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">製品の機能</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>モバイル画面の共有</dt>
						<dd>
							お客様の端末の画面を共有し、直<br />
							接オペレーターが作業することがで<br />
							きます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>複数オペレーター共同作業</dt>
						<dd>
							サポート中に、他のオペレーターを<br />
							招待し、複数名のオペレーターで同<br />
							時に共同作業ができます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>多様な接続方式に対応</dt>
						<dd>
							Wi-Fi、3G、LTEといった様々な接続<br />
							方式を通してのリモートサポートが<br />
							可能です。
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>複数顧客への同時サポート</dt>
						<dd>
							１人のオペレーターに対して、同時<br />
							に10名のお客様へのサポートが可<br />
							能です。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>ファイル送受信</dt>
						<dd>
							作業中に修正プログラムやURLを<br />
							送信したり、ログファイルを受信した<br />
							りすることが可能です。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>ペイント機能</dt>
						<dd>
							線や図形を直接、画面に書き込み、<br />
							お客様へ視覚的に説明することが<br />
							できます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>多様なデバイスとOSに対応</dt>
						<dd>
							モバイル、タブレットPCといったモバ<br />
							イル端末用に最適化された形で作<br />
							られています。
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>サポート画面の保存・録画</dt>
						<dd>
							作業中の画面のキャプチャーや、<br />
							作業過程を録画して映像で保存で<br />
							きます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>チャット機能</dt>
						<dd>
							リモートサポート中も、チャットを通し<br />
							て、お客様との会話が可能です。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>便利なオペレーター画面</dt>
						<dd>
							お客様端末の画面上からだけでは<br />
							なく、サポート業務用のクイック画面<br />
							からでも端末情報の確認や設定の<br />
							操作が可能です。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>