<?
$wz['pid']  = "20";
$wz['gtt']  = "機能";
$wz['gtt02']  = "Video Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>AnySupport１つですべてのリモートサポートが可能！</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>是非、この機会にお試しを！</dt>
				<dd><a href="/index/jp/free/free_form.php">無料トライアル</a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/jp/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02"><a href="/index/jp/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03_on"><a href="/index/jp/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">Video Edition</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">製品の特長</a></li>
				<li><a href="#use" class="arctic_scroll">ご利用方法</a></li>
				<li><a href="#beneficial" class="arctic_scroll">活用シーン</a></li>
			</ul>
			<div class="product_top_div03">
				<dl class="product_top_dl01">
					<dt>
						AnySupport Video Editionでは、お客様のモバイル端末<br />
						搭載カメラで現場の状況を撮影し、その映像と音声を<br />
						リアルタイムでオペレーターと共有する仕組みなので、<br />
						ネットに接続していない自動車などのトラブル解決時<br />
						にもリモートサポートが可能なのです。
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											「 Video Edition使用説明書」
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											「Video Edition使用説明書」
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div04">
		<div class="product_content" id="features">
			<div class="videoedition_con_bg">
				<p class="underline_tit">製品の特長</p>
				<dl class="product_center_dl02">
					<dt>
						プロフェッショナルな顧客サポートに<br />
						<span class="product_italic_span">現場の状況を高画質の映像と音声で共有</span>
					</dt>
					<dd>
						AnySupport Video Editionはリアルタイムで現場の映像と音声をオペ<br />
						レーターと共有できるので、音も動きもそのままオペレーターに伝える<br />
						ことができます。電話やネットを通してのサポートが難しい場合でも、<br />
						すぐに解決ができるのです。
						<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg05">
				<p class="product_gray_p">トップクラスのリモートサポートツール</p>
				<dl class="product_con_dl02">
					<dt>ほぼリアルタイムでの画面共有を実現</dt>
					<dd>
						リモートアクセスというと、通常は双方の画面共有において大きなタイムラ<br />
						グが出やすいものですが、 AnySupportは自社開発の高速データ転送技<br />
						術により、他社よりもタイムラグの少ないリアルタイムでの動作を実現し<br />
						ました。AnySupport Video Editionはサポート業務において、オペレータ<br />
						ーにもお客様にもストレスを感じさせません！
						<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>家電メーカーのサービスセンターに </li>
					<li>保険会社や修理センターに</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg06">
				<p class="product_gray_p">サポート業務にかかる時間を短縮して効率的に</p>
				<dl class="product_con_dl03">
					<dt>お客様へのサポート機能も満載</dt>
					<dd>
						AnySupport ビデオエディションは、単にカメラを通してリアルタイムの映<br />
						像を共有するだけではありません。企業のカスタマーサポート業務にご<br />
						利用いただけるように、業務に役立つ機能が数多く搭載されています。
						<a href="/index/jp/free/free_form.php" class="blue_link_a">無料トライアル ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>案内ボタンやメッセージ機能</li>
					<li> ペイント機能　など</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">ご利用方法</p>
			<div class="product_movie_area">
				<img src="/index/jp/img/product_common_movie_img02.gif" alt="" />
			</div>
			<p class="product_gray_p">製品チュートリアル</p>
			<dl class="product_con_dl02">
				<dt>AnySupport Video Editionのご利用方法</dt>
				<dd>
					お客様からお問合せを受け、オペレーターはAnySupportのプログラムにログインしてお客<br />
					様用の接続認証コードを発行。お客様はAnySupport専用アプリにて接続認証コードを入力<br />
					していただくだけです。
					<a href="/index/jp/free/free_form.php" class="blue_link_a02">無料トライアル ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/jp/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							Download<br />
							「 Video Edition 使用説明書」
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">活用シーン</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">家電製品の故障時の<br />サポートに</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/videoedition_list_img01.gif" alt="Home appliance servise center images" /></p>
							</div>
						</dt>
						<dd>
							インターネットにつながっていない洗濯機や<br />
							掃除機の異常な動きや音に関するお問合せ<br />
							も、お客様が現場の映像を撮影することで、<br />
							オペレーターが音も動きもリアルタイム映像<br />
							で確認可能なため、迅速な状況把握および<br />
							サポート作業ができるのです。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">自動車保険や<br />修理の見積もりにも</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/videoedition_list_img02.gif" alt="Insurance company images" /></p>
							</div>
						</dt>
						<dd>
							これまでは出張サポートが必要だった業務も、<br />
							AnySupport Video Editionを通して先に状<br />
							況を把握しておくことで、無駄な出張なしに<br />
							適切な対応が可能なので、より効率的な業<br />
							務が実現できます。
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">複雑な配線・接続に<br />関するお問合せに</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/jp/img/videoedition_list_img03.gif" alt="Construction engineer and site manager images" /></p>
							</div>
						</dt>
						<dd>
							言葉では説明しきれない複雑な配線や接続<br />
							に関するお問合せも、AnySupport Video -<br />
							Editionならば現場の状況をすぐに把握でき<br />
							ます。また、メッセージ機能やペイント機能な<br />
							どでお客様への適切な指示も可能。電子機<br />
							器だけでなく、家具の組み立てサポートなど<br />
							にもご活用いただけます.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">製品の機能</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>リアルタイムでの画面共有</dt>
						<dd>
							リアルタイムでの画面共有を実現。<br />
							現場の状況を高画質の映像と音声 <br />
							を通して把握することができます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>案内ボタン</dt>
						<dd>
							オペレーターからお客様に撮影して<br />
							ほしい部分をお客様の端末上の画<br />
							面上に矢印マークでお伝えします。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>メッセージ</dt>
						<dd>
							業務の過程でお客様にお伝えし<br />
							たいことをお客様の端末上の画<br />
							面上に表示します。
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>ペイント機能</dt>
						<dd>
							線や図形を直接、画面に書き込み、<br />
							お客様へ視覚的に説明することが<br />
							できます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>自動フォーカス調整</dt>
						<dd>
							撮影対象とカメラのピントがずれた<br />
							場合、改めて撮影対象にピントを合<br />
							わせることができます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>フラッシュの切替</dt>
						<dd>
							オペレーターがカメラのフラッシュを<br />
							ON／OFFに切りかえることができ<br />
							ます
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>ビデオ通話</dt>
						<dd>
							オペレーター側の端末のカメラの<br />
							映像をお客様の端末の画面に表<br />
							示させ、ビデオ通話ができます。
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>スピーカーフォンの切替</dt>
						<dd>
							スピーカーフォンにより、お客様に<br />
							ハンズフリーで通話いただけます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>サポート画面の保存・録画 </dt>
						<dd>
							作業中の画面のキャプチャーや、<br />
							作業 過程を録画して映像で保存で<br />
							きます。
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>映像の調整機能</dt>
						<dd>
							映像の画質や大きさを、サポート業<br />
							務に便利なように変更していただく<br />
							ことができます。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>