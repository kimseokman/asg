<?
$wz['pid']  = "10";
$wz['gtt']  = "";
$wz['gtt02']  = "製品概要";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
	<!-- 내용시작 -->
	<div id="content">

		<ul class="overview_list_ul">
			<li>
				<a href="#overview_con01" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/jp/img/overview_top_list_box01.gif" alt="" />
						<p class="overview_box_tit">AnySupport</p>
						<dl class="overview_dl">
							<dt>PC to PC</dt>
							<dd>
								お客様のパソコン画面を共有して トラブル解決<br />
								や設定変更などを直接サポート
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/jp/img/overview_top_list_box01_hover.png" alt="" />
					</div>
				</a>
			</li>
			<li>
				<a href="#overview_con02" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/jp/img/overview_top_list_box02.gif" alt="" />
						<p class="overview_box_tit">Mobile Edition</p>
						<dl class="overview_dl">
							<dt>PC to Smart devices</dt>
							<dd>
								お客様のスマートフォンやタブレットPCなどの <br />
								モバイル端末を直接リモートサポート
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/jp/img/overview_top_list_box02_hover.png" alt="" />
					</div>
				</a>
			</li>
			<li class="last">
				<a href="#overview_con03" class="arctic_scroll">
					<div class="overview_list_div">
						<img src="/index/jp/img/overview_top_list_box03.gif" alt="" />
						<p class="overview_box_tit">Video Edition</p>
						<dl class="overview_dl02">
							<dt>Shareing real-time video</dt>
							<dd>
								お客様のモバイル端末搭載カメラを通して現場<br />
								の映像と音声をリアルタイムで共有
							</dd>
						</dl>
					</div>
					<div class="overview_hover_bg">
						<img src="/index/jp/img/overview_top_list_box03_hover.png" alt="" />
					</div>
				</a>
			</li>
		</ul>

		<!-- overview content -->


			<div class="overview_common_div" id="overview_con01">
				<p class="overview_bottom_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/jp/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
				<div class="overview_left_div">
					<img src="/index/jp/img/overview_con_img01.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit01">Easy</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_anysupport_circle01.gif" alt="" /></dt>
									<dd>Connecting</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_anysupport_circle02.gif" alt="" /></dt>
									<dd>no set up</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_anysupport_circle03.gif" alt="" /></dt>
									<dd>User interfaces</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							AnySupportのご利用はシンプルで簡単。<br />
							面倒なインストール作業や複雑な設定なしで、<br />
							簡単にリモートサポートを始めることができます。<br />
							また、ユーザビリティの高いオペレーター画面など、<br />
							使いやすさを重視したつくりになっています。
						</p>
					</div>
				</div>
				<div class="overview_right_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit01">
							<dt>AnySupport</dt>
							<dd>
								お客様のパソコン画面を共有して<br />
								トラブル解決や設定変更などを直接サポート
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>パソコン画面の共有</li>
							<li>高度なセキュリティ</li>
							<li>ファイルの送受信</li>
							<li>幅広いOSに対応</li>
							<li>複数名への同時サポート</li>
							<li>再起動後も自動再接続</li>
							<li>サポート画面の録画保存</li>
							<li>複数名での共同サポート</li>
							<li>サポート内容の記録・閲覧</li>
						</ul>
						<p class="overview_con_link"><a href="/index/jp/product/anysupport.php">詳細はこちら</a></p>
					</div>
				</div>
			</div>

			<div class="overview_common_div" id="overview_con02">
				<p class="overview_top_arrow"><a href="#overview_con01" class="arctic_scroll"><img src="/index/jp/img/overview_con_arrow_top.png" alt="top" /></a></p>
				<p class="overview_bottom_arrow"><a href="#overview_con03" class="arctic_scroll"><img src="/index/jp/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
				<div class="overview_left_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit02">
							<dt>Mobile Edition</dt>
							<dd>
									    お客様のスマートフォンやタブレットPCなどの<br />
										モバイル端末を直接リモートサポート
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>モバイル画面の共有</li>
							<li>端末のリモート操作</li>
							<li>ネットワークへの安定接続</li>
							<li>Android／iOSに対応</li>
							<li>ファイルの送受信</li>
							<li>高度なセキュリティ</li>
						</ul>
						<p class="overview_con_link"><a href="/index/jp/product/mobile_edition.php">詳細はこちら</a></p>
					</div>
				</div>
				<div class="overview_right_div">
					<img src="/index/jp/img/overview_con_img02.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit02">Fast</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_mobileedition_circle01.gif" alt="" /></dt>
									<dd> To set up</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_mobileedition_circle02.gif" alt="" /></dt>
									<dd>To connect session</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_mobileedition_circle03.gif" alt="" /></dt>
									<dd>Without time gap</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							AnySupportなら、オペレーターとお客様の端末を<br />
							スピーディーに接続、すぐにサポート開始が可能です。<br />
							また、ほぼリアルタイムで画面共有ができるので、<br />
							タイムラグに煩わされることはありません。
						</p>
					</div>
				</div>
			</div>

			<div class="overview_common_div" id="overview_con03">
			<p class="overview_top_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/jp/img/overview_con_arrow_top.png" alt="top" /></a></p>
				<div class="overview_left_div">
					<img src="/index/jp/img/overview_con_img03.png" alt="" class="overview_con_img"/>
					<div class="overview_440">
						<p class="overview_con_tit03">Effective</p>
						<ul class="overview_circle_ul">
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_videoedition_circle01.gif" alt="" /></dt>
									<dd>Work togather</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_videoedition_circle02.gif" alt="" /></dt>
									<dd>Smart devices</dd>
								</dl>
							</li>
							<li>
								<dl class="overview_circle_dl">
									<dt><img src="/index/jp/img/overview_videoedition_circle03.gif" alt="" /></dt>
									<dd>Functions</dd>
								</dl>
							</li>
						</ul>
						<p class="overview_con_p">
							効率性の高いサポート業務を実現するAnySupport。<br />
							お客様と画面を共有することで、より伝わりやすい形での<br />
							サポートが可能です。また、強力なセキュリティのもとで<br />
							端末の環境に最適化されたサービスをご提供しています。  
						</p>
					</div>
				</div>
				<div class="overview_right_div">
					<div class="overview_440">
						<dl class="overview_con_color_tit03">
							<dt>Video Edition</dt>
							<dd>
								お客様のモバイル端末搭載カメラを通して<br />
								現場の映像と音声をリアルタイムで共有
							</dd>
						</dl>
						<ul class="overview_check_ul">
							<li>端末搭載カメラを活用</li>
							<li>自動フォーカス調整</li>
							<li>フラッシュON/OFF</li>
							<li>メッセージ伝達機能</li>
							<li> ビデオ通話機能</li>
							<li>画面の録画保存</li>
						</ul>
						<p class="overview_con_link"><a href="/index/jp/product/video_edition.php">詳細はこちら</a></p>
					</div>
				</div>

			</div>


		<!-- overview content -->
		<?
		include_once("../allinone.php");
		?>
	</div>

	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>