	</div>
	<!--내용시작 -->
		<div id="footer">
			<div class="foot">
				<div class="foot_div01">
					<dl class="foot_tit_dl">
						<!--dt>Follow US</dt>
						<dd>
							<ul class="foot_follow_ul">
								<li><a href="#"><img src="/index/jp/img/foot_line_icon01.gif" alt="페이스북" /></a></li>
								<li><a href="#"><img src="/index/jp/img/foot_line_icon02.gif" alt="트위터" /></a></li>
								<li><a href="#"><img src="/index/jp/img/foot_line_icon03.gif" alt="구글" /></a></li>
								<li><a href="#"><img src="/index/jp/img/foot_line_icon04.gif" alt="" /></a></li>
								<li><a href="#"><img src="/index/jp/img/foot_line_icon05.gif" alt="유튜브" /></a></li>
							</ul>
						</dd-->
					</dl>
				</div>
				<div class="foot_div02">
					<ul class="foot_site_ul">
						<li>ABOUT
							<ul class="foot_sitemap">
								<li><a href="http://koino.net/" target="_blank">会社概要</a></li>
								<!--li><a href="#">お知らせ</a></li>
								<li><a href="#">ブログ</a></li>
								<li><a href="#">プレスリリース</a></li>
								<li><a href="#">採用情報</a></li-->
								<li><a href="/index/jp/sitemap/sitemap.php">サイトマップ</a></li>
							</ul>
						</li>
						<li>
							SERVICES
							<ul class="foot_sitemap">
								<li><a href="/index/jp/product/anysupport.php">ご紹介</a></li>
								<li><a href="/index/jp/product/anysupport.php#features">製品の特長</a></li>
								<li><a href="/index/jp/product/anysupport.php#use">ご利用方法</a></li>
								<li><a href="/index/jp/product/mobile_edition.php">Mobile Edition</a></li>
								<li><a href="/index/jp/product/video_edition.php">Video Edition</a></li>
								<li><a href="/index/jp/help/contact_us.php">お問合せ</a></li>
								<!--li><a href="#">他製品との比較</a></li-->
								<li><a href="/index/jp/pricing/pricing.php">価格</a></li>
								<li><a href="/index/jp/info/faq.php">FAQ</a></li>
							</ul>
						</li>
						<li>
							PARTNERS
							<ul class="foot_sitemap">
								<li><a href="/index/en/overview/overview.php">製品のご紹介</a></li>
								<li><a href="/index/en/product/anysupport.php#beneficial">製品の購入</a></li>
								<li><a href="/index/jp/info/partners.php">導入企業リスト</a></li>
							</ul>
						</li>
						<li class="big_foot_site_ul">
							Term and Policy
							<ul class="foot_sitemap">
								<li><a href="/index/jp/etc/terms setvice.php" target="_blank">利用規約</a></li>
								<li><a href="/index/jp/etc/privacy_policy.php" target="_blank">個人情報保護方針</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="foot_div03">
					<!--ul class="foot_app">
						<li><a href="#"><img src="/index/jp/img/foot_link_app01.gif" alt="App Store" /></a></li>
						<li><a href="#"><img src="/index/jp/img/foot_link_app02.gif" alt="Google play" /></a></li>
					</ul-->
				</div>

				<ul class="foot_under_ul">
					<li>ANYSUPPORT 2015 All rights Reserved</li>
					<!--li><a href="/index/jp/etc/terms setvice.php" target="_blank">Terms of Service</a></li>
					<li><a href="/index/jp/etc/privacy_policy.php" target="_blank">Privacy Policy</a></li-->
				</ul>

			</div>
		</div>
	</div>
	<!-- 내용끝 -->
	<script type="text/javascript" src="/index/_public/js/common.js"></script>
	<?
	$inc_file_name = $utils_obj->GetIndexPart($current_url);
	$link_js = "/index/_public/js/".$inc_file_name.".js";

	$exist_flag = file_exists($_SERVER['DOCUMENT_ROOT'].$link_js);
	if($exist_flag){
	?>
	<script type="text/javascript" src="<? echo $link_js; ?>"></script>
	<?
	}//end of : if($exist_flag)
	?>
</body>
</html>