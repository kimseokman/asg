<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<dl class="buy_txt_dl">
			<dt>
				ご契約ありがとうございました！
			</dt>
			<dd>
				ご入金を確認いたしました。<br />
				すぐにAnysupportをご利用いただけます。
			</dd>
		</dl>
		<dl class="buy_txt_bottom">
			<dt><img src="/index/jp/img/buy_finish.png" alt="finish img" /></dt>
			<dd><a href="/admin/index.php" target="_blank">管理者ページへ</a><img src="/index/jp/img/buy_arrow.gif" alt="buy arrow" /></dd>
		</dl>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>