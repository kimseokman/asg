<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<p class="buy_edit_p">ご契約内容のご変更</p>
		<div class="buy_edit_div">
			<p class="buy_edit_con_p">ご契約内容</p>
			<div class="buy_edit_con">
				<dl class="edit_sel_dl">
					<dt>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0">Anysupport1+2</a></li>
									<li><a href="javascript:;" data-path="" id="1">Anysupport1+2</a></li>
								</ul>
							</div>
						</div>
					</dt>
					<dd>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0">１ID(オペレーター１人)</a></li>
									<li><a href="javascript:;" data-path="" id="1">１ID(オペレーター１人)</a></li>
								</ul>
							</div>
						</div>
					</dd>
				</dl>
				<ul class="common_radio_ul02">
					<li><input type="radio" name="buy_radiobutton1" id="buy_radio1" class="css-radio" /><label for="buy_radio1" class="css-label03 radioGroup1">月払い</label></li>
					<li class="last"><input type="radio" name="buy_radiobutton1" id="buy_radio2" class="css-radio" /><label for="buy_radio2" class="css-label03 radioGroup2">年払い<span class="buy_label_span">20%Addtional Discount</span></label></li>
				</ul>
				<dl class="buy_edit_total">
					<dt>
						オールインワンパック<br />
						10ID(オペレーター10人) 月払い
					</dt>
					<dd>
						合計 : 400.00 円/１ヶ月
					</dd>
				</dl>
			</div>
		</div>
		<ul class="buy_edit_btn">
			<li><a href="/index/jp/buy/buy_finish.php" class="edit_btn01">決定</a></li>
			<li class="last"><a href="/index/jp/buy/buy_card.php" class="edit_btn02">戻る</a></li>
		</ul>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>