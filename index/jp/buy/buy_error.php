<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<dl class="buy_txt_dl">
			<dt>
				入金処理が正しく行われませんでした。
			</dt>
			<dd>
				お支払いにご指定のクレジットカードの認証が得られませんでした。<br />
				お手数ですが、ご指定のクレジットカード情報をご確認の上、<br />
				もう一度お試しください。
			</dd>
		</dl>
		<dl class="buy_txt_bottom">
			<dt>
				<img src="/index/jp/img/buy_error.png" alt="error img" /><br />
				<span class="txt_bottom_span">We appologize you.</span>
			</dt>
			<dd><a href="/index/jp/help/contact_us.php">お問合せページへ</a><img src="/index/jp/img/buy_arrow.gif" alt="buy arrow" /></dd>
		</dl>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>