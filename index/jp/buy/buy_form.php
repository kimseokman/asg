<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<p class="freetrial_tit">Welcome to AnySupport! </p>
	<dl class="freetrial_dl01">
		<dt><img src="/index/jp/img/free_trial_circle_img.png" alt="" /></dt>
		<dd>アカウントをお持ちの方はこちら<a href="/index/jp/login/login.php" class="free_link_a">ログイン</a></dd>
	</dl>
	<div class="freetrial_content">
		<p class="free_sel_lead"><img src="/index/jp/img/freetrial_myplan.png" alt="my plan" /></p>
		<div class="free_sel_div">
			<dl class="common_select_dl02">
				<dt>
					<div class="select_differ_div01">
						<div class="selectbox_differ">
							<ul>  
								<li><a href="javascript:;" data-path="" id="0">Anysupport1+2</a></li>
								<li><a href="javascript:;" data-path="" id="1">Anysupport1+2</a></li>
							</ul>
						</div>
					</div>
				</dt>
				<dd>
					<div class="select_differ_div01">
						<div class="selectbox_differ">
							<ul>  
								<li><a href="javascript:;" data-path="" id="2">１ID(オペレーター１人)</a></li>
								<li><a href="javascript:;" data-path="" id="3">１ID(オペレーター１人)</a></li>
							</ul>
						</div>
					</div>
				</dd>
			</dl>
			<ul class="common_radio_ul">
				<li><input type="radio" name="buy_radiobutton1" id="buy_radio1" class="css-radio" /><label for="buy_radio1" class="css-label02 radioGroup1">月払い</label></li>
				<li class="last"><input type="radio" name="buy_radiobutton1" id="buy_radio2" class="css-radio" /><label for="buy_radio2" class="css-label02 radioGroup2">年払い</label></li>
			</ul>
			<dl class="common_sel_dl02">
				<dt>Anysupport 1+2</dt>
				<dd>Monthly plan 10 technician</dd>
			</dl>
			<dl class="common_sel_total">
				<dt>合計:</dt>
				<dd>$59.00 円/１ヶ月</dd>
			</dl>
		</div>
		<div class="free_input_div">
			<p class="free_input_close_p">Please check the marked red</p>
			<ul class="free_input_ul01">
				<li><input type="text" name="" id="" value="" placeholder="姓" class="common_input"/></li>
				<li><input type="text" name="" id="" value="" placeholder="名" class="common_input"/></li>
				<li class="circle_check">Hello user!</li>
			</ul>
			<ul class="free_input_ul02">
				<li><input type="text" name="" id="" value="" placeholder="メールアドレス" class="common_input"/></li>
				<li class="circle_check">Ok!</li>
			</ul>
			<p class="free_input_txt">Your email become your ID when you log in</p>
			<ul class="free_input_ul02">
				<li><input type="password" name="" id="" value="" placeholder="パスワード" class="common_input"/></li>
				<li class="circle_check">Looks nice!</li>
			</ul>
			<p class="free_input_txt">It must include 8-12 digit characters and number</p>
			<ul class="free_input_ul02">
				<li><input type="text" name="" id="" value="" placeholder=" Confirm Password" class="common_input_error" disabled/></li>
				<li class="circle_error">Error</li>
			</ul>
			<dl class="free_input_dl">
				<dt><input type="checkbox" name="" id="" value="" /></dt>
				<dd>I agree the trems and servise</dd>
			</dl>
		</div>
	</div>
	<div class="freetrial_content_bottom">
		<p class="right_blue_btn"><a href="#">購入</a></p>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>