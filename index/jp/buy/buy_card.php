<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<p class="card_top_p01">お支払いはクレジットカードのみ可能です。</p>
		<ul class="buy_top_ul">
			<li class="buy_color01">Plan detail</li>
			<li class="buy_color02">
				ご契約内容<br />
				○○○○○○○○(나중에)
				<p class="buy_absol_btn"><a href="/index/jp/buy/buy_edite.php">修正</a></p>
			</li>
			<li class="buy_color03 last">
				開始日：2015年２月５日<br />
				終了日：2015年３月５日
				<p class="buy_top_total">合計 : 400.00 円/１ヶ月</p>
			</li>
		</ul>
		<p class="buy_top_line"></p>
		<p class="buy_red_p">ご確認の上、もう一度ご入力ください。</p>
		<dl class="buy_con_dl">
			<dt>カード氏名</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="姓" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="名" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>カード番号</dt>
			<dd>
				<ul class="buy_con_ul02">
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">該当する情報が確認できません。</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>カード有効期限(月/年)</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0" class="atg">月</a></li>
									<li><a href="javascript:;" data-path="" id="1" class="atg">月</a></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="last">
						<div class="select_differ02_div">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="2" class="atg">年</a></li>
									<li><a href="javascript:;" data-path="" id="3" class="atg">年</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>セキュリティコード</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="Last 3 digit" class="common_input"/></li>
					<li class="last"><img src="/index/jp/img/card_icon.gif" alt="" /></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>ご住所</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" placeholder="Town/city" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="State" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>郵便番号</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">もう一度、ご確認ください。</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>国名</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="4" class="atg">Country1</a></li>
									<li><a href="javascript:;" data-path="" id="5" class="atg">Country2</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>電話番号</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
	</div>
	<div class="freetrial_content_bottom">
		<p class="right_blue_btn"><a href="#">購入</a></p>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>