<?
include_once("./header.php");
?>
<!-- 내용시작 -->
<!-- main visual -->
<script type="text/javascript" src="/index/jp/js/jquery.bxslider.js"></script>
<div id="slider"> 
	<div class="slide-wrap">
		<div class="visual_search_con">
			<div class="visual_search">
				<dl class="main_visual_search">
					<dt><input type="text" name="start_free_trial_mail" id="" value="" placeholder="メールアドレス" /></dt>
					<dd><a href="#" class="visual_free_trial_btn">今すぐ無料トライアル</a></dd>
				</dl>
			</div>
		</div>
		<ul class="bxslider">
			<li class="main_visual_li01">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>トップクラスのリモートサポート</dt>
						<dd>
							AnySupportは 高度なセキュリティのもと、リアルタイムでの<br />
							画面共有により、スピーディーで質の高い顧客サポートを<br />
							実現するリモートサポートツールです。
						</dd>
					</dl>
				</div>
			</li>
			<li class="main_visual_li02">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>Anysupport Mobile Edition</dt>
						<dd>
							AnySupport Mobile Editionでは お客様のモバイル端末を<br />
							直接リモート操作できるので、トラブル対応や設定変更などの<br />
							サポート業務がスピーディー＆スムーズに行えます。
						</dd>
					</dl>
				</div>
			</li>
			<li class="main_visual_li03">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>Anysupport Video Edition</dt>
						<dd>
							インターネットに接続していない家電製品や自動車などの<br />
							トラブル解決時にも、スピーディー＆スムーズなリモート<br />
							サポートが可能になりました。
						</dd>
					</dl>
				</div>
			</li>
		</ul>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.bxslider').bxSlider();
		});
	</script>
</div>

<!-- main block bar -->
<div class="common_black_bar">
	<div class="black_bar_1080">
		<dl class="black_dl01">
			<dt>AnySupportでスピーディーかつ質の高い顧客サポートを！</dt>
			<dd>
				<ul class="black_btn">
					<li class="black_btn_orther"><a href="/index/jp/free/free_form.php">無料トライアル</a></li>
					<!--li><a href="/index/jp/buy/buy_form.php">製品の購入</a></li-->
				</ul>
			</dd>
		</dl>
	</div>
</div>
<!-- main block bar -->

<div class="main_underline_div">
	<div id="main_common_content">
		<ul class="main_middle_con">
			<li>
				<div class="main_middle_img">
					<dl class="main_middle_img_dl">
						<dt>
							<img src="/index/jp/img/main_top_img01.png" alt="" />
							<div class="main_middle_hover_div">
								<dl class="middle_label_dl">
									<dt>$45.99</dt>
									<dd>月払い</dd>
								</dl>
								<p class="middle_hover_p"><img src="/index/jp/img/main_top_right_img.png" alt="hot deal" /></p>
							</div>
						</dt>
						<dd class="main_middle_bg01">Anysupport  1+2</dd>
					</dl>
				</div>
				<dl class="main_middle_dl">
					<dt><img src="/index/jp/img/main_top_img03.gif" alt="" /></dt>
					<dd>
						<span class="main_middle_dl_span">１ID購入でさらに２IDをプレゼント</span><br /><br />
						Anysupportなら１ID分の価格で、３IDをご利用い<br />
						ただけます。<a href="/index/jp/pricing/pricing.php" class="main_middle_blue_link">詳細はこちら++</a> 
					</dd>
				</dl>
			</li>
			<li class="last">
				<div class="main_middle_img">
					<dl class="main_middle_img_dl">
						<dt>
							<img src="/index/jp/img/main_top_img02.png" alt="" />
							<div class="main_middle_hover_div">
								<dl class="middle_label_dl">
									<dt>$59.99</dt>
									<dd>月払い</dd>
								</dl>
								<p class="middle_hover_p"><img src="/index/jp/img/main_top_right_img.png" alt="hot deal" /></p>
							</div>
						</dt>
						<dd class="main_middle_bg02"> Anysupport ‘All in one’</dd>
					</dl>
				</div>
				<dl class="main_middle_dl">
					<dt><img src="/index/jp/img/main_top_img04.gif" alt="" /></dt>
					<dd>
						<span class="main_middle_dl_span">プログラム１つで全利用が可能</span><br /><br />
						PCやモバイル、ネットにつながっていない製品も<br />
						すべてリモートサポート可能です。<a href="/index/jp/pricing/pricing.php" class="main_middle_blue_link">詳細はこちら++</a> 
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>

<!-- middle visual -->
<script type="text/javascript" src="/index/jp/js/jquery.bxslider02.js"></script>
<div id="slider02"> 
	<div class="slide-wrap02">
		<ul class="bxslider02">
			<li>
				<div class="middle_slide_div01">
					<p class="middle_slide_icon"><img src="/index/jp/img/main_middle_visual_icon01.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							ストレスフリーな<br />
							サポート業務を！
						</dt>
						<dd>
							Anysupportならお客様の端末上で直接サポー<br />
							ト業務が行えるので、もうお客様にご 足労いた<br />
							だいたり、出張サポートをお待ちいただく必要は<br />
							ありません。
						</dd>
					</dl>
				</div>
				<img src="/index/jp/img/main_middle_visual_01.png" alt="" />
			</li>
			<li>
				<div class="middle_slide_div02">
					<p class="middle_slide_icon"><img src="/index/jp/img/main_middle_visual_icon02.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							モバイル端末にも<br />
							もちろん対応
						</dt>
						<dd>
							Anysupport Mobile Editionは、スマートフォンや<br />
							タブレットPCへのリモートサポートを実現しました。
						</dd>
					</dl>
				</div>
				<img src="/index/jp/img/main_middle_visual_02.png" alt="" />
			</li>
			<li>
				<div class="middle_slide_div03">
					<p class="middle_slide_icon"><img src="/index/jp/img/main_middle_visual_icon03.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							リアルタイムに<br />
							映像を共有
						</dt>
						<dd>
							モバイル端末に搭載されたカメラを利用したサポ<br />
							ートなので、インターネットにつながっていない家<br />
							電製品の故障や自動車などの状態確認もモート<br />
							サポート可能になりました。
						</dd>
					</dl>
				</div>
				<img src="/index/jp/img/main_middle_visual_03.png" alt="" />
			</li>
		</ul>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider02').bxSlider02();
	});
</script>
<!-- middle visual -->

<div class="main_gray_div">
	<div class="main_gray_content">
		<dl class="gray_dl">
			<dt>AnySupportが選ばれる理由</dt>
			<dd>トップクラスのリモートサポートツールを低価格でご提供</dd>
		</dl>
		<ul class="gray_con_ul">
			<li>様々なデバイスに対応</li>
			<li>
				 高い稼動性を誇るサーバー
				<div class="gray_ul_percent">
					99.9%
				</div>
			</li>
			<li>同時に複数名へのサポート可能</li>
			<li>高度なセキュリティ</li>
			<li>スピーディーで高品質<br />
				<span class="normal_gray">
					- 高速接続でサポート開始<br />
					- リアルタイムでの画面共有
				</span>
			</li>
			<li>リーズナブルなご利用価格</li>
		</ul>
		<dl class="gray_icon_dl">
			<dt>
				<ul class="gray_icon_ul">
					<li><img src="/index/jp/img/main_middle_icon01.gif" alt="크롬" /></li>
					<li><img src="/index/jp/img/main_middle_icon02.gif" alt="파이어폭스" /></li>
					<li><img src="/index/jp/img/main_middle_icon03.gif" alt="오페라" /></li>
					<li><img src="/index/jp/img/main_middle_icon04.gif" alt="사파리" /></li>
					<li><img src="/index/jp/img/main_middle_icon05.gif" alt="익스플로러" /></li>
					<li class="phone_icon"><img src="/index/jp/img/main_middle_icon06.gif" alt="안드로이드" /></li>
					<li><img src="/index/jp/img/main_middle_icon07.gif" alt="애플" /></li>
				</ul>
			</dt>
			<dd><a href="/index/jp/product/anysupport.php">詳細はこちら+</a></dd>
		</dl>
	</div>
</div>

<div id="main_common_content">
	<div class="main_bottom_content">
		<p class="main_bottom_p">様々なご利用シーン</p>
		<ul class="main_bottom_ul">
			<li>
				<dl class="main_bottom_dl">
					<dt>ヘルプデスクなどの顧客サポートに</dt>
					<dd>
						webサービス提供会社や電子機器メーカーなどでの顧客サポートにAnysupportのリモートサポート<br />
						を導入いただくことで、よりスピーディーで質の高い顧客サポートを実現いただけます。           
					</dd>
				</dl>
			</li>
			<li>
				<dl class="main_bottom_dl">
					<dt>ソフトウェアやシステム納入後のアフターケアに</dt>
					<dd>
						ソフトウェアやシステム開発会社や、 サーバー・ネットワーク運用管理会社などでも、AnySupport<br />
						のリモートサポートをご利用いただくことで低コストで効率的なアフターケアサービスをご提供い<br />
						ただけます        
					</dd>
				</dl>
			</li>
			<li>
				<dl class="main_bottom_dl">
					<dt>社内のシステム対応やメンテナンスに</dt>
					<dd>
						 Anysupportは自分の席を離れずとも、その場でシステム対応やメンテナンスなどをリモートサポート<br />
						 できるので、社内のシステムやサーバールーム担当者様にとっても非常に有益なツールです。        
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>

<!-- main bottom banner -->
<div class="bottom_banner">
	<ul class="bottom_banner_ul">
		<li><img src="/index/jp/img/main_bottom_banner_img01.gif" alt="SAMSUNG" /></li>
		<li><img src="/index/jp/img/main_bottom_banner_img02.gif" alt="standard Chartered" /></li>
		<li><img src="/index/jp/img/main_bottom_banner_img03.gif" alt="IBM" /></li>
		<li><img src="/index/jp/img/main_bottom_banner_img04.gif" alt="HYUNDAI" /></li>
		<li><img src="/index/jp/img/main_bottom_banner_img05.gif" alt="LOTTECARD" /></li>
		<li class="last"><img src="/index/jp/img/main_bottom_banner_img06.gif" alt="LG CNS" /></li>
	</ul>
</div>
<!-- main bottom banner -->
<!-- 내용끝 -->
<?
include_once("./footer.php");
?>