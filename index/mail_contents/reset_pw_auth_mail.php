<?php
/**
 * reset_pw_auth_mail.php
 * 2015.06.03 | KSM
 */

header('Content-Type: text/html; charset=utf-8');
session_start();

$reset_pw_auth_mail_content_en = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>AnySupport</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.link_a{color:#36a3f0 !important;}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">Hello ".$to_adid.",</p>
	<p class=\"email_p01\">Please click the link below to reset password of AnySupport.</p>
	<p class=\"email_p01\"><a href='".$link_url."' class=\"link_a\" target=\"_blank\">Reset password of AnySupport.</a></p>
	<p class=\"email_p02\">If you have some troubles or questions please <a href='".$contact_url."' class=\"link_a\" target=\"_blank\">contact us. </a></p>
	<dl class=\"email_dl\">
		<dt>Best regards,</dt>
		<dd>Anysupport customer center</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$reset_pw_auth_mail_content_jp = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>AnySupport</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.link_a{color:#36a3f0 !important;}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">".$to_adid."様</p>
	<p class=\"email_p01\">Anysupportをご利用いただきまして、まことにありがとうございます。</p>
	<p class=\"email_p01\">
		パスワード再設定用のメールをお送りいたします。<br />
		メールに記載されたリンクをクリックすると、パスワードの再設定が可能です。
	</p>
	<p class=\"email_p01\"><a href='".$link_url."' class=\"link_a\" target=\"_blank\">AnySupportのパスワードをリセットします。</a></p>
	<p class=\"email_p02\">ご不明な点がありましたら、お気軽にお問い合わせください。<a href='".$contact_url."' class=\"link_a\" target=\"_blank\">お問合せはこちら</a></p>
	<dl class=\"email_dl\">
		<dt>今後ともAnysupportをよろしくお願いいたします。</dt>
		<dd>Anysupportカスタマーセンター</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$reset_pw_auth_mail_content_kr = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>애니서포트</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.login_link_a{color:#36a3f0 !important;}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">안녕하세요, ".$to_adid."님</p>
	<p class=\"email_p01\">아래 링크를 클릭하여 비밀번호를 재 생성 해주세요.</p>
	<p class=\"email_p01\"><a href='".$link_url."' class=\"link_a\" target=\"_blank\">애니서포트 비밀번호 변경</a></p>
	<p class=\"email_p02\">오류가 발생했습니까? <a href='".$contact_url."' class=\"link_a\" target=\"_blank\">고객센터</a>로 문의해 주세요.</p>
	<dl class=\"email_dl\">
		<dt>감사합니다.</dt>
		<dd>Anysupport customer center</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";