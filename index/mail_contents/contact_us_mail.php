<?php
/**
 * contact_us_mail.php
 * 2015.06.03 | KSM
 */

header('Content-Type: text/html; charset=utf-8');
session_start();

$contact_us_mail_content_en = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:120px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">1.Tell us how to contact you.</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Name : </span>
				<span class=\"email_detail_item\">".$customer_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Phone : </span>
				<span class=\"email_detail_item\">".$customer_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Email : </span>
				<span class=\"email_detail_item\">".$customer_mail."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">2.Tell us what should we help you?</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Msg : </span>
				<span class=\"email_detail_item\">".$customer_message."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">3.Tell us about your company.</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Company : </span>
				<span class=\"email_detail_item\">".$customer_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Department : </span>
				<span class=\"email_detail_item\">".$customer_department."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>
		<dl class=\"email_dl\">
			<dt>Best regards,</dt>
			<dd>Anysupport customer center</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$contact_us_mail_content_jp = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:120px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">１．ご連絡先</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 名前 : </span>
				<span class=\"email_detail_item\">".$customer_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 電話番号 : </span>
				<span class=\"email_detail_item\">".$customer_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- メール : </span>
				<span class=\"email_detail_item\">".$customer_mail."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">２．お問合せの内容</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- メッセージ : </span>
				<span class=\"email_detail_item\">".$customer_message."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">３．貴社名および部署名</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 貴社名 : </span>
				<span class=\"email_detail_item\">".$customer_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 部署名 : </span>
				<span class=\"email_detail_item\">".$customer_department."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>
		<dl class=\"email_dl\">
			<dt>今後ともAnysupportをよろしくお願いいたします。</dt>
			<dd>Anysupportカスタマーセンター</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$contact_us_mail_content_kr = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:120px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">1.이메일 및 연락 가능한 정보를 입력해 주세요</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 이름 : </span>
				<span class=\"email_detail_item\">".$customer_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 전화번호 : </span>
				<span class=\"email_detail_item\">".$customer_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 메일 : </span>
				<span class=\"email_detail_item\">".$customer_mail."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">2.어떤 도움이 필요하십니까?</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 메시지 : </span>
				<span class=\"email_detail_item\">".$customer_message."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<p class=\"email_p01\">3.고객님의 회사 정보를 입력해 주세요</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 회사 : </span>
				<span class=\"email_detail_item\">".$customer_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 부서 : </span>
				<span class=\"email_detail_item\">".$customer_department."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>
		<dl class=\"email_dl\">
			<dt>감사합니다.</dt>
			<dd>애니서포트 고객센터</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";