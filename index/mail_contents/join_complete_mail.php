<?php
/**
 * join_complete_mail.php
 * 2015.06.03 | KSM
 */

header('Content-Type: text/html; charset=utf-8');
session_start();

$join_complete_mail_content_en = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>AnySupport</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_detail{display:block;width:100%;}
	.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
	.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.link_a{color:#36a3f0 !important;}

	.clearfix{display:block;clear:both;content:\"\";}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">Hi, ".$user_name.".</p>
	<p class=\"email_p01\">Thank you for joining AnySupport.</p>
	<div class=\"email_p01_content\">
		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- Name : </span>
			<span class=\"email_detail_item\">".$user_name."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- ID : </span>
			<span class=\"email_detail_item\">".$user_id."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- Password : </span>
			<span class=\"email_detail_item\">".$user_password."</span>
		</p>

		<p class=\"clearfix\"></p>
	</div>
	<p class=\"email_p02\">If you have some troubles or questions please <a href='".$contact_url."' class=\"link_a\" target=\"_blank\">contact us. </a></p>

	<dl class=\"email_dl\">
		<dt>Best regards,</dt>
		<dd>Anysupport customer center</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$join_complete_mail_content_jp = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>AnySupport</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_detail{display:block;width:100%;}
	.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
	.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.link_a{color:#36a3f0 !important;}

	.clearfix{display:block;clear:both;content:\"\";}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">".$user_name."様</p>
	<p class=\"email_p01\">Thank you for joining AnySupport.</p>
	<div class=\"email_p01_content\">
		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- 名 : </span>
			<span class=\"email_detail_item\">".$user_name."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- ID : </span>
			<span class=\"email_detail_item\">".$user_id."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- パスワード : </span>
			<span class=\"email_detail_item\">".$user_password."</span>
		</p>

		<p class=\"clearfix\"></p>
	</div>
	<p class=\"email_p02\">ご不明な点がありましたら、お気軽にお問い合わせください。<a href='".$contact_url."' class=\"link_a\" target=\"_blank\">お問合せはこちら</a></p>
	<dl class=\"email_dl\">
		<dt>今後ともAnysupportをよろしくお願いいたします。</dt>
		<dd>Anysupportカスタマーセンター</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$join_complete_mail_content_kr = "
	<!doctype html>
	<html>
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
	<title>애니서포트</title>
	<style>
	.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
	.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
	.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
	.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}

	.email_detail{display:block;width:100%;}
	.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
	.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

	.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
	.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
	.email_dl dd{font-style:italic; font-weight:bold;}

	.link_a{color:#36a3f0 !important;}

	.clearfix{display:block;clear:both;content:\"\";}
	</style>
	</head>

	<body>
	<p class=\"email_p01\">안녕하세요, ".$user_name."님</p>
	<p class=\"email_p01\">애니서포트를 무료체험을 이용해 주셔서 감사합니다.</p>
	<div class=\"email_p01_content\">
		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- 이름 : </span>
			<span class=\"email_detail_item\">".$user_name."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- 아이디 : </span>
			<span class=\"email_detail_item\">".$user_id."</span>
		</p>

		<p class=\"clearfix\"></p>

		<p class=\"email_detail\">
			<span class=\"email_detail_label\">- 비밀번호 : </span>
			<span class=\"email_detail_item\">".$user_password."</span>
		</p>

		<p class=\"clearfix\"></p>
	</div>
	<p class=\"email_p02\">오류가 발생했습니까? <a href='".$contact_url."' class=\"link_a\" target=\"_blank\">고객센터</a>로 문의해 주세요.</p>
	<dl class=\"email_dl\">
		<dt>감사합니다.</dt>
		<dd>애니서포트 고객센터</dd>
	</dl>
	<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";