<?php
/**
 * free_join_alarm_mail.php
 * 2015.06.03 | KSM
 */

header('Content-Type: text/html; charset=utf-8');
session_start();

$free_join_alarm_mail_content_en = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">Free Trial User Information</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Name : </span>
				<span class=\"email_detail_item\">".$user_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- ID : </span>
				<span class=\"email_detail_item\">".$user_id."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Phone : </span>
				<span class=\"email_detail_item\">".$user_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Email : </span>
				<span class=\"email_detail_item\">".$user_mail."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Company : </span>
				<span class=\"email_detail_item\">".$user_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- Service Expired : </span>
				<span class=\"email_detail_item\">".$user_service_expired_date."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<dl class=\"email_dl\">
			<dt>Best regards,</dt>
			<dd>Anysupport customer center</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$free_join_alarm_mail_content_jp = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">無料トライアルユーザー情報</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 名 : </span>
				<span class=\"email_detail_item\">".$user_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- ID : </span>
				<span class=\"email_detail_item\">".$user_id."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 電話 : </span>
				<span class=\"email_detail_item\">".$user_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- メール : </span>
				<span class=\"email_detail_item\">".$user_mail."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 会社 : </span>
				<span class=\"email_detail_item\">".$user_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- サービスが期限切れ : </span>
				<span class=\"email_detail_item\">".$user_service_expired_date."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<dl class=\"email_dl\">
			<dt>Best regards,</dt>
			<dd>Anysupport customer center</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";

$free_join_alarm_mail_content_kr = "
	<!doctype html>
	<html>
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
		<meta name=\"viewport\" content=\"width=1220,maximum-scale=1.5,user-scalable=yes\">
		<title>AnySupport</title>
		<style>
			.email_p01{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto; font-size:20px; color:#000000;}
			.email_p01_content{position:relative; width:100%; overflow:hidden; margin:0 auto 30px auto;}
			.email_p02{position:relative; width:100%; overflow:hidden; margin:0 auto 90px auto; font-size:20px; color:#000000;}
			.email_p03{position:relative; width:100%; overflow:hidden; margin:0 auto; font-size:14px; color:#666666;}
			
			.email_detail{display:block;width:100%;}
			.email_detail_label{display:block;float:left;width:150px;margin-left:20px;font-size:15px;color:#666;}
			.email_detail_item{display:block;float:left;margin-left:5px;font-size:15px;font-weight:bold;color:#000;}

			.email_dl{position:relative; width:100%; overflow:hidden; margin:0 auto 14px auto;}
			.email_dl dt{font-size:20px; color:#000000; margin:0 0 12px 0;}
			.email_dl dd{font-style:italic; font-weight:bold;}

			.clearfix{display:block;clear:both;content:\"\";}
		</style>
	</head>

	<body>
		<p class=\"email_p01\">무료 평가판 가입 사용자 정보</p>
		<div class=\"email_p01_content\">
			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 이름 : </span>
				<span class=\"email_detail_item\">".$user_name."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 아이디 : </span>
				<span class=\"email_detail_item\">".$user_id."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 전화번호 : </span>
				<span class=\"email_detail_item\">".$user_phone."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 메일 : </span>
				<span class=\"email_detail_item\">".$user_mail."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 회사명 : </span>
				<span class=\"email_detail_item\">".$user_company."</span>
			</p>

			<p class=\"clearfix\"></p>

			<p class=\"email_detail\">
				<span class=\"email_detail_label\">- 서비스 만료 : </span>
				<span class=\"email_detail_item\">".$user_service_expired_date."</span>
			</p>

			<p class=\"clearfix\"></p>
		</div>

		<dl class=\"email_dl\">
			<dt>Best regards,</dt>
			<dd>Anysupport customer center</dd>
		</dl>
		<p class=\"email_p03\">© 2015 Anysupport, All rights reserved.Anysupport .</p>
	</body>
	</html>
";