<?php
/**
 * Name : validate_free_form.php
 * Version : AnySupport Global
 * Date :  2015.06.22
 * Author : KSM
 */
header('Content-Type: application/json');
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$id_length_min = ID_MIN_LENGTH;
$id_length_max = ID_MAX_LENGTH;
$pw_length_min = PW_MIN_LENGTH;
$pw_length_max = PW_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];


//
//	Msg Define ( error msg )
//
$error_msg_en = array(
	"ERROR" => "Error",
	"OK" => "OK!",
	"HELLO" => "Welcome!",
	"NICE" => "Looks nice!"
);

$error_msg_jp = array(
	"ERROR" => "エラー",
	"OK" => "通常の!",
	"HELLO" => "ようこそ!",
	"NICE" => "好き!"
);

$error_msg_kr = array(
	"ERROR" => "오류",
	"OK" => "정상!",
	"HELLO" => "환영합니다!",
	"NICE" => "정상!"
);

$error_msg = array(
	"en" => $error_msg_en,
	"jp" => $error_msg_jp,
	"kr" => $error_msg_kr
);

//
//	Msg Define ( guide msg )
//
$guide_msg_en = array(
	"ERROR" => "Please Check the marked red",
	"AGREE_ERROR" => "Please Checked Agree",
	"OK" => "&nbsp"
);

$guide_msg_jp = array(
	"ERROR" => "もう一度ご確認の上、ご記入ください。",
	"AGREE_ERROR" => "同意チェックしてください",
	"OK" => "&nbsp"
);

$guide_msg_kr = array(
	"ERROR" => "오류사항을 다시 확인해 주세요. ",
	"AGREE_ERROR" => "동의해주세요.",
	"OK" => "&nbsp"
);

$guide_msg = array(
	"en" => $guide_msg_en,
	"jp" => $guide_msg_jp,
	"kr" => $guide_msg_kr
);

$error = array("flag" => FALSE, "msg" => "&nbsp");
$guide = array("flag" => FALSE, "msg" => "&nbsp");

if($_POST['check_type'] == 'id'){
	$id = $_POST['admin_id'];
	$id_length = $_POST['length'];

	$error['flag'] = FALSE;
	$error['msg'] = "&nbsp";

	if($id_length == 0){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else if($id_length < $id_length_min || $id_length > $id_length_max){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else if(!preg_match("`^[_0-9a-z-.]{".$id_length_min.",".$id_length_max."}$`i", $id)){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else{		
		$spt_res = $asg_obj->DuplicatedCheckID("SUPPORTER", $id);	//supporter 중복
		$admin_res = $asg_obj->DuplicatedCheckID("ADMIN", $id);	//admin 중복
		$super_res = $asg_obj->DuplicatedCheckID("SUPERADMIN", $id);//superadmin 중복

		if($spt_res == "OK" && $admin_res == "OK" && $super_res == "OK"){
			$error['flag'] = FALSE;
			$error['msg'] = $error_msg[$msg_lang]["OK"];
			$guide['flag'] = FALSE;
			$guide['msg'] = $guide_msg[$msg_lang]["OK"];
		}else{
			$error['flag'] = TRUE;
			$error['msg'] = $error_msg[$msg_lang]["ERROR"];
			$guide['flag'] = TRUE;
			$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
		}
	}
}
else if($_POST['check_type'] == 'name'){
	if($_POST['first_name'] == "" || $_POST['last_name'] == ""){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else{
		$error['flag'] = FALSE;
		$error['msg'] = $error_msg[$msg_lang]["HELLO"];
		$guide['flag'] = FALSE;
		$guide['msg'] = $guide_msg[$msg_lang]["OK"];
	}
}
else if($_POST['check_type'] == 'mail'){
	$check_mail = $utils_obj->IsValidEmail($_POST['mail']);

	if(!$check_mail){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else{//pass
		$error['flag'] = FALSE;
		$error['msg'] = $error_msg[$msg_lang]["OK"];
		$guide['flag'] = FALSE;
		$guide['msg'] = $guide_msg[$msg_lang]["OK"];
	}
}
else if($_POST['check_type'] == 'password'){//case by password validate
	if($_POST['length'] < $pw_length_min || $_POST['length'] > $pw_length_max){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else{
		$check_pw = $utils_obj->IsValidPassword($_POST['change_pw']);

		if(!$check_pw){
			$error['flag'] = TRUE;
			$error['msg'] = $error_msg[$msg_lang]["ERROR"];
			$guide['flag'] = TRUE;
			$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
		}else{//pass
			$error['flag'] = FALSE;
			$error['msg'] = $error_msg[$msg_lang]["NICE"];
			$guide['flag'] = FALSE;
			$guide['msg'] = $guide_msg[$msg_lang]["OK"];
		}
	}
}
else if($_POST['check_type'] == 'retype_password'){
	if(strcmp($_POST['change_pw'], $_POST['retype_pw'])){
		$error['flag'] = TRUE;
		$error['msg'] = $error_msg[$msg_lang]["ERROR"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $guide_msg[$msg_lang]["ERROR"];
	}else{
		$error['flag'] = FALSE;
		$error['msg'] = $error_msg[$msg_lang]["OK"];
		$guide['flag'] = FALSE;
		$guide['msg'] = $guide_msg[$msg_lang]["OK"];
	}
}
else if($_POST['check_type'] == 'check_agree'){
	if($_POST['check_agree'] != 'true'){
		$guide['flag'] = TRUE;
		$guide['msg'] = $error_msg[$msg_lang]["AGREE_ERROR"];
	}else{
		$guide['flag'] = FALSE;
		$guide['msg'] = $guide_msg[$msg_lang]["OK"];
	}
}

$validateData = array(
	'error' => $error,
	'guide' => $guide
);

$output = json_encode($validateData);

echo urldecode($output);