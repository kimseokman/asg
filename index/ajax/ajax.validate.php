<?php
/**
 * Name : validate.php
 * Version : AnySupport Global
 * Date :  2015.05.27
 * Author : KSM
 */
header('Content-Type: application/json');
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");

$pw_length_min = PW_MIN_LENGTH;
$pw_length_max = PW_MAX_LENGTH;

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Msg Define
//
$msg_en = array(
	"LONG_PW" => "The password can only be character or number of ".$pw_length_min."-".$pw_length_max.".",
	"CHECK_RED_MARK" => "Check the red mark.",
	"NOT_MATCH_PW" => "Passwords does not match."
);

$msg_jp = array(
	"LONG_PW" => "パスワードは、".$pw_length_min."-".$pw_length_max."の英語または数字のみ可能です。",
	"CHECK_RED_MARK" => "赤色の表示を確認してください。",
	"NOT_MATCH_PW" => "パスワードが一致しません。"
);

$msg_kr = array(
	"LONG_PW" => "비밀번호는 ".$pw_length_min."-".$pw_length_max."의 영문 또는 숫자만 가능 합니다.",
	"CHECK_RED_MARK" => "적색 표시를 확인 하세요.",
	"NOT_MATCH_PW" => "비밀번호가 일치하지 않습니다."
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//
$error = array("flag" => FALSE, "msg" => "&nbsp");
$guide = array("flag" => FALSE, "msg" => "&nbsp");

if($_POST['check_type'] == 'password'){//case by password validate
	if($_POST['length'] < $pw_length_min || $_POST['length'] > $pw_length_max){
		$error['flag'] = TRUE;
		$error['msg'] = $msg[$msg_lang]["LONG_PW"];
		$guide['flag'] = TRUE;
		$guide['msg'] = $msg[$msg_lang]["CHECK_RED_MARK"];
	}
}else if($_POST['check_type'] == 'retype_password'){
	if(strcmp($_POST['change_pw'], $_POST['retype_pw'])){
		$error['flag'] = TRUE;
		$error['msg'] = $msg[$msg_lang]["NOT_MATCH_PW"];
	}
}

$validateData = array(
	'error' => $error,
	'guide' => $guide
);

$output = json_encode($validateData);

echo urldecode($output);