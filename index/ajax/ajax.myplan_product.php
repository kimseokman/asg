<?php
/**
 * Name : myplan_product.php
 * Version : AnySupport Global
 * Date :  2015.06.22
 * Author : KSM
 */
header('Content-Type: text/html; charset=utf-8');
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");

$utils_obj = new ASGUtils();

$product_code = $_POST['product_code'];
$product = $utils_obj->GetCodeProduct($product_code);

echo $product;