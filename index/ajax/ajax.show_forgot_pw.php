<?php
/**
 * Name : show_forgot_pw.php
 * Version : AnySupport Global
 * Date :  2015.06.05
 * Author : KSM
 */
header('Content-Type: application/json');

//
//	Set Language
//
$msg_lang = $_COOKIE['client_lang'];

//
//	Page Define
//	
$page = array(
	"RESET_PW_SEND_MAIL"=> "/index/".$msg_lang."/login/forgot_pw_send_mail.php"
);

//
//	Msg Define
//
$msg_en = array(
	"FORGOT_PASSWORD" => "Forgot your password?"
);

$msg_jp = array(
	"FORGOT_PASSWORD" => "パスワードをお忘れの方?"
);

$msg_kr = array(
	"FORGOT_PASSWORD" => "비밀번호를 잊으셨습니까?"
);

$msg = array(
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

//
//	Logic
//
$show_flag = FALSE;
$show_msg = "&nbsp;";

if($_POST['access_user_type'] == 'technician'){
	$show_flag = FALSE;
	$show_msg = "&nbsp;";
}else if($_POST['access_user_type'] == 'manager'){
	$show_flag = TRUE;
	$show_msg = "<a href='".$page["RESET_PW_SEND_MAIL"]."' target=\"_blank\">".$msg[$msg_lang]["FORGOT_PASSWORD"]."</a>";
}

$responseData = array(
	//'access_user_type' => $_POST['access_user_type'],//for debugging
	'show_flag' => $show_flag,
	'show_msg' => $show_msg
);

$output = json_encode($responseData);

echo urldecode($output);