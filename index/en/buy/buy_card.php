<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<p class="card_top_p01">Smart choice for you! You can pay with credit card.</p>
		<ul class="buy_top_ul">
			<li class="buy_color01">Plan detail</li>
			<li class="buy_color02">
				Anysupport 1+2 Monthly plan  10 Technicians
				<p class="buy_absol_btn"><a href="/index/en/buy/buy_edite.php">Edit</a></p>
			</li>
			<li class="buy_color03 last">
				Start date: Feb-05-2015<br />
				End date: Mar-05-2015
				<p class="buy_top_total">Total : 400.00 USD</p>
			</li>
		</ul>
		<p class="buy_top_line"></p>
		<p class="buy_red_p">Please check the erroness</p>
		<dl class="buy_con_dl">
			<dt>Name on card</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="First name |" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="Last name" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>Card number</dt>
			<dd>
				<ul class="buy_con_ul02">
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">invailed card number</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>Expire date</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0" class="atg">Month</a></li>
									<li><a href="javascript:;" data-path="" id="1" class="atg">Month2</a></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="last">
						<div class="select_differ02_div">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="2" class="atg">Year</a></li>
									<li><a href="javascript:;" data-path="" id="3" class="atg">Year2</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>Security code</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="Last 3 digit" class="common_input"/></li>
					<li class="last"><img src="/index/en/img/card_icon.gif" alt="" /></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>Billing address</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" placeholder="Town/city" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="State" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>Postcode</dt>
			<dd>
				<ul class="buy_con_ul">
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last">invailed card number</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl02">
			<dt>Country</dt>
			<dd>
				<ul class="buy_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="4" class="atg">Country1</a></li>
									<li><a href="javascript:;" data-path="" id="5" class="atg">Country2</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="buy_con_dl">
			<dt>Phone</dt>
			<dd>
				<ul class="buy_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
	</div>
	<div class="freetrial_content_bottom">
		<p class="right_blue_btn"><a href="#">Next</a></p>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>