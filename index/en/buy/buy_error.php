<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<dl class="buy_txt_dl">
			<dt>
				Oops..<br />
				Sorry, some errorness occured
			</dt>
			<dd>Your payment process failed. Some technical problem happened.</dd>
		</dl>
		<dl class="buy_txt_bottom">
			<dt>
				<img src="/index/en/img/buy_error.png" alt="error img" /><br />
				<span class="txt_bottom_span">We appologize you.</span>
			</dt>
			<dd><a href="/index/en/help/contact_us.php">Do you need to help?</a><img src="/index/en/img/buy_arrow.gif" alt="buy arrow" /></dd>
		</dl>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>