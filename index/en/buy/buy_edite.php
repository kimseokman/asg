<?
include_once("../header03.php");

?>
	<!-- 내용시작 -->
	<div class="buy_content">
		<p class="buy_edit_p">Edite your Plan</p>
		<div class="buy_edit_div">
			<p class="buy_edit_con_p">Plan detail</p>
			<div class="buy_edit_con">
				<dl class="edit_sel_dl">
					<dt>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0">Anysupport1+2</a></li>
									<li><a href="javascript:;" data-path="" id="1">Anysupport1+2</a></li>
								</ul>
							</div>
						</div>
					</dt>
					<dd>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0">1Technician</a></li>
									<li><a href="javascript:;" data-path="" id="1">2Technician</a></li>
								</ul>
							</div>
						</div>
					</dd>
				</dl>
				<ul class="common_radio_ul02">
					<li><input type="radio" name="buy_radiobutton1" id="buy_radio1" class="css-radio" /><label for="buy_radio1" class="css-label03 radioGroup1">Anysupport annual plan</label></li>
					<li class="last"><input type="radio" name="buy_radiobutton1" id="buy_radio2" class="css-radio" /><label for="buy_radio2" class="css-label03 radioGroup2">Anysupport annual plan <span class="buy_label_span">20%Addtional Discount</span></label></li>
				</ul>
				<dl class="buy_edit_total">
					<dt>
						Anysupport all in one<br />
						Monthly plan 10 technician
					</dt>
					<dd>
						Total : 400.00 USD
					</dd>
				</dl>
			</div>
		</div>
		<ul class="buy_edit_btn">
			<li><a href="/index/en/buy/buy_finish.php" class="edit_btn01">Save</a></li>
			<li class="last"><a href="/index/en/buy/buy_card.php" class="edit_btn02">Cancel</a></li>
		</ul>
	</div>
	<?
		include_once("../common_quick.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../sub_visual.php");
include_once("../footer02.php");
?>