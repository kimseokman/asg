<?
$wz['pid']  = "20";
$wz['gtt']  = "product";
$wz['gtt02']  = "Mobile Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>Hey, it’s amazing deal for you! How cool, people!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>if you are not ready to buy?  </dt>
				<dd><a href="/index/en/free/free_form.php">Try it for FREE  </a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/en/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02_on"><a href="/index/en/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03"><a href="/index/en/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">Mobile Edition</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">Features</a></li>
				<li><a href="#use" class="arctic_scroll">How to use</a></li>
				<li><a href="#beneficial" class="arctic_scroll">Beneficial</a></li>
			</ul>
			<div class="product_top_div02">
				<dl class="product_top_dl01">
					<dt>
						AnySuport Mobile Edition allows you to<br />
						access any  kinds of customers mobile<br />
						devices with the easiest App. installation.
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											features for AnySupport
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											Process for AnySupport
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div02">
		<div class="product_content" id="features">
			<p class="underline_tit">Features</p>
			<dl class="product_center_dl01">
				<dt>
					Professional Services<br />
					<span class="product_italic_span">Easy and fast access support tool</span>
				</dt>
				<dd>
					An remotely view and control your client's computers in order<br />
					to quickly diagnose and solve problems.Service engineers can up
					date applications,back up files and fix device problems re- <br />motely in a simple and efficient way.
					<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
				</dd>
			</dl>
			<p class="center_p_img"><img src="/index/en/img/mobileedition_con_img02.gif" alt="" /></p>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg03">
				<p class="product_gray_p">Save your time, works more efficient</p>
				<dl class="product_con_dl02">
					<dt>Support variety smart devices and OS</dt>
					<dd>
						can remotely diagnose computer problems and expedite your s-<br />
						ervice while keeping your connections protected with AES en-<br />
						cryption and Secure Socket Layer security. By remotely manag-<br />
						ing and troubleshooting computer issues, IT members gain ext-<br />
						ra time for other important tasks.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>School and Government Offices IT engineers</li>
					<li> Enterprise Helpdesk</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg04">
				<p class="product_gray_p">Customer Relation</p>
				<dl class="product_con_dl03">
					<dt>Powerful Network capability.</dt>
					<dd>
						can travel less while improving your effectiveness by sharing s-<br />
						ales presentations remotely at anywhereat anytime. Perform<br />
						online demonstration and remotely guide or train customers<br />
						asif sale member is sitting next to the customers. Sales teams<br />
						can implify and improve customer sales experience.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Go to start ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>Top-notch for Customer Service</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">How to use</p>
			<div class="product_movie_area">
				<iframe width="1040" height="400" src="https://www.youtube.com/embed/kVwqYuaBJ1I?wmode=opaque" frameborder="0" allowfullscreen></iframe>
			</div>
			<p class="product_gray_p">Way to use AnySupport</p>
			<dl class="product_con_dl02">
				<dt>How to use your AnySupport?</dt>
				<dd>
					can travel less while improving your effectiveness by sharing s-<br />
					ales presentations remotely at anywhereat anytime. Perform<br />
					online demonstration and remotely guide or train customers<br />
					asif sale.
					<a href="/index/en/free/free_form.php" class="blue_link_a02">Free Trial ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							Download<br />
							Process for AnySupport
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">Beneficial</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Mobile device<br />Provider</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/mobileedition_list_img01.gif" alt="Mobile device Provider images" /></p>
							</div>
						</dt>
						<dd>
							The smart device was installed the Anysu-<br />
							pport Mobile Edition App make user to save<br />
							thire time and money because they do not<br />
							need to go to dissolve thire mobile problems.<br />
							Hance,also the Manufacturers make more<br />
							efficient values as the selling point.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Mobile Servise<br />Help Desk</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/mobileedition_list_img02.gif" alt="Mobile Servise Help Desk images" /></p>
							</div>
						</dt>
						<dd>
							The clear diagnosis of the issus leads to<br />
							satisfaction of customers so AnySupport<br />
							Mobile Edition can be a best tool to solve<br />
							many kind of mobile issues as getting of the<br />
							those point directly.   
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Groupware , VPN<br />Supporter</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/mobileedition_list_img03.gif" alt="Groupware , VPN Supporter images" /></p>
							</div>
						</dt>
						<dd>
							An remotely view and control your client's<br />
							computers in order to quickly diagnose<br />
							and solve problems.Service engineers can<br />
							up date applications,back up files and fix<br />
							device problems remot-ely in a simple and<br />
							efficient way.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">Function</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>Screen Sharing</dt>
						<dd>
							Both your customers and your<br /> 
							IT technicians can share their scr<br />
							eens.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Invite Supporter</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Mac Support</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Multi-Session</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>File Transfer</dt>
						<dd>
							Useful for applying patches, sendi<br />
							ng URLs and receiving log files
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Draw</dt>
						<dd>
							Integrate with existing infrastruct-<br />
							ure and other applications.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Session Transfer</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time. 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Session Recording</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>In-session chat</dt>
						<dd>
							Take notes during and at end of<br />
							sessions, assign unattended se-<br />
							ssions to companies and track<br />
							time spent supporting users.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>available Screen shot </dt>
						<dd>
							IT help desk admins can remotely<br />
							log in to customer computers to<br />
							perform system administrative tasks.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>