<?
$wz['pid']  = "20";
$wz['gtt']  = "product";
$wz['gtt02']  = "Video Edition";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>Hey, it’s amazing deal for you! How cool, people!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>if you are not ready to buy?  </dt>
				<dd><a href="/index/en/free/free_form.php">Try it for FREE  </a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01"><a href="/index/en/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02"><a href="/index/en/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03_on"><a href="/index/en/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">Video Edition</h3>
			<ul class="sub_a_link_ul02">
				<li><a href="#features" class="arctic_scroll">Features</a></li>
				<li><a href="#use" class="arctic_scroll">How to use</a></li>
				<li><a href="#beneficial" class="arctic_scroll">Beneficial</a></li>
			</ul>
			<div class="product_top_div03">
				<dl class="product_top_dl01">
					<dt>
						AnySupport Video Edition allows technicians to see<br />
						what customer is  experiencing through mobile ca-<br />
						mera,Sharing videos and photos, technicians prov-<br />
						ide more accurate diagnosis with reduced commu-<br />
						nication time.
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											features for AnySupport
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											Process for AnySupport
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div04">
		<div class="product_content" id="features">
			<div class="videoedition_con_bg">
				<p class="underline_tit">Features</p>
				<dl class="product_center_dl02">
					<dt>
						Professional Services<br />
						<span class="product_italic_span">Transfer clear realtime video with voice</span>
					</dt>
					<dd>
						An remotely view and control your client's computers in order to<br />
						quickly diagnose and solve problems.Service engineers can up<br />
						date applications,back up files and fix device problems remot-<br />
						ely in a simple and efficient way.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg05">
				<p class="product_gray_p">Customer Relation</p>
				<dl class="product_con_dl02">
					<dt>It’s so fast to transfer video streaming</dt>
					<dd>
						an travel less while improving your effectiveness by sharing s-<br />
						ales presentations remotely at anywhereat anytime. Perform<br />
						online demonstration and remotely guide or train customers<br />
						asif sale member is sitting next to the customers. Sales teams<br />
						can implify and improve customer sales experience.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>Top-notch for Customer Service </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg06">
				<p class="product_gray_p">Save your time, works more efficient</p>
				<dl class="product_con_dl03">
					<dt>Save Time and money</dt>
					<dd>
						can remotely diagnose computer problems and expedite your s-<br />
						ervice while keeping your connections protected with AES en-<br />
						cryption and Secure Socket Layer security. By remotely manag-<br />
						ing and troubleshooting computer issues, IT members gain ext-<br />
						ra time for other important tasks.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Go to start ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>School and Government Offices IT engineers </li>
					<li> Enterprise Helpdesk  </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">How to use</p>
			<div class="product_movie_area">
				<img src="/index/en/img/product_common_movie_img02.gif" alt="" />
			</div>
			<p class="product_gray_p">Way to use Video Edition</p>
			<dl class="product_con_dl02">
				<dt>How to use your AnySupport Video Edition?</dt>
				<dd>
					can travel less while improving your effectiveness by sharing s-<br />
					ales presentations remotely at anywhereat anytime. Perform<br />
					online demonstration and remotely guide or train customers<br />
					asif sale.
					<a href="/index/en/free/free_form.php" class="blue_link_a02">Free Trial ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							Download<br />
							Process for AnySupport
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">Beneficial</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Home appliance<br />servise center</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/videoedition_list_img01.gif" alt="Home appliance servise center images" /></p>
							</div>
						</dt>
						<dd>
							To share relatime video with home applia-
							nce service centers is able to reduce sup-
							pot visits for resolving amoun of issues.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Insurance company</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/videoedition_list_img02.gif" alt="Insurance company images" /></p>
							</div>
						</dt>
						<dd>
							Insurance agencies detect what accidents  
							ocuure by the Video Edition,Moreover the
							Video Edition affects in an emergency.  
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Construction engineer<br />and site manager</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/videoedition_list_img03.gif" alt="Construction engineer and site manager images" /></p>
							</div>
						</dt>
						<dd>
							An remotely view and manage construction<br />
							sitein order to quickly diagnose and solve<br />
							problems. Engineers and managers can fix<br />
							some problems remotely in a simple and<br />
							efficient way.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">Function</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>Realtime video streaming</dt>
						<dd>
							Both your customers and your<br />
							IT technicians can share their scr<br />
							eens.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Flash ON/ OFF</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Re-trensfer streaming</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Camera focuse</dt>
						<dd>
							Provide live remote support<br />
							to both PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>File Transfer</dt>
						<dd>
							Useful for applying patches, sendi<br />
							ng URLs and receiving log files
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Adjustment Quality</dt>
						<dd>
							Integrate with existing infrastruct-<br />
							ure and other applications.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Switch camera</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time. 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Draw</dt>
						<dd>
							Provide live remote support<br />
							to both PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>In session Chat</dt>
						<dd>
							Take notes during and at end of<br />
							sessions, assign unattended se-<br />
							ssions to companies and track<br />
							time spent supporting users.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Speaker Phone</dt>
						<dd>
							IT help desk admins can remotely<br />
							log in to customer computers to<br />
							perform system administrative tasks.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Show Guide Camera</dt>
						<dd></dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Rotation  viewer</dt>
						<dd></dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>