<?
$wz['pid']  = "20";
$wz['gtt']  = "product";
$wz['gtt02']  = "Anysupport";

include_once("../header.php");
include_once("../black_bar.php");
?>
	<!-- 내용시작 -->
	<div class="sub_visual">
		<div class="sub_visual_1080">
			<p class="sub_visual_p"></p>
			<dl class="product_visual_txt01">
				<dt>AnySupport All in one</dt>
				<dd>Hey, it’s amazing deal for you! How cool, people!</dd>
			</dl>
			<dl class="product_visual_txt02">
				<dt>if you are not ready to buy?  </dt>
				<dd><a href="/index/en/free/free_form.php">Try it for FREE  </a></dd>
			</dl>
		</div>
		<div class="product_link_tab">
			<ul class="product_link">
				<li class="product_tab01_on"><a href="/index/en/product/anysupport.php">AnySupport</a></li>
				<li class="product_tab02"><a href="/index/en/product/mobile_edition.php">Mobile Edition</a></li>
				<li class="product_tab03"><a href="/index/en/product/video_edition.php">Video Edition</a></li>
			</ul>
		</div>
	</div>
	<?
	include_once("../location.php");
	?>
	<div class="product_box_div01">
		<div class="product_content">
			<h3 class="sub_common_h3">AnySupport</h3>
			<ul class="sub_a_link_ul01">
				<li><a href="#features" class="arctic_scroll">Features</a></li>
				<li><a href="#use" class="arctic_scroll">How to use</a></li>
				<li><a href="#beneficial" class="arctic_scroll">Beneficial</a></li>
			</ul>
			<div class="product_top_div">
				<dl class="product_top_dl01">
					<dt>
						Anysuppoer provides solid, secure and fast remote<br />
						seivice. With AnySupport, you can remotely view,<br />
						diagnose and resolve computer and mobile device<br />
						related problems.
					</dt>
					<dd>
						<ul class="product_top_ul">
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											features for AnySupport
										</dd>
									</dl>
								</a>
							</li>
							<li>
								<a href="#" target="_blank">
									<dl class="product_top_dl02">
										<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pad icon" /></dt>
										<dd>
											Download<br />
											Process for AnySupport
										</dd>
									</dl>
								</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>
		</div>
	</div>

	<div class="product_box_div02">
		<div class="product_content" id="features">
			<p class="underline_tit">Features</p>
			<dl class="product_center_dl01">
				<dt>
					Professional Services<br />
					<span class="product_italic_span">Easy and fast remote support tool</span>
				</dt>
				<dd>
					A remotely view and control your client's computers in order to<br />
					quickly diagnose and solve problems.Service engineers can up<br />
					date applications,back up files and fix device problems remot-<br />
					ely in a simple and efficient way.
					<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
				</dd>
			</dl>
			<p class="center_p_img"><img src="/index/en/img/anysupport_con_img02.gif" alt="" /></p>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<div class="support_div_bg">
				<p class="product_gray_p">Save your time, works more efficient</p>
				<dl class="product_con_dl02">
					<dt>Professional remote support</dt>
					<dd>
						For companies and individuals who need to provide excellent cus-<br />
						tomer support and professional service to their clients, we offer an<br />
						ideal remote support solution By remotely managing and trouble-<br />
						shooting computer issues, Professinal members gain extra time<br />
						for other important tasks.
						<a href="/index/en/free/free_form.php" class="blue_link_a">Free Trial ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li>School and Government Offices IT engineers</li>
					<li>Enterprise Helpdesk  </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content">
			<div class="support_div_bg02">
				<p class="product_gray_p">Effective management service   </p>
				<dl class="product_con_dl03">
					<dt>Useful report and management</dt>
					<dd>
						AnySupport also delivers a management tool such as statistic<br />
						charts and visual graphes.Eventhough they includes huge datas,<br />
						those are so understandable visually because it takes intuitive<br />
						concept so anyone need to develop theire service qality can<br />
						use AnySupport as powerful management tool. 
						<a href="/index/en/free/free_form.php" class="blue_link_a">Go to start ></a>
					</dd>
				</dl>
				<ul class="product_dot_ul">
					<li> Company need more management</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content" id="use">
			<p class="underline_tit">How to use</p>
			<div class="product_movie_area">
				<iframe width="1040" height="400" src="https://www.youtube.com/embed/8Srh1CliXCU?wmode=opaque" frameborder="0" allowfullscreen></iframe>
			</div>
			<p class="product_gray_p">The process of using AnySupport </p>
			<dl class="product_con_dl02">
				<dt>How to use your AnySupport?</dt>
				<dd>
					See how easy it is to use AnySupport Remote control technology to deliver<br />
					fast live support and access unattended computers and servers that has ins-<br />
					tant start, simple setup,easy-interfaces with stable connections.<br />
					<a href="/index/en/free/free_form.php" class="blue_link_a02">Free Trial ></a>
				</dd>
			</dl>
			<div class="product_absol_pdf">
				<a href="#" target="_blank">
					<dl class="absol_pdf">
						<dt><img src="/index/en/img/product_pdf_icon.gif" alt="pdf icon" /></dt>
						<dd>
							Download<br />
							Process for AnySupport
						</dd>
					</dl>
				</a>
			</div>
		</div>
	</div>

	<div class="product_box_div03">
		<div class="product_content" id="beneficial">
			<p class="underline_tit">Beneficial</p>
			<ul class="product_bottom_img_list">
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Software Service<br />Providers</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/anysupport_list_img01.gif" alt="Software Service Providers images" /></p>
							</div>
						</dt>
						<dd>
							Support Service Providers provide service<br />
							remotely and be always available at any<br />
							time, in any place.No matter where you<br />
							are, as long as there is Internet connecti-<br />
							on, Support engineers can reach customers<br />
							instantly.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">IT Organization</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/anysupport_list_img02.gif" alt="IT Organization images" /></p>
							</div>
						</dt>
						<dd>
							Sales Team can travel less while improving<br />
							your effectiveness by sharing sales prese-<br />
							ntations remotely at anywhere at anytime.<br />
							Perform online demonstration and remotely<br />
							guide or train customers as if sale member<br />
							is sitting next to the customers. Sales teams<br />
							can implify and improve customer sales ex-<br />
							perience.
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="img_list_dl">
						<dt>
							<div class="img_list_nor">
								<p class="img_list_tit">Help Desk</p>
								<div class="img_list_hover_dim"></div>
								<p class="list_nor_img"><img src="/index/en/img/anysupport_list_img03.gif" alt="Help Desk images" /></p>
							</div>
						</dt>
						<dd>
							can remotely diagnose computer problems<br />
							and expedite your service while keeping<br />
							your connections protected with AES enc-<br />
							ryption and Secure Socket Layer security-<br />
							By remotely managing and troubleshooti-<br />
							ng computer issues, IT members gain ex-<br />
							tra time for other important tasks.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>

	<div class="product_box_div01">
		<div class="product_content">
			<p class="underline_tit">Function</p>
			<ul class="product_function_ul">
				<li>
					<dl class="product_function_dl">
						<dt>Screen Sharing</dt>
						<dd>
							Both your customers and your<br />
							IT technicians can share their scr<br />
							eens.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>In-Session Chat</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Mac, Linux  Support</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC,Linux and Mac users 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Multi-Session</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>File Transfer</dt>
						<dd>
							Useful for applying patches, sendi<br />
							ng URLs and receiving log files
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Integration APIs</dt>
						<dd>
							Integrate with existing infrastruct-<br />
							ure and other applications.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Session Transfer</dt>
						<dd>
							Each technician can chat with up<br />
							to 8 customers at a time. 
						</dd>
					</dl>
				</li>
				<li class="last">
					<dl class="product_function_dl">
						<dt>Session Recording</dt>
						<dd>
							Provide live remote support to bo-<br />
							th PC and Mac users 
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Session Reporting</dt>
						<dd>
							Take notes during and at end of <br />
							sessions, assign unattended se-<br />
							ssions to companies and track <br />
							time spent supporting users.
						</dd>
					</dl>
				</li>
				<li>
					<dl class="product_function_dl">
						<dt>Log-In as Administrator</dt>
						<dd>
							IT help desk admins can remotely<br />
							log in to customer computers to <br />
							perform system administrative tasks.
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<?
	include_once("../allinone.php");
	?>
	<!-- 내용끝 -->

<?
include_once("../footer.php");
?>