<?
$wz['pid']  = "50";
$wz['gtt']  = "Help";
$wz['gtt02']  = "Contact us";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");

$request_mail_res = "NORMAL"; 
if($_POST['request_mail_res']){
	$request_mail_res = $_POST['request_mail_res'];
	$request_mail_name = $_POST['request_mail_name'];
}
?>
<!-- 내용시작 -->
<dl class="help_top_dl">
	<dt>Do you have some trouble?</dt>
	<dd>
		or questions?<br />
		We’ve alredy ready to solve the problem you need to make sure about. 
	</dd>
</dl>

<div class="help_content">
	<div class="help_con_cc">
		<form name="contact_form" method="post" action="/index/_func/function.contact_us_send_mail.php">
			<p class="help_con_p">Please fill out your contact information below </p>
			<p class="help_common_txt">1.Tell us how to contact you.</p>
			<ul class="help_input_ul">
				<li class="help_ul_half"><input type="text" name="contact_first_name" id="" value="" placeholder="First Name" class="help_input" /></li>
				<li class="help_ul_half_last"><input type="text" name="contact_last_name" id="" value="" placeholder="Last Name" class="help_input"/></li>
				<li><input type="text" name="contact_phone" id="" value="" placeholder="Phone" class="help_input"/></li>
				<li><input type="text" name="contact_mail" id="" value="" placeholder="Email" class="help_input"/></li>
			</ul>
			<p class="help_common_txt">2.Tell us what should we help you?</p>
			<textarea name="contact_msg" id="" class="help_area" placeholder="What is your questions? Leave the message.."></textarea>
			<p class="help_common_txt">3.Tell us about your company.</p>
			<ul class="help_input_ul">
				<li><input type="text" name="contact_company" id="" value="" placeholder="Company Name" class="help_input"/></li>
				<li><input type="text" name="contact_department" id="" value="" placeholder="Department" class="help_input"/></li>
			</ul>
			<p class="common_blue_btn contact_send_btn"><a href="#">Send</a></p>
		</form>
	</div>
</div>
<?
include_once("../help_quick.php");
?>
<?
if($request_mail_res == "SUCCESS"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>Thanks, <? echo $request_mail_name; ?>!</dt>
		<dd>
			Your message has been sent to the representative.
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/en/help/contact_us.php" class="pop_link_a02">OK</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "SUCCESS")

if($request_mail_res == "FAIL"){
?>
<div class="fixed_dim"></div>
<div class="free_pop">
	<dl class="free_pop_dl">
		<dt>Sorry, <? echo $request_mail_name; ?>!</dt>
		<dd>
			Failed to send mail.
		</dd>
	</dl>
	<ul class="free_pop_ul">
		<li class="solo"><a href="/index/en/help/contact_us.php" class="pop_link_a02">OK</a></li>
	</ul>
</div>
<?
}//end of : if($request_mail_res == "FAIL")
?>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>