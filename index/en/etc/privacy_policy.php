<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=1220,maximum-scale=1.5,user-scalable=yes">
<title>코이노</title>
<link rel="stylesheet" href="/index/en/css/base.css" type="text/css" />
</head>

<body>
	<div class="policy_div">
		<p class="etc_big_tit">Anysupport</p>
		<p class="etc_con_tit">Privacy Policy</p>
		<p class="etc_con_p">
			AnySupport Remote Support is a remote support service provided by AnySupport, INC.<br /><br />
			Your privacy is important to us. To better protect your privacy we provide this policy explaining our online information practices and the choices you can make about the way your information is collected and used. To make this policy easy to find, we make it available on our homepage and at every point where personally identifiable information may be requested.
		</p>
		<p class="etc_con_tit">Web Site Information Collection:</p>
		<p class="etc_con_p">
			This notice applies to all information collected or submitted on the AnySupport website. On some pages, you can order products, make requests, and register to receive materials. The types of personal information collected at these pages are:<br /><br />
			Name<br />
			Address<br />
			Email<br />
			address<br />
			Phone number<br />
			Credit/Debit Card Information<br />
			Other demanding information for the service<br /><br />
			On some pages, you can submit information about other people. For example, if you tell your friend about us through 'Tell a Friend' web page, you will need to submit the recipient's email address or phone number. In this circumstance, the types of personal information collected are:<br /><br />
			Name<br />
			Email<br />
			Address<br />
			Phone Number<br />
			Other demanding information for the service
		</p>
		<p class="etc_con_tit">Use of Information collected</p>
		<p class="etc_con_p">
			We use the information you provide about yourself when placing an order only to complete that order. We do not share this information with outside parties except to the extent necessary to complete that order.<br /><br />
			We use the information you provide about someone else when placing an order only to ship the product and to confirm delivery. We do not share this information with outside parties except to the extent necessary to complete that order.<br /><br />
			We offer gift-cards by which you can personalize a product you order for another person. Information you provide to us to create a gift-card is only used for that purpose, and it is only disclosed to the person receiving the gift.<br /><br />
			We use return email addresses to answer the email we receive. Such addresses are not used for any other purpose and are not shared with outside parties.<br /><br />
			You can register with our website if you would like to receive our catalog as well as updates on our new products and services. Information you submit on our website will not be used for this purpose unless you fill out the registration form.<br /><br />
			We use non-identifying and aggregate information to better design our website and to share with advertisers. For example, we may tell an advertiser that X number of individuals visited a certain area on our website, or that Y number of men and Z number of women filled out our registration form, but we would not disclose anything that could be used to identify those individuals.<br /><br />
			Finally, we never use or share the personally identifiable information provided to us online in ways unrelated to the ones described above without also providing you an opportunity to opt-out or otherwise prohibit such unrelated uses.
		</p>
		<p class="etc_con_tit">Data Security</p>
		<p class="etc_con_p">
			To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.
		</p>
		<p class="etc_con_tit">Children's Privacy:</p>
		<p class="etc_con_p">
			Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our website from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.
		</p>
		<p class="etc_con_tit">Access Or Correct Your Information</p>
		<p class="etc_con_p">
			You can access all your personally identifiable information that we collect online and maintain by login procedure. We use this procedure to better safeguard your information.<br /><br />
			You can correct factual errors in your personally identifiable information by sending us a request that credibly shows error.<br /><br />
			To protect your privacy and security, we will also take reasonable steps to verify your identity before granting access or making corrections.
		</p>
		<p class="etc_con_tit">Contact Us</p>
		<p class="etc_con_p">
			Should you have other questions or concerns about these privacy policies, please send us an email at <a href="mailto:contact@AnySupport.net" class="etc_link_a">contact@AnySupport.net</a>
		</p>
	</div>
</body>
</html>