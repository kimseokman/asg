<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=1220,maximum-scale=1.5,user-scalable=yes">
<title>코이노</title>
<link rel="stylesheet" href="/index/en/css/base.css" type="text/css" />
</head>

<body>
	<div class="service_div">
		<p class="etc_big_tit">Anysupport</p>
		<p class="etc_con_tit">Terms of Services</p>
		<p class="etc_con_tit_small">Revision date: May 1, 2015</p>
		<p class="etc_con_italic_p">
			AnySupport, Inc. (“AnySupport”) provides the AnySupport Web site including the AnySupport Home Page and other AnySupport contents including application program (collectively, the "AnySupport Site") subject to Customer’s compliance with the terms and conditions below. 
		</p>
		<p class="etc_con_italic_p">
			By checking the acceptance button on the page or application program and completing the electronic acceptance process, you represent and warrant that you: 
		</p>
		<ul class="etc_ul">
			<li>are, or are authorized to sign for, the contracting party defined herein as “Customer”;</li>
			<li>have read, understand and agree to be bound by all of the terms and conditions of this online subscription agreement;</li>
			<li>are 18 years of age or older</li>
		</ul>
		<p class="etc_con_italic_p">
			PLEASE READ THIS BEFORE ACCESSING THE ANYSUPPORT SITE. BY ACCESSING THE ANYSUPPORT SITE, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS BELOW. IF YOU DO NOT WISH TO BE BOUND BY THESE TERMS AND CONDITIONS, YOU MAY NOT ACCESS OR USE THE ANYSUPPORT SITE.
		</p>
		<p class="etc_con_tit">Basic Rules</p>
		<p class="etc_con_p">
			While visiting the AnySupport Site, Customer may also not: post, transmit or otherwise distribute information constituting or encouraging conduct that would constitute a criminal offense or give rise to civil liability, or otherwise use the AnySupport Site in a manner which is contrary to law or would serve to restrict or inhibit any other user from using or enjoying the AnySupport Site or the Internet; post or transmit any information or software which contains a virus, cancelbot, trojan horse, worm or other harmful or disruptive component; upload, post, publish, transmit, reproduce, or distribute in any way, information, software or other material obtained through the AnySupport Site which is protected by copyright, or other intellectual property right, or derivative works with respect hereto, without obtaining permission of the copyright owner or rightholder.
		</p>
		<p class="etc_con_tit">Services</p>
		<p class="etc_con_p">
			Customer may use <strong>AnySupport</strong> remote support service only to enable a designated user to provide remote assistance to its customers. Customer may use <strong>AnySupport</strong> remote access service only to enable authorized individuals to access and control designated users’ PCs remotely.<br /><br />
			Customer must complete the online registration process including this Agreement. AnySupport may reject an online registration by a potential Customer in its sole discretion and is not obligated to provide a reason for the rejection. Customer or on behalf of Customer shall provide all registration data in accurate, current, and complete. The registration data should be updated by Customer in its sole responsibility. AnySupport reserves the right to terminate this Agreement immediately in the event submitted registration data is found to be inaccurate, not current, or incomplete.<br /><br />
			During online registration process, AnySupport will collect additional information on billing and payment related matters. It will include a valid credit card or debit card number with available credit, preferred billing frequency, and other information required by AnySupport. AnySupport reserves the right to terminate the Agreement immediately in the event any payment information is found to be inaccurate, not current or incomplete. Any overdraft charge or other fees that may be issued by AnySupport’s use of Customer’s credit or debit card shall not be AnySupport’s responsibility.<br /><br />
			Customer is entirely responsible for all activities under its account including management of keeping confidentiality of its password and account. Customer shall agree to inform AnySupport immediately of any type of security breach. AnySupport shall not be liable for any loss aroused from the Customer’s activity which may bring as a result of other entity using its password or account, either with or without its knowledge. Customer may be held liable for losses incurred by AnySupport in such case as described above.<br /><br />
			Customer is responsible for all subscription fees and all applicable taxes (sales tax, value-added tax, goods and services tax, withholding tax, etc.). Upon Customer’s payment of subscription fees, AnySupport will allow Customer to access and use the services described above. All contents transferred by Customer or any other party in connection with Customer’s access and/or use of the services, of all kinds of communication methods, are shall be Customer’s responsibility.<br /><br />
			Customer must not reverse engineer, decompile or otherwise attempt to decipher any code in connection with the services or any other aspect of AnySupport’s technology. Customer shall not access and/or use any of the services in any manner that could damage, disable, overburden, impair or otherwise interfere with or disrupt the AnySupport sites, services or related systems of AnySupport. Customer may not resell, distribute, or use any of the services on a timeshare or disapproved method basis.<br /><br />
			AnySupport may offer a trial and/or promotional offers from time to time. AnySupport reserves the right to discard or change coupons, credits, trials and promotional offers at its discretion and without notice. Just one (1) offer per customer is applied and limited to any kind of trial or promotional offers.
		</p>
		<p class="etc_con_tit">Monitoring</p>
		<p class="etc_con_p">
			AnySupport has no obligation to monitor the AnySupport Site. However, Customer agrees that AnySupport has the right to monitor the AnySupport Site electronically from time to time and to disclose any information as necessary to satisfy any law, regulation or other governmental request, to operate the AnySupport Site properly, or to protect itself or its subscribers. AnySupport will not intentionally monitor or disclose any private electronic-mail message unless required by law. AnySupport reserves the right to refuse to post or to remove any information or materials, in whole or in part, that, in its sole discretion, are unacceptable, undesirable, or in violation of this Agreement.
		</p>
		<p class="etc_con_tit">Termination</p>
		<p class="etc_con_p">
			AnySupport reserves the right to terminate this Agreement immediately if Customer breaches any of its material obligations under the Agreement. Upon termination of the Agreement, Customer will immediately stop access to and use of the services. AnySupport shall not be liable for any damages resulting from a termination of the Agreement. Provided, claims arising prior to the termination shall not be affected.
		</p>
		<p class="etc_con_tit">Privacy</p>
		<p class="etc_con_p">
			It is therefore recommended that the services shall not be used for the transmission of confidential information. AnySupport cannot ensure or guarantee privacy for AnySupport users. Any such use shall be at the sole risk of Customer and other users in connection with Customer, and AnySupport and its affiliate and related companies shall be relieved of all liability in connection therewith.
		</p>
		<p class="etc_con_tit">Limitation of Liability</p>
		<p class="etc_con_p">
			AnySupport takes no responsibility for the accuracy or validity of any claims or statements contained in the documents and related graphics on the AnySupport Site. Further, AnySupport makes no representations about the suitability of any of the information contained in software programs, documents and related graphics on the AnySupport Site for any purpose. All such software programs, documents and related graphics are provided without warranty of any kind. In no event shall AnySupport be liable for any damages whatsoever, including special, indirect or consequential damages, arising out of or in connection with the use or performance of information available from the service. AnySupport’s liability is limited to maximum $50.<br /><br />
			If Customer is dissatisfied with the AnySupport Site or with any terms, conditions, rules, policies, guidelines, or practices of AnySupport in operating the AnySupport Site, Customer’s sole and exclusive remedy is to discontinue using the AnySupport Site.
		</p>
		<p class="etc_con_tit">Confidential Information</p>
		<p class="etc_con_p">
			Both parties shall not disclose non-public information or materials which are reasonably understood to be confidential (“Confidential Information”) except the case of any information that receiving party of Confidential Information is obligated to produce pursuant to an order of a court of competent jurisdiction with timely notice to disclosing party. Customer will keep in strict confidence all passwords and other access information to the services.<br /><br />
			Customer shall authorize AnySupport to collect from any party and to retain all relevant information relating to Customer’s use of the AnySupport Site, and hereby authorize any party to provide AnySupport with such information. Customer shall understand and agree that unless Customer notify AnySupport to the contrary by e-mailing, Customer further authorize AnySupport to disclose, on a confidential basis, to any party with whom AnySupport has business relations all relevant information relating to Customer’s dealings with AnySupport and the AnySupport Site.
		</p>
		<p class="etc_con_tit">Indemnity</p>
		<p class="etc_con_p">
			Customer agrees, at its sole expense, to defend, indemnify and hold AnySupport and its affiliate and related companies harmless from any and all liabilities, costs and expenses, including reasonable attorneys' fees, related to a third party claim, suit, or infringement based on information, data, or any content submitted by Customer or related to any Violation of this Agreement by Customer or users of Customer’s account, or in connection with the use of the AnySupport Site or the Internet or the placement or transmission of any message, information, software or other materials on the AnySupport Site or on the Internet by Customer or users of Customer’s account.
		</p>
		<p class="etc_con_tit">Trademarks & Proprietary Rights</p>
		<p class="etc_con_p">
			AnySupport™ and other names, logos and icons identifying AnySupport and AnySupport' products and services referenced herein are trademarks or registered trademarks of AnySupport. All other product and/or brand or company names mentioned herein are the trademarks of their respective owners. AnySupport retains ownership of all proprietary rights in or associated with all its products and services.
		</p>
		<p class="etc_con_tit">DISCLAIMER</p>
		<p class="etc_con_p">
			CUSTOMER HEREBY ACKNOWLEDGES AND AGREES THAT ALL MATERIALS CONTAINED IN THE SERVICES IS PROVIDED BY ANYSUPPORT ON AN "AS IS" BASIS, AND CUSTOMER’S ACCESS TO AND/OR USE OF THE SERVICES IS AT ITS SOLE RISK WITHOUT WARRANTY OF ANY KIND. THE ENTIRE RISK AS TO THE RESULTS AND THE PERFORMANCE OF THE SOFTWARE IS ASSUMED BY THE USER, AND IN NO EVENT WILL ANYSUPPORT BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL OR DIRECT DAMAGES SUFFERED IN THE COURSE OF USING THE SOFTWARE IN THE SERVICES. USE OF ANY MATERIAL AND/OR DATA DOWNLOADED OR OBTAINED PROGRAMS (INCLUDING PLUG-INS) CONTAINED IN THE SERVICES IS DONE AT THE SOLE RISK OF CUSTOMER. CUSTOMER WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO ITS COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF SUCH MATERIAL AND/OR DATA.
		</p>
		<p class="etc_con_tit">Headings</p>
		<p class="etc_con_p">
			The headings used in this Agreement are used for convenience only and are not to be considered in construing or interpreting this Agreement.
		</p>
		<p class="etc_con_tit">Governing Law</p>
		<p class="etc_con_p">
			This Agreement and the rights and obligations of the parties hereto shall be governed, construed and interpreted in accordance with the laws of the State of California, U.S.A., without giving effect to principles of conflicts of law.  Any dispute with respect to this Agreement, Customer consents to the exclusive personal jurisdiction and venue in the State and Federal courts within San Jose, CA.
		</p>
		<p class="etc_con_tit">Force Majeure</p>
		<p class="etc_con_p">
			AnySupport shall not be responsible for delays or failure to perform resulting from acts beyond the reasonable control of such party and which could not have been reasonably foreseen and provided against and with respect to which such party shall exercise continuing diligence to resume performance of its obligations. Such acts shall include, without limitation, acts of God, fires, casualties, floods, earthquakes, acts of war, strikes, lockouts, epidemics, destruction of production facilities, riots, insurrections, walkouts, government order, power failures, and other disasters.
		</p>
		<p class="etc_con_tit">No waiver & Severability</p>
		<p class="etc_con_p">
			The failure of either Customer or AnySupport in any one or more instance(s) to insist upon strict performance of any of the terms of this Agreement will not be construed as a waiver or relinquishment of the right to assert or rely upon any such term(s) on any future occasion(s). In the event that any provision of this Agreement (or portion thereof) is determined by a court of competent jurisdiction to be invalid or otherwise unenforceable, such provision (or part thereof) shall be enforced or, incapable of such enforcement, shall be deemed to be deleted from this Agreement, while the remainder of this Agreement shall continue to remain in full force and effect according to its stated terms and conditions.
		</p>
	</div>
</body>
</html>