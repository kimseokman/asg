<?
$wz['pid']  = "10";
$wz['gtt']  = "";
$wz['gtt02']  = "Overview";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
<!-- 내용시작 -->
<div id="content">
	<ul class="overview_list_ul">
		<li>
			<a href="#overview_con01" class="arctic_scroll">
				<div class="overview_list_div">
					<img src="/index/en/img/overview_top_list_box01.gif" alt="" />
					<p class="overview_box_tit">AnySupport</p>
					<dl class="overview_dl">
						<dt>PC to PC</dt>
						<dd>
							Remotely access the customer’s PC<br />
							screen to directly view and control it.
						</dd>
					</dl>
				</div>
				<div class="overview_hover_bg">
					<img src="/index/en/img/overview_top_list_box01_hover.png" alt="" />
				</div>
			</a>
		</li>
		<li>
			<a href="#overview_con02" class="arctic_scroll">
				<div class="overview_list_div">
					<img src="/index/en/img/overview_top_list_box02.gif" alt="" />
					<p class="overview_box_tit">Mobile Edition</p>
					<dl class="overview_dl">
						<dt>PC to Smart devices</dt>
						<dd>
							Mobile Edition supports to users control and<br />
							fix any smart devices ‘s problems anywhere 
						</dd>
					</dl>
				</div>
				<div class="overview_hover_bg">
					<img src="/index/en/img/overview_top_list_box02_hover.png" alt="" />
				</div>
			</a>
		</li>
		<li class="last">
			<a href="#overview_con03" class="arctic_scroll">
				<div class="overview_list_div">
					<img src="/index/en/img/overview_top_list_box03.gif" alt="" />
					<p class="overview_box_tit">Video Edition</p>
					<dl class="overview_dl02">
						<dt>Shareing real-time video</dt>
						<dd>
							Video Edition can easily figure out what user<br />
							have problem as shareing real time video of<br />
							that issue.
						</dd>
					</dl>
				</div>
				<div class="overview_hover_bg">
					<img src="/index/en/img/overview_top_list_box03_hover.png" alt="" />
				</div>
			</a>
		</li>
	</ul>
	
	<!-- overview content -->
	<div class="overview_common_div" id="overview_con01">
		<p class="overview_bottom_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/en/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
		<div class="overview_left_div">
			<img src="/index/en/img/overview_con_img01.png" alt="" class="overview_con_img"/>
			<div class="overview_440">
				<p class="overview_con_tit01">Easy</p>
				<ul class="overview_circle_ul">
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_anysupport_circle01.gif" alt="" /></dt>
							<dd>Connecting</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_anysupport_circle02.gif" alt="" /></dt>
							<dd>no set up</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_anysupport_circle03.gif" alt="" /></dt>
							<dd>User interfaces</dd>
						</dl>
					</li>
				</ul>
				<p class="overview_con_p">
					AnySupport provides<br />
					easy to connect, a simple<br />
					setup process and intuitive user interface.<br />
					These superlative features change your<br />
					work more easy
				</p>
			</div>
		</div>
		<div class="overview_right_div">
			<div class="overview_440">
				<dl class="overview_con_color_tit01">
					<dt>AnySupport</dt>
					<dd>
						User can diagnose and solve problemes<br />
						with sharing  custimers’ PC screen anytime, anywhere
					</dd>
				</dl>
				<ul class="overview_check_ul">
					<li>Sharing PC</li>
					<li>Poweful security</li>
					<li>File transfering</li>
					<li>Support multi OS</li>
					<li>Support multi-Sesstion</li>
					<li>Statistics and management</li>
					<li> Recording remote support</li>
					<li>Invite supporters</li>
					<li>Auto-Reconnect</li>
				</ul>
				<p class="overview_con_link"><a href="/index/en/product/anysupport.php">See more detail</a></p>
			</div>
		</div>
	</div>

	<div class="overview_common_div" id="overview_con02">
		<p class="overview_top_arrow"><a href="#overview_con01" class="arctic_scroll"><img src="/index/en/img/overview_con_arrow_top.png" alt="top" /></a></p>
		<p class="overview_bottom_arrow"><a href="#overview_con03" class="arctic_scroll"><img src="/index/en/img/overview_con_arrow_bottom.png" alt="bottom" /></a></p>
		<div class="overview_left_div">
			<div class="overview_440">
				<dl class="overview_con_color_tit02">
					<dt>Mobile Edition</dt>
					<dd>
							For using Mobile Edition is only required download App into<br />
							user smart devices Then user can get remote service <br />
							without any bothering such wating time
					</dd>
				</dl>
				<ul class="overview_check_ul">
					<li>Control Smat devices </li>
					<li>Multi smart device </li>
					<li>Stability network</li>
					<li>Support Android / iOS </li>
					<li>File Transfer</li>
					<li>Powerful Security</li>
				</ul>
				<p class="overview_con_link"><a href="/index/en/product/mobile_edition.php">See more detail</a></p>
			</div>
		</div>
		<div class="overview_right_div">
			<img src="/index/en/img/overview_con_img02.png" alt="" class="overview_con_img"/>
			<div class="overview_440">
				<p class="overview_con_tit02">Fast</p>
				<ul class="overview_circle_ul">
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_mobileedition_circle01.gif" alt="" /></dt>
							<dd> To set up</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_mobileedition_circle02.gif" alt="" /></dt>
							<dd>To connect session</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_mobileedition_circle03.gif" alt="" /></dt>
							<dd>Without time gap</dd>
						</dl>
					</li>
				</ul>
				<p class="overview_con_p">
					Mobile Edition makes<br />
					you save time time through<br />
					the fast set up, connecting session,<br />
					without no traffic time. 
				</p>
			</div>
		</div>
	</div>

	<div class="overview_common_div" id="overview_con03">
	<p class="overview_top_arrow"><a href="#overview_con02" class="arctic_scroll"><img src="/index/en/img/overview_con_arrow_top.png" alt="top" /></a></p>
		<div class="overview_left_div">
			<img src="/index/en/img/overview_con_img03.png" alt="" class="overview_con_img"/>
			<div class="overview_440">
				<p class="overview_con_tit03">Effective</p>
				<ul class="overview_circle_ul">
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_videoedition_circle01.gif" alt="" /></dt>
							<dd>Work togather</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_videoedition_circle02.gif" alt="" /></dt>
							<dd>Smart devices</dd>
						</dl>
					</li>
					<li>
						<dl class="overview_circle_dl">
							<dt><img src="/index/en/img/overview_videoedition_circle03.gif" alt="" /></dt>
							<dd>Functions</dd>
						</dl>
					</li>
				</ul>
				<p class="overview_con_p">
					A lot of functions of Video Edition such as<br />
					drawing, chatting and recordng in-session make more<br />
					effective remote support with real time video      
				</p>
			</div>
		</div>
		<div class="overview_right_div">
			<div class="overview_440">
				<dl class="overview_con_color_tit03">
					<dt>Video Edition</dt>
					<dd>
						To share real-time video with clear voice<br />
						maximizes work efficiency. Futhermore customer can<br />
						save the cost and time to work 
					</dd>
				</dl>
				<ul class="overview_check_ul">
					<li>Use camera in smartdevice </li>
					<li>Auto focus</li>
					<li>Flash ON/OFF</li>
					<li>Annotation</li>
					<li>Support internet voice call</li>
					<li>Recording and save video </li>
				</ul>
				<p class="overview_con_link"><a href="/index/en/product/video_edition.php">See more detail</a></p>
			</div>
		</div>

	</div>
	<!-- overview content -->
	<?
	include_once("../allinone.php");
	?>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>