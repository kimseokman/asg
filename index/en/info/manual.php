<?
$wz['pid']  = "40";
$wz['lid']  = "20";
$wz['gtt']  = "resources";
$wz['gtt02']  = "Manual";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
<!-- 내용시작 -->
<div class="product_content02">
	<h3 class="sub_common_h3">Manual</h3>
	<ul class="sub_a_link_ul04">
		<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/en/info/faq.php">FAQ</a></li>
		<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/en/info/manual.php">Manual</a></li>
		<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/en/info/partners.php">Partners</a></li>
	</ul>

	<ul class="manual_list_ul">
		<li>
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Step-by-step Guide<br />
						for Support Reps
					</dd>
				</dl>
			</a>
		</li>
		<li>
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Step-by-step Guide<br />
						for Mobile Edition 
					</dd>
				</dl>
			</a>
		</li>
		<li class="last">
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Step-by-step Guide<br />
						for Video Edition
					</dd>
				</dl>
			</a>
		</li>
		<li>
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Step-by-step Guide<br />
						for User manual
					</dd>
				</dl>
			</a>
		</li>
		<li>
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Quick start guide
					</dd>
				</dl>
			</a>
		</li>
		<li class="last">
			<a href="#">
				<dl class="manual_dl">
					<dt></dt>
					<dd>
						Step-by-step Guide<br />
						for Customer
					</dd>
				</dl>
			</a>
		</li>
	</ul>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>