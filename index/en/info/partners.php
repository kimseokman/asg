<?
$wz['pid']  = "40";
$wz['lid']  = "30";
$wz['gtt']  = "resources";
$wz['gtt02']  = "Partners";

include_once("../header.php");
include_once("../black_bar.php");
include_once("../location.php");
?>
<!-- 내용시작 -->
<div class="product_content02">
	<h3 class="sub_common_h3">Partners</h3>
	<ul class="sub_a_link_ul05">
		<li <?if($wz['lid']=="10"){?>class="on"<?}?>><a href="/index/en/info/faq.php">FAQ</a></li>
		<li <?if($wz['lid']=="20"){?>class="on"<?}?>><a href="/index/en/info/manual.php">Manual</a></li>
		<li <?if($wz['lid']=="30"){?>class="on"<?}?>><a href="/index/en/info/partners.php">Partners</a></li>
	</ul>
	<p class="partners_txt_p">
		AnySupport solution can be used by technician,engineers and sale people in any kinds of fields to<br />
		provide speedy and quality service. Accountants, computer technicians, IT Helpdesk associates,<br />
		insurance agents, teachers, more. Achieve your goals with Anysupport.
	</p>

	<div class="partners_con_dl">
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img01.gif" alt="SAMSUNG" /></dt>
			<dd>
				<p class="partners_right_p">SAMSUNG</p>
				<dl class="partners_right_dl">
					<dt>AnySupport</dt>
					<dd>
						Anysupport provides remote control systmes to Samsungs’ customers for<br />
						resolving any kinds of technical issues when they use desktop or lap top<br />
						from Samsung Electronics
					</dd>
				</dl>
			</dd>
		</dl>
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img02.gif" alt="IBM" /></dt>
			<dd>
				<p class="partners_right_p">IBM</p>
				<dl class="partners_right_dl">
					<dt>AnySupport</dt>
					<dd>
						AnySupport remote controling technics assists to slove IT problematic situat-<br />
						ions for IBM computers’ customers  
					</dd>
				</dl>
			</dd>
		</dl>
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img03.gif" alt="LG CNS" /></dt>
			<dd>
				<p class="partners_right_p">LG CNS</p>
				<dl class="partners_right_dl">
					<dt>Video Edition</dt>
					<dd>
						The Video Edtion that has transnffering realtime video with remote controling<br />
						applies to Telemedicine servise in US, Canada, and Vietnam. 
					</dd>
				</dl>
			</dd>
		</dl>
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img04.gif" alt="HYUNDAI" /></dt>
			<dd>
				<p class="partners_right_p">HYUNDAI</p>
				<dl class="partners_right_dl">
					<dt>AnySupport</dt>
					<dd>AnySupport provides service to solve IT issues for Hyundai subsidiary company.</dd>
				</dl>
			</dd>
		</dl>
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img05.gif" alt="Standard Charted" /></dt>
			<dd>
				<p class="partners_right_p">Standard Charted</p>
				<dl class="partners_right_dl">
					<dt>Video, remote sharing, control</dt>
					<dd>
						In this csae our technical supporting is used an impersonal banking is required<br />
						some technology such as transffeing realtime video, remote sharing and conroling
					</dd>
				</dl>
			</dd>
		</dl>
		<dl class="partners_dl">
			<dt><img src="/index/en/img/partner_list_img06.gif" alt="Doosan" /></dt>
			<dd>
				<p class="partners_right_p">Doosan</p>
				<dl class="partners_right_dl">
					<dt>AnySupport</dt>
					<dd>AnySupport assist to Doosan companys’ affiliates for settling IT issues.</dd>
				</dl>
			</dd>
		</dl>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>