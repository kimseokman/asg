<?
include_once("../header02.php");

$pw_min = PW_MIN_LENGTH;
$pw_max = PW_MAX_LENGTH;
?>
<!-- 내용시작 -->
<div class="login_content">
	<dl class="lolgin_dl">
		<dt><span class="login_dl_big_span">A</span>NYSUPPORT</dt>
		<dd>
			Reset your new passward.
			<p class="login_dl_small_span02">Password must be included <? echo $pw_min; ?>-<? echo $pw_max; ?>characters and number </p>
		</dd>
	</dl>
	<div class="login_con_div02">
		<form name="form_reset_pw" method="post" action="/index/_func/function.pw_reset.php">
			<input type="hidden" name="code" value="<? echo $_POST['code']; ?>" />
			<p class="login_img_p"><img src="/index/en/img/login_lock_img.png" alt="login lock" /></p>
			<ul class="login_input_ul">
				<li><input type="password" name="reset_pw" id="" value="" placeholder="Enter your new password" class="login_input"/></li>
				<li class="last"><input type="password" name="reset_retype_pw" id="" value="" placeholder="Re-enter your password" class="login_input"/></li>
			</ul>
			<div class="login_btn_area02">
				<p class="common_blue_btn password_reset_btn"><a href="#">Enter</a></p>
			</div>
		</form>
	</div>
</div>
<?
	include_once("../common_quick.php");
?>
<!-- 내용끝 -->

<?
include_once("../footer02.php");
?>