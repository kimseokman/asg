<?
include_once("./header.php");
?>
<!-- 내용시작 -->
<!-- main visual -->
<script type="text/javascript" src="/index/en/js/jquery.bxslider.js"></script>
<div id="slider"> 
	<div class="slide-wrap">
		<div class="visual_search_con">
			<div class="visual_search">
				<dl class="main_visual_search">
					<dt><input type="text" name="start_free_trial_mail" id="" value="" placeholder="Enter your Email" /></dt>
					<dd><a href="#" class="visual_free_trial_btn">START FREE TRIAL</a></dd>
				</dl>
			</div>
		</div>
		<ul class="bxslider">
			<li class="main_visual_li01">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>Excellence in remote support</dt>
						<dd>
							AnySupport provides fast, secure and easy remote
							support solution.<br /> Remotely access, diagnose and 
							fix computers and  mobile devices <br />  with AnySupport.
						</dd>
					</dl>
				</div>
			</li>
			<li class="main_visual_li02">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>AnySupport Mobile Edition</dt>
						<dd>
							Access to customer’s mobile devices from your PC.
							<!--<span class="main_visual_italic">-->Easiest application<br /> download, Fast service, Superior 
							customers saticfaction.<!--</span>-->
						</dd>
					</dl>
				</div>
			</li>
			<li class="main_visual_li03">
				<div class="main_visual_con_div">
					<dl class="main_visual_dl">
						<dt>AnySupport Video Edition</dt>
						<dd>
							Share the real time video that is shown your customer
							is experiencing<br /> some problemetic situation.  
						</dd>
					</dl>
				</div>
			</li>
		</ul>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.bxslider').bxSlider();
		});
	</script>
</div>

<!-- main block bar -->
<div class="common_black_bar">
	<div class="black_bar_1080">
		<dl class="black_dl01">
			<dt>Our technology support your business succeed. Try and  Enjoy!</dt>
			<dd>
				<ul class="black_btn">
					<li class="black_btn_orther visual_free_trial_btn"><a href="/index/en/free/free_form.php">FREE TRIAL</a></li>
					<!--li><a href="/index/en/buy/buy_form.php">BUY IT NOW</a></li-->
				</ul>
			</dd>
		</dl>
	</div>
</div>
<!-- main block bar -->

<div class="main_underline_div">
	<div id="main_common_content">
		<ul class="main_middle_con">
			<li>
				<div class="main_middle_img">
					<dl class="main_middle_img_dl">
						<dt>
							<img src="/index/en/img/main_top_img01.png" alt="" />
							<div class="main_middle_hover_div">
								<dl class="middle_label_dl">
									<dt>$45.99</dt>
									<dd>PER MONTH</dd>
								</dl>
								<p class="middle_hover_p"><img src="/index/en/img/main_top_right_img.png" alt="hot deal" /></p>
							</div>
						</dt>
						<dd class="main_middle_bg01">AnySupport  1+2</dd>
					</dl>
				</div>
				<dl class="main_middle_dl">
					<dt><img src="/index/en/img/main_top_img03.gif" alt="" /></dt>
					<dd>
						<span class="main_middle_dl_span">Buy one get two free</span><br /><br />
						Buy one account then you will get two additional<br />
						accounts.<a href="/index/en/pricing/pricing.php" class="main_middle_blue_link">Learn more+</a> 
					</dd>
				</dl>
			</li>
			<li class="last">
				<div class="main_middle_img">
					<dl class="main_middle_img_dl">
						<dt>
							<img src="/index/en/img/main_top_img02.png" alt="" />
							<div class="main_middle_hover_div">
								<dl class="middle_label_dl">
									<dt>$59.99</dt>
									<dd>PER MONTH</dd>
								</dl>
								<p class="middle_hover_p"><img src="/index/en/img/main_top_right_img.png" alt="hot deal" /></p>
							</div>
						</dt>
						<dd class="main_middle_bg02"> AnySupport ‘All in one’</dd>
					</dl>
				</div>
				<dl class="main_middle_dl">
					<dt><img src="/index/en/img/main_top_img04.gif" alt="" /></dt>
					<dd>
						<span class="main_middle_dl_span">Buy all in one package</span><br /><br />
						Anysuport provides all three products as one<br />
						package (pc, mobile, video )<a href="/index/en/pricing/pricing.php" class="main_middle_blue_link">Learn more+</a> 
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>

<!-- middle visual -->
<script type="text/javascript" src="/index/en/js/jquery.bxslider02.js"></script>
<div id="slider02"> 
	<div class="slide-wrap02">
		<ul class="bxslider02">
			<li>
				<div class="middle_slide_div01">
					<p class="middle_slide_icon"><img src="/index/en/img/main_middle_visual_icon01.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							Save time<br />
							with AnySupport
						</dt>
						<dd>
							As user have some problems in thire PC<br />
							or smartdevices,they do not need to go to<br />
							the service center because AnySupport<br />
							provides perfect remote service.
						</dd>
					</dl>
				</div>
				<img src="/index/en/img/main_middle_visual_01.png" alt="" />
			</li>
			<li>
				<div class="middle_slide_div02">
					<p class="middle_slide_icon"><img src="/index/en/img/main_middle_visual_icon02.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							Support<br />
							smart devices 
						</dt>
						<dd>
							The technology of AnySupport provides diverse<br />
							platform for smart devices include tablet.   
						</dd>
					</dl>
				</div>
				<img src="/index/en/img/main_middle_visual_02.png" alt="" />
			</li>
			<li>
				<div class="middle_slide_div03">
					<p class="middle_slide_icon"><img src="/index/en/img/main_middle_visual_icon03.png" alt="" /></p>
					<dl class="middle_visual_dl">
						<dt>
							Resolve<br />
							with Realtime video
						</dt>
						<dd>
							As user have some pc or smart devices proble<br />
							ms,they do not need to go to the service center<br />
							becasuse AnySupport provides perfect service<br />
							environment remotly.
						</dd>
					</dl>
				</div>
				<img src="/index/en/img/main_middle_visual_03.png" alt="" />
			</li>
		</ul>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider02').bxSlider02();
	});
</script>
<!-- middle visual -->

<div class="main_gray_div">
	<div class="main_gray_content">
		<dl class="gray_dl">
			<dt>Why use AnySupport?</dt>
			<dd>We provide high quality service with lower price</dd>
		</dl>
		<ul class="gray_con_ul">
			<li>Multi-Devices capability</li>
			<li>
				Perfect Hit ratio to access
				<div class="gray_ul_percent">
					99.9%
				</div>
			</li>
			<li>Simultaneous multi session support</li>
			<li>Poweful Security</li>
			<li>Fast and Efficient<br />
				<span class="normal_gray">
					-Easy Access<br />
					-No Time Gap 
				</span>
			</li>
			<li>Reasonable prices </li>
		</ul>
		<dl class="gray_icon_dl">
			<dt>
				<ul class="gray_icon_ul">
					<li><img src="/index/en/img/main_middle_icon01.gif" alt="크롬" /></li>
					<li><img src="/index/en/img/main_middle_icon02.gif" alt="파이어폭스" /></li>
					<li><img src="/index/en/img/main_middle_icon03.gif" alt="오페라" /></li>
					<li><img src="/index/en/img/main_middle_icon04.gif" alt="사파리" /></li>
					<li><img src="/index/en/img/main_middle_icon05.gif" alt="익스플로러" /></li>
					<li class="phone_icon"><img src="/index/en/img/main_middle_icon06.gif" alt="안드로이드" /></li>
					<li><img src="/index/en/img/main_middle_icon07.gif" alt="애플" /></li>
				</ul>
			</dt>
			<dd><a href="/index/en/product/anysupport.php">See more about features+ </a></dd>
		</dl>
	</div>
</div>

<div id="main_common_content">
	<div class="main_bottom_content">
		<p class="main_bottom_p">Who is using AnySupport?</p>
		<ul class="main_bottom_ul">
			<li>
				<dl class="main_bottom_dl">
					<dt>Help Desk</dt>
					<dd>
						Most of customer service center that use AnySupport remote service would provide a high qiality service<br />
						without spending time to visit customers. AnySupport allows our user to save time and cost by Anysuppo-<br />
						rt’s remote control.               
					</dd>
				</dl>
			</li>
			<li>
				<dl class="main_bottom_dl">
					<dt>Software, System development company</dt>
					<dd>
						Software and system development companies need to offer an efficient assurance services. Anysupp-<br />
						ort can help to resolve the problematic issuses directly and quickly.These bebefits give a lot of advanatages<br />
						fot thire customer. Futhuremor, they would gives creadits for thire customers,                 
					</dd>
				</dl>
			</li>
			<li>
				<dl class="main_bottom_dl">
					<dt>IT Managers</dt>
					<dd>
						AnySupport is a valuable tool for management of sever system in IT company because AnySupport is ev-<br />
						en without leaving your seat , It is not only possible to remote support but whole system support  and ma-<br />
						intenance on the spot.           
					</dd>
				</dl>
			</li>
		</ul>
	</div>
</div>

<!-- main bottom banner -->
<div class="bottom_banner">
	<ul class="bottom_banner_ul">
		<li><img src="/index/en/img/main_bottom_banner_img01.gif" alt="SAMSUNG" /></li>
		<li><img src="/index/en/img/main_bottom_banner_img02.gif" alt="standard Chartered" /></li>
		<li><img src="/index/en/img/main_bottom_banner_img03.gif" alt="IBM" /></li>
		<li><img src="/index/en/img/main_bottom_banner_img04.gif" alt="HYUNDAI" /></li>
		<li><img src="/index/en/img/main_bottom_banner_img05.gif" alt="LOTTECARD" /></li>
		<li class="last"><img src="/index/en/img/main_bottom_banner_img06.gif" alt="LG CNS" /></li>
	</ul>
</div>
<!-- main bottom banner -->

<!-- 내용끝 -->

<?
include_once("./footer.php");
?>