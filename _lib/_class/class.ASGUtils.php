<?php
/**
 * Description
 * 	ASGUtils()							생성자
 * 	GetCurrentPageURL()						현재 페이지 URL 구하기
 *  	RedirectURL($req_url)						해당 URL로 리다이렉트
 *  	GetAccessAuth()						해당 URL에 대한 사용자 권한 구하기
 *  	GetLangFromURL($url)						intro 페이지에서 현재 언어 구하기(intro 에서 사용)
 *  	GetSiteLang()							Session에서 언어를 String 형태로 반환(admin 에서 사용)
 *  	GetRequestURL()						Request URL 구하기
 *  	GetIndexPart($url)						현재 URL 기준으로 Index Part 나누기
 *  	GetAdminPart($url)						현재 URL 기준으로 Admin Part 나누기
 *  	GetIsEven($number)						해당 숫자 짝수 인지 판단
 *  	GetProduct($function)						상품 권한으로 상품 문자열 구하기
 *   	GetCodeProduct($code)						상품 코드로 상품 이름 얻기
 *    	GetCodeTechnician($code)					상품 코드로 상담원 볼륨 얻기
 *  	GetLangs()							정의된 언어 목록 구하기
 *  	SetBasePage()							User Type에 따른 기본 페이지 설정
 *  	GetReportPage()						URL로 Report 페이지 분기
 *  	GetCurrentUserType()						현재 User Type 구하기
 *  	GetSearchDate($constant_date_type)				정의된 날짜 상수 type 따른 날짜 구하기
 *  	GetYears()							year 목록 구하기
 *  	GetMonths($maxMonth=12)					month 목록 구하기(max month 는 default 12월)
 *  	GetDays($maxDay)						1일부터 maxDay 까지 day 목록 구하기
 *  	GetLastDay($base_m, $base_y)					해당 년, 월말 일 구하기
 *  	PrintDisabled($flag)						disabled 문자열 리턴
 *  	PrintSelected($base, $val)					selected 문자열 리턴
 *  	PrintSelectedLang($base_c = "en", $val_c = "en")			언어 match 후 selected 문자열 리턴
 *  	PrintSelectedCurrentYear($val_y)				현재 해 비교 후 selected 문자열 리턴
 *  	PrintSelectedCurrentMonth($val_m)				현재 월 비교 후 selected 문자열 리턴
 *  	PrintSelectedCurrentDay($val_d)					현재 일 비교 후 selected 문자열 리턴
 *  	GetSearchType()						상수로 정의된 Search Type List 리턴
 *  	GetOperationLog($oper_type)					상수로 정의된 Operation log 타입에 따른 문자열 리턴
 *  	GetOS($os_type)						상수로 정의된 OS 타입에 따른 문자열 리턴
 *  	GetSatisfaction($satisfaction_type)				상수로 정의된 만족도 타입에 따른 문자열 리턴
 *  	IsValidID($str)							ID 문자열 validate
 *  	IsValidPassword($str)						Password 문자열 validate
 *  	IsValidEmail($str)						Email 문자열 validate
 *  	
 * @ KSM | 2015.06.22 | For AnySupportGlobal
 */

class ASGUtils{

	public function ASGUtils(){
		//empty
	}

	public function GetCurrentPageURL() {// get current page url
	 	$pageURL = 'http';
	 	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 	$pageURL .= "://";
	 	if ($_SERVER["SERVER_PORT"] != "80") {
	 		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 	} else {
	 		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 	}
 	
 		return $pageURL;
	}

	public function RedirectURL($req_url){
		header("Location: ".$req_url);
		exit(0);
	}

	public function GetAccessAuth(){// get site Auth
		$request_url = $_SERVER["REQUEST_URI"];

		$web_access_auth = "All";

		if(strpos($request_url, 'admin/manage') || strpos($request_url, 'admin/notice') || strpos($request_url, 'admin/reports')){
			$web_access_auth = "Only Admin";
		}else if(strpos($request_url, 'admin/supporter_index')){	
			$web_access_auth = "Only Supporter";
		}else{
			$web_access_auth = "All";
		}

		return $web_access_auth;
	}

	public function GetLangFromURL($intro_url){// get language in use index page
		$split_url = explode("/", $intro_url);
		return $split_url[4];
	}

	public function GetSiteLang(){//get site language in use admin page
		if($_SESSION['language'] == 'en'){
			return 'English';
		}else if($_SESSION['language'] == 'kr'){
			return 'Korean';
		}else if($_SESSION['language'] == 'jp'){
			return 'Japanese';
		}else{
			return 'NO_SESSION';
		}
	}

	public function GetRequestURL(){// use admin page
		return $_SERVER["REQUEST_URI"];
	}

	public function GetIsEven($number){
		$number%=2;
		if($number > 0){//isOdd
			return FALSE;
		}else{//isEven
			return TRUE;
		}
	}

	public function GetCodeProduct($code){//get product
		if($code == CODE_ALL_IN_ONE){
			return "Anysupport All In One";
		}
		else if($code == CODE_PC){
			return "Anysupport 1+2";
		}

		return "UNKNOWN";
	}

	public function GetCodeTechnician($code){//get product
		if($code == CODE_ALL_IN_ONE){
			return 1;
		}
		else if($code == CODE_PC){
			return 3;
		}

		return "UNKNOWN";
	}

	public function GetProduct($function){//get product
		if($function & PERMISSION_ALL_IN_ONE){
			return "Anysupport All In One";
		}else{
			return "Anysupport PC";
		}

		return "UNKNOWN";
	}

	public function GetLangs(){//get constant :: ASG_LANGUAGE
		return eval('return ' . ASG_LANGUAGE . ';');
	}

	public function SetBasePage($type){
		if($type == "ADMIN"){
			return ADMIN_INDEX;
		}else if($type == "SUPPORTER"){
			return SUPPORTER_INDEX;
		}else if($type == "GUEST"){
			return GUEST_INDEX;
		}
	}

	public function GetReportPage(){
		$request_url = $_SERVER["REQUEST_URI"];
		$request_tag = explode("/", $request_url);
		
		return $request_tag[3];
	}

	public function GetCurrentUserType(){// 2015.06.05 | KSM | Check User
		$user = array(
			"type" => "GUEST",
			"id" => "GUEST"
		);

		if($_SESSION["usertype"] != ""){
			$user['type'] = $_SESSION["usertype"];

			if($user['type'] == "ADMIN" || $_SESSION["usertype"] != "SUPERADMIN"){
				$user['id'] = $_SESSION["adid"];
			}else if($user['type'] == "SUPPORTER"){
				$user['id'] = $_SESSION["sptid"];
			}
		}
		
		return $user;
	}

	public function GetSearchDate($constant_date_type){
		$date_arr = array();
		$type_str = explode('_', $constant_date_type);

		if($type_str[0] == "0"){
			$date_arr["Y"] = date("Y");
			$date_arr["M"] = date("m");
			$date_arr["D"] = date("d");
		}else{
			if($type_str[1] == "DAY"){
				$strtotime_sub_str = "days";	
			}else if($type_str[1] == "MONTH"){
				$strtotime_sub_str = "month";
			}else if($type_str[1] == "YEAR"){
				$strtotime_sub_str = "year";
			}

			$strtotime_str = "-".$type_str[0]." ".$strtotime_sub_str;
			
			$date_arr["Y"] = date('Y', strtotime($strtotime_str));
			$date_arr["M"] = date("m", strtotime($strtotime_str));
			$date_arr["D"] = date("d", strtotime($strtotime_str));
		}

		return $date_arr;
	}

	public function GetYears(){
		$minYear = SEARCH_START_YEAR;
		$maxYear = date("Y");
		$year_arr = array();

		$year_arr[2012] = 2012;	//hard code
		$year_arr[2013] = 2013;	//hard code
		$year_arr[2014] = 2014;	//hard code

		for($i=$minYear; $i <= $maxYear; $i++){
			$year_arr[$i] = $i;	
		}
		
		return $year_arr;
	}

	public function GetMonths($maxMonth=12){
		$minMonth = 1;
		$const_month_en_list = eval('return ' . MONTH_LIST_EN . ';');
		$month_arr = array();

		for($i=$minMonth; $i <= $maxMonth; $i++){
			if($_SESSION['language'] == 'en'){
				$month_arr[$i] = $const_month_en_list[$i];
			}else{
				$month_arr[$i] = $i;
			}
		}
		
		return $month_arr;
	}

	public function GetDays($maxDay){
		$minDay = 1;
		$day_arr = array();

		for($i=$minDay; $i <= $maxDay; $i++){
			$day_arr[$i] = $i;	
		}

		return $day_arr;
	}

	public function GetLastDay($base_m, $base_y){
		$lastDay = date("t", mktime(0, 0, 0, $base_m,1, $base_y));

		return $lastDay;	
	}

	public function PrintDisabled($flag){
		if($flag == TRUE){
			return "disabled";
		}
		return "";
	}

	public function PrintSelected($base, $val){
		if($base == $val){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedLang($base_c = "en", $val_c = "en"){
		if($base_c == $val_c){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentYear($val_y){
		$thisYear = date("Y");
		if($thisYear == $val_y){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentMonth($val_m){
		$thisMonth = date("m");
		if($thisMonth == $val_m){
			return "selected";
		}

		return "";
	}

	public function PrintSelectedCurrentDay($val_d){
		$thisDay = date("d");
		if($thisDay == $val_d){
			return "selected";
		}

		return "";
	}

	public function GetSearchType(){
		if($_SESSION['language'] == 'en'){
			return eval('return ' . SEARCH_TYPE_LIST_EN . ';');	
		}else if($_SESSION['language'] == 'jp'){
			return eval('return ' . SEARCH_TYPE_LIST_JP . ';');	
		}else if($_SESSION['language'] == 'kr'){
			return eval('return ' . SEARCH_TYPE_LIST_KR . ';');
		}else{
			return eval('return ' . SEARCH_TYPE_LIST_EN . ';');	
		}
		
	}

	public function GetOperationLog($oper_type){
		if($_SESSION['language'] == 'en'){
			$oper_log = UNCHECKED_EN;
		}else if($_SESSION['language'] == 'jp'){
			$oper_log = UNCHECKED_JP;
		}else if($_SESSION['language'] == 'kr'){
			$oper_log = UNCHECKED_KR;
		}
		
		if($oper_type == 1){
			if($_SESSION['language'] == 'en'){
				$oper_log = RESOLVE_EN;
			}else if($_SESSION['language'] == 'jp'){
				$oper_log = RESOLVE_JP;
			}else if($_SESSION['language'] == 'kr'){
				$oper_log = RESOLVE_KR;
			}	
		}else if($oper_type == 2){
			if($_SESSION['language'] == 'en'){
				$oper_log = HOLD_EN;
			}else if($_SESSION['language'] == 'jp'){
				$oper_log = HOLD_JP;
			}else if($_SESSION['language'] == 'kr'){
				$oper_log = HOLD_KR;
			}	
		}else if($oper_type == 3){
			if($_SESSION['language'] == 'en'){
				$oper_log = NEED_DIRECT_SUPPORT_EN;
			}else if($_SESSION['language'] == 'jp'){
				$oper_log = NEED_DIRECT_SUPPORT_JP;
			}else if($_SESSION['language'] == 'kr'){
				$oper_log = NEED_DIRECT_SUPPORT_KR;
			}	
		}else{
			if($_SESSION['language'] == 'en'){
				$oper_log = UNCHECKED_EN;
			}else if($_SESSION['language'] == 'jp'){
				$oper_log = UNCHECKED_JP;
			}else if($_SESSION['language'] == 'kr'){
				$oper_log = UNCHECKED_KR;
			}
		}

		return $oper_log;
	}

	public function GetOS($os_type){
		$os_str = OS_UNKNOWN_EN;
		if($_SESSION['language'] == 'kr'){
			$os_str = OS_UNKNOWN_KR;
		}

		if($os_type == 1){
			$os_str = OS_WIN_98_EN;
		}else if($os_type == 2){
			$os_str = OS_WIN_2000_EN;
		}else if($os_type == 3){
			$os_str = OS_WIN_XP_EN;
		}else if($os_type == 4){
			$os_str = OS_WIN_2003_EN;
		}else if($os_type== 5){
			$os_str = OS_WIN_VISTA_EN;
		}else if($os_type == 6){
			$os_str = OS_WIN_7_EN;
		}else if($os_type == 7){
			if($_SESSION['language'] == 'kr'){
				$os_str = OS_UNKNOWN_KR;
			}else{
				$os_str = OS_UNKNOWN_EN;
			}
		}else if($os_type == 8){
			$os_str = OS_WIN_8_EN;
		}else if($os_type == 9){
			$os_str = OS_WIN_2008_EN;
		}else if($os_type == 10){
			$os_str = OS_WIN_2012_EN;
		}else if($os_type == 100){
			$os_str = OS_ANDROID_EN;
		}else if($os_type == 101){
			$os_str = OS_MAC_EN;
		}

		return $os_str;
	}

	public function GetSatisfaction($satisfaction_type){
		if($_SESSION['language'] == 'en'){
			$satisfaction_log = NO_ANSWER_EN;
		}else if($_SESSION['language'] == 'jp'){
			$satisfaction_log = NO_ANSWER_JP;
		}else if($_SESSION['language'] == 'kr'){
			$satisfaction_log = NO_ANSWER_KR;
		}

		if($satisfaction_type == 1){
			if($_SESSION['language'] == 'en'){
				$satisfaction_log = SATISFACTION_EN;
			}else if($_SESSION['language'] == 'jp'){
				$satisfaction_log = SATISFACTION_JP;
			}else if($_SESSION['language'] == 'kr'){
				$satisfaction_log = SATISFACTION_KR;
			}	
		}else if($satisfaction_type == 2){
			if($_SESSION['language'] == 'en'){
				$satisfaction_log = NORMAL_EN;
			}else if($_SESSION['language'] == 'jp'){
				$satisfaction_log = NORMAL_JP;
			}else if($_SESSION['language'] == 'kr'){
				$satisfaction_log = NORMAL_KR;
			}	
		}else if($satisfaction_type == 3){
			if($_SESSION['language'] == 'en'){
				$satisfaction_log = UNSATISFACTION_EN;
			}else if($_SESSION['language'] == 'jp'){
				$satisfaction_log = UNSATISFACTION_JP;
			}else if($_SESSION['language'] == 'kr'){
				$satisfaction_log = UNSATISFACTION_KR;
			}	
		}else{
			if($_SESSION['language'] == 'en'){
				$satisfaction_log = NO_ANSWER_EN;
			}else if($_SESSION['language'] == 'jp'){
				$satisfaction_log = NO_ANSWER_JP;
			}else if($_SESSION['language'] == 'kr'){
				$satisfaction_log = NO_ANSWER_KR;
			}
		}

		return $satisfaction_log;
	}

	public function GetIndexPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[5] == NULL){
			$inc_file_name = "index";
		}
		else{
			$inc_file_name = str_replace(".php", "", $url_split[5]);//remove .php
		}
		return $inc_file_name;
	}

	public function GetAdminPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[5] == NULL){
			$inc_file_name = "supporter_index";
		}
		else{	
			if($url_split[4] == "notice"){
				$inc_file_name = str_replace(".php", "", $url_split[4]);//remove .php
			}
			else{
				$inc_file_name = str_replace(".php", "", $url_split[4]."_".$url_split[5]);//remove .php	
			}
		}
		return $inc_file_name;
	}

	public function GetAgentPart($url){
		$url_split = explode("/", $url);

		$inc_file_name = "";
		if($url_split[4] == NULL){
			$inc_file_name = "index";
		}
		else{
			$inc_file_name = str_replace(".php", "", $url_split[4]);//remove .php
		}
		return $inc_file_name;
	}

	public function IsValidID($str){
		$id_length_min = ID_MIN_LENGTH;
		$id_length_max = ID_MAX_LENGTH;
		return ereg("(^[0-9a-zA-Z]{".$id_length_min.",".$id_length_max."}$)", $str);
	}

	public function IsValidPassword($str){
		
		$pw_length_min = PW_MIN_LENGTH;
		$pw_length_max = PW_MAX_LENGTH;
		return ereg("(^[0-9a-zA-Z]{".$pw_length_min.",".$pw_length_max."}$)", $str);
	}

	public function IsValidEmail($str){
		return ereg("(^[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+)*@[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*$)", $str);
	}
	
}