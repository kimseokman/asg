﻿<?
	$lang = array();	

	if(getCountry() == "ko") {
		$lang['NO_INPUT_PAGE1_2_H1'] = '애니서포트를 설치하여 주십시오.';
		$lang['NO_INPUT_PAGE1_2_H2'] = '원격지원을 위해서는 <span class="txt2">anysupport.zip</span>을 설치해야 합니다.';
		$lang['NO_INPUT_PAGE1_2_H3'] = '<span class="txt2">다운로드</span> 버튼을 클릭하여 프로그램을 설치 하십시오.';
	} else {
		$lang['NO_INPUT_PAGE1_2_H1'] = 'Please install the AnySupport.';
		$lang['NO_INPUT_PAGE1_2_H2'] = 'In order to receive remote support, you must install the  <span class="txt2">anysupport.zip</span> file.';
		$lang['NO_INPUT_PAGE1_2_H3'] = 'Please click the <span class="txt2">download</span> button and then install.';
	}

	function getCountry() {
		$country = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		if (strstr($country, "ko"))  $country = "ko";
		else if (strstr($country, "kr")) $country = "ko";
		else if (strstr($country, "ja")) $country = "ja";
		else if (strstr($country, "en")) $country = "en";
		else $country = "en";
		return $country;
	}
?>