<?
	include_once "info.php";
	include("../class/CLiveSupport.php");
	
	$clivesupport = new CLiveSupport();

	$name = $_POST["name"];
	$phone1 = $_POST["phone1"];
	$phone2 = $_POST["phone2"];
	$phone3 = $_POST["phone3"];
	$email = $_POST["email"];
	$save = $_POST["save"];
	
//	$accesscode = $_POST["accesscode"];
	$accesscode = preg_replace("/\s+/", "",$_POST["accesscode"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo $title ?></title>
<?
	if(!$clivesupport->IsAccesscodeValid($accesscode)) 
	{
		// livesupport_cs에 없는 accesscode
		echo "<script> alert('존재하지 않는 인증 암호입니다. 다시 시도해주세요.'); history.go(-1); </script>";
	}
	else 
	{
		$tel = $phone1."-".$phone2."-".$phone3;
		if($tel == "--") $tel = "";
		
		$res = $clivesupport->OpenCS($accesscode, $name, $tel, $email);
		
		if($res == "NOCS") 
		{
			// livesupport_cs에 없는 accesscode
			echo "<script> alert('존재하지 않는 인증 암호입니다. 다시 시도해주세요.'); history.go(-1); </script>";
		}
		else
		{
			
			$osver = $_SERVER['HTTP_USER_AGENT'];
			//echo "<script> alert('".$osver."'); history.go(-1); </script>";
			
			if(eregi('windows NT 6.0', $osver) || eregi('windows vista', $osver) || eregi('windows NT 6.1', $osver))
				echo "<script> document.location='./open.html?access=".$accesscode."&osver=vista'; </script>";
			else
				echo "<script> document.location='./open.html?access=".$accesscode."&osver=xp'; </script>";
		}
	}
?>
</head>
</html>