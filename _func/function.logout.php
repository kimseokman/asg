<?php
session_start();
/**
 * logout.php
 * 2015.06.05 | KSM | detroy session
 */
header('Content-Type: text/html; charset=utf-8');

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$index_page = "/index/".$_SESSION['language']."/index.php";

$asg_obj->Logout();

echo "
	<script>
		document.location='".$index_page."';
	</script>
";