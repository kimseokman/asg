<?php
/**
 * constants.php
 * 2015.06.09 | KSM | Constants
 */
//Define : User Type Index Page
define("ADMIN_INDEX", "/admin/manage/billing/billing_index.php");
define("SUPPORTER_INDEX", "/admin/supporter_index.php");
define("GUEST_INDEX", "/");

//Define : AnySupportGlobe Web Language List
define("ASG_LANGUAGE", var_export(
       array(
              "en"  => "English",
              "kr" => "Korean",
              "jp" => "Japanese"
       ), true
));

//Define : 메일 관련 설정
define("ASG_URL", "http://192.168.1.10");               //Base URL
define("ASG_MAIL_HOST", "mail.koino.net");                     //보내는 메일 서버
define("ASG_MAIL_PORT", 25);
define("ASG_MAILER", "smtp");
define("ASG_MAIL_CHARSET", "utf-8");
define("ASG_MAIL_ID", "global@koino.net");                     //보내는 메일 계정 아이디
define("ASG_MAIL_PW", "ko2470153ino");                       //보내는 메일 계정 비밀번호
define("ASG_MAIL_ADDR", "global@koino.net");             //관리자 메일
define("ASG_MAIL_NAME", "Anysupport");                       //관리자 메일 이름

//Define : 무료 사용 기간
define("FREE_DAYS", 30);

//Define : 고객 URL
define("DEFAULT_CUSTOMER_URL", "http://192.168.1.10/start");

//Define : 상품 permission
define("PERMISSION_DEFAULT", 252391);
define("PERMISSION_ALL_IN_ONE", 8388608);

//Define : 상품 code
define("CODE_PC", 524287);
define("CODE_ALL_IN_ONE", 8912895);

//Define : Agent Add Default Values
define("SETTING_DEFAULT", 1081);
define("MOBILE_SETTING_DEFAULT", 6);
define("MOBILE_EXPIRED_DATE_DEFAULT", "0000-00-00");

//Define : Trouble Type Max Row 
define("TROUBLE_MAX_LINE", 10);

//Define : ID
define("ID_MIN_LENGTH", 4);
define("ID_MAX_LENGTH", 13);

//Define : Password
define("PW_MIN_LENGTH", 4);
define("PW_MAX_LENGTH", 16);

//Define : Name
define("NAME_MAX_LENGTH", 30);//case by validate
define("NAME_MAX_LENGTH_KR", 15);
define("NAME_MAX_LENGTH_JP", 15);
define("NAME_MAX_LENGTH_EN", 30);

//Define : Phone
define("PHONE_MAX_LENGTH", 20);

//Define : Email
define("MAIL_ADDR_MAX_LENGTH", 50);//byte

//Define : Company
define("COMPANY_MAX_LENGTH", 30);//case by validate
define("COMPANY_MAX_LENGTH_KR", 15);
define("COMPANY_MAX_LENGTH_JP", 15);
define("COMPANY_MAX_LENGTH_EN", 30);

//Define : Department
define("DEPARTMENT_MAX_LENGTH", 30);//case by validate
define("DEPARTMENT_MAX_LENGTH_KR", 15);
define("DEPARTMENT_MAX_LENGTH_JP", 15);
define("DEPARTMENT_MAX_LENGTH_EN", 30);

//Define : Trouble Type
define("TROUBLE_MAX_LENGTH", 40);//case by validate
define("TROUBLE_MAX_LENGTH_KR", 20);
define("TROUBLE_MAX_LENGTH_JP", 40);
define("TROUBLE_MAX_LENGTH_EN", 40);
define("TROUBLE_NAME_EN", "Not selected");
define("TROUBLE_NAME_KR", "선택안됨");
define("TROUBLE_NAME_JP", "選択不可");

//Define : Search
define('CONST_SERVER_TIMEZONE', 'UTC');//server timezone
define('CONST_SERVER_DATEFORMAT', 'YmdHis');//server dateformat
define("SEARCH_MIN_DATE", "2000-01-01");
define("SEARCH_MAX_DATE", "2037-01-01");
define("SEARCH_START_YEAR", 2015);//start 2015
define("SEARCH_START_MONTH", 1);//start 01
define("SEARCH_START_DAY", 1);//start 01
define("MONTH_LIST_EN", var_export(
	array(
		1  => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 
       		5 => "May", 6 => "Jun", 7 => "Jul",8 => "Aug",
       		9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec"
       	), true
));
define("SEARCH_TYPE_LIST_EN", var_export(
              array(
                     1  => "Agent ID", 
                     2 => "Agent Name", 
                     3 => "Whether solve", 
                     4 => "Custom Name"
              ), true
));
define("SEARCH_TYPE_LIST_JP", var_export(
              array(
                     1  => "エージェントID", 
                     2 => "エージェント名", 
                     3 => "解決するかどうか", 
                     4 => "お客様の名前"
              ), true
));
define("SEARCH_TYPE_LIST_KR", var_export(
              array(
                     1  => "상담원 ID", 
                     2 => "상담원명", 
                     3 => "해결여부", 
                     4 => "고객명"
              ), true
));

//Define : 해결 여부
define("RESOLVE_EN", "Settlement");
define("RESOLVE_KR", "해결");
define("RESOLVE_JP", "解決");
define("HOLD_EN", "Put off");
define("HOLD_KR", "보류");
define("HOLD_JP", "保留");
define("NEED_DIRECT_SUPPORT_EN", "Direct support");
define("NEED_DIRECT_SUPPORT_KR", "지원 필요");
define("NEED_DIRECT_SUPPORT_JP", "直接サポートが必要");
define("UNCHECKED_EN", "Check yet");
define("UNCHECKED_KR", "미체크");
define("UNCHECKED_JP", "未チェック");

//Define : OS Type
define("OS_UNKNOWN_EN", "ETC. OS");
define("OS_UNKNOWN_KR", "기타 OS");
define("OS_WIN_98_EN", "Windows 98");
define("OS_WIN_2000_EN", "Windows 2000");
define("OS_WIN_XP_EN", "Windows XP");
define("OS_WIN_2003_EN", "Windows 2003");
define("OS_WIN_VISTA_EN", "Windows Vista");
define("OS_WIN_7_EN", "Windows 7");
define("OS_WIN_8_EN", "Windows 8");
define("OS_WIN_2008_EN", "Windows 2008");
define("OS_WIN_2012_EN", "Windows 2012");
define("OS_ANDROID_EN", "Android");
define("OS_MAC_EN", "Mac");
define("OS_LINUX_EN", "Linux");

//Define : 고객 평가
define("SATISFACTION_EN", "Satisfied");
define("SATISFACTION_KR", "만족");
define("SATISFACTION_JP", "満足");
define("NORMAL_EN", "Normal");
define("NORMAL_KR", "보통");
define("NORMAL_JP", "ノーマル");
define("UNSATISFACTION_EN", "Unsatisfied");
define("UNSATISFACTION_KR", "불만족");
define("UNSATISFACTION_JP", "不満");
define("NO_ANSWER_EN", "No answer");
define("NO_ANSWER_KR", "무응답");
define("NO_ANSWER_JP", "無応答");

//define : use agent
define("AGENT_WEB_ACCESS_URL", ASG_URL."/agent/history/search_history.php");
define("AGENT_WEB_BAN_URL", ASG_URL."/agent/history/no_auth.php");
define("AGENT_WEB_LOGIN_PW", "modeWebLogin");
define("AGENT_REPORT_MAX", 5000);
define("AGENT_REPORT_MAX_EN", 5000);
define("AGENT_REPORT_MAX_KR", 2500);

return "ASGConstants.php Loaded";