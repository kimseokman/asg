<?php
session_start();
/**
 * common.php
 * 2015.06.05 | KSM | for session
 */
header('Content-Type: text/html; charset=utf-8');


include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$request_url = $utils_obj->GetRequestURL();
$access_auth = $utils_obj->GetAccessAuth();
$current_user = $utils_obj->GetCurrentUserType();

$admin = $asg_obj->GetAdminInfo();//get Admin Info

if($access_auth == "Only Admin"){
	if($current_user['type'] == "SUPPORTER"){			//case by access supporter
		$utils_obj->RedirectURL(SUPPORTER_INDEX);			// deny redirection
	}else if($current_user['type'] == "GUEST"){
		$utils_obj->RedirectURL(GUEST_INDEX);			//case by access guest
	}
}else if($access_auth == "Only Supporter"){
	if($current_user['type'] == "ADMIN"){				//case by access admin
		$utils_obj->RedirectURL(ADMIN_INDEX);			// deny redirection
	}else if($current_user['type'] == "GUEST"){
		$utils_obj->RedirectURL(GUEST_INDEX);			//case by access guest
	}
}else if($access_auth == "All"){
	//not any more
}

$lang_list = $utils_obj->GetLangs();