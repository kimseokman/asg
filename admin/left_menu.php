<?
	$user_product = $utils_obj->GetProduct($admin->function);
?>
<div class="sub_left">
	<div class="left_menu_top">
		<dl class="left_menu_dl">
			<dt><img src="/admin/img/admin_sub_left_img.gif" alt="" /></dt>
			<dd><?php lang_print('LOS.010') ?></dd>
		</dl>
		<p class="left_menu_p">
			<?php lang_print('LOS.020') ?>: <? echo $_SESSION['adid']; ?><br />
			<?php lang_print('LOS.030') ?>: <? echo $admin->service_expired_date; ?><br />
			<? echo $user_product; ?><br />
			<?php lang_print('LOS.050', $admin->volume) ?>
		</p>
	</div>
	<ul id="lnb">
		<li class="lnb_tit01"><?php lang_print('MNU.010') ?></li>
		<li <?if($wz['pid']=="5" ){?>class="on"<?}?>><a href="/admin/manage/billing/billing_index.php"><?php lang_print('MNU.020') ?></a></li>
		<li <?if($wz['pid']=="10"){?>class="on"<?}?>><a href="/admin/manage/department/list.php"><?php lang_print('MNU.030') ?></a></li>
		<li <?if($wz['pid']=="15"){?>class="on"<?}?>><a href="/admin/manage/agent/list.php"><?php lang_print('MNU.040') ?></a></li>
		<li <?if($wz['pid']=="20"){?>class="on"<?}?>><a href="/admin/manage/trouble/list.php"><?php lang_print('MNU.050') ?></a></li>
		<!--li <?if($wz['pid']=="25"){?>class="on"<?}?>><a href="/admin/manage/setting/setting.php"><?php lang_print('MNU.060') ?></a></li-->
		<li class="lnb_tit02"><?php lang_print('MNU.070') ?></li>
		<li <?if($wz['pid']=="30"){?>class="on"<?}?>><a href="/admin/reports/term/list.php"><?php lang_print('MNU.080') ?></a></li>
		<li <?if($wz['pid']=="35"){?>class="on"<?}?>><a href="/admin/reports/monthly/list.php"><?php lang_print('MNU.090') ?></a></li>
		<li <?if($wz['pid']=="40"){?>class="on"<?}?>><a href="/admin/reports/daily/list.php"><?php lang_print('MNU.100') ?></a></li>
		<li <?if($wz['pid']=="45"){?>class="on"<?}?>><a href="/admin/reports/hourly/list.php"><?php lang_print('MNU.110') ?></a></li>
		<li <?if($wz['pid']=="50"){?>class="on"<?}?>><a href="/admin/reports/agent/list.php"><?php lang_print('MNU.120') ?></a></li>
		<li <?if($wz['pid']=="55"){?>class="on"<?}?>><a href="/admin/reports/os/list.php"><?php lang_print('MNU.130') ?></a></li>
		<li <?if($wz['pid']=="60"){?>class="on"<?}?>><a href="/admin/reports/trouble/list.php"><?php lang_print('MNU.140') ?></a></li>
		<li <?if($wz['pid']=="65"){?>class="on"<?}?>><a href="/admin/reports/satisfaction/list.php"><?php lang_print('MNU.150') ?></a></li>
		<!--li <?if($wz['pid']=="70"){?>class="on"<?}?>><a href="/admin/reports/history/list.php"><?php lang_print('MNU.160') ?></a></li-->
		<li class="lnb_tit03"><?php lang_print('MNU.170') ?></li>
		<li <?if($wz['pid']=="75"){?>class="on"<?}?>><a href="/admin/notice/list.php"><?php lang_print('MNU.180') ?></a></li>
	</ul>
</div>