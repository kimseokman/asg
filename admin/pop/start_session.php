<?php
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$asg_obj = new ASGMain();
$isLogin = $asg_obj->IsLogin();

if($isLogin == FALSE){
	exit(0);
}

$access = "";
$asg_obj->GenerateTitleBar($access, $_SESSION["sptnum"]);
?>
<!doctype html>
<html>
<head>
<script language="javascript">

	function setTitle()
	{
		<?
			echo "this.document.title = \"Koino AnySupport ActiveX ". $access."\";";
		?>
	}
</script>

<meta charset="utf-8">
<title>Start Session</title>
<link rel="stylesheet" type="text/css" href="/admin/css/main.css">
</head>

<body OnLoad="setTitle()">
	<div class="start_session_pop_wrap">
		<h2>Start Session</h2>
		<p>Please click on "Run" or "Install" button<br> when a security warning message<br> appears. Click on "Run" or "Install" button once more if another security warning<br> message appears again.<br><br> Click on "Continue" button to proceed to next step.</p>
		<div><a href="javascript:document.location='../../download/SupporterLauncher.exe'" class="start_session_pop_btn"><span>Start Session</span></a></div>
	</div>
</body>
</html>