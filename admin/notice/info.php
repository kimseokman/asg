<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.210'); //"Notice and Event";
$wz['pid']  = "75";

include_once("../header.php");

$seq = $_GET['seq'];
$info = $asg_obj->GetNotice($seq);
?>
<!-- 내용시작 -->
<?
	include_once("../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">

		<table summary="" class="notice_detail_table">
			<colgroup>
				<col width="100px" />
				<col width="*" />
				<col width="60px" />
				<col width="130px" />
			</colgroup>
			<tbody>

				<tr>
					<th class="notice_orther_color">Title</th>
					<td colspan="3" class="notice_orther_color"><? echo $info['title']; ?></td>
				</tr>

				<tr>
					<th>Writer</th>
					<td><? echo $info['name']; ?></td>
					<td><strong>Date</strong></td>
					<td><? echo $info['regdate']; ?></td>
				</tr>

				<tr>
					<th class="last">Content</th>
					<td colspan="3" class="last">
						<? echo $info['content']; ?>
					</td>
				</tr>

			</tbody>
		</table>
		
		<p class="notice_btn_p"><a href="/admin/notice/list.php" class="notice_btn">List</a></p>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../footer.php");
?>