<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.120'); //"Term of supporting Report";
$wz['pid']  = "30";

include_once("../../header.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$isAdmin = $asg_obj->ConfirmAdmin();

$page = array(//page define
	"INFO_GET_FAIL"=> "/admin/reports/term/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_LOG"=> "no_data",
	"UNKNOWN"=> "unknown"
);

$msg_jp = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_LOG"=> "-",
	"UNKNOWN"=> "-"
);

$msg_kr = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_LOG"=> "원격 지원 기록이 없습니다.",
	"UNKNOWN"=> "알수 없음"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['INFO_GET_FAIL']."';
		</script>
	";
	exit(1);
}

$reportinfo = $asg_obj->GetReport($_POST["report_num"]);

$sTime = mktime(intVal(substr($reportinfo->starttime,11,2)),intVal(substr($reportinfo->starttime,14,2)),intVal(substr($reportinfo->starttime,17,2)),intVal(substr($reportinfo->starttime,5,2)),intVal(substr($reportinfo->starttime,8,2)),intVal(substr($reportinfo->starttime,0,4)));
$eTime = mktime(intVal(substr($reportinfo->endtime,11,2)),intVal(substr($reportinfo->endtime,14,2)),intVal(substr($reportinfo->endtime,17,2)),intVal(substr($reportinfo->endtime,5,2)),intVal(substr($reportinfo->endtime,8,2)),intVal(substr($reportinfo->endtime,0,4)));

$useTime = intVal($eTime) - intVal($sTime);
if($useTime < 0){
	$useTime = $msg[$msg_lang]['UNKNOWN'];
}
else{
	$hours = intVal($useTime / 3600);//get hour
	$leftover = $useTime - ( $hours * 3600); 
	$min = intVal($leftover / 60);//get min
	$sec = $leftover - ( $min * 60);//get sec

	$useTime = str_pad($hours,"2","0",STR_PAD_LEFT).":".str_pad($min,"2","0",STR_PAD_LEFT).":".str_pad($sec,"2","0",STR_PAD_LEFT);
}

if($reportinfo == NULL) {
	echo "
		<script> 
			alert('".$msg[$msg_lang]['NO_LOG']."');
			location.href='".$page['INFO_GET_FAIL']."';
		</script>
	";
	exit(1);
}
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<p class="terms_tit"><?php lang_print('201020.010') ?></p>
		<form name='search_memory' method='post' action='./list.php'>
			<input type="hidden" value="TRUE" name="auto_search_flag" />
			<input type="hidden" value="<? echo $_POST['search_type']; ?>" name="search_type" />
			<input type="hidden" value="<? echo $_POST['search_val']; ?>" name="search_val" />
			<input type="hidden" value="<? echo $_POST['start_time']; ?>" name="start_time" />
			<input type="hidden" value="<? echo $_POST['end_time']; ?>" name="end_time" />
			<input type="hidden" value="<? echo $_POST['page']; ?>" name="page" />
		</form>
		<?
			$agent_id = $msg[$msg_lang]['UNKNOWN'];
			if($reportinfo->deleted == ""){
				$agent_id = $reportinfo->sptid;
			}else{
				$agent_id = $reportinfo->deleted;
			}

			$user_os = $utils_obj->GetOS($reportinfo->user_os);
			$user_oper = $utils_obj->GetOperationLog($reportinfo->operationlog);
			$user_satis = $utils_obj->GetSatisfaction($reportinfo->satisfaction);
		?>
		<table summary="" class="common_table04">
			<colgroup>
				<col width="172px" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th><?php lang_print('201020.020') ?></th>
					<td><? echo $agent_id; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.030') ?></th>
					<td class="common_table_color05"><? echo $reportinfo->sptname; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.040') ?></th>
					<td><? echo $reportinfo->spt_pub_ip." / ".$reportinfo->spt_pri_ip; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.050') ?></th>
					<td class="common_table_color05"><? echo $reportinfo->spt_com_name; ?></td>
				</tr>
			</tbody>
		</table>

		<p class="terms_tit"><?php lang_print('201020.060') ?></p>
		<table summary="" class="common_table04">
			<colgroup>
				<col width="172px" />
				<col width="*" />
			</colgroup>

			<tbody>
				<tr>
					<th><?php lang_print('201020.070') ?></th>
					<td><? echo $reportinfo->name; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.090') ?></th>
					<td class="common_table_color05"><? echo $reportinfo->user_pub_ip." / ".$reportinfo->user_pri_ip; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.100') ?></th>
					<td><? echo $reportinfo->user_com_name; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.110') ?></th>
					<td class="common_table_color05"><? echo $user_os; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.120') ?></th>
					<td><? echo $reportinfo->email; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.125') ?></th>
					<td class="common_table_color05"><? echo $reportinfo->tel; ?></td>
				</tr>
			</tbody>
		</table>

		<p class="terms_tit"><?php lang_print('201020.130') ?></p>
		<table summary="" class="common_table04">
			<colgroup>
				<col width="172px" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th><?php lang_print('201020.140') ?></th>
					<td><? echo $reportinfo->trbname; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.150') ?></th>
					<td class="common_table_color05"><? echo $reportinfo->starttime." ~ ".$reportinfo->endtime."(".$useTime.")"; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.160') ?></th>
					<td><? echo htmlspecialchars($reportinfo->report); ?></td>
				</tr>
				<!--tr>
					<th><?php lang_print('201020.170') ?></th>
					<td class="common_table_color05">&nbsp;</td>
				</tr>
				<tr>
					<th><?php lang_print('201020.180') ?></th>
					<td>&nbsp;</td>
				</tr-->
				<tr>
					<th><?php lang_print('201020.190') ?></th>
					<td class="common_table_color05"><? echo $user_oper; ?></td>
				</tr>
				<tr>
					<th><?php lang_print('201020.200') ?></th>
					<td><? echo $user_satis; ?></td>
				</tr>
			</tbody>
		</table>
		<p class="termas_p"><a href="/admin/reports/term/list.php" class="agent_list_btn memory_agent_list_btn"><?php lang_print('BTN.go_list') ?></a></p>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>