<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.120'); //"Term of supporting Report";
$wz['pid']  = "30";

include_once("../../header.php");

$spt_empty = array(
	"en" => "no_data",
	"jp" => "-",
	"kr" => "원격 지원 기록이 없습니다."
);
$spt_empty_lang = $_SESSION['language'];

?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<input type="hidden" name="auto_agent_search_flag" value="<? echo $_POST['auto_search_flag']; ?>">
		<input type="hidden" value="<? echo $_POST['search_type']; ?>" name="search_type" />
		<input type="hidden" value="<? echo $_POST['search_val']; ?>" name="search_val" />
		<input type="hidden" value="<? echo $_POST['start_time']; ?>" name="start_time" />
		<input type="hidden" value="<? echo $_POST['end_time']; ?>" name="end_time" />
		<input type="hidden" value="<? echo $_POST['page']; ?>" name="page" />

		<? include_once("../search_bar/type_detail.php"); ?>
		
		<div class="ajax_report_supporting">
			<table summary="" class="common_table03 report_search_table">
				<colgroup>
					<col width="60px" />
					<col width="60px" />
					<col width="*" />
					<col width="180px" />
					<col width="180px" />
					<col width="90px" />
					<col width="120px" />
					<col width="90px" />
				</colgroup>
				<thead>
					<tr>
						<th><?php lang_print('201010.070') ?></th>
						<th><?php lang_print('201010.080') ?></th>
						<th><?php lang_print('201010.090') ?></th>
						<th><?php lang_print('201010.100') ?></th>
						<th><?php lang_print('201010.110') ?></th>
						<th><?php lang_print('201010.120') ?></th>
						<th><?php lang_print('201010.130') ?></th>
						<th><?php lang_print('201010.140') ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="colspan_sort" colspan="8">
							<span>
								<? echo $spt_empty[$spt_empty_lang]; ?>
							</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>