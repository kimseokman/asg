<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.180'); //"Reports of error Pattern";
$wz['pid']  = "60";

include_once("../../header.php");

$this_start_date = date("Y-m-d");
$this_end_date = date("Y-m-d");

$stats_arr = $asg_obj->GetReportByTroubleType($this_start_date, $this_end_date);

$column_lang = $_SESSION['language'];

$column_en = array(//option define
	"RESOLVE" => RESOLVE_EN,
	"HOLD" => HOLD_EN,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_EN,
	"UNCHECKED" => UNCHECKED_EN
);

$column_jp = array(
	"RESOLVE" => RESOLVE_JP,
	"HOLD" => HOLD_JP,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_JP,
	"UNCHECKED" => UNCHECKED_JP
);

$column_kr = array(
	"RESOLVE" => RESOLVE_KR,
	"HOLD" => HOLD_KR,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_KR,
	"UNCHECKED" => UNCHECKED_KR
);

$column = array(
	"en" => $column_en,
	"jp" => $column_jp,
	"kr" => $column_kr
);

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

$res1sum = 0;
$res2sum = 0;
$res3sum = 0;
$res0sum = 0;
$sumsum = 0;

$report_stats_arr = $stats_arr;

$trouble_type_name_arr = array();
$resolve_data_arr = array();
$hold_data_arr = array();
$direct_data_arr = array();
$uncheck_data_arr = array();
while(list($key, $val) = each($report_stats_arr)) {
	$trouble_type_name_arr[$key] = $val["trbname"];
	$resolve_data_arr[$key] = $val["res1"];
	$hold_data_arr[$key] = $val["res2"];
	$direct_data_arr[$key] = $val["res3"];
	$uncheck_data_arr[$key] = $val["res0"];
}
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<? include_once("../search_bar/type_keyword_year_month_day.php"); ?>
		<div class="ajax_report_trouble">
			<p class="common_grapic_tit"><?php lang_print('207000.140') ?></p>
			<div class="admin_grapic_div report_trouble_chart" id="admin_grapic_div"  style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			<script>
				$(document).ready(function(){
					$('.report_trouble_chart').highcharts({
						chart: {
							marginTop: 50,
							type: 'column'
						},
						title: {
							text: ''
						},
						xAxis: {
							categories: [
							<?
							$while_cnt = 0;
							$trouble_type_length = count($trouble_type_name_arr);
							while(list($key, $val) = each($trouble_type_name_arr)) {
								echo "'".$val."'";
								$while_cnt++;
								if($while_cnt != $trouble_type_length){
									echo ",";	
								}
							}	
							?>
							]
						},
						yAxis: {
							min: 0,
							title: {
                							text: 'Sum of Type'
							},
							stackLabels: {
								enabled: true,
								style: {
									fontWeight: 'bold',
									color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
								}
							}
						},
						legend: {
							align: 'right',
							x: -30,
							verticalAlign: 'top',
							y: 0,
							floating: true,
							backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
							borderColor: '#CCC',
							borderWidth: 1,
							shadow: false
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.x + '</b><br/>' +
									this.series.name + ': ' + this.y + '<br/>' +
									'Total: ' + this.point.stackTotal;
							}
						},
						plotOptions: {
							column: {
								stacking: 'normal'
							}
						},
						series: [
							{
								name: <? echo "'".$column[$column_lang]["RESOLVE"]."'"; ?>,
								data: [
								<?
								$while_cnt = 0;
								$resolve_data_length = count($resolve_data_arr);
								while(list($key, $val) = each($resolve_data_arr)){
									echo $val;
									$while_cnt++;
									if($while_cnt != $resolve_data_length){
										echo ",";	
									}
								}
								?>
								]
							}, {
								name: <? echo "'".$column[$column_lang]["HOLD"]."'"; ?>,
								data: [
								<?
								$while_cnt = 0;
								$hold_data_length = count($hold_data_arr);
								while(list($key, $val) = each($hold_data_arr)){
									echo $val;
									$while_cnt++;
									if($while_cnt != $hold_data_length){
										echo ",";	
									}
								}
								?>
								]
							}, {
								name: <? echo "'".$column[$column_lang]["NEED_DIRECT_SUPPORT"]."'"; ?>,
								data: [
								<?
								$while_cnt = 0;
								$direct_data_length = count($direct_data_arr);
								while(list($key, $val) = each($direct_data_arr)){
									echo $val;
									$while_cnt++;
									if($while_cnt != $direct_data_length){
										echo ",";	
									}
								}
								?>
								]
							}, {
								name: <? echo "'".$column[$column_lang]["UNCHECKED"]."'"; ?>,
								data: [
								<?
								$while_cnt = 0;
								$uncheck_data_length = count($uncheck_data_arr);
								while(list($key, $val) = each($uncheck_data_arr)){
									echo $val;
									$while_cnt++;
									if($while_cnt != $uncheck_data_length){
										echo ",";	
									}
								}
								?>
								]
							}
						]
					});
				});
			</script>

			<p class="table_bottom_p"></p>

			<table summary="" class="common_table03 report_search_table">
				<colgroup>
					<col width="110px" />
					<col width="*" />
					<col width="120px" />
					<col width="130px" />
					<col width="100px" />
					<col width="100px" />
				</colgroup>
				<thead>
					<tr>
						<th><?php lang_print('207000.070') ?></th>
						<th><?php lang_print('207000.080') ?></th>
						<th><?php lang_print('207000.090') ?></th>
						<th><?php lang_print('207000.100') ?></th>
						<th><?php lang_print('207000.110') ?></th>
						<th><?php lang_print('207000.120') ?></th>
					</tr>
				</thead>
				<tbody>
				<?
				while(list($key, $val) = each($stats_arr)) {
				?>
					<tr>
						<td class="<? echo print_table_class($key); ?>"><? echo $val["trbname"]; ?></td>
						<td class="<? echo print_table_class($key); ?>"><? echo $val["res1"]; ?></td>
						<td class="<? echo print_table_class($key); ?>"><? echo $val["res2"]; ?></td>
						<td class="<? echo print_table_class($key); ?>"><? echo $val["res3"]; ?></td>
						<td class="<? echo print_table_class($key); ?>"><? echo $val["res0"]; ?></td>
						<td class="<? echo print_table_class($key); ?>"><? echo ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]); ?></td>
					</tr>
				<?
					$res1sum += $val["res1"];
					$res2sum += $val["res2"];
					$res3sum += $val["res3"];
					$res0sum += $val["res0"];
					$sumsum += ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]);
				}// end of : while(list($key, $val) = each($stats_arr))
				?>
					<tr class="model_tfoot">
						<td><?php lang_print('207000.130') ?></td>
						<td><? echo $res1sum; ?></td>
						<td><? echo $res2sum; ?></td>
						<td><? echo $res3sum; ?></td>
						<td><? echo $res0sum; ?></td>
						<td><? echo $sumsum; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>