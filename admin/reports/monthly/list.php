<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.130'); //"Usage reports of monthly";
$wz['pid']  = "35";

include_once("../../header.php");

$thisYear = date("Y");
$result = $asg_obj->GetReportByMonth($thisYear);

$month_arr = $utils_obj->GetMonths();

$total = 0;
for($i=0;$i<12;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$tmp = explode("-",$row['starttime']);
	$t = intVal($tmp[1]);
	$z = --$t;
	$data[$z]++;
	$total++;
}


$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}

	$totalPer += $per[$i];
}

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

$last_node = sizeof($data)-1;
?>
<!-- 내용시작 -->
<?
include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<? include_once("../search_bar/type_keyword_year.php"); ?>
		
		<div class="ajax_report_monthly">
			<p class="common_grapic_tit"><?php lang_print('202000.050') ?></p>

			<div class="admin_grapic_div report_monthly_chart" id="admin_grapic_div" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			<script>
				$(document).ready(function(){
					$('.report_monthly_chart').highcharts({
						chart: {
							marginTop: 50,
							type: 'column'
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							categories: [
							<? 	
								for($i=0; $i < sizeof($data); $i++){
									echo "'".$month_arr[$i+1]."'";
									if($i != $last_node){
										echo ",";	
									}
								}
								
							?>
							],
							crosshair: true
						},
						yAxis: {
							min: 0,
							title: {
								text: 'Session'
							}
						},
						tooltip: {
							headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								'<td style="padding:0"><b>{point.y}</b></td></tr>',
							footerFormat: '</table>',
							shared: true,
							useHTML: true
						},
						plotOptions: {
							column: {
								pointPadding: 0.2,
								borderWidth: 0
							}
						},
						series: [{
							name: 'count',
							data: [								
							<?
								for($i=0; $i < sizeof($data); $i++){
									echo $data[$i];
									if($i != $last_node){
										echo ",";	
									}
								}
							?>
							]
						}]
					});
				});
			</script>

			<p class="table_bottom_p"></p>

			<table summary="" class="common_table03 report_search_table">
				<colgroup>
					<col width="180px" />
					<col width="220px" />
					<col width="*" />
				</colgroup>
				<thead>
					<tr>
						<th><?php lang_print('202000.010') ?></th>
						<th><?php lang_print('202000.020') ?></th>
						<th><?php lang_print('202000.030') ?></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?php lang_print('202000.040') ?></th>
						<th><? echo $total; ?></th>
						<th><? echo $totalPer; ?>%</th>
					</tr>
				</tfoot>
				<tbody>
				<?
				for($i=0;$i<sizeof($data);$i++){
				?>
					<tr>
						<td class="<? echo print_table_class($i); ?>"><? echo $month_arr[$i+1]; ?></td>
						<td class="<? echo print_table_class($i); ?>"><? echo $data[$i]; ?></td>
						<td class="<? echo print_table_class($i); ?>"><? echo sprintf("%.2f",$per[$i]); ?>%</td>
					</tr>
				<?
				}// end of : for($i=0;$i<sizeof($data);$i++)
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>