<?
	$report_type = $utils_obj->GetReportPage();

	$year_arr = $utils_obj->GetYears();
?>
<div class="common_searchbar01">
	<dl class="searchbar_dl02">
		<dt>
			<div class="select_div01">
				<select class="searchbar_input" name="year">
				<?
				foreach($year_arr as $k => $v){//init start year
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentYear($k);?>>
						<? echo $v; ?> <?php lang_print('UNIT.year') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>	
				</select>
			</div>
		</dt>
		<dd>
			<ul class="searchbar_right02">
				<li><a href="#" class="search_btn report_<? echo $report_type; ?>_search_btn"><img src="/admin/img/admin_plus_icon.png" alt="search icon" /></a></li>
				<li class="last"><a href="#" class="down_btn report_<? echo $report_type; ?>_export_btn"><img src="/admin/img/admin_down.png" alt="down icon" /></a></li>
			</ul>
		</dd>
	</dl>
</div>