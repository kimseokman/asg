<?
	$report_type = $utils_obj->GetReportPage();

	$year_arr = $utils_obj->GetYears();
	$month_arr = $utils_obj->GetMonths();
	$this_last_day = $utils_obj->GetLastDay(date('m'), date('Y'));
	$day_arr = $utils_obj->GetDays($this_last_day);
	$search_type = $utils_obj->GetSearchType();
?>
<div class="common_searchbar02">
	<dl class="searchbar02_dl">
		<dt><?php lang_print('200010.010') ?></dt>
		<dd>
			<ul class="searchbar02_ul">
				<li><a href="#" class="reports_btn search_condition_today"><?php lang_print('BTN.today') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_yesterday"><?php lang_print('BTN.yesterday') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_3day">3<?php lang_print('BTN.day') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_week"><?php lang_print('BTN.week') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_month"><?php lang_print('BTN.month') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_3month">3<?php lang_print('BTN.month') ?></a></li>
				<li><a href="#" class="reports_btn search_condition_6month">6<?php lang_print('BTN.month') ?></a></li>
				<li class="last"><a href="#" class="reports_btn search_condition_year"><?php lang_print('BTN.year') ?></a></li>
			</ul>
		</dd>
	</dl>
	<div class="searchbar_sel_div">
		<div class="search_sel_float01">
			<div class="select_div02">
				<select  class="searchbar_input" id="startYear"  name="startYear">
				<?
				foreach($year_arr as $k => $v){//init start year
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentYear($k);?>>
						<? echo $v; ?> <?php lang_print('UNIT.year') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>	
				</select>
			</div>
		</div>
		<div class="search_sel_float01">
			<div class="select_div03">
				<select  class="searchbar_input" id="startMonth" name="startMonth">
				<?
				foreach($month_arr as $k => $v){//init start month
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentMonth($k); ?>>
						<? echo $v; ?> <?php lang_print('UNIT.month') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>
				</select>
			</div>
		</div>
		<div class="search_sel_float02">
			<div class="select_div04">
				<select  class="searchbar_input" id="startDay" name="startDay">
				<?
				foreach($day_arr as $k => $v){//init start day
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentDay($k); ?>>
						<? echo $v; ?> <?php lang_print('UNIT.day') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>
				</select>
			</div>
		</div>
		<p class="searchbar_hyphen">-</p>
		<div class="search_sel_float01">
			<div class="select_div02">
				<select  class="searchbar_input" id="endYear" name="endYear">
				<?
				foreach($year_arr as $k => $v){//init end year
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentYear($k);?>>
						<? echo $v; ?> <?php lang_print('UNIT.year') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>	
				</select>
			</div>
		</div>
		<div class="search_sel_float01">
			<div class="select_div03">
				<select  class="searchbar_input" id="endMonth" name="endMonth">
				<?
				foreach($month_arr as $k => $v){//init end month
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentMonth($k); ?>>
						<? echo $v; ?> <?php lang_print('UNIT.month') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>	
				</select>
			</div>
		</div>
		<div class="search_sel_float02">
			<div class="select_div04">
				<select  class="searchbar_input" id="endDay" name="endDay">
				<?
				foreach($day_arr as $k => $v){//init end day
				?>
					<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelectedCurrentDay($k); ?>>
						<? echo $v; ?> <?php lang_print('UNIT.day') ?>
					</option>
				<?
				}//end of : foreach($year_arr as $k => $v)
				?>	
				</select>
			</div>
		</div>
	</div>
	<dl class="searchbar03_dl">
		<dd>
			<ul class="searchbar03_ul">
				<li><a href="#" class="search_btn report_<? echo $report_type; ?>_search_btn"><img src="/admin/img/admin_plus_icon.png" alt="search icon" /></a></li>
				<li class="last"><a href="#" class="down_btn report_<? echo $report_type; ?>_export_btn"><img src="/admin/img/admin_down.png" alt="down icon" /></a></li>
			</ul>
		</dd>
	</dl>
</div>