<?
	$report_type = $utils_obj->GetReportPage();
?>
<div class="common_searchbar01">
	<dl class="searchbar_dl04">
		<dt><?php lang_print('209000.010') ?></dt>
		<dd>
			<div class="searchbar_dl04_float01">
				<ul class="searchbar04_input">
					<li><input type="text" id="" name="" value="" class="searchbar_input" placeholder="<?php lang_print('209000.020') ?>" /></li>
					<li class="searchbar04_input_hyphen">-</li>
					<li><input type="text" id="" name="" value="" class="searchbar_input" placeholder="<?php lang_print('209000.030') ?>" /></li>
				</ul>
			</div>
			<div class="searchbar_dl04_float02">
				<div class="select_div01">
					<div class="selectbox">
						<ul>  
							<li><a href="javascript:;" data-path="" id="0" class="sel_cen"><?php lang_print('209000.040') ?></a></li>
							<li><a href="javascript:;" data-path="" id="1" class="sel_cen">ID2</a></li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="searchbar_right04">
				<li><a href="#" class="search_btn report_<? echo $report_type; ?>_search_btn"><img src="/admin/img/admin_plus_icon.png" alt="search icon" /></a></li>
				<li class="last"><a href="#" class="down_btn report_<? echo $report_type; ?>_export_btn"><img src="/admin/img/admin_down.png" alt="down icon" /></a></li>
			</ul>
		</dd>
	</dl>
</div>