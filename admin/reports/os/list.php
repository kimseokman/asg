<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.170'); //"Reports of OS";
$wz['pid']  = "55";

include_once("../../header.php");

$this_start_date = date("Y-m-d");
$this_end_date = date("Y-m-d");

$result = $asg_obj->GetReportByOS($this_start_date, $this_end_date);

$total = 0;

$os_count = 14;
for($i=0;$i<$os_count;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){


	$index = $row['os'];

	if($index == 100)
		$index  = 11;
	else if($index == 101)
		$index = 12;
	else if($index == 102)
		$index = 13;

	$data[$index] = $row['cnt'];

	$total += intVal($row['cnt']);

}
$data[7] += $data[0]; // 기존에 저장안되었던 os 정보는 기타 os에 저장한다.


$totalPer = 0;
for($i=1;$i<sizeof($data);$i++){
	if($total != 0){
		//$per[$i] = $rate=round($data[$i]/$total*1000)/10;
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

$os[0] = "";
$os[1] = OS_WIN_98_EN;
$os[2] = OS_WIN_2000_EN;
$os[3] = OS_WIN_XP_EN;
$os[4] = OS_WIN_2003_EN;
$os[5] = OS_WIN_VISTA_EN;
$os[6] = OS_WIN_7_EN;
if($_SESSION['language'] == 'kr'){
	$os[7] = OS_UNKNOWN_KR;
}else{
	$os[7] = OS_UNKNOWN_EN;	
};
$os[8] = OS_WIN_8_EN;
$os[9] = OS_WIN_2008_EN;
$os[10] = OS_WIN_2012_EN;
$os[11] = OS_ANDROID_EN;
$os[12] = OS_MAC_EN;
$os[13] = OS_LINUX_EN;

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

$last_node = sizeof($data)-1;
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<? include_once("../search_bar/type_keyword_year_month_day.php"); ?>
		<div class="ajax_report_os">
			<p class="common_grapic_tit"><?php lang_print('206000.110') ?></p>
			<div class="admin_grapic_div report_os_chart" id="admin_grapic_div"  style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			<script>
				$(document).ready(function(){
					$('.report_os_chart').highcharts({
						chart: {
							marginTop: 50,
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false
						},
						title: {
							text: ''
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: true,
									format: '<b>{point.name}</b>: {point.percentage:.1f} %',
									style: {
										color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
									}
								}
							}
						},
						series: [{
							type: 'pie',
							name: 'OS share',
							data: [
							<?
								for($i=1; $i < sizeof($data); $i++){
									echo "['".$os[$i]."',".$data[$i]."]";
									if($i != $last_node){
										echo ",";	
									}
								}
							?>
							]
						}]
					});
				});
			</script>

			<p class="table_bottom_p"></p>

			<table summary="" class="common_table03 report_search_table">
				<colgroup>
					<col width="180px" />
					<col width="220px" />
					<col width="*" />
				</colgroup>
				<thead>
					<tr>
						<th><?php lang_print('206000.070') ?></th>
						<th><?php lang_print('206000.080') ?></th>
						<th><?php lang_print('206000.090') ?></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?php lang_print('206000.100') ?></th>
						<th><? echo $total; ?></th>
						<th><? echo $totalPer; ?>%</th>
					</tr>
				</tfoot>
				<tbody>
				<?
				for($i=1;$i<sizeof($data);$i++){
				?>
					<tr>
						<td class="<? echo print_table_class($i); ?>"><? echo $os[$i]; ?></td>
						<td class="<? echo print_table_class($i); ?>"><? echo $data[$i]; ?></td>
						<td class="<? echo print_table_class($i); ?>"><? echo sprintf("%.2f",$per[$i]); ?>%</td>
					</tr>
				<?
				}//end of : for($i=1;$i<sizeof($data);$i++)
				?>
				</tbody>
			</table>
		</div>	
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>