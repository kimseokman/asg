<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.100'); //"Manage Error";
$wz['pid']  = "20";

include_once("../../header.php");

$isAdmin = $asg_obj->ConfirmAdmin();

$page = array(//page define
	"GET_TROUBLE_TYPE_FAIL"=> ADMIN_INDEX
);

$msg_lang = $_SESSION['language'];

$msg_en = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function."
);

$msg_jp = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function."
);

$msg_kr = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function."
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['GET_TROUBLE_TYPE_FAIL']."';
		</script>
	";
	exit(1);
}

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color02";
	}
}

$total_num = $asg_obj->GetTroubleTypeCount();
$trouble_type_arr = $asg_obj->GetTroubleTypeList();
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<div class="common_searchbar01">
			<ul class="searchbar_left">
				<?
				if($total_num < TROUBLE_MAX_LINE){
				?>
				<li>
					<a href="/admin/_func/function.create_trouble_type.php" class="add_error_btn">
						<?php lang_print('BTN.add_type') ?>
					</a>
				</li>
				<?
				}// end of : if($total_num >= 10)
				?>
			</ul>
			<?
			if($total_num >= TROUBLE_MAX_LINE){
			?>
			<span>
				<?php lang_print('PTT.101', TROUBLE_MAX_LINE) ?>
			</span>
			<?
			}//end of : if($total_num >= 10)
			?>
		</div>
		<form name="trouble_edit_form" method="post">
			<input name="edit_trbnum" type="hidden" value="<? echo $_POST['edit_trbnum']; ?>" />
			<input name="edit_trbname" type="hidden" value="<? echo $_POST['edit_trbname']; ?>" />
			<input name="edit_flag" type="hidden" />
		</form>
		<table summary="" class="common_table01">
			<colgroup>
				<col width="138px" />
				<col width="*" />
				<col width="138px" />
			</colgroup>
			<thead>
				<tr>
					<th><?php lang_print('104000.040') ?></th>
					<th><?php lang_print('104000.050') ?></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?
				$tr_index = 0;
				while(list($key, $val) = each($trouble_type_arr)){
					if($val["deleted"] == 1) continue;
				?>
				<tr>
					<td class="<? echo print_table_class($tr_index); ?>">
						<strong>
							<? echo $tr_index+1; ?>
						</strong>
					</td>
					<td class="<? echo print_table_class($tr_index); ?>">
						<p class="error_td_p">
							<input class="table_input02 trouble_name_input trbnum_<? echo $val["trbnum"]; ?>" type="text" name="trbname" id="" value="<? echo $val["trbname"]; ?>" size="60" />
						</p>
					</td>
					<td class="<? echo print_table_class($tr_index); ?>">
						<ul class="table_btn_ul01">
							<li>
								<a href="#" class="table_edit_btn" onclick="edit_trouble_name_confirm(event, <? echo $val["trbnum"]; ?>)">
									<?php lang_print('BTN.edit') ?>
								</a>
							</li>
							<li class="last">
								<a href="/admin/manage/trouble/list.php?trbnum=<? echo $val["trbnum"]; ?>&del_flag=<? echo TRUE; ?>" class="table_del_btn">
									<?php lang_print('BTN.delete') ?>
								</a>
							</li>
						</ul>
					</td>
				</tr>
				<?
					$tr_index++;
				}//end of : while(list($key, $val) = each($trouble_type_arr))
				?>
			</tbody>
		</table>
	</div>
</div>
<!-- 내용끝 -->
<?
if($_GET['del_flag']){
?>
<div class="fixed_dim"></div>
<div class="detail_pop detail_pop_lg">
	<dl class="detail_pop_dl02">
		<dt><?php lang_print('104000.060') ?></dt>
		<dd><?php lang_print('104000.080') ?></dd>
		<dd><?php lang_print('104000.070') ?></dd>
	</dl>
	<ul class="detail_pop_btn">
		<li><a href="/admin/_func/function.delete_trouble_type.php?trbnum=<? echo $_GET["trbnum"]; ?>" class="common_pop_btn01"><?php lang_print('BTN.yes') ?></a></li>
		<li class="last"><a href="/admin/manage/trouble/list.php" class="common_pop_btn02"><?php lang_print('BTN.cancel') ?></a></li>
	</ul>
</div>
<?
}// end of : if($_GET['del_flag']==TRUE)
?>
<?
if($_POST['edit_flag'] == "STRING_TRUE"){
?>
<div class="fixed_dim"></div>
<div class="detail_pop detail_pop_lg">
	<dl class="detail_pop_dl02">
		<dt><?php lang_print('104000.061') ?></dt>
		<dd><?php lang_print('104000.081') ?></dd>
		<dd><?php lang_print('104000.071') ?></dd>
	</dl>
	<ul class="detail_pop_btn">
		<li><a href="#" class="common_pop_btn01 error_edit_confirm_yes_btn"><?php lang_print('BTN.yes') ?></a></li>
		<li class="last"><a href="/admin/manage/trouble/list.php" class="common_pop_btn02"><?php lang_print('BTN.cancel') ?></a></li>
	</ul>
</div>
<?
}// end of : if($_POST['del_flag']==TRUE)
?>
<?
include_once("../../footer.php");
?>