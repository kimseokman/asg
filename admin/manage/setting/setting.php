<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.110'); //"Setting Anysupport Management";
$wz['pid']  = "25";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<div class="common_searchbar01">
			<ul class="searchbar_left">
				<li class="last"><a href="#" class="edite_btn"><?php lang_print('BTN.save') ?></a></li>
			</ul>
		</div>
		<table summary="" class="common_table01">
			<colgroup>
				<col width="138px" />
				<col width="*" />
			</colgroup>
			<thead>
				<tr>
					<th><?php lang_print('105000.020') ?></th>
					<th><?php lang_print('105000.030') ?></th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td class="orther_align02"><?php lang_print('105000.040') ?></td>
					<td class="orther_align">http://as1234.net/wsjang</td>
				</tr>
				<tr>
					<td class="common_table_color02 orther_align02"><?php lang_print('105000.050') ?></td>
					<td class="common_table_color02">
						<ul class="setting_ul">
							<li><input type="radio" name="setting_radio1" id="setting_radio01" class="css-radio" /><label for="setting_radio01" class="css-label rdioGroup1"> http://as1234.net/wsjang</label></li>
							<li><input type="radio" name="setting_radio1" id="setting_radio02" class="css-radio" /><label for="setting_radio02" class="css-label rdioGroup1">  http://as1234.net/wsjang</label></li>
							<li class="last"><input type="radio" name="setting_radio1" id="setting_radio03" class="css-radio" /><label for="setting_radio03" class="css-label rdioGroup1"> http://as1234.net/wsjang</label></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td class="orther_align02"><?php lang_print('105000.060') ?></td>
					<td>
						<ul class="setting_ul">
							<li><input type="radio" name="setting_radio2" id="setting_radio04" class="css-radio" /><label for="setting_radio04" class="css-label rdioGroup2"> http://as1234.net/wsjang</label></li>
							<li class="last"><input type="radio" name="setting_radio2" id="setting_radio05" class="css-radio" /><label for="setting_radio05" class="css-label rdioGroup2">  http://as1234.net/wsjang</label></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td class="common_table_color02 orther_align02"><?php lang_print('105000.070') ?></td>
					<td class="common_table_color02"><p class="setting_td_p"><input type="text" name="" id="" value="" class="table_input03"/></p></td>
				</tr>
				<tr>
					<td class="orther_align02"><?php lang_print('105000.080') ?></td>
					<td class="orther_align">
						<ul class="setting_ul02">
							<li><input type="text" name="" id="" value="" class="table_input03"/></li>
							<li><input type="text" name="" id="" value="" class="table_input03"/></li>
							<li class="last"><input type="text" name="" id="" value="" class="table_input03"/></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td class="common_table_color02 orther_align02"><?php lang_print('105000.090') ?></td>
					<td class="common_table_color02"><p class="setting_td_p"><input type="text" name="" id="" value="" class="table_input03"/></p></td>
				</tr>

			</tbody>
		</table>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>