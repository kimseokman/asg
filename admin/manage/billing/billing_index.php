<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.010'); //"My account and billing";
$wz['pid']  = "5";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<div class="billing_div mb36">
			<p class="billing_tit"><?php lang_print('PTT.020') ?></p>
			<div class="billing_con_div">
				<p class="billing_index_p01 mb16"><? echo $user_product; ?></p>
				<dl class="billing_index_dl01">
					<dt><?php lang_print('101000.030', $admin->service_expired_date) ?></dt>
					<dd>
						<?php lang_print('101000.050', $admin->volume) ?>
					</dd>
				</dl>
				<!--p class="edit_absol_btn"><a href="/admin/manage/billing/plan_info.php"><?php lang_print('BTN.edit') ?></a></p-->
			</div>
		</div>

		<div class="billing_div mb36">
			<p class="billing_tit"><?php lang_print('101000.060') ?></p>
			<div class="billing_con_div">
				<p class="billing_index_p01">
					<? echo $_SESSION['adid']; ?><br />
					<?php lang_print('101000.080') ?>: ************
				</p>
				<p class="edit_absol_btn"><a href="/admin/manage/billing/login_info.php"><?php lang_print('BTN.edit') ?></a></p>
			</div>
		</div>

		<div class="billing_div">
			<p class="billing_tit"><?php lang_print('101000.090') ?></p>
			<div class="billing_con_div">
				<p class="billing_index_p01">
					<?php lang_print('101000.110') ?>: <? echo $_SESSION['adid']; ?><br />
					<?php lang_print('101000.120') ?>: <? echo $utils_obj->GetSiteLang(); ?>
				</p>
				<p class="edit_absol_btn">
					<a href="/admin/manage/billing/personal.php">
						<?php lang_print('BTN.edit') ?>
					</a>
				</p>
			</div>
		</div>

	</div>
</div>
<?
if($_POST['auto_login_flag'] == TRUE){
?>
<div class="fixed_dim"></div>
<div class="detail_pop">
	<dl class="detail_pop_dl02">
		<dt><?php lang_print('IDX.050') ?></dt>
	</dl>
	<ul class="detail_pop_btn">
		<li class="solo">
			<a href="/admin/manage/billing/billing_index.php" class="common_pop_btn01">
				<?php lang_print('BTN.ok') ?>
			</a>
		</li>
	</ul>
</div>
<?
}//end of : if($_POST['auto_login_flag'] == TRUE)
?>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>