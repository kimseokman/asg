<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.010'); //"My account and billing";
$wz['pid']  = "5";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">	
	<div class="cc">
		<p class="common_top_tit"><?php lang_print('PTT.020') ?></p>
		<p class="info_top_p01"><img src="/admin/img/plan_info_hand.gif" alt="" /><?php lang_print('101010.010') ?></p>
		<ul class="info_top_ul">
			<li class="info_color01"><?php lang_print('101010.020') ?></li>
			<li class="info_color02">
				<?php lang_print('101010.030', '10') ?>
				<p class="info_absol_btn"><a href="/admin/manage/billing/detail01.php"><?php lang_print('BTN.edit') ?></a></p>
			</li>
			<li class="info_color03 last">
				<?php lang_print('101010.050') ?>: Feb-05-2015<br />
				<?php lang_print('101010.060') ?>: Mar-05-2015
				<p class="info_top_total"><?php lang_print('101010.070') ?> : 400.00 USD</p>
			</li>
		</ul>

		<p class="info_red_p"><?php lang_print('101010.080') ?></p>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.090') ?></dt>
			<dd>
				<ul class="info_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="<?php lang_print('101010.100') ?>" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="<?php lang_print('101010.110') ?>" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.120') ?></dt>
			<dd>
				<ul class="info_con_ul02">
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last"><?php lang_print('101010.130') ?></li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl02">
			<dt><?php lang_print('101010.140') ?></dt>
			<dd>
				<ul class="info_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0" class="atg"><?php lang_print('101010.150') ?></a></li>
									<li><a href="javascript:;" data-path="" id="1" class="atg">Month2</a></li>
								</ul>
							</div>
						</div>
					</li>
					<li class="last">
						<div class="select_differ02_div">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="2" class="atg"><?php lang_print('101010.160') ?></a></li>
									<li><a href="javascript:;" data-path="" id="3" class="atg">Year2</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.170') ?></dt>
			<dd>
				<ul class="info_con_ul">
					<li><input type="text" name="" id="" value="" placeholder="<?php lang_print('101010.180') ?>" class="common_input"/></li>
					<li class="last"><img src="/index/en/img/card_icon.gif" alt="" /></li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.185') ?></dt>
			<dd>
				<ul class="info_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
					<li><input type="text" name="" id="" value="" placeholder="<?php lang_print('101010.190') ?>" class="common_input"/></li>
					<li class="last"><input type="text" name="" id="" value="" placeholder="<?php lang_print('101010.200') ?>" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.210') ?></dt>
			<dd>
				<ul class="info_con_ul">
					<li><input type="text" name="" id="" value="" class="common_input_error" disabled/></li>
					<li class="circle_close last"><?php lang_print('101010.220') ?></li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl02">
			<dt><?php lang_print('101010.230') ?></dt>
			<dd>
				<ul class="info_con_ul03">
					<li>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="4" class="atg">Country1</a></li>
									<li><a href="javascript:;" data-path="" id="5" class="atg">Country2</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</dd>
		</dl>
		<dl class="info_con_dl">
			<dt><?php lang_print('101010.240') ?></dt>
			<dd>
				<ul class="info_con_ul04">
					<li class="differ_li"><input type="text" name="" id="" value="" class="common_input"/></li>
				</ul>
			</dd>
		</dl>
		<ul class="info_btn_ul">
			<li><a href="#" class="save_btn"><?php lang_print('BTN.save') ?></a></li>
			<li><a href="#" class="cancel_btn"><?php lang_print('BTN.cancel') ?></a></li>
		</ul>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>