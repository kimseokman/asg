<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.010'); //"My account and billing";
$wz['pid']  = "5";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<p class="common_top_tit"><?php lang_print('PTT.040') ?></p>
		<p class="personal_p"><?php lang_print('101050.010') ?></p>
		<div class="personal_con_div">
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.020') ?></dt>
				<dd>
					<ul class="personal_ul01">
						<li><input type="text" name="" id="" value="" class="common_input"/></li>
						<li class="last"><input type="text" name="" id="" value="" class="common_input"/></li>
					</ul>
				</dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.030') ?></dt>
				<dd>
					<ul class="personal_ul01">
						<li>
							<div class="select_differ_div01">
								<div class="selectbox_differ">
									<ul>  
										<li><a href="javascript:;" data-path="" id="0" class="atg">English</a></li>
										<li><a href="javascript:;" data-path="" id="1" class="atg">korea</a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.040') ?></dt>
				<dd class="personal_dd_100">
					<div class="select_differ_div01">
						<div class="selectbox_differ">
							<ul>  
								<li><a href="javascript:;" data-path="" id="0" class="atg">(GMT-08:00) Pacific Time</a></li>
								<li><a href="javascript:;" data-path="" id="1" class="atg">(GMT-09:00) Pacific Time</a></li>
							</ul>
						</div>
					</div>
				</dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.050') ?></dt>
				<dd class="personal_dd_100"><input type="text" name="" id="" value="" class="common_input"/></dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.060') ?></dt>
				<dd>
					<ul class="personal_ul02">
						<li><input type="text" name="" id="" value="" class="common_input"/></li>
						<li><input type="text" name="" id="" value="" class="common_input"/></li>
						<li class="last"><input type="text" name="" id="" value="" class="common_input"/></li>
					</ul>
				</dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.070') ?></dt>
				<dd class="personal_dd_100"><input type="text" name="" id="" value="" class="common_input"/></dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.080') ?> (<?php lang_print('101050.110') ?>)</dt>
				<dd class="personal_dd_100"><input type="text" name="" id="" value="" class="common_input"/></dd>
			</dl>
			<dl class="personal_dl mb22">
				<dt><?php lang_print('101050.090') ?> (<?php lang_print('101050.110') ?>)</dt>
				<dd class="personal_dd_100"><input type="text" name="" id="" value="" class="common_input"/></dd>
			</dl>
			<dl class="personal_dl">
				<dt><?php lang_print('101050.100') ?> (<?php lang_print('101050.110') ?>)</dt>
				<dd class="personal_dd_100"><input type="text" name="" id="" value="" class="common_input"/></dd>
			</dl>
		</div>
		<ul class="detail_btn">
			<li><a href="#" class="save_btn"><?php lang_print('BTN.save') ?></a></li>
			<li class="last"><a href="#" class="cancel_btn"><?php lang_print('BTN.cancel') ?></a></li>
		</ul>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>