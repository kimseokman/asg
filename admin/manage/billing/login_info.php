<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.010'); //"My account and billing";
$wz['pid']  = "5";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<form name="admin_info" method="POST" action="/admin/_func/function.set_admin_info.php">
		<div class="cc">
			<p class="common_top_tit"><?php lang_print('PTT.030') ?></p>
			<dl class="admin_login">
				<dt><?php lang_print('101040.010') ?></dt>
				<dd><?php lang_print('101040.020') ?></dd>
			</dl>
			<div class="admin_login_div">
				<ul class="admin_input_ul mb30">
					<li class="input_ul_titi"><?php lang_print('101040.030') ?></li>
					<li><input type="text" name="admin_email" id="" value="<? echo $admin->email; ?>" class="common_input" placeholder="" disabled /></li>
					<li class="input_ul_right_area01"></li>
				</ul>
				<ul class="admin_input_ul mb30">
					<li class="input_ul_titi"><?php lang_print('101040.040') ?></li>
					<li><input type="password" name="admin_current_pw" id="" value="" class="common_input" /></li>
					<!--li class="input_ul_right_area01"><a href="/index/en/login/pw01.php" target="_blank"><?php lang_print('101040.050') ?></a></li-->
				</ul>
				<ul class="admin_input_ul mb30">
					<li class="input_ul_titi"><?php lang_print('101040.060') ?></li>
					<li><input type="password" name="admin_change_pw" id="admin_change_pw" value="" class="common_input" /></li>
					<li class="input_ul_right_area02"></li>
				</ul>
				<ul class="admin_input_ul">
					<li class="input_ul_titi"><?php lang_print('101040.080') ?></li>
					<li><input type="password" name="admin_retype_pw" id="admin_retype_pw" value="" class="common_input"/></li>
					<li class="input_ul_right_area03"></li>
				</ul>
			</div>
			<ul class="detail_btn">
				<li><a href="#" class="save_btn admin_info_save_btn"><?php lang_print('BTN.save') ?></a></li>
				<li class="last"><a href="#" class="cancel_btn admin_info_cancel_btn"><?php lang_print('BTN.cancel') ?></a></li>
			</ul>
		</div>
	</form>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>