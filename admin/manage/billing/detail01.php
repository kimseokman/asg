<?php

include_once("../../common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.010'); //"My account and billing";
$wz['pid']  = "5";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<p class="common_top_tit"><?php lang_print('PTT.020') ?></p>
		<div class="detail_div_p">
			<?php lang_print('101020.010') ?>
			<p class="absol_detail_p"><?php lang_print('101020.020', '1', '2') ?></p>
		</div>
		<div class="detail_div">
			<p class="detail_con_p"><?php lang_print('101020.030') ?></p>
			<div class="detail_con">
				<dl class="detail_sel_dl">
					<dt>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0"><?php lang_print('101020.040', '1+2') ?></a></li>
									<li><a href="javascript:;" data-path="" id="1"><?php lang_print('101020.040', '1+2') ?></a></li>
								</ul>
							</div>
						</div>
					</dt>
					<dd>
						<div class="select_differ_div01">
							<div class="selectbox_differ">
								<ul>  
									<li><a href="javascript:;" data-path="" id="0"><?php lang_print('101020.050', '1') ?></a></li>
									<li><a href="javascript:;" data-path="" id="1"><?php lang_print('101020.050', '2') ?></a></li>
								</ul>
							</div>
						</div>
					</dd>
				</dl>
				<ul class="common_radio_ul02">
					<li><input type="radio" name="buy_radiobutton1" id="buy_radio1" class="css-radio02" /><label for="buy_radio1" class="css-label02 radioGroup1"><?php lang_print('101020.060') ?></label></li>
					<li class="last"><input type="radio" name="buy_radiobutton1" id="buy_radio2" class="css-radio02" /><label for="buy_radio2" class="css-label02 radioGroup2"><?php lang_print('101020.060') ?><span class="buy_label_span"><?php lang_print('101020.070', '20') ?></span></label></li>
				</ul>
				<dl class="detail_total">
					<dt>
						<?php lang_print('101020.080') ?><br />
						<?php lang_print('101020.090', '10') ?>
					</dt>
					<dd>
						<?php lang_print('101020.100') ?> : 400.00 USD
					</dd>
				</dl>
			</div>
		</div>
		<ul class="detail_btn">
			<li><a href="/admin/manage/billing/detail02.php" class="save_btn"><?php lang_print('BTN.save') ?></a></li>
			<li class="last"><a href="#" class="cancel_btn"><?php lang_print('BTN.cancel') ?></a></li>
		</ul>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>