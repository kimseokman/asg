<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.050'); //"Manage Department";
$wz['pid']  = "10";

include_once("../../header.php");
?>
<!-- 내용시작 -->
<?
include_once("../../left_menu.php");

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "common_table_color01";
	}
	else{//isEven
		return "common_table_color02";
	}
}

function print_edit_row_class($depnum){
	if($_POST['depnum'] == $depnum){
		return "edit_on_input";
	}
	else{
		return "edit_off_input";
	}
}

function print_disabled($depnum){
	if($_POST['depnum'] == $depnum){
		return "";
	}
	else{
		return "disabled";
	}
}

if($_POST['dept_page_status']){//department add row event
	$dept_page_status = $_POST['dept_page_status'];
}
else{
	$dept_page_status = "NORMAL";	
}

$department_arr = $asg_obj->GetDepList();

?>
<form name="create_department_row_form" method="post">	
	<div class="sub_right">
		<div class="cc">
			<input type="hidden" name="dept_page_status" value="<? echo $dept_page_status; ?>" />
			<input type="hidden" name="depnum" value="<? echo $_POST['depnum']; ?>" />
			<input type="hidden" name="edit_dep_name" value="" />
			<p class="common_top_tit"><?php lang_print('PTT.050') ?></p>
			<a href="#" class="create_btn mb10 department_add_row_btn">
				<?php lang_print('BTN.create') ?>
			</a>
			<table summary="" class="common_table01">
				<colgroup>
					<col width="138px" />
					<col width="*" />
					<col width="138px" />
					<col width="139px" />
					<col width="138px" />
				</colgroup>
				<thead>
					<tr>
						<th><?php lang_print('102000.040') ?></th>
						<th><?php lang_print('102000.050') ?></th>
						<th><?php lang_print('102000.060') ?></th>
						<th><?php lang_print('102000.070') ?></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?
					if(count($department_arr) != 0){
						$table_row_index = 0;
						while($department_list = each($department_arr)){
							
							//echo print_r($department_list[value]);
							$department_row = $department_list[value];
							$table_row = $department_row[numofspt];//use in html

							$etc_spt_row = $table_row-1;
							$table_spt_row_index = 0;
							$spt_id_arr = $department_row[sptarray];
							$first_spt_id = array_shift($spt_id_arr);

							$spt_name_arr = $department_row[sptname];
							$first_spt_name = array_shift($spt_name_arr);						
					?>
					<tr>
						<td rowspan="<? echo $table_row; ?>">
							<input type="text" id="" name="" value="<? echo $department_row[depname]; ?>" class="table_input <? echo print_edit_row_class($department_row[depnum]); ?>" <? echo print_disabled($department_row[depnum]); ?>  />
						</td>
						<td rowspan="<? echo $table_row; ?>" class="<? echo print_table_class($table_row_index); ?>">
							<input type="text" id="" name="" value="<? echo $department_row[numofspt]; ?>" class="table_input"  disabled  />
						</td>
						<td>
							<input type="text" id="" name="" value="<? echo $first_spt_id; ?>" class="table_input"  disabled  />
						</td>
						<td class="<? echo print_table_class($table_spt_row_index); ?>">
							<input type="text" id="" name="" value="<? echo $first_spt_name; ?>" class="table_input"  disabled  />
						</td>
						<td rowspan="<? echo $table_row; ?>">
							<ul class="table_btn_ul01">
								<li>
									<a href="#" class="table_edit_btn" onclick="department_edit_row(event, <? echo $department_row[depnum]; ?>)">
										<?php lang_print('BTN.edit') ?>
									</a>
								</li>
								<li class="last">
									<a href="#" class="table_del_btn" onclick="department_del_row(event, <? echo $department_row[depnum]; ?>)">
										<?php lang_print('BTN.delete') ?>
									</a>
								</li>
							</ul>
						</td>
					</tr>
					<?
							for($i = 0; $i < $etc_spt_row; $i++){
								$table_spt_row_index++;
					?>
					<tr>
						<td>
							<input type="text" id="" name="" value="<? echo $spt_id_arr[$i];?>" class="table_input"  disabled  />
						</td>
						<td class="<? echo print_table_class($table_spt_row_index); ?>">
							<input type="text" id="" name="" value="<? echo $spt_name_arr[$i];?>" class="table_input"  disabled  />
						</td>
					</tr>	
					<?		
							}//end of : for($i = 0; $i < $table_row; $i++)
					?>
					<?
							$table_row_index++;
						}//end of : while($department_list = each($department_arr))
					}//end of : if(count($deparray) != 0)
					?>
					<?
					if($dept_page_status == 'ADD_ROW'){
					?>
					<tr>
						<td class="<? echo print_table_class($table_row_index); ?>">
							<input type="text" id="" name="create_dept_name" value="" placeholder="Dept Name Input" class="table_input" />
						</td>
						<td class="<? echo print_table_class($table_row_index); ?>">&nbsp;</td>
						<td class="<? echo print_table_class($table_row_index); ?>">&nbsp;</td>
						<td class="<? echo print_table_class($table_row_index); ?>">&nbsp;</td>
						<td class="<? echo print_table_class($table_row_index); ?>">&nbsp;</td>
					</tr>
					<?
					}//end of : if($dept_page_status == 'ADD_ROW')
					?>
				</tbody>
			</table>
		</div>
	</div>
</form>

<?
if($dept_page_status == 'DEL_ROW'){//case by delete row open modal
?>
<div class="fixed_dim"></div>
<div class="detail_pop">
	<dl class="detail_pop_dl02">
		<dt><?php lang_print('102000.080') ?></dt>
		<dd><?php lang_print('102000.090') ?></dd>
	</dl>
	<ul class="detail_pop_btn">
		<li>
			<a href="#" class="common_pop_btn01 department_popup_yes_btn">
				<?php lang_print('BTN.yes') ?>
			</a>
		</li>
		<li class="last">
			<a href="#" class="common_pop_btn02 department_popup_cancel_btn">
				<?php lang_print('BTN.cancel') ?>
			</a>
		</li>
	</ul>
</div>
<?	
}//end of : if($dept_page_status == 'DEL_ROW')
?>

<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>