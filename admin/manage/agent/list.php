<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.060'); //"Account Agent";
$wz['pid']  = "15";

include_once("../../header.php");

$asg_obj = new ASGMain();

//make order link
if($_GET['order'] == ""){
	$order = "asc";
}else if($_GET['order'] == "asc"){
	$order = "desc";
}else if($_GET['order'] == "desc"){
	$order = "asc";
}

$link = "?";
$link .= "order=".$order;

if($_POST['search'] != ""){
	$link .= "&search=".$_POST['search'];
	$search = $_POST['search'];
}else if($_GET['search'] != ""){
	$link .= "&search=".$_GET['search'];
	$search = $_GET['search'];
}else{
	$search = "";
}

if($_POST['searchVal'] != ""){
	$link .= "&searchVal=".$_POST['searchVal'];
	$searchVal = $_POST['searchVal'];
}else if($_GET['searchVal'] != ""){
	$link .= "&searchVal=".$_GET['searchVal'];
	$searchVal = $_GET['searchVal'];
}else{
	$searchVal = "";
}

$spt_empty = array(
	"en" => "There is no information agent. Please create an agent.",
	"jp" => "エージェント情報はありません。カウンセラーを生成してください。",
	"kr" => "상담원 정보가 없습니다. 상담원을 생성해 주세요."
);
$spt_empty_lang = $_SESSION['language'];

$spt_arr = $asg_obj->GetSupporterList($search, $searchVal, $order);

$admin_arr = $asg_obj->GetAdminInfo();

$current_total_supporter_num = $asg_obj->GetSupporterNum();				//전체 생성 상담원 수
$current_mobile_supporter_num = $asg_obj->GetMobileSupporterNum();			//pc + mobile 상담원 수
$current_pc_supporter_num = $current_total_supporter_num - $current_mobile_supporter_num; //pc 상담원 수

$function = $adminarray->function;
$total_volume = $admin_arr->volume;			//전체 상담원 volume
$mobile_volume = $adminarray->mobile_volume;

function print_table_td_class($tr_index, $td_index){
	$tr_index%=2;
	if($tr_index > 0){//case by tr : Odd
		return "common_table_color02";
	}else{//case by tr : Even
		$td_index%=2;
		if($td_index > 0)
			return "common_table_color01";
		return "";
	}
}

if($order == "asc"){
	$order_type = "<img src='/admin/img_old/icon/asc_icon.gif' />";
}else{
	$order_type = "<img src='/admin/img_old/icon/desc_icon.gif' />";
}

?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<p class="common_top_tit"><?php lang_print('PTT.070') ?></p>
		<ul class="agent_info_top">
			<li>
				<dl class="agent_info_top_dl">
					<dt><?php lang_print('103010.010') ?></dt>
					<dd>
					<?
						echo $current_total_supporter_num."/".$total_volume;
					?>
					</dd>
				</dl>
			</li>
			<li>
				<dl class="agent_info_top_dl">
					<dt><?php lang_print('103010.020') ?></dt>
					<dd>
					<?
						echo $current_pc_supporter_num;
					?>
					</dd>
				</dl>
			</li>
			<li class="last">
				<dl class="agent_info_top_dl">
					<dt><?php lang_print('103010.030') ?></dt>
					<dd>
					<?
						echo $current_mobile_supporter_num;
					?>
					</dd>
				</dl>
			</li>
		</ul>
		<div class="common_searchbar01">
			<ul class="searchbar_left">
				<li><a href="/admin/manage/agent/create.php" class="add_agent_btn"><?php lang_print('BTN.add_agent') ?></a></li>
				<li class="last"><a href="#" class="refresh_btn"><?php lang_print('BTN.refresh') ?></a></li>
			</ul>
			<dl class="searchbar_dl05">
				<dt><?php lang_print('103010.060') ?></dt>
				<dd>
					<form name="agent_list_search" method="post" action="list.php">
					<ul class="searchbar_right">
						<li>
							<select name="search">
								<option value="sptid">상담원 ID</option>
								<option value="name">상담원 명</option>
							</select>
						</li>
						<li>
							<input class="agent_search_value" name="searchVal" value="" />	
						</li>
						<li class="last"><a href="#" class="search_btn agent_search_btn"><img src="/admin/img/admin_plus_icon.png" alt="search icon" /></a></li>
					</ul>
					</form>
				</dd>
			</dl>
		</div>

		<table summary="" class="common_table01">
			<colgroup>
				<col width="80px" />
				<col width="80px" />
				<col width="100px" />
				<col width="100px" />
				<col width="*" />
				<col width="134px" />
				<col width="100px" />
			</colgroup>
			<thead>
				<tr>
					<th>
						<a href="<? echo $link; ?>" class="agent_list_order">
						<?
							lang_print('103010.070');
							echo $order_type;
						?>
						</a>
					</th>
					<th><?php lang_print('103010.080') ?></th>
					<th><?php lang_print('103010.090') ?></th>
					<th><?php lang_print('103010.100') ?></th>
					<th><?php lang_print('103010.110') ?></th>
					<th><?php lang_print('103010.120') ?></th>
					<th><?php lang_print('103010.130') ?></th>
				</tr>
			</thead>
			<tbody class="agent_search_table">
				<?
				if(count($spt_arr) == 0) {//상담원 존재하지 않을 경우
				?>
				<tr>
					<td colspan="7">
						<? echo $spt_empty[$spt_empty_lang]; ?>
		                    		</td>
		                            </tr>
				<?
				}else{
					$row_index = 0;
					$column_index = 0;
					while(list($key, $val) = each($spt_arr)){
						//set status type
						if($val["logon"] == 0){
							$status_type = "
								<img src=\"/admin/img_old/icon/offline_icon.gif\" />
								<span class=\"font_color_offline\">
									Logout
								</sapn>
							";
						} else if($val["logon"] == 1) {
							if($val["cnt"] == 0) {
								$status_type = "
									<img src=\"/admin/img_old/icon/online_icon.gif\" />
									<span class=\"font_color_online\">
										Login
									</span>";
							} else {
								$status_type = "
									<img src=\"/admin/img_old/icon/supporter_ing.gif\" />
									<span class=\"font_color_service\">
										Calling
									</span>
								";
							}
						} else if($val["logon"] == 2) {
							$status_type = "
								<img src=\"/admin/img_old/icon/supporter_ing.gif\" />
								<span class=\"font_color_service\">
									Calling
								</span>
							";
						}

						//set right type
						$right_type = "<img src=\"/admin/img_old/icon/pc.gif\" />";

						if( $val["function"] & PERMISSION_ALL_IN_ONE){
							$right_type = $right_type."<img src=\"/admin/img_old/icon/mobile_edition.gif\" />"."<img src=\"/admin/img_old/icon/video_edition.gif\" />";
						}
					
						//set department name
						$depname = $asg_obj->GetDepByDepnum($val["depnum"]);
						$depname = $depname->depname;
				?>
				<tr>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
						<a class="shortcuts_link" href="/admin/manage/agent/edit.php?sptnum=<? echo $val["sptnum"]; ?>">
							<? echo $val["sptid"]; ?>	
						</a>
					<? 
						$column_index++;
					?>
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
					<? 
						echo $val["name"]; 
						$column_index++;
					?>	
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
					<? 
						echo $depname; 
						$column_index++;
					?>	
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
					<? 
						echo $status_type; 
						$column_index++;
					?>
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
						<? 
						echo $val["lastlogin"]; 
						$column_index++;
					?>
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
					<? 
						echo $val["spt_pub_ip"].'<br />'.$val["spt_pri_ip"]; 
						$column_index++;
					?>
					</td>
					<td class="<? echo print_table_td_class($row_index, $column_index); ?>">
					<? 
						echo $right_type; 
						$column_index++;
					?>	
					</td>
				</tr>
				<?
						$row_index++;
					}//end of : while(list($key, $val) = each($spt_arr))
				}//end of : if(count($spt_arr) != 0) //상담원이 존재 할 경우
				?>
			</tbody>
		</table>

	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>