<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.060'); //"Account Agent";
$wz['pid']  = "15";

include_once("../../header.php");

$admin_arr = $asg_obj->GetAdminInfo();

$current_total_supporter_num = $asg_obj->GetSupporterNum();				//전체 생성 상담원 수
$current_mobile_supporter_num = $asg_obj->GetMobileSupporterNum();			//pc + mobile 상담원 수
$current_pc_supporter_num = $current_total_supporter_num - $current_mobile_supporter_num; //pc 상담원 수

$total_volume = $admin_arr->volume;			//전체 상담원 volume

$maximum_volume = $total_volume - $current_total_supporter_num;

$dep_arr = $asg_obj->GetDepList();
?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<p class="common_top_tit"><?php lang_print('PTT.080') ?></p>
		<?
		if(count($dep_arr) == 0) {
		?>
		<p class="agent_create_p"><?php lang_print('103020.000') ?></p>
		<ul class="agent_btn_ul">
			<li><a href="/admin/manage/department/list.php" class="shortcuts_btn"><?php lang_print('BTN.shortcuts') ?></a></li>
			<li class="last"><a href="/admin/manage/agent/list.php" class="agent_list_btn"><?php lang_print('BTN.go_list') ?></a></li>
		</ul>
		<?
		}else{//end of : if(count($dep_arr) == 0)
		?>
		<ul class="agent_info_top">
			<li class="last">
				<dl class="agent_info_top_dl">
					<dt><?php lang_print('103020.010') ?></dt>
					<dd>
					<?
						echo $current_total_supporter_num."/".$total_volume;
					?>
					</dd>
				</dl>
				<p class="agent_info_top_p">*<?php lang_print('103020.020', $maximum_volume) ?></p>
			</li>
		</ul>
		<div class="agent_create">
			<form name="agent_create_info_from" method="post" action="/admin/_func/function.create_agent.php">
			<p class="agent_create_p"><?php lang_print('103020.030') ?></p>
			<table summary="" class="common_table02">
				<colgroup>
					<col width="181px" />
					<col width="*" />
				</colgroup>
				<tbody>

					<tr>
						<th><?php lang_print('103020.040') ?></th>
						<td>
							<p class="create_td_p"><input type="text" id="" name="new_spt_id" value="" class="common_input agent_create_id" /></p>
							<span class="common_table02_span agent_create_id_res">
								<?php //lang_print('103020.050') ?>
							</span>
						</td>
					</tr>
					<tr>
						<th class="common_table_color03"><?php lang_print('103020.060') ?></th>
						<td>
							<p class="create_td_p"><input type="password" id="" name="new_spt_pw" value="" class="common_input agent_change_pw" /></p>
							<span class="common_table02_span agent_change_pw_res"></span>
						</td>
					</tr>
					<tr>
						<th><?php lang_print('103020.070') ?></th>
						<td>
							<p class="create_td_p"><input type="password" id="" name="new_spt_retype_pw" value="" class="common_input agent_retype_pw" /></p>
							<span class="common_table02_span agent_retype_pw_res"></span>
						</td>
					</tr>
					<tr>
						<th class="common_table_color03"><?php lang_print('103020.080') ?></th>
						<td>
							<p class="create_td_p"><input type="text" id="" name="new_spt_name" value="" class="common_input" /></p>
						</td>
					</tr>
					<tr>
						<th><?php lang_print('103020.090') ?></th>
						<td>
							<p class="create_td_p"><input type="text" id="" name="new_spt_phone" value="" class="common_input" /></p>
						</td>
					</tr>
					<tr>
						<th class="common_table_color03"><?php lang_print('103020.100') ?></th>
						<td>
							<ul class="create_td_ul">
								<li><input type="text" id="" name="new_spt_email" value="" class="common_input" /></li>
							</ul>
						</td>
					</tr>
					<tr>
						<th><?php lang_print('103020.110') ?></th>
						<td>
							<p class="create_td_p"><input type="text" id="" name="new_spt_company" value="<? echo $_SESSION['company']; ?>" class="common_input" /></p>
						</td>
					</tr>
					<tr>
						<th class="common_table_color03"><?php lang_print('103020.120') ?></th>
						<td>
							<select name="new_spt_depnum">
								<option selected value="0"><?php lang_print('103020.121') ?></option>
							<?
							while(list($key, $val) = each($dep_arr)){
							?>
								<option value="<? echo $val["depnum"]; ?>"><? echo $val["depname"]; ?></option>
							<?
							}//end of : while(list($key, $val) = each($dep_arr))
							?>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>
		<ul class="agent_btn_ul">
			<li><a href="#" class="agent_create_btn"><?php lang_print('BTN.create') ?></a></li>
			<li class="last"><a href="/admin/manage/agent/list.php" class="agent_list_btn"><?php lang_print('BTN.go_list') ?></a></li>
		</ul>
		<?
		}//end of : if(count($dep_arr) != 0)
		?>
	</div>
</div>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>