<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");;

$wz['gid']  = "0";
$wz['adminsel']  = "10";
$wz['gtt']  = lang('PTT.060'); //"Account Agent";
$wz['pid']  = "15";

include_once("../../header.php");

$page = array(//page define
	"MODIFY_DENY"=> "/admin/manage/agent/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"MODIFY_DENY"=> "상담원 정보를 얻을 수 없습니다. 다시 시도해주세요."
);

$msg_en = array(//message define
	"MODIFY_DENY"=> "You can not get the information agent. Please try again."
);

$msg_jp = array(//message define
	"MODIFY_DENY"=> "エージェントの情報を取得することができません。再試行してください。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

$sptdata = $asg_obj->GetSupporterBySptnum($_GET["sptnum"]);

if($sptdata == NULL) {
	echo "
		<script>
			alert('".$msg[$msg_lang]['MODIFY_DENY']."');
			location.href='".$page['MODIFY_DENY']."';
		</script>
	";
}

$adminarray = $asg_obj->GetAdminInfo();
$mobile_volume = $adminarray->mobile_volume;
$dep_arr = $asg_obj->GetDepList();

if($_GET["del_flag"]){
	$del_flag = $_GET["del_flag"];
}else{
	$del_flag = FALSE;
}

?>
<!-- 내용시작 -->
<?
	include_once("../../left_menu.php");
?>
<div class="sub_right">
	<div class="cc">
		<form name="agent_info_from" method="post" action="/admin/_func/function.edit_agent.php">
			<input  type="hidden" name="sptnum" value="<? echo $_GET["sptnum"]; ?>" />

			<p class="common_top_tit"><?php lang_print('PTT.090') ?></p>
			<p class="agent_del_p"><a href="/admin/manage/agent/edit.php?sptnum=<? echo $_GET["sptnum"] ?>&del_flag=<? echo TRUE; ?>" class="agent_del_btn"><?php lang_print('BTN.delete') ?></a></p>
			<div class="agent_create">
				<p class="agent_create_p"><?php lang_print('103030.010') ?></p>
				<table summary="" class="common_table02">
					<colgroup>
						<col width="181px" />
						<col width="*" />
					</colgroup>
					<tbody>

						<tr>
							<th><?php lang_print('103030.020') ?></th>
							<td><? echo $sptdata->sptid; ?></td>
						</tr>
						<tr>
							<th class="common_table_color03"><?php lang_print('103030.030') ?></th>
							<td>
								<p class="create_td_p">
									<input type="password" id="" name="spt_pw" value="" class="common_input agent_change_pw" />
								</p>
								<span class="common_table02_span agent_change_pw_res"></span>
							</td>
						</tr>
						<tr>
							<th><?php lang_print('103030.040') ?></th>
							<td>
								<p class="create_td_p">
									<input type="password" id="" name="spt_retype_pw" value="" class="common_input agent_retype_pw" />
								</p>
								<span class="common_table02_span agent_retype_pw_res"></span>
							</td>
						</tr>
						<tr>
							<th class="common_table_color03"><?php lang_print('103030.050') ?></th>
							<td>
								<p class="create_td_p">
									<input type="text" id="" name="spt_name" value="<? echo $sptdata->name; ?>" class="common_input" />
								</p>
							</td>
						</tr>
						<tr>
							<th><?php lang_print('103030.060') ?></th>
							<td>
								<p class="create_td_p"><input type="text" id="" name="spt_phone" value="<? echo $sptdata->phone; ?>" class="common_input" /></p>
							</td>
						</tr>
						<tr>
							<th class="common_table_color03"><?php lang_print('103030.070') ?></th>
							<td>
								<ul class="create_td_ul">
									<li><input type="text" id="" name="spt_email" value="<? echo $sptdata->email; ?>" class="common_input" /></li>
								</ul>
							</td>
						</tr>
						<tr>
							<th><?php lang_print('103020.110') ?></th>
							<td>
								<p class="create_td_p"><input type="text" id="" name="spt_company" value="<? echo $sptdata->company; ?>" class="common_input" /></p>
							</td>
						</tr>
						<tr>
							<th class="common_table_color03"><?php lang_print('103020.120') ?></th>
							<td>
								<select name="spt_depnum">
									<option value="0"><?php lang_print('103020.121') ?></option>
								<?
								while(list($key, $val) = each($dep_arr)){
									if($sptdata->depnum == $val["depnum"]){
								?>
										<option selected value="<? echo $val["depnum"]; ?>"><? echo $val["depname"]; ?></option>
								<?		
									}else{//end of : if($sptdata->depnum == $val["depnum"])
								?>
										<option value="<? echo $val["depnum"]; ?>"><? echo $val["depname"]; ?></option>
								<?
									}//end of : if($sptdata->depnum != $val["depnum"])
								}//end of : while(list($key, $val) = each($dep_arr))
								?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</from>
		<ul class="agent_btn_ul">
			<li><a href="#" class="agent_create_btn agent_edit_btn"><?php lang_print('BTN.edit') ?></a></li>
			<li class="last"><a href="/admin/manage/agent/list.php" class="agent_list_btn"><?php lang_print('BTN.go_list') ?></a></li>
		</ul>
	</div>
</div>
<?
if($del_flag == TRUE){
?>
<div class="fixed_dim"></div>
<div class="detail_pop">
	<dl class="detail_pop_dl02">
		<dt><?php lang_print('103030.100') ?></dt>
		<dd><?php lang_print('103030.110') ?></dd>
	</dl>
	<ul class="detail_pop_btn">
		<li><a href="/admin/_func/function.delete_agent.php?sptnum=<? echo $_GET["sptnum"] ?>" class="common_pop_btn01"><?php lang_print('BTN.yes') ?></a></li>
		<li class="last"><a href="/admin/manage/agent/edit.php?sptnum=<? echo $_GET["sptnum"] ?>" class="common_pop_btn02"><?php lang_print('BTN.cancel') ?></a></li>
	</ul>
</div>
<?	
}//end of : if($del_flag == TRUE)
?>
<!-- 내용끝 -->
<?
include_once("../../footer.php");
?>