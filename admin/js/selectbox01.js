var shift = false;

$(document).ready(function(){

    selectbox_AD();

    $(document).keydown(function(event){

        if(event.keyCode == 16 && shift != true) shift = true;

    });

    $(document).keyup(function(event){

        if(event.keyCode == 16) shift = false;

    });

});

function selectbox_AD(){

    var sb = $(".selectbox");

    sb.ul = sb.find(">ul");

    sb.ul.li = sb.ul.find(">li");

    sb.ul.li.a = sb.ul.li.find(" a");

    sb.ul.li.inputs = sb.ul.li.find(" input");

    sb.shift = false;

     

    for(var i=0; i<sb.size(); i++ ){

        if(sb.eq(i).hasClass("selectbox_top")){

            sb.eq(i).find(">ul").css({"position":"absolute","left":"0","bottom":"0"});

        } else {

            sb.eq(i).find(">ul").css({"position":"absolute","left":"0","top":"0"});

        }

    }

 

    sb.ul.li.hide();

    sb.ul.li.find(">input:checked").removeAttr("checked");

    sb.ul.find(">li:first").show().addClass("on").find(" input[type=radio]").attr("checked","checked");

 

    //마우스및 포커스시 해당 selectbox포인트

    $(".selectbox a, .selectbox input, .selectbox>ul").on("mouseover focus",function(){

        $(this).parents(".selectbox").addClass("on");

    });

    $(".selectbox a, .selectbox input, .selectbox>ul").on("mouseout blur",function(){

        $(this).parents(".selectbox").removeClass("on");

    });

     

    //링크이벤트

    sb.ul.li.a.click(function(e){

        var obj = $(this).parents(".selectbox");

        if(obj.find(">ul>li:hidden").size() != 0){

            e.preventDefault();     

            obj.find(">ul>li").show();

            obj.css("z-index","100");

        } else {

            obj.find(">ul>li").hide();

            obj.find(">ul>li.on").removeClass();

            $(this).parent().show().addClass("on");

            obj.css("z-index","0");

        }

    });

    sb.ul.on("mouseleave blur",function(){

        var obj = $(this).parents(".selectbox");

        obj.find(">ul>li").hide();

        obj.find(">ul>li.on").show();

        obj.css("z-index","0");

    });

    $(".selectbox > ul > li > a, .selectbox > ul > li > input").on("keypress",function(e){

        if(e.keyCode == 27){

            var obj = $(this).parents(".selectbox");

            obj.find(">ul>li").hide();

            obj.find(">ul>li.on").show();

            obj.css("z-index","0");

        }

    });

    sb.ul.find(">li:last>a").keypress(function(e){        

        if(shift == false && e.keyCode == 9){

            var obj = $(this).parents(".selectbox");

            obj.find(">ul>li").hide();

            obj.find(">ul>li.on").show();

        }

    });

    sb.ul.find(">li:first>a").keypress(function(e){

        if(shift == true && e.keyCode == 9){

            var obj = $(this).parents(".selectbox");

            obj.find(">ul>li").hide();

            obj.find(">ul>li.on").show();

        }

    });

 

 

    //form select

    sb.ul.li.click(function(e){

        var obj = $(this).parents(".selectbox");

         

        if(obj.find(" input").size() != 0){

            if(obj.find(">ul>li:hidden").size() != 0){

                e.preventDefault();

                obj.find(">ul>li").show();

                obj.css("z-index","100");

            } else {

                e.preventDefault();

                obj.find(">ul>li.on").removeClass("on");

                //obj.find(">ul>li").hide();

                obj.find(">ul>li>input:checked").removeAttr("checked");

                $(this).show().addClass("on").find("input[type=radio]").attr("checked","checked");

 

                //모바일에서만 작동

                $(document).on("touchstart",function(){

                    obj.find(">ul>li:not('.on')").hide();

                });

            }

        }

         

    });

     

    $(".selectbox > ul > li > input").keypress(function(e){    

        if(e.keyCode == 13){

            var obj = $(this).parents(".selectbox");

 

            if(obj.find(">ul>li:hidden").size() != 0){

                e.preventDefault();

                obj.find(">ul>li").show();

                obj.css("z-index","100");

            } else {

                obj.find(">ul>li").hide();

                $(this).parent().show();

            }

        } else if(e.keyCode == 9){

            var obj = $(this).parents(".selectbox");

            obj.find(">ul>li").hide();

            $(this).parent().show();

        }

    });

 

     

}