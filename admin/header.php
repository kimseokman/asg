<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=1220,maximum-scale=1.5,user-scalable=yes">
<title><?php lang_print('LOT.000') ?></title>
<link rel="stylesheet" href="/admin/css/base.css" type="text/css" />

<script type="text/javascript" src="/_lib/_inc/jquery/1.11.3/jquery-1.11.3.min.js"></script>

<script type="text/javascript" src="/admin/js/selectbox01.js"></script>
<script type="text/javascript" src="/admin/js/selectbox02.js"></script>
<script type="text/javascript" src="/admin/js/placeholders.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js"></script>
<script type="text/javascript">
	
	$( document ).ready( function() {
		var jbOffset = $( '.sub_left').offset();
		$( window ).scroll( function() {
		 if ( $( document ).scrollTop() > jbOffset.top ) {
			$( '.sub_left' ).addClass( 'subFixed' );
		  }
		else {
			$( '.sub_left' ).removeClass( 'subFixed' );
			}
		});
	});
	
	<? 
	if ($_SESSION['language'] == "kr") { 
	?>
		WebFont.load({
			google: {// For google fonts
				families: ['Droid Sans', 'Droid Serif']
			},
			custom: {// For early access or custom font
				families: ['Nanum Gothic'],
				urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
			} 
		});
	<?
	} 
	?>
</script>

</head>

<body class="lang_<?php echo $_SESSION['language'] ?>">
<!-- 내용시작 -->
<div id="wrap">
	<?	
	if($_SESSION['usertype'] == 'ADMIN'){
	?>
	<div id="header_box">
		<ul class="header_box_ul">
			<li><a href="#"><?php lang_print('LOT.040') ?></a></li>
			<li><a href="#" class="logout_btn"><?php lang_print('LOT.050') ?></a></li>
		</ul>
	</div>
	<?
	}// end of : if($_SESSION['usertype'] == 'ADMIN')
	else if($_SESSION['usertype'] == 'SUPPORTER'){
	?>
	<div id="header_box" class="SUPPORTER_header_box">
		<ul class="header_box_ul supporter_header_box">
			<li><a href="#" class="logout_btn"><?php lang_print('LOT.050') ?></a></li>
		</ul>
	</div>	
	<?
	}// end of : if($_SESSION['usertype'] == 'ADMIN')
	?>
	<script type="text/javascript">
		function header_box(id) {
			var e = document.getElementById(id);
			if(e.style.display == 'block')
				e.style.display = 'none';
			else
				e.style.display = 'block';
		}
	</script>
	<div id="header">
		<div class="header_top">
			<h1 class="logo"><a href="/admin/index.php"><img src="/admin/img/admin_logo.gif" alt="Koino admin logo" /></a></h1>
			<?
			if($_SESSION['usertype'] == 'ADMIN'){
			?>
			<p class="header_top_btn01">
				<a href="/admin/manage/billing/billing_index.php" <?if($wz['adminsel']=="10"){?>class="on"<?}?>><?php lang_print('LOT.010') ?></a>
			</p>
			<?
			}// end of : if($_SESSION['usertype'] == 'ADMIN')
			?>
			<ul class="admin_util">
				<li>
					<a href="#" style="cursor:default;">
					<?
						$hello_id = 'GUEST';
						if($_SESSION['usertype']=='ADMIN'){
							$hello_id = $_SESSION['adid'];
						}else if($_SESSION['usertype']=='SUPPORTER'){
							$hello_id = $_SESSION['sptid'];
						}
												
						lang_print('LOT.020', $hello_id) ;
					?>
					</a>
				</li>
				<!--li><a href="#" onclick="header_box('header_box');"><?php lang_print('LOT.030') ?></a></li-->
				<li><a href="#" class="logout_btn"><?php lang_print('LOT.050') ?></a></li>
				<li><a href="#"><?php lang_print('LOT.060') ?></a></li>
				<li class="last">
					<select onchange="if(this.value) location.href=(this.value);" class="header_select">
						<option value="">Languages</option>
						<?
						while(list($k, $v) = each($lang_list)){
						?>
						<option value="<? echo $utils_obj->SetBasePage($_SESSION['usertype']); ?>?language=<? echo $k; ?>" <? echo $utils_obj->PrintSelectedLang($_SESSION['language'], $k); ?>><? echo $v; ?></option>
						<?
						}// end of : while(list($k, $v) = each($lang_list))
						?>
					</select>
				</li>
			</ul>
		</div>
	</div>
<!-- 내용끝 -->
<div id="container">
	<?if( $wz['gid'] == "HM" ){?>
		<div class="location_header">
			<div class="location_div">
				<p class="location_p"><?=$wz['gtt']?></p>
				<dl class="admin_buy">
					<dt><a href="#" target="_blank"><?php lang_print('IDX.060') ?></a></dt>
					<dd><?php lang_print('IDX.070', '40') ?></dd>
				</dl>
			</div>
		</div>
		<div id="main_content">
	<?}else{?>
		<div class="location_header">
			<div class="location_div">
				<p class="location_p02"><?=$wz['gtt']?></p>
			</div>
		</div>
		<div id="content">
	<?}?>