<?php
/**
 * Name : search_condition_days.php
 * Version : AnySupport Global
 * Date :  2015.06.12
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$utils_obj = new ASGUtils();

$this_last_day = $utils_obj->GetLastDay($_POST['month'], $_POST['year']);

$day_arr = $utils_obj->GetDays($this_last_day);

if($_POST['type']){
	$search_date = $utils_obj->GetSearchDate($_POST['type']);
}

foreach($day_arr as $k => $v){
?>
<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($search_date["D"], $k); ?>>
	<? echo $v; ?> <?php lang_print('UNIT.day') ?>
</option>
<?	
}//end of : foreach($day_arr as $k => $v)