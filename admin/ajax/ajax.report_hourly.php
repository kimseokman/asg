<?php
/**
 * Name : report_hourly.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportByTime($_POST['start_time'], $_POST['end_time']);

$total = 0;

for($i=0;$i<24;$i++){
	$data[$i]->ccount = 0;
	$data[$i]->totaltime = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],11,2));
	$z = $t;

	$data[$z]->ccount++;
	$total++;
	
	//zisako 2014.1.23 통계데이터 -값 오류 수정
	$intervaltime = $row['intervaltime'];
	if($intervaltime<0) $intervaltime = 0;
	$data[$z]->totaltime +=$intervaltime;
}

$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]->ccount/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

$last_node = sizeof($data)-1;

function getTimeFromSeconds($seconds){
	$h = sprintf("%02d", intval($seconds) / 3600);
	$tmp = $seconds % 3600;
	$m = sprintf("%02d", $tmp / 60);
	$s = sprintf("%02d", $tmp % 60);
	return $h.':'.$m.':'.$s;
}

$totaltotaltime;
for($i=0;$i<sizeof($data);$i++){
	$totaltotaltime += $data[$i]->totaltime;
}
?>
<p class="common_grapic_tit"><?php lang_print('204000.120') ?></p>
<div class="admin_grapic_div report_hourly_chart" id="admin_grapic_div" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script>
	$(document).ready(function(){
		$('.report_hourly_chart').highcharts({
			chart: {
				marginTop: 50,
				type: 'column'
			},
			title: {
				text: ''
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				categories: [
				<? 	
					for($i=0; $i < sizeof($data); $i++){
						echo "'".$i."'";
						if($i != $last_node){
							echo ",";	
						}
					}
					
				?>
				],
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Session'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'count',
				data: [
				<?
					for($i=0; $i < sizeof($data); $i++){
						echo $data[$i]->ccount;
						if($i != $last_node){
							echo ",";	
						}
					}
				?>
				]
			}]
		});
	});
</script>

<p class="table_bottom_p"></p>

<table summary="" class="common_table03 report_search_table">
	<colgroup>
		<col width="90px" />
		<col width="110px" />
		<col width="194px" />
		<col width="*" />
		<col width="140px" />
	</colgroup>
	<thead>
		<tr>
			<th><?php lang_print('204000.070') ?></th>
			<th><?php lang_print('204000.080') ?></th>
			<th><?php lang_print('204000.090') ?></th>
			<th><?php lang_print('204000.100') ?></th>
			<th><?php lang_print('204000.110') ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th><?php lang_print('204000.115') ?></th>
			<th><? echo $total; ?></th>
			<th><? echo getTimeFromSeconds($totaltotaltime); ?></th>
			<th><? echo getTimeFromSeconds($totaltotaltime/$total); ?></th>
			<th><? echo $totalPer; ?>%</th>
		</tr>
	</tfoot>
	<tbody>
	<?
	for($i=0;$i<sizeof($data);$i++){
	?>
		<tr>
			<td class="table03_align <? echo print_table_class($i); ?>"><? echo $i; ?></td>
			<td class="table03_align <? echo print_table_class($i); ?>"><? echo $data[$i]->ccount; ?></td>
			<td class="table03_align <? echo print_table_class($i); ?>"><? echo getTimeFromSeconds($data[$i]->totaltime); ?></td>
			<td class="table03_align <? echo print_table_class($i); ?>"><? echo getTimeFromSeconds($data[$i]->totaltime/$data[$i]->ccount); ?></td>
			<td class="table03_align <? echo print_table_class($i); ?>"><? echo sprintf("%.2f",$per[$i]); ?>%</td>
		</tr>
	<?
	}//end of : for($i=0;$i<sizeof($data);$i++)
	?>
	</tbody>
</table>