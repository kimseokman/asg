<?php
/**
 * Name : search_condition_months.php
 * Version : AnySupport Global
 * Date :  2015.06.12
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$utils_obj = new ASGUtils();

$month_arr = $utils_obj->GetMonths();

if($_POST['type']){
	$search_date = $utils_obj->GetSearchDate($_POST['type']);
}

foreach($month_arr as $k => $v){
?>
<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($search_date["M"], $k); ?>>
	<? echo $v; ?> <?php lang_print('UNIT.month') ?>
</option>
<?		
}// end of : foreach($month_arr as $k => $v)