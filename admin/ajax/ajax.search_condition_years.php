<?php
/**
 * Name : search_condition_years.php
 * Version : AnySupport Global
 * Date :  2015.06.12
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$utils_obj = new ASGUtils();

$year_arr = $utils_obj->GetYears();

if($_POST['type']){
	$search_date = $utils_obj->GetSearchDate($_POST['type']);
}

foreach($year_arr as $k => $v){
?>
<option value="<? echo $k; ?>" <? echo $utils_obj->PrintSelected($search_date["Y"], $k); ?>>
	<? echo $v; ?> <?php lang_print('UNIT.year') ?>
</option>
<?		
}// end of : foreach($year_arr as $k => $v)