<?php
/**
 * Name : search_condition_value.php
 * Version : AnySupport Global
 * Date :  2015.06.16
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$utils_obj = new ASGUtils();

$option_lang = $_SESSION['language'];

$option_en = array(//option define
	"RESOLVE" => RESOLVE_EN,
	"HOLD" => HOLD_EN,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_EN,
	"UNCHECKED" => UNCHECKED_EN
);

$option_jp = array(
	"RESOLVE" => RESOLVE_JP,
	"HOLD" => HOLD_JP,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_JP,
	"UNCHECKED" => UNCHECKED_JP
);

$option_kr = array(
	"RESOLVE" => RESOLVE_KR,
	"HOLD" => HOLD_KR,
	"NEED_DIRECT_SUPPORT" => NEED_DIRECT_SUPPORT_KR,
	"UNCHECKED" => UNCHECKED_KR
);

$option = array(
	"en" => $option_en,
	"jp" => $option_jp,
	"kr" => $option_kr
);

if($_POST['type'] == 3){//case by 해결 여부
?>
<input type="radio" name="search_val" value="1" id="radio_resolve" />
<label for="radio_resolve">
	<? echo $option[$option_lang]["RESOLVE"]; ?>
</label>
<input type="radio" name="search_val" value="2" id="radio_hold" />
<label for="radio_hold">
	<? echo $option[$option_lang]["HOLD"]; ?>
</label>
<input type="radio" name="search_val" value="3" id="radio_need_support" />
<label for="radio_need_support">
	<? echo $option[$option_lang]["NEED_DIRECT_SUPPORT"]; ?>
</label>
<input type="radio" name="search_val" value="4" id="radio_unchecked" />
<label for="radio_unchecked">
	<? echo $option[$option_lang]["UNCHECKED"]; ?>
</label>
<?
}
else{
?>
<input type="text" class="search_val search_type_input" />
<?
}