<?
/**
 * Name : report_supporting.php
 * Version : AnySupport Global
 * Date :  2015.06.15
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

function print_on($print_flag){
	if($print_flag){
		return "on";
	}else{
		return "";
	}
}

function print_last($print_flag){
	if($print_flag){
		return "last";
	}else{
		return "";
	}
}

$search_limit_min = SEARCH_MIN_DATE;
$search_limit_max = SEARCH_MAX_DATE;

$msg_lang = $_SESSION['language'];

$msg_en = array(//message define
	"NO_LOG"=> "no_data",
	"UNKNOWN"=> "unknown"
);

$msg_jp = array(//message define
	"NO_LOG"=> "-",
	"UNKNOWN"=> "-"
);

$msg_kr = array(//message define
	"NO_LOG"=> "원격 지원 기록이 없습니다.",
	"UNKNOWN"=> "알수 없음"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if($_POST['page'] == ""){
	$page = 1;
}else{
	$page = $_POST['page'];
}

if($_POST['search_type'] != ""){
	$search_type = $_POST['search_type'];
}

if($_POST['search_val'] != ""){
	$search_val = $_POST['search_val'];
}

if($_POST['start_time'] != ""){
	$start_time = $_POST['start_time'];
}

if($_POST['end_time'] != ""){
	$end_time = $_POST['end_time'];
}

$exist_report = $asg_obj->CheckReportExsit($start_time, $end_time);

$report_arr = $asg_obj->GetReportList($page, $search_type, $search_val, $start_time, $end_time);
?>
<table summary="" class="common_table03 report_search_table">
	<colgroup>
		<col width="60px" />
		<col width="60px" />
		<col width="*" />
		<col width="180px" />
		<col width="180px" />
		<col width="90px" />
		<col width="120px" />
		<col width="90px" />
	</colgroup>
	<thead>
		<tr>
			<th><?php lang_print('201010.070') ?></th>
			<th><?php lang_print('201010.080') ?></th>
			<th><?php lang_print('201010.090') ?></th>
			<th><?php lang_print('201010.100') ?></th>
			<th><?php lang_print('201010.110') ?></th>
			<th><?php lang_print('201010.120') ?></th>
			<th><?php lang_print('201010.130') ?></th>
			<th><?php lang_print('201010.140') ?></th>
		</tr>
	</thead>
	<tbody>
<?

if($exist_report != FALSE){//exist report
	$page_num = $report_arr["pageNum"];

	$row_num = 0;
	while(list($key, $val) = each($report_arr["datainfo"])) {
		$sTime = mktime(intVal(substr($val["starttime"],11,2)),intVal(substr($val["starttime"],14,2)),intVal(substr($val["starttime"],17,2)),intVal(substr($val["starttime"],5,2)),intVal(substr($val["starttime"],8,2)),intVal(substr($val["starttime"],0,4)));
		$eTime = mktime(intVal(substr($val["endtime"],11,2)),intVal(substr($val["endtime"],14,2)),intVal(substr($val["endtime"],17,2)),intVal(substr($val["endtime"],5,2)),intVal(substr($val["endtime"],8,2)),intVal(substr($val["endtime"],0,4)));

		$useTime = intVal($eTime) - intVal($sTime);

		if($useTime < 0){
			$useTime = $msg[$msg_lang]['UNKNOWN'];
		}else{
			$hours = intVal($useTime / 3600);//get hour
			$leftover = $useTime - ( $hours * 3600); 
			$min = intVal($leftover / 60);//get min
			$sec = $leftover - ( $min * 60);//get sec

			$useTime = str_pad($hours,"2","0",STR_PAD_LEFT).":".str_pad($min,"2","0",STR_PAD_LEFT).":".str_pad($sec,"2","0",STR_PAD_LEFT);
		}

		$user_os = $utils_obj->GetOS($val["user_os"]);
		$user_oper = $utils_obj->GetOperationLog($val["operationlog"]);
?>
		<tr>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $page_num; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<a href="#" class="shortcuts_link" onClick="detail_agent_info(event, <? echo $val["reportnum"]; ?>)">
					<? echo $val["sptid"]; ?>
				</a>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $val["name"]; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $val["starttime"]; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $val["endtime"]; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $useTime; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $user_os; ?>
			</td>
			<td class="<? echo print_table_class($row_num); ?>">
				<? echo $user_oper; ?>
			</td>
		</tr>
<?
		$page_num--;
		$row_num++;
	}// end of : while(list($k, $v) = each($report_arr["datainfo"]))
}else{//do not exist report
?>
		<tr>
			<td class="colspan_sort" colspan="8">
				<span>
					<? echo $msg[$msg_lang]['NO_LOG']; ?>
				</span>
			</td>
		</tr>
<?	
}//end of : if(count($report_arr) != 0)
?>
	</tbody>
</table>
<?
$page_prev = $report_arr["pageInfo"]->GetIsPrev();
$page_list = $report_arr["pageInfo"]->GetPageList();
$page_next = $report_arr["pageInfo"]->GetIsNext();

if($exist_report != FALSE){
?>
<ul class="paging report_supporting_page_list">
<?
	if($page_prev['is_older_page']){
?>	
	<li class="paging_a01">
		<a href="#" onClick="search_term_report_pagination(event, <? echo $page_prev['older_page']; ?>)">
			≪ Previous
		</a>
	</li>
<?
	}//end of : if($page_prev['is_older_page'])

	while(list($k, $v) = each($page_list)){
?>
	<li class="<? echo print_on($v['is_current']); ?> <? echo print_last($v['is_last']); ?>">
		<a href="#"  onClick="search_term_report_pagination(event, <? echo $k; ?>)">
			<? echo $k; ?>
		</a>
	</li>
<?
	}// end of  : while(list($k, $v) = each($page_list))

	if($page_next['is_newer_page']){
?>
	<li class="paging_a02">
		<a href="#" onClick="search_term_report_pagination(event, <? echo $page_next['newer_page']; ?>)">
			Next ≫
		</a>
	</li>
<?
	}//end of : if($page_next['is_newer_page'] == TRUE)
?>	
</ul>
<?
}//end of : if($exist_report != FALSE)
?>