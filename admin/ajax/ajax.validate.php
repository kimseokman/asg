<?php
/**
 * Name : validate.php
 * Version : AnySupport Global
 * Date :  2015.06.01
 * Author : KSM
 */
header('Content-Type: application/json');
session_start();

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();

$error = array();
$guide = array();

if($_POST['check_type'] == 'password'){
	$pw_length_min = PW_MIN_LENGTH;
	$pw_length_max = PW_MAX_LENGTH;

	$change_pw = $_POST['change_pw'];
	$change_pw_length = $_POST['change_pw_length'];
	$retype_pw = $_POST['retype_pw'];
	$retype_pw_length = $_POST['retype_pw_length'];

	$error['change_pw_flag'] = FALSE;
	$error['retype_pw_flag'] = FALSE;
	$error['change_pw_msg'] = "";
	$error['retype_pw_msg'] = "";

	if($change_pw_length < $pw_length_min || $change_pw_length > $pw_length_max){
		$error['change_pw_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['change_pw_msg'] = "The password can only be character or number of ".$pw_length_min."-".$pw_length_max;
		}else if($_SESSION['language'] == 'kr'){
			$error['change_pw_msg'] = "비밀번호는 ".$pw_length_min."-".$pw_length_max."의 영문 또는 숫자만 가능 합니다";
		}if($_SESSION['language'] == 'jp'){
			$error['change_pw_msg'] = "パスワードは、".$pw_length_min."~".$pw_length_max."の英語または数字のみ可能です。";
		}
	}

	if($retype_pw_length < $pw_length_min || $retype_pw_length > $pw_length_max){
		$error['retype_pw_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['change_pw_msg'] = "The password can only be character or number of ".$pw_length_min."-".$pw_length_max;
		}else if($_SESSION['language'] == 'kr'){
			$error['change_pw_msg'] = "비밀번호는 ".$pw_length_min."-".$pw_length_max."의 영문 또는 숫자만 가능 합니다";
		}if($_SESSION['language'] == 'jp'){
			$error['change_pw_msg'] = "パスワードは、".$pw_length_min."~".$pw_length_max."の英語または数字のみ可能です。";
		}
	}

	if(strcmp($change_pw, $retype_pw)){
		$error['retype_pw_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['retype_pw_msg'] = "Passwords does not match.";
		}else if($_SESSION['language'] == 'kr'){
			$error['retype_pw_msg'] = "비밀번호가 일치하지 않습니다.";
		}if($_SESSION['language'] == 'jp'){
			$error['retype_pw_msg'] = "パスワードが一致しません。";
		}
	}
}else if($_POST['check_type'] == 'id'){
	$id = $_POST['id'];
	$id_length = $_POST['id_length'];

	$error['id_flag'] = FALSE;
	$error['id_msg'] = "";

	$id_length_min = ID_MIN_LENGTH;
	$id_length_max = ID_MAX_LENGTH;

	if($id_length == 0){
		$error['id_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['id_msg'] = "Please enter your ID.";
		}else if($_SESSION['language'] == 'kr'){
			$error['id_msg'] = "아이디를 입력하세요.";
		}if($_SESSION['language'] == 'jp'){
			$error['id_msg'] = "IDを入力してください。";
		}
	}else if($id_length < $id_length_min || $id_length > $id_length_max){
		$error['id_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['id_msg'] = "The id can only be character or number of ".$id_length_min."-".$id_length_max.".";
		}else if($_SESSION['language'] == 'kr'){
			$error['id_msg'] = "아이디는 ".$id_length_min."-".$id_length_max."의 영문 또는 숫자만 가능합니다.";
		}if($_SESSION['language'] == 'jp'){
			$error['id_msg'] = "IDは".$id_length_min."-".$id_length_max."の英語または数字のみ可能です。";
		}
	}else if(!preg_match("`^[_0-9a-z-.]{".$id_length_min.",".$id_length_max."}$`i", $id)){
		$error['id_flag'] = TRUE;
		if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
			$error['id_msg'] = "The id can only be character or number of ".$id_length_min."-".$id_length_max.".";
		}else if($_SESSION['language'] == 'kr'){
			$error['id_msg'] = "아이디는 ".$id_length_min."-".$id_length_max."의 영문 또는 숫자만 가능합니다.";
		}if($_SESSION['language'] == 'jp'){
			$error['id_msg'] = "IDは".$id_length_min."-".$id_length_max."の英語または数字のみ可能です。";
		}
	}else{		
		$spt_res = $asg_obj->DuplicatedCheckID("SUPPORTER", $id);	//supporter 중복
		$admin_res = $asg_obj->DuplicatedCheckID("ADMIN", $id);	//admin 중복
		$super_res = $asg_obj->DuplicatedCheckID("SUPERADMIN", $id);//superadmin 중복

		if($spt_res == "OK" && $admin_res == "OK" && $super_res == "OK"){
			$error['id_flag'] = FALSE;
			if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
				$error['id_msg'] = "Available ID.";
			}else if($_SESSION['language'] == 'kr'){
				$error['id_msg'] = "사용 가능한 아이디입니다.";
			}if($_SESSION['language'] == 'jp'){
				$error['id_msg'] = "使用可能なIDです。";
			}
		}else{
			$error['id_flag'] = TRUE;
			if($_SESSION['language'] == 'en' || empty($_SESSION['language'])){
				$error['id_msg'] = "A duplicate ID.";
			}else if($_SESSION['language'] == 'kr'){
				$error['id_msg'] = "중복 아이디 입니다.";
			}if($_SESSION['language'] == 'jp'){
				$error['id_msg'] = "重複IDです。";
			}
		}
	}
}

$validateData = array(
	'error' => $error,
	'guide' => $guide
);

$output = json_encode($validateData);

echo urldecode($output);
?>