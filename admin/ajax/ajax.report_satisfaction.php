<?php
/**
 * Name : report_satisfaction.php
 * Version : AnySupport Global
 * Date :  2015.06.18
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$column_lang = $_SESSION['language'];

$column_en = array(//option define
	"SATISFACTION" => SATISFACTION_EN,
	"NORMAL" => NORMAL_EN,
	"UNSATISFACTION" => UNSATISFACTION_EN,
	"NO_ANSWER" => NO_ANSWER_EN
);

$column_jp = array(
	"SATISFACTION" => SATISFACTION_JP,
	"NORMAL" => NORMAL_JP,
	"UNSATISFACTION" => UNSATISFACTION_JP,
	"NO_ANSWER" => NO_ANSWER_JP
);

$column_kr = array(
	"SATISFACTION" => SATISFACTION_KR,
	"NORMAL" => NORMAL_KR,
	"UNSATISFACTION" => UNSATISFACTION_KR,
	"NO_ANSWER" => NO_ANSWER_KR
);

$column = array(
	"en" => $column_en,
	"jp" => $column_jp,
	"kr" => $column_kr
);

$stats_arr = $asg_obj->GetReportBySatisfactionOfTroubleType($_POST['start_time'], $_POST['end_time']);

$res1sum = 0;
$res2sum = 0;
$res3sum = 0;
$res0sum = 0;
$sumsum = 0;

function print_table_class($table_index){
	$table_index%=2;
	if($table_index > 0){//isOdd
		return "";
	}else{//isEven
		return "common_table_color04";
	}
}

$report_stats_arr = $stats_arr;

$trouble_type_name_arr = array();
$satisfaction_data_arr = array();
$normal_data_arr = array();
$unsatisfaction_data_arr = array();
$no_answer_data_arr = array();
while(list($key, $val) = each($report_stats_arr)){
	$trouble_type_name_arr[$key] = $val["trbname"];
	$satisfaction_data_arr[$key] = $val["res1"];
	$normal_data_arr[$key] = $val["res2"];
	$unsatisfaction_data_arr[$key] = $val["res3"];
	$no_answer_data_arr[$key] = $val["res0"];
}

?>

<p class="common_grapic_tit"><?php lang_print('208000.140') ?></p>
<div class="admin_grapic_div report_satisfaction_chart" id="admin_grapic_div"  style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script>
	$(document).ready(function(){
		$('.report_satisfaction_chart').highcharts({
			chart: {
				marginTop: 50,
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: [
				<?
				$while_cnt = 0;
				$trouble_type_length = count($trouble_type_name_arr);
				while(list($key, $val) = each($trouble_type_name_arr)) {
					echo "'".$val."'";
					$while_cnt++;
					if($while_cnt != $trouble_type_length){
						echo ",";	
					}
				}	
				?>
				]
			},
			yAxis: {
				min: 0,
				title: {
    							text: 'Sum of Type'
				},
				stackLabels: {
					enabled: true,
					style: {
						fontWeight: 'bold',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
					}
				}
			},
			legend: {
				align: 'right',
				x: -30,
				verticalAlign: 'top',
				y: 0,
				floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
				borderColor: '#CCC',
				borderWidth: 1,
				shadow: false
			},
			tooltip: {
				formatter: function () {
					return '<b>' + this.x + '</b><br/>' +
						this.series.name + ': ' + this.y + '<br/>' +
						'Total: ' + this.point.stackTotal;
				}
			},
			plotOptions: {
				column: {
					stacking: 'normal'
				}
			},
			series: [
				{
					name: <? echo "'".$column[$column_lang]["SATISFACTION"]."'"; ?>,
					data: [
					<?
					$while_cnt = 0;
					$satisfaction_data_length = count($satisfaction_data_arr);
					while(list($key, $val) = each($satisfaction_data_arr)){
						echo $val;
						$while_cnt++;
						if($while_cnt != $satisfaction_data_length){
							echo ",";	
						}
					}
					?>
					]
				}, {
					name: <? echo "'".$column[$column_lang]["NORMAL"]."'"; ?>,
					data: [
					<?
					$while_cnt = 0;
					$normal_data_length = count($normal_data_arr);
					while(list($key, $val) = each($normal_data_arr)){
						echo $val;
						$while_cnt++;
						if($while_cnt != $normal_data_length){
							echo ",";	
						}
					}
					?>
					]
				}, {
					name: <? echo "'".$column[$column_lang]["UNSATISFACTION"]."'"; ?>,
					data: [
					<?
					$while_cnt = 0;
					$unsatisfaction_data_length = count($unsatisfaction_data_arr);
					while(list($key, $val) = each($unsatisfaction_data_arr)){
						echo $val;
						$while_cnt++;
						if($while_cnt != $unsatisfaction_data_length){
							echo ",";	
						}
					}
					?>
					]
				}, {
					name: <? echo "'".$column[$column_lang]["NO_ANSWER"]."'"; ?>,
					data: [
					<?
					$while_cnt = 0;
					$no_answer_data_length = count($no_answer_data_arr);
					while(list($key, $val) = each($no_answer_data_arr)){
						echo $val;
						$while_cnt++;
						if($while_cnt != $no_answer_data_length){
							echo ",";	
						}
					}
					?>
					]
				}
			]
		});
	});
</script>

<p class="table_bottom_p"></p>

<table summary="" class="common_table03 report_search_table">
	<colgroup>
		<col width="*" />
		<col width="110px" />
		<col width="110px" />
		<col width="120px" />
		<col width="120px" />
		<col width="110px" />
	</colgroup>
	<thead>
		<tr>
			<th><?php lang_print('208000.070') ?></th>
			<th><?php lang_print('208000.080') ?></th>
			<th><?php lang_print('208000.090') ?></th>
			<th><?php lang_print('208000.100') ?></th>
			<th><?php lang_print('208000.110') ?></th>
			<th><?php lang_print('208000.120') ?></th>
		</tr>
	</thead>
	<tbody>
	<?
	while(list($key, $val) = each($stats_arr)){
	?>
		<tr>
			<td class="<? echo print_table_class($key); ?>"><? echo $val["trbname"]; ?></td>
			<td class="<? echo print_table_class($key); ?>"><? echo $val["res1"]; ?></td>
			<td class="<? echo print_table_class($key); ?>"><? echo $val["res2"]; ?></td>
			<td class="<? echo print_table_class($key); ?>"><? echo $val["res3"]; ?></td>
			<td class="<? echo print_table_class($key); ?>"><? echo $val["res0"]; ?></td>
			<td class="<? echo print_table_class($key); ?>"><? echo ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]); ?></td>
		</tr>
	<?
		$res1sum += $val["res1"];
		$res2sum += $val["res2"];
		$res3sum += $val["res3"];
		$res0sum += $val["res0"];
		$sumsum += ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]);
	}// end of : while(list($key, $val) = each($stats_arr))
	?>
		<tr class="model_tfoot">
			<td><?php lang_print('208000.130') ?></td>
			<td><? echo $res1sum; ?></td>
			<td><? echo $res2sum; ?></td>
			<td><? echo $res3sum; ?></td>
			<td><? echo $res0sum; ?></td>
			<td><? echo $sumsum; ?></td>
		</tr>
	</tbody>
</table>