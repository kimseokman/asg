<?php
/**
 * delete_trouble_type.php
 * 2015.06.04 | KSM
 */

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$page = array(//page define
	"DEL_FAIL"=> "/admin/manage/trouble/list.php",
	"DEL_SUCCESS"=> "/admin/manage/trouble/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(
	"NO_TROUBLETYPE"=> "The type of counseling that does not exist.",
	"DEL_OK"=> "This type of counseling has been deleted."
);

$msg_jp = array(
	"NO_TROUBLETYPE"=> "存在しない相談タイプです。",
	"DEL_OK"=> "相談の種類が削除されました。"
);

$msg_kr = array(
	"NO_TROUBLETYPE"=> "존재하지 않는 상담유형입니다.",
	"DEL_OK"=> "상담유형이 삭제 되었습니다."
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

$res = $asg_obj->DeleteTroubleType($_GET["trbnum"]);

if($res == "NOTROUBLETYPE") {// 해당 장애유형이 존재하지 않을 때
	echo "
		<script> 
			alert('".$msg[$msg_lang]['NO_TROUBLETYPE']."');
			location.href='".$page['DEL_FAIL']."';
		</script>
	";
	exit(1);
} else {// 삭제 성공
	echo "
		<script> 
			alert('".$msg[$msg_lang]['DEL_OK']."');
			location.href='".$page['DEL_SUCCESS']."';
		</script>
	";
}