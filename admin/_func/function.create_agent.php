<?php
/**
 * create_agent.php
 * 2015.06.04 | KSM | create department
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();
$isAdmin = $asg_obj->ConfirmAdmin();

$admin = $asg_obj->GetAdminInfo();//get Admin Info

$name_max = NAME_MAX_LENGTH;
$name_max_kr = NAME_MAX_LENGTH_KR;
$name_max_jp = NAME_MAX_LENGTH_JP;
$name_max_en = NAME_MAX_LENGTH_EN;
$phone_max = PHONE_MAX_LENGTH;
$mail_max = MAIL_ADDR_MAX_LENGTH;

$page = array(//page define
	"CREATE_FAIL"=> "/admin/manage/agent/create.php",
	"CREATE_SUCCESS"=> "/admin/manage/agent/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_ID"=> "Please enter the ID.",
	"EMPTY_NAME"=> "Please enter your username.",
	"LONG_NAME"=> "The name is too long (less than ".$name_max_en." letters)",
	"EMPTY_PASSWORD"=> "Please enter your password.",
	"PASSWORD_NOT_MATCH"=> "The passwords do not match.",
	"LONG_PHONE"=> "The telephone number is too long (less than ".$phone_max." numeric)",
	"LONG_EMAIL"=> "E-mail address length must be ".$mail_max." characters or less.",
	"DUPICATED"=> "A duplicate ID. Please enter a different username.",
	"EMPTY_DEPARTMENT"=> "Please select your department.",
	"OK"=>" The new agent was successfully created."
);

$msg_jp = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_ID"=> "IDをとして記入してください。",
	"EMPTY_NAME"=> "名前を入力してください。",
	"LONG_NAME"=> "長すぎる名前です。（".$name_max_jp."文字以内）",
	"EMPTY_PASSWORD"=> "パスワードを入力してください。",
	"PASSWORD_NOT_MATCH"=> "パスワードが一致しません。",
	"LONG_PHONE"=> "電話番号は、".$phone_max."文字以内で入力してください。",
	"LONG_EMAIL"=> "メールアドレスの長さは、".$mail_max."文字以内にしてください。",
	"DUPICATED"=> "重複したIDです。他のIDを入力してください",
	"EMPTY_DEPARTMENT"=> "部門を選択してください。",
	"OK"=>" 新しいエージェントが正常に作成されました。"
);

$msg_kr = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_ID"=> "ID를 입력해 주세요.",
	"EMPTY_NAME"=> "이름을 입력해주세요.",
	"LONG_NAME"=> "이름은 한글 ".$name_max_kr."자, 영문 ".$name_max_en."자 이내로 입력해주세요.",
	"EMPTY_PASSWORD"=> "비밀번호를 일력 하세요.",
	"PASSWORD_NOT_MATCH"=> "비밀번호가 일치하지 않습니다.",
	"LONG_PHONE"=> "전화번호는 ".$phone_max."자 이내로 입력해주세요.",
	"LONG_EMAIL"=> "이메일 주소 길이는 ".$mail_max."글자 이내여야 합니다.",
	"DUPICATED"=> "이미 존재하는 ID입니다. 다른 ID를 사용해주세요.",
	"EMPTY_DEPARTMENT"=> "부서를 선택해주세요.",
	"OK"=>" 새로운 상담원이 성공적으로 생성되었습니다."
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}

if(strlen($_POST["new_spt_name"]) == 0) {// 이름을 입력하지 않았을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_NAME']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["new_spt_name"]) > $name_max) {// 이름의 길이가 30자가 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_NAME']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["new_spt_pw"]) == 0) {// 패스워드를 입력하지 않았을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_PASSWORD']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if($_POST["new_spt_pw"] != $_POST["new_spt_retype_pw"]) {// 패스워드가 일치하지 않을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['PASSWORD_NOT_MATCH']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["new_spt_phone"]) > $phone_max) {// 전화번호가 20자를 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_PHONE']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["new_spt_email"]) > $mail_max) {// 이메일 주소가 50자를 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_EMAIL']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if($_POST["new_spt_depnum"] == "0") {// 부서를 선택하지 않았을 경우
	echo "
		<script> 
			alert('".$msg[$lang]['EMPTY_DEPARTMENT']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["new_spt_id"]) == 0){//있을리 없는 id가 비어있는 경우
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_ID']."');
			location.href='".$page['CREATE_FAIL']."';
		</script>
	";
	exit(1);
}
else{
	//use values in CAnySupportGlobalConstant.php
	if($admin->function & PERMISSION_ALL_IN_ONE){//case by all in one
		$spt_function = PERMISSION_DEFAULT + PERMISSION_ALL_IN_ONE;	//253951 + 8388608
		$spt_setting = SETTING_DEFAULT;					//1081
		$spt_mobile_setting = MOBILE_SETTING_DEFAULT;			//6
		$spt_mobile_expired_date = MOBILE_EXPIRED_DATE_DEFAULT;
	}
	else{//case by pc
		$spt_function = PERMISSION_DEFAULT;					//253951
		$spt_setting = SETTING_DEFAULT;					//1081
		$spt_mobile_setting = MOBILE_SETTING_DEFAULT;			//6
		$spt_mobile_expired_date = MOBILE_EXPIRED_DATE_DEFAULT;
	}
	
	$sptdata = array(
		"depnum"=>$_POST["new_spt_depnum"], 
		"sptid"=>$_POST["new_spt_id"], 
		"password"=>$_POST["new_spt_pw"], 
		"company"=>$_POST["new_spt_company"], 
		"name"=>$_POST["new_spt_name"], 
		"phone"=>$_POST["new_spt_phone"], 
		"email"=>$_POST["new_spt_email"], 
		"fr"=>$spt_function, 
		"opt"=>$spt_setting , 
		"mobile_expired_date"=>$spt_mobile_expired_date
	);

	$res = $asg_obj->CreateSupporter($sptdata);
	
	if($res == "SUPPORTERALREADYEXISTS" || $res == "ADMINALREADYEXISTS") {// 이미 존재하는 ID인 경우
		echo "
			<script> 
				alert('".$msg[$msg_lang]["DUPICATED"]."');
				location.href='".$page['CREATE_FAIL']."';
			</script>
		";
		exit(1);
	}else {		
		echo "
			<script> 
				alert('".$msg[$msg_lang]['OK']."');
				location.href='".$page['CREATE_SUCCESS']."';
			</script>
		";
	}
}