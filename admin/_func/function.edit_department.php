<?php
/**
 * edit_department.php
 * 2015.06.04 | KSM | edit department
 */

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$department_max = DEPARTMENT_MAX_LENGTH;
$department_max_kr = DEPARTMENT_MAX_LENGTH_KR;
$department_max_jp = DEPARTMENT_MAX_LENGTH_JP;
$department_max_en = DEPARTMENT_MAX_LENGTH_EN;

$page = array(//page define
	"EDIT_DEPARTMENT_FAIL"=> "/admin/manage/department/list.php",
	"EDIT_DEPARTMENT_SUCCESS"=> "/admin/manage/department/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"EMPTY_DEPT_NAME" => "부서명을 입력하세요.",
	"LONG_DEPT_NAME" => "부서 이름을 한글 ".$department_max_kr."자, 영문 ".$department_max_en."자 이내로 입력해주세요.",
	"NOT_EXIST_DEPT" => "존재하지 않는 부서명 입니다."
);

$msg_en = array(//message define
	"EMPTY_DEPT_NAME" => "Enter the department name.",
	"LONG_DEPT_NAME" => "The department name is too long (less than ".$department_max_en." characters)",
	"NOT_EXIST_DEPT" => "The department does not exist."
);

$msg_jp = array(//message define
	"EMPTY_DEPT_NAME" => "部署名を入力してください。",
	"LONG_DEPT_NAME" => "部門名が長すぎる（".$department_max_jp."文字未満）",
	"NOT_EXIST_DEPT" => "存在しない部署名です。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(strlen($_POST["edit_dep_name"]) == 0) {//case by dept name empty
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['EDIT_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='EDIT_ROW' />
			<input type='hidden' name='depnum' value='".$_POST["depnum"]."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['EMPTY_DEPT_NAME']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else if(strlen($_POST["edit_dep_name"]) > $department_max) {//case by dept name too long
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['EDIT_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='EDIT_ROW' />
			<input type='hidden' name='depnum' value='".$_POST["depnum"]."' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['LONG_DEPT_NAME']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else if($asg_obj->GetDepByDepnum($_POST["depnum"]) == NULL) { //case by do not exist dept
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['EDIT_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='NORMAL' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['NOT_EXIST_DEPT']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else {//case by try to edit department
	$edit_res = $asg_obj->RenameDep($_POST["depnum"], $_POST["edit_dep_name"]);

	if($edit_res =='OK'){
		echo "
			<form name='department_page_status_maintain_form' method='post' action='".$page['EDIT_DEPARTMENT_SUCCESS']."'>
				<input type='hidden' name='dept_page_status' value='NORMAL' />
			</form>
			<script>
				document.department_page_status_maintain_form.submit();
			</script>
		";
	}
	else if($edit_res =='INVALIDDEP'){
		echo "
			<form name='department_page_status_maintain_form' method='post' action='".$page['EDIT_DEPARTMENT_FAIL']."'>
				<input type='hidden' name='dept_page_status' value='NORMAL' />
			</form>
			<script>
				alert('".$msg[$msg_lang]['NOT_EXIST_DEPT']."');
				document.department_page_status_maintain_form.submit();
			</script>
		";
		exit(1);
	}
}