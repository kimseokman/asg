<?php
/**
 * edit_agent.php
 * 2015.06.04 | KSM | edit department
 */

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();
$isAdmin = $asg_obj->ConfirmAdmin();

$admin = $asg_obj->GetAdminInfo();//get Admin Info

$name_max = NAME_MAX_LENGTH;
$name_max_kr = NAME_MAX_LENGTH_KR;
$name_max_jp = NAME_MAX_LENGTH_JP;
$name_max_en = NAME_MAX_LENGTH_EN;
$phone_max = PHONE_MAX_LENGTH;
$mail_max = MAIL_ADDR_MAX_LENGTH;
$company_max = COMPANY_MAX_LENGTH;
$company_max_kr = COMPANY_MAX_LENGTH_KR;
$company_max_jp = COMPANY_MAX_LENGTH_JP;
$company_max_en = COMPANY_MAX_LENGTH_EN;

$mobile_volume = $adminarray->mobile_volume;
$currentMobileSupporters = $asg_obj->GetMobileSupporterNum();

$page = array(//page define
	"MODIFY_FAIL" => "/admin/manage/agent/edit.php?sptnum=".$_POST['sptnum'],
	"MODIFY_SUCCESS" => "/admin/manage/agent/edit.php?sptnum=".$_POST['sptnum']
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_NAME" => "이름을 입력해주세요.",
	"LONG_NAME"=> "이름은 한글 ".$name_max_kr."자, 영문 ".$name_max_en."자 이내로 입력해주세요.",
	"PASSWORD_NOT_MATCH" => "비밀번호가 일치하지 않습니다.",
	"LONG_PHONE" => "전화번호는 ".$phone_max."자 이내로 입력해주세요.",
	"LONG_EMAIL" => "이메일 주소 길이는 ".$mail_max."글자 이내여야 합니다.",
	"NO_SUPPORTER" => "해당 상담원이 존재하지 않습니다. 다시 시도해주세요.",
	"EMPTY_COMPANY"=> "회사 이름을 입력하세요.",
	"LONG_COMPANY"=> "회사 이름은 한글 ".$company_max_kr."자, 영문 ".$company_max_en."자 이내로 입력해주세요.",
	"EMPTY_DEPARTMENT"=> "부서를 선택해주세요.",
	"OK" => "상담원 정보가 성공적으로 수정되었습니다."
);

$msg_en = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_NAME" => "Please enter your name.",
	"LONG_NAME" => "The name is too long (less than ".$name_max_en." letters)",
	"PASSWORD_NOT_MATCH" => "The passwords do not match.",
	"LONG_PHONE" => "The telephone number is too long (less than ".$phone_max." numeric)",
	"LONG_EMAIL" => "E-mail address length must be ".$mail_max." characters or less.",
	"NO_SUPPORTER" => "The agent that does not exist. Please try again.",
	"EMPTY_COMPANY"=> "Please enter a company name.",
	"LONG_COMPANY"=> "The company name is too long (less than ".$company_max_en." characters)",
	"EMPTY_DEPARTMENT"=> "Please select your department.",
	"OK" => "Agent Information has been modified successfully."
);

$msg_jp = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"EMPTY_NAME" => "名前を入力してください。",
	"LONG_NAME" => "長すぎる名前です。（".$name_max_jp."文字以内）",
	"PASSWORD_NOT_MATCH" => "パスワードが一致しません。",
	"LONG_PHONE" => "電話番号は、".$phone_max."文字以内で入力してください。",
	"LONG_EMAIL" => "メールアドレスの長さは、".$mail_max."文字以内にしてください。",
	"NO_SUPPORTER" => "そのエージェントが存在しません。再試行してください。",
	"EMPTY_COMPANY"=> "会社名を入力してください。",
	"LONG_COMPANY"=> "会社名が長すぎる（".$company_max_jp."文字未満）",
	"EMPTY_DEPARTMENT"=> "部門を選択してください。",
	"OK" => "エージェント情報が正常に変更されました。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}


if(strlen($_POST["spt_name"]) == 0) {// 이름을 입력하지 않았을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_NAME']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["spt_name"]) > $name_max) {// 이름의 길이가 30자가 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_NAME']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if($_POST["spt_pw"] != $_POST["spt_retype_pw"]) {// 패스워드가 일치하지 않을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['PASSWORD_NOT_MATCH']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["spt_phone"]) > $phone_max) {// 전화번호가 20자를 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_PHONE']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["spt_email"]) > $mail_max) {// 이메일 주소가 50자를 넘을 경우 에러
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_EMAIL']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["spt_company"]) == 0) {// 회사를 입력하지 않았을 경우
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_COMPANY']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if(strlen($_POST["spt_company"]) > $company_max) {// 회사 이름이 30자를 넘을 경우 
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_COMPANY']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else if($_POST["spt_depnum"] == "0") {// 부서를 선택하지 않았을 경우
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_DEPARTMENT']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}
else{
	//use values in CAnySupportGlobalConstant.php
	if($admin->function & PERMISSION_ALL_IN_ONE){//case by all in one
		$spt_function = PERMISSION_DEFAULT + PERMISSION_ALL_IN_ONE;	//253951 + 8388608
		$spt_setting = SETTING_DEFAULT;					//1081
		$spt_mobile_setting = MOBILE_SETTING_DEFAULT;			//6
		$spt_mobile_expired_date = MOBILE_EXPIRED_DATE_DEFAULT;
	}else{//case by pc
		$spt_function = PERMISSION_DEFAULT;					//253951
		$spt_setting = SETTING_DEFAULT;					//1081
		$spt_mobile_setting = MOBILE_SETTING_DEFAULT;			//6
		$spt_mobile_expired_date = MOBILE_EXPIRED_DATE_DEFAULT;
	}

	$sptdata = array(
		"depnum"=>$_POST["spt_depnum"], 
		"password"=>$_POST["spt_pw"], 
		"company"=>$_POST["spt_company"], 
		"name"=>$_POST["spt_name"], 
		"phone"=>$_POST["spt_phone"], 
		"email"=>$_POST["spt_email"], 
		"fr"=>$spt_function, 
		"opt"=>$spt_setting, 
		"mobile_setting"=>$spt_mobile_setting
	);

	$res = $asg_obj->ModifySupporter($_POST["sptnum"], $sptdata);

	if($res == "NOSUPPORTER") {// 수정하려는 상담자가 존재하지 않을 경우
		echo "
			<script> 
				alert('".$msg[$msg_lang]['NO_SUPPORTER']."');
				location.href='".$page['MODIFY_FAIL']."';
			</script>
		";
		exit(1);
	} else { // 성공적으로 수정. 메시지 띄운 후 이동
		echo "
			<script> 
				alert('".$msg[$msg_lang]['OK']."');
				location.href='".$page['MODIFY_SUCCESS']."';
			</script>
		";
	}
}