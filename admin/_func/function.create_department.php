<?php
/**
 * create_department.php
 * 2015.06.04 | KSM | create department
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$department_max = DEPARTMENT_MAX_LENGTH;
$department_max_kr = DEPARTMENT_MAX_LENGTH_KR;
$department_max_jp = DEPARTMENT_MAX_LENGTH_JP;
$department_max_en = DEPARTMENT_MAX_LENGTH_EN;

$page = array(//page define
	"CREATE_DEPARTMENT_FAIL"=> "/admin/manage/department/list.php",
	"CREATE_DEPARTMENT_SUCCESS"=> "/admin/manage/department/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"EMPTY_DEPT_NAME" => "부서 이름을 입력해주세요.",
	"LONG_DEPT_NAME" => "부서 이름을 한글 ".$department_max_kr."자, 영문 ".$department_max_en."자 이내로 입력해주세요.",
	"DEPT_NAME_DUPLICATE" => "중복된 부서명 입니다."
);

$msg_en = array(//message define
	"EMPTY_DEPT_NAME" => "Please enter a department name.",
	"LONG_DEPT_NAME" => "The department name is too long (less than ".$department_max_en." characters)",
	"DEPT_NAME_DUPLICATE" => "Duplicate Department."
);

$msg_jp = array(//message define
	"EMPTY_DEPT_NAME" => "部署名を入力してください。",
	"LONG_DEPT_NAME" => "部門名が長すぎる（".$department_max_jp."文字未満）",
	"DEPT_NAME_DUPLICATE" => "重複している部署名です。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(strlen($_POST["create_dept_name"]) == 0) {//case by department name is empty
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['CREATE_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='ADD_ROW' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['EMPTY_DEPT_NAME']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else if(strlen($_POST["create_dept_name"]) > $department_max) {//case by department name is too long
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['CREATE_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='ADD_ROW' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['LONG_DEPT_NAME']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else {//case by try to create department
	$create_res = $asg_obj->CreateDep($_POST["create_dept_name"]);

	if($create_res == 'OK'){
		echo "
			<form name='department_page_status_maintain_form' method='post' action='".$page['CREATE_DEPARTMENT_SUCCESS']."'>
				<input type='hidden' name='dept_page_status' value='NORMAL' />
			</form>
			<script>
				document.department_page_status_maintain_form.submit();
			</script>
		";	
	}
	else if($create_res == 'DEPNAMEALREADYEXISTS'){
		echo "
			<form name='department_page_status_maintain_form' method='post' action='".$page['CREATE_DEPARTMENT_FAIL']."'>
				<input type='hidden' name='dept_page_status' value='ADD_ROW' />
			</form>
			<script>
				alert('".$msg[$msg_lang]['DEPT_NAME_DUPLICATE']."');
				document.department_page_status_maintain_form.submit();
			</script>
		";
		exit(1);

	}
}