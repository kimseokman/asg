<?php
/**
 * func_login.php
 * 2015.06.01 | KSM | login
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$page = array(//page define
	"MODIFY_FAIL"=> "/admin/manage/billing/login.php",
	"MODIFY_SUCCESS"=> "/admin/manage/billing/billing_index.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(//message define
	"CURRENT_PASSWORD_ERROR"=> "Passwords does not match.",
	"RETYPE_PASSWORD_ERROR"=> "Passwords does not match.",
	"PASSWORD_MODIFY_ERROR"=> "It failed to change password.",
	"SUCCESS"=> "Your password has been changed."
);

$msg_kr = array(//message define
	"CURRENT_PASSWORD_ERROR"=> "비밀번호가 일치하지 않습니다.",
	"RETYPE_PASSWORD_ERROR"=> "비밀번호가 일치하지 않습니다.",
	"PASSWORD_MODIFY_ERROR"=> "비밀번호 변경에 실패 하였습니다.",
	"SUCCESS"=> "비밀번호가 변경 되었습니다."
);

$msg_jp = array(//message define
	"CURRENT_PASSWORD_ERROR"=> "パスワードが一致しません",
	"RETYPE_PASSWORD_ERROR"=> "パスワードが一致しません",
	"PASSWORD_MODIFY_ERROR"=> "パスワードの変更に失敗しました。",
	"SUCCESS"=> "パスワードが変更されました。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

$admin = $asg_obj->GetAdminInfo();//get Admin Info
$pw_check_flag = $asg_obj->CheckAdminPW($_POST['admin_current_pw']);

if(strcmp($pw_check_flag , "OK")){
	echo "
		<script>
			alert('".$msg[$msg_lang]['CURRENT_PASSWORD_ERROR']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}

if(strcmp($_POST['admin_change_pw'], $_POST['admin_retype_pw'])){
	echo "
		<script>
			alert('".$msg[$msg_lang]['RETYPE_PASSWORD_ERROR']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>
	";
	exit(1);
}

$admin_data = array(
	"password" => $_POST['admin_change_pw']
);

$result = $asg_obj->ChangeAdminInfo($admin_data);

if(strcmp($result, "OK")){
	echo "
		<script>
			alert('".$msg[$msg_lang]['PASSWORD_MODIFY_ERROR']."');
			location.href='".$page['MODIFY_FAIL']."';
		</script>	
	";
	exit(1);
}else{
	echo "
		<script>
			alert('".$msg[$msg_lang]['SUCCESS']."');
			location.href='".$page['MODIFY_SUCCESS']."';
		</script>
	";
}