<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.constants.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGUtils.php");
include_once($_SERVER['DOCUMENT_ROOT']."/_lib/_class/class.ASGMain.php");

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$current_url = $utils_obj->GetCurrentPageURL();
//
// 파라미터로 전달된 언어코드가 있으면 설정,
// (없으면 Default 영문으로 설정)
//

if(!empty($_REQUEST['language'])) {
	$_SESSION['language'] = $_REQUEST['language'];
} 
else if (empty($_SESSION['language'])) {
    	$_SESSION['language'] = "en";
}

//
// 설정된 언어 파일 Include
// (언어 파일이 존재하지 않으명 영문 파일 Include)
//
$lang_path = "{$_SERVER['DOCUMENT_ROOT']}/admin/lang/lang.{$_SESSION['language']}.php";

if (!file_exists($lang_path)) {
    	$lang_path = "{$_SERVER['DOCUMENT_ROOT']}/admin/lang/lang.en.php";
}

include_once($lang_path);

//
// FUNCTION : 언어 문자열 구하기
//
function lang() {
    	global $lang;
    
    	// 가변 파라미터로 처리
    	$args = func_get_args();
    
    	// 첫번째 파라미터는 무조건 언어코드
    	$code = array_shift($args);
    	$lang_str = $lang[$code];
    
    	// 추가 파라미터는 변환에 사용 (1번부터 사용)
    	// 예) "{1}, Only {2} days left!" 
    	foreach ($args as $k=>$v) {
        		$lang_str = str_replace("{".($k+1)."}", $v, $lang_str);
    	}
    
    	return $lang_str;
}

//
// FUNCTION : 언어 문자열 출력
//
function lang_print() {
    	$args = func_get_args();
    	echo call_user_func_array("lang", $args);
}