<?php
/**
 * delete_department.php
 * 2015.06.04 | KSM | delete department
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$page = array(//page define
	"DEL_DEPARTMENT_FAIL"=> "/admin/manage/department/list.php",
	"DEL_DEPARTMENT_SUCCESS"=> "/admin/manage/department/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"DEL_OK" => "부서정보가 삭제 되었습니다.",
	"EXIST_SUPPORTER" => "상담원이 존재하는 부서 입니다. 부서 내의 상담원을 삭제하거나 다른 부서로 옮겨야 합니다.",
	"NOT_EXIST_DEPT" => "존재하지 않는 부서명 입니다."
);

$msg_en = array(//message define
	"DEL_OK" => "Department information has been deleted.",
	"EXIST_SUPPORTER" => "The department exists an agent. Deleting an agent within the department or to move to another department.",
	"NOT_EXIST_DEPT" => "The department does not exist."
);

$msg_jp = array(//message define
	"DEL_OK" => "部門情報が削除されました。",
	"EXIST_SUPPORTER" => "エージェントが存在する部門です。部門内の相談員を削除したり、他の部署に移動しなければ",
	"NOT_EXIST_DEPT" => "存在しない部署名です。"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if($asg_obj->GetDepByDepnum($_POST["depnum"]) == NULL) {//case by do not exist department
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['DEL_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='NORMAL' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['NOT_EXIST_DEPT']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else if($asg_obj->GetSupporterNumByDepnum($_POST["depnum"]) != 0) {//case by exist servant supporter
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['DEL_DEPARTMENT_FAIL']."'>
			<input type='hidden' name='dept_page_status' value='NORMAL' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['EXIST_SUPPORTER']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
	exit(1);
}
else{//case by try to delete department
	$asg_obj->DeleteDep($_POST["depnum"]);
	echo "
		<form name='department_page_status_maintain_form' method='post' action='".$page['DEL_DEPARTMENT_SUCCESS']."'>
			<input type='hidden' name='dept_page_status' value='NORMAL' />
		</form>
		<script>
			alert('".$msg[$msg_lang]['DEL_OK']."');
			document.department_page_status_maintain_form.submit();
		</script>
	";
}