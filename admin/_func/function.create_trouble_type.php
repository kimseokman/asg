<?
/**
 * create_trouble_type.php
 * 2015.06.04 | KSM
 */

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$asg_obj->CreateTroubleType();

$page = array(//page define
	"CREATE_SUCCESS" => "/admin/manage/trouble/list.php"
);

echo "
	<script> 
		location.href='".$page['CREATE_SUCCESS']."';
	</script>
";
?>