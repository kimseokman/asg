<?php
/**
 * delete_agent.php
 * 2015.06.04 | KSM | create department
 */
include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();
$isAdmin = $asg_obj->ConfirmAdmin();

$page = array(//page define
	"DEL_FAIL"=> "/admin/manage/agent/list.php",
	"DEL_SUCCESS"=> "/admin/manage/agent/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_SUPPORTER"=> "The agent that does not exist. Please try again.",
	"DEL_OK"=> "The agent has been removed."
);

$msg_jp = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_SUPPORTER"=> "そのエージェントが存在しません。再試行してください。",
	"DEL_OK"=> "エージェントは削除されました。"
);

$msg_kr = array(
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_SUPPORTER"=> "해당 상담원이 존재하지 않습니다. 다시 시도해주세요.",
	"DEL_OK"=> "상담원이 삭제되었습니다."
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['DEL_FAIL']."';
		</script>
	";
	exit(1);
}

if($asg_obj->GetSupporterBySptnum($_GET["sptnum"]) == NULL) {
	echo "
		<script> 
			alert('".$msg[$msg_lang]['NO_SUPPORTER']."');
			location.href='".$page['DEL_FAIL']."';
		</script>
	";
	exit(1);
} 
else {// 상담원 삭제하기
	$res = $asg_obj->DeleteSupporter($_GET["sptnum"]);
	
	if($res == "NOSUPPORTER") {
		echo "
			<script> 
				alert('".$msg[$msg_lang]['NO_SUPPORTER']."');
				location.href='".$page['DEL_FAIL']."';
			</script>
		";
		exit(1);
	}
	
	echo "
		<script> 
			alert('".$msg[$msg_lang]['DEL_OK']."');
			location.href='".$page['DEL_SUCCESS']."';
		</script>
	";
}