<?php
/**
 * edit_trouble_type.php
 * 2015.06.04 | KSM
 */

include_once($_SERVER['DOCUMENT_ROOT']."/_func/function.common.php");

$asg_obj = new ASGMain();

$trouble_max = TROUBLE_MAX_LENGTH;
$trouble_max_kr = TROUBLE_MAX_LENGTH_KR;
$trouble_max_jp = TROUBLE_MAX_LENGTH_JP;
$trouble_max_en = TROUBLE_MAX_LENGTH_EN;

$page = array(//page define
	"EDIT_FAIL"=> "/admin/manage/trouble/list.php",
	"EDIT_SUCCESS"=> "/admin/manage/trouble/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_en = array(
	"NO_TROUBLETYPE" => "The type of counseling that does not exist.",
	"EMPTY_TYPE" => "Enter the type of consultation.",
	"LONG_TYPE" => "Your input is too long.(less than ".$trouble_max_en." characters)",
	"EDIT_OK" => "The type of consultation has been modified."
);

$msg_jp = array(
	"NO_TROUBLETYPE" => "存在しない相談タイプです。",
	"EMPTY_TYPE" => "相談の種類を入力してください。",
	"LONG_TYPE" => "あなたの入力が長すぎます。（".$trouble_max_jp."文字未満）",
	"EDIT_OK" => "相談の種類が変更されました。"
);

$msg_kr = array(
	"NO_TROUBLETYPE" => "존재하지 않는 상담유형입니다.",
	"EMPTY_TYPE" => "상담유형을 입력하세요.",
	"LONG_TYPE" => "상담유형은 한글 ".$trouble_max_kr."자, 영어 ".$trouble_max_en."자 이내로 입력해주세요.",
	"EDIT_OK" => "상담유형이 수정 되었습니다."
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if($asg_obj->GetTroubleTypeByTrbnum($_POST["edit_trbnum"]) == NULL){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['NO_TROUBLETYPE']."');
			location.href='".$page['EDIT_FAIL']."';
		</script>
	";
	exit(1);
}else if(strlen($_POST["edit_trbname"]) == 0) {
	echo "
		<script> 
			alert('".$msg[$msg_lang]['EMPTY_TYPE']."');
			location.href='".$page['EDIT_FAIL']."';
		</script>
	";
	exit(1);
}else if(strlen($_POST["edit_trbname"]) > $trouble_max) {
	echo "
		<script> 
			alert('".$msg[$msg_lang]['LONG_TYPE']."');
			location.href='".$page['EDIT_FAIL']."';
		</script>
	";
	exit(1);
} else {
	$res = $asg_obj->ModifyTroubleType($_POST["edit_trbnum"], $_POST["edit_trbname"]);

	if($res == "NOTROUBLETYPE") {// 해당 장애유형이 존재하지 않을 때
		echo "
			<script> 
				alert('".$msg[$msg_lang]['NO_TROUBLETYPE']."');
				location.href='".$page['EDIT_FAIL']."';
			</script>
		";
		exit(1);
	} else {
		echo "
			<script> 
				alert('".$msg[$msg_lang]['EDIT_OK']."');
				location.href='".$page['EDIT_SUCCESS']."';
			</script>
		";
	}
}