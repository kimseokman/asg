<?php
/**
 * Name : excel_report_hourly.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"시간대별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportByTime($_POST['start_time'], $_POST['end_time']);

$total = 0;

for($i=0;$i<24;$i++){
	$data[$i]->ccount = 0;
	$data[$i]->totaltime = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],11,2));
	$z = $t;

	$data[$z]->ccount++;
	$total++;
	
	//zisako 2014.1.23 통계데이터 -값 오류 수정
	$intervaltime = $row['intervaltime'];
	if($intervaltime<0) $intervaltime = 0;
	$data[$z]->totaltime +=$intervaltime;
}

$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]->ccount/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function getTimeFromSeconds($seconds){
	$h = sprintf("%02d", intval($seconds) / 3600);
	$tmp = $seconds % 3600;
	$m = sprintf("%02d", $tmp / 60);
	$s = sprintf("%02d", $tmp % 60);
	return $h.':'.$m.':'.$s;
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="90px" />
			<col width="110px" />
			<col width="194px" />
			<col width="*" />
			<col width="140px" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('204000.070') ?></th>
				<th><?php lang_print('204000.080') ?></th>
				<th><?php lang_print('204000.090') ?></th>
				<th><?php lang_print('204000.100') ?></th>
				<th><?php lang_print('204000.110') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		$totaltotaltime;
		for($i=0;$i<sizeof($data);$i++){
			$totaltotaltime += $data[$i]->totaltime;
		?>
			<tr>
				<td><? echo $i; ?></td>
				<td><? echo $data[$i]->ccount; ?></td>
				<td><? echo getTimeFromSeconds($data[$i]->totaltime); ?></td>
				<td><? echo getTimeFromSeconds($data[$i]->totaltime/$data[$i]->ccount); ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of : for($i=0;$i<sizeof($data);$i++)
		?>
			<tr class="bg_color_gray">
				<td class="text_mark"><?php lang_print('204000.115') ?></td>
				<td class="text_mark"><? echo $total; ?></td>
				<td class="text_mark"><? echo getTimeFromSeconds($totaltotaltime); ?></td>
				<td class="text_mark"><? echo getTimeFromSeconds($totaltotaltime/$total); ?></td>
				<td class="text_mark"><? echo $totalPer; ?>%</td>
			</tr>
		</tbody>
	</table>
</body> 
</html>