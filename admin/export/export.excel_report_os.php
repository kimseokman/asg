<?php
/**
 * Name : excel_report_os.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"고객OS별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportByOS($_POST['start_time'], $_POST['end_time']);

$total = 0;

$os_count = 14;
for($i=0;$i<$os_count;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){


	$index = $row['os'];

	if($index == 100)
		$index  = 11;
	else if($index == 101)
		$index = 12;
	else if($index == 102)
		$index = 13;

	$data[$index] = $row['cnt'];

	$total += intVal($row['cnt']);

}
$data[7] += $data[0]; // 기존에 저장안되었던 os 정보는 기타 os에 저장한다.


$totalPer = 0;
for($i=1;$i<sizeof($data);$i++){
	if($total != 0){
		//$per[$i] = $rate=round($data[$i]/$total*1000)/10;
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

$os[0] = "";
$os[1] = OS_WIN_98_EN;
$os[2] = OS_WIN_2000_EN;
$os[3] = OS_WIN_XP_EN;
$os[4] = OS_WIN_2003_EN;
$os[5] = OS_WIN_VISTA_EN;
$os[6] = OS_WIN_7_EN;
if($_SESSION['language'] == 'kr'){
	$os[7] = OS_UNKNOWN_KR;
}else{
	$os[7] = OS_UNKNOWN_EN;	
};
$os[8] = OS_WIN_8_EN;
$os[9] = OS_WIN_2008_EN;
$os[10] = OS_WIN_2012_EN;
$os[11] = OS_ANDROID_EN;
$os[12] = OS_MAC_EN;
$os[13] = OS_LINUX_EN;
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="180px" />
			<col width="220px" />
			<col width="*" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('206000.070') ?></th>
				<th><?php lang_print('206000.080') ?></th>
				<th><?php lang_print('206000.090') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=1;$i<sizeof($data);$i++){
		?>
			<tr>
				<td><? echo $os[$i]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of :  for($i=1;$i<sizeof($data);$i++)
		?>
			<tr class="bg_color_gray">
				<td class="text_mark"><?php lang_print('206000.100') ?></td>
				<td class="text_mark"><? echo $total; ?></td>
				<td class="text_mark"><? echo $totalPer; ?>%</td>
			</tr>
		</tbody>
	</table>
</body> 
</html>