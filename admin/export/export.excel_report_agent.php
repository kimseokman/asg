<?php
/**
 * Name : excel_report_agent.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"상담원별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportBySpt($_POST['start_time'], $_POST['end_time']);

$total = 0;
$cnt = mysql_num_rows($result);

$i=0;
while($row = mysql_fetch_array($result)){
	$data[$i] = $row['cnt'];
	$name[$i] = $row['sptid'];
	$totaltime[$i] = $row['totaltime'];
	$total += $row['cnt'];
	$i++;
}

$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}

function getTimeFromSeconds($seconds)
{
     $h = sprintf("%02d", intval($seconds) / 3600);
     $tmp = $seconds % 3600;
     $m = sprintf("%02d", $tmp / 60);
     $s = sprintf("%02d", $tmp % 60);
 
    return $h.':'.$m.':'.$s;
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="120px" />
			<col width="140px" />
			<col width="140px" />
			<col width="*" />
			<col width="140px" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('205000.070') ?></th>
				<th><?php lang_print('205000.080') ?></th>
				<th><?php lang_print('205000.090') ?></th>
				<th><?php lang_print('205000.100') ?></th>
				<th><?php lang_print('205000.110') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		$totaltotaltime;
		for($i=0;$i<sizeof($data);$i++){
			$totaltotaltime += $totaltime[$i];	
		?>
			<tr>
				<td><? echo $name[$i]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo getTimeFromSeconds($totaltime[$i]); ?></td>
				<td><? echo getTimeFromSeconds($totaltime[$i]/$data[$i]); ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of :  for($i=0;$i<sizeof($data);$i++)
		?>
			<tr class="bg_color_gray">
				<td class="text_mark"><?php lang_print('204000.115') ?></td>
				<td class="text_mark"><? echo $total; ?></td>
				<td class="text_mark"><? echo getTimeFromSeconds($totaltotaltime); ?></td>
				<td class="text_mark"><? echo getTimeFromSeconds($totaltotaltime/$total); ?></td>
				<td class="text_mark"><? echo $totalPer; ?>%</td>
			</tr>
		</tbody>
	</table>
</body> 
</html>