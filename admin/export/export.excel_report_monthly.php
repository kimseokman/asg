<?php
/**
 * Name : excel_report_monthly.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel;");   
header("Content-Disposition: attachment; filename=\"월별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportByMonth($_POST['year']);

$month_arr = $utils_obj->GetMonths();

$total = 0;
for($i=0;$i<12;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$tmp = explode("-",$row['starttime']);
	$t = intVal($tmp[1]);
	$z = --$t;
	$data[$z]++;
	$total++;
}


$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}

	$totalPer += $per[$i];
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="180px" />
			<col width="220px" />
			<col width="*" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('202000.010') ?></th>
				<th><?php lang_print('202000.020') ?></th>
				<th><?php lang_print('202000.030') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=0;$i<sizeof($data);$i++){
		?>
			<tr>
				<td><? echo $month_arr[$i+1]; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of : for($i=0;$i<sizeof($data);$i++)
		?>
			<tr class="bg_color_gray">
				<td class="text_mark"><?php lang_print('202000.040') ?></td>
				<td class="text_mark"><? echo $total; ?></td>
				<td class="text_mark"><? echo $totalPer; ?>%</td>
			</tr>
		</tbody>
	</table>
</body> 
</html>