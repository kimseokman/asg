<?php
/**
 * Name : excel_report_trouble.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"장애유형별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$stats_arr = $asg_obj->GetReportByTroubleType($_POST['start_time'], $_POST['end_time']);

?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="application/vnd.ms-excel; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="110px" />
			<col width="*" />
			<col width="120px" />
			<col width="130px" />
			<col width="100px" />
			<col width="100px" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('207000.070') ?></th>
				<th><?php lang_print('207000.080') ?></th>
				<th><?php lang_print('207000.090') ?></th>
				<th><?php lang_print('207000.100') ?></th>
				<th><?php lang_print('207000.110') ?></th>
				<th><?php lang_print('207000.120') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		$res1sum = 0;
		$res2sum = 0;
		$res3sum = 0;
		$res0sum = 0;
		$sumsum = 0;
		while(list($key, $val) = each($stats_arr)) {
		?>
			<tr>
				<td><? echo $val["trbname"]; ?></td>
				<td><? echo $val["res1"]; ?></td>
				<td><? echo $val["res2"]; ?></td>
				<td><? echo $val["res3"]; ?></td>
				<td><? echo $val["res0"]; ?></td>
				<td><? echo ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]); ?></td>
			</tr>
		<?
			$res1sum += $val["res1"];
			$res2sum += $val["res2"];
			$res3sum += $val["res3"];
			$res0sum += $val["res0"];
			$sumsum += ($val["res1"]+$val["res2"]+$val["res3"]+$val["res0"]);
		}// end of : while(list($key, $val) = each($stats_arr))
		?>
			<tr class="bg_color_gray">	
				<td class="text_mark"><?php lang_print('207000.130') ?></td>
				<td class="text_mark"><? echo $res1sum; ?></td>
				<td class="text_mark"><? echo $res2sum; ?></td>
				<td class="text_mark"><? echo $res3sum; ?></td>
				<td class="text_mark"><? echo $res0sum; ?></td>
				<td class="text_mark"><? echo $sumsum; ?></td>
			</tr>
		</tbody>
	</table>
</body> 
</html>