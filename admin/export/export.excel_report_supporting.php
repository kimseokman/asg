<?php
/**
 * Name : excel_report_supporting.php
 * Version : AnySupport Global
 * Date :  2015.06.16
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"report.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$isAdmin = $asg_obj->ConfirmAdmin();

$page = array(//page define
	"EXPORT_FAIL"=> "/admin/reports/term/list.php"
);

$msg_lang = $_SESSION['language'];

$msg_kr = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_DATA" => "원격 기록이 존재하지 않습니다. 다시 시도해주세요.",
	"NO_SELECTED" => "선택안됨"
);

$msg_en = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_DATA" => "원격 기록이 존재하지 않습니다. 다시 시도해주세요.",
	"NO_SELECTED" => "선택안됨"
);

$msg_jp = array(//message define
	"CONFIRM_ADMIN" => "Only administrators can use this function.",
	"NO_DATA" => "원격 기록이 존재하지 않습니다. 다시 시도해주세요.",
	"NO_SELECTED" => "선택안됨"
);

$msg = array(//message define
	"en" => $msg_en,
	"jp" => $msg_jp,
	"kr" => $msg_kr
);

if(!$isAdmin){
	echo "
		<script> 
			alert('".$msg[$msg_lang]['CONFIRM_ADMIN']."');
			location.href='".$page['EXPORT_FAIL']."';
		</script>
	";
	exit(1);
}

if($_POST['search_type'] != ""){
	$search_type = $_POST['search_type'];
}

if($_POST['search_val'] != ""){
	$search_val = $_POST['search_val'];
}

if($_POST['start_time'] != ""){
	$start_time = $_POST['start_time'];
}

if($_POST['end_time'] != ""){
	$end_time = $_POST['end_time'];
}

$report_arr = $asg_obj->GetReportExcel($search_type, $search_val, $start_time, $end_time);

function my_nl2br($str){
	$str = preg_replace("/\r/", "", $str);
	$str = preg_replace("/(\>[ ]*)\n/", ">\n", $str);
	$str = preg_replace("/((font|span|a)\>[ ]*|[^\>])\n/i", "\\1<br />\n", $str);
	return $str;
}

if(count($report_arr) == 0) {
	echo "
		<script> 
			alert('".$msg[$msg_lang]['NO_DATA']."');
			location.href='".$page['EXPORT_FAIL']."';
		</script>
	";
	exit(1);
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>			
			<col width="40px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="80px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="90px" />
			<col width="*" />
			<col width="100px" />
			<col width="120px" />
			<col width="120px" />
			<col width="90px" />
			<col width="100px" />
			<col width="90px" />
			<col width="90px" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('201010.200') ?></th>
				<th><?php lang_print('201010.210') ?></th>
				<th><?php lang_print('201010.220') ?></th>
				<th><?php lang_print('201010.230') ?></th>
				<th><?php lang_print('201010.240') ?></th>
				<th><?php lang_print('201010.250') ?></th>
				<th><?php lang_print('201010.260') ?></th>
				<th><?php lang_print('201010.270') ?></th>
				<th><?php lang_print('201010.280') ?></th>
				<th><?php lang_print('201010.290') ?></th>
				<th><?php lang_print('201010.300') ?></th>
				<th><?php lang_print('201010.310') ?></th>
				<th><?php lang_print('201010.320') ?></th>
				<th><?php lang_print('201010.330') ?></th>
				<th><?php lang_print('201010.340') ?></th>
				<th><?php lang_print('201010.350') ?></th>
				<th><?php lang_print('201010.360') ?></th>
				<th><?php lang_print('201010.370') ?></th>
			</tr>
		</thead>
		<tbody>
<?
$cnt = 1;
while(list($key, $val) = each($report_arr["datainfo"])) {
	$sTime = mktime(intVal(substr($val["starttime"],11,2)),intVal(substr($val["starttime"],14,2)),intVal(substr($val["starttime"],17,2)),intVal(substr($val["starttime"],5,2)),intVal(substr($val["starttime"],8,2)),intVal(substr($val["starttime"],0,4)));
	$eTime = mktime(intVal(substr($val["endtime"],11,2)),intVal(substr($val["endtime"],14,2)),intVal(substr($val["endtime"],17,2)),intVal(substr($val["endtime"],5,2)),intVal(substr($val["endtime"],8,2)),intVal(substr($val["endtime"],0,4)));

	$useTime = intVal($eTime) - intVal($sTime);

	if($useTime < 0){
		$useTime = $msg[$msg_lang]['UNKNOWN'];
	}else{
		$hours = intVal($useTime / 3600);//get hour
		$leftover = $useTime - ( $hours * 3600); 
		$min = intVal($leftover / 60);//get min
		$sec = $leftover - ( $min * 60);//get sec

		$useTime = str_pad($hours,"2","0",STR_PAD_LEFT).":".str_pad($min,"2","0",STR_PAD_LEFT).":".str_pad($sec,"2","0",STR_PAD_LEFT);
	}

	$user_os = $utils_obj->GetOS($val["user_os"]);
	$user_oper = $utils_obj->GetOperationLog($val["operationlog"]);
	$user_satis = $utils_obj->GetSatisfaction($val["satisfaction"]);
	$trouble_name = $msg[$msg_lang]["NO_SELECTED"];
	if($val["trbnum"] != 0){
		$trouble_name = $val["trbname"];
	}

	$report = my_nl2br($val["report"]);
?>
			<tr>
				<td><? echo $cnt++; ?></td>
				<td class="text_mark"><? echo $val["sptid"]; ?></td>
				<td class="text_mark"><? echo $val["sptname"]; ?></td>
				<td class="text_mark"><? echo $val["spt_pub_ip"]." / ".$val["spt_pri_ip"]; ?></td>
				<td class="text_mark"><? echo $val["spt_com_name"]; ?></td>
				<td><? echo $val["name"]; ?></td>
				<td class="text_mark"><? echo $val["user_pub_ip"]." / ".$val["user_pri_ip"]; ?></td>
				<td class="text_mark"><? echo $val["user_com_name"]; ?></td>
				<td class="text_mark"><? echo $val["email"]; ?></td>
				<td class="text_mark"><? echo $val["tel"]; ?></td>
				<td><? echo substr($val["starttime"], 0, -3); ?></td>
				<td><? echo $useTime; ?></td>
				<td><? echo $user_os; ?></td>
				<td class="text_mark"><? echo $trouble_name; ?></td>
				<td class="text_mark"><? echo $report; ?></td>
				<td><? echo $user_oper; ?></td>
				<td class="text_mark"><? echo $user_satis; ?></td>
				<td class="text_mark"><? echo $val["usercomment"]; ?></td>
			</tr>
<?
}//while(list($key, $val) = each($report_arr["datainfo"]))	
?>
		</tbody>
	</table>
</body> 
</html>