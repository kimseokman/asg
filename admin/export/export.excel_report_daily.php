<?php
/**
 * Name : excel_report_daily.php
 * Version : AnySupport Global
 * Date :  2015.06.17
 * Author : KSM
 */
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");   
header("Content-Disposition: attachment; filename=\"일별.xls\""); 
header("Content-Description: PHP4 Generated Data");
session_start();

$asg_obj = new ASGMain();
$utils_obj = new ASGUtils();

$result = $asg_obj->GetReportByDay($_POST['year'], $_POST['month']);

$total = 0;
if($year == ""){
	$tmp = mktime();
}else{
	$tmp = mktime(0,0,1,$month,1,$year);
}

$days = date("t",$tmp);

for($i=0;$i<$days;$i++){
	$data[$i] = 0;
}

while($row = mysql_fetch_array($result)){
	$t = intVal(substr($row['starttime'],8,2));
	$z = --$t;
	$data[$z]++;
	$total++;
}

$totalPer = 0;
for($i=0;$i<sizeof($data);$i++){
	if($total != 0){
		$per[$i] = $rate = $data[$i]/$total*100;
	}else{
		$per[$i] = 0;
	}
	$totalPer += $per[$i];
}
?>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style> 
br{mso-data-placement:same-cell;}
.bg_color_white{background-color:#FFF;}
.bg_color_gray{background-color:#F3F3F3;}
.text_mark{font-weight:bold;}
table td{text-align:center;}
</style>
</head>
<body class="bg_color_white"> 
	<table border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col width="180px" />
			<col width="220px" />
			<col width="*" />
		</colgroup>
		<thead>
			<tr class="bg_color_gray">
				<th><?php lang_print('203000.020') ?></th>
				<th><?php lang_print('203000.030') ?></th>
				<th><?php lang_print('203000.040') ?></th>
			</tr>
		</thead>
		<tbody>
		<?
		for($i=0;$i<sizeof($data);$i++){
		?>
			<tr>
				<td><? echo $i+1; ?></td>
				<td><? echo $data[$i]; ?></td>
				<td><? echo sprintf("%.2f",$per[$i]); ?>%</td>
			</tr>
		<?
		}// end of : for($i=0;$i<sizeof($data);$i++)
		?>
			<tr class="bg_color_gray">
				<td class="text_mark"><?php lang_print('203000.050') ?></td>
				<td class="text_mark"><? echo $total; ?></td>
				<td class="text_mark"><? echo $totalPer; ?>%</td>
			</tr>
		</tbody>
	</table>
</body> 
</html>