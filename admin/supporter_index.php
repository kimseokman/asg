<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/_func/function.admin_common.php");

$wz['gid']  = "HM";
$wz['gtt']  = lang('IDX.100'); //"Welcome to Anysupport";

include_once("./header.php");
?>
<!-- 내용시작 -->

<div class="admin_top">
	<p class="admin_top_img"><img src="/admin/img/admin_main_circle_img.gif" alt="" /></p>
	<dl class="admin_top_dl">
		<dt><?php lang_print('IDX.080') ?></dt>
		<dd><?php lang_print('IDX.090') ?></dd>
	</dl>
</div>
<ul class="admin_ul">
	<li class="admin_bg_div01">
		<div class="admin_center_div">
			<p class="admin_center_p01"><?php lang_print('IDX.100') ?></p>
			<p class="admin_center_btn">
				<a href="#" class="start_session_btn">
					<!--<?php lang_print('BTN.start_session') ?>-->
					<span class="">Start Session</span>
				</a>
			</p>
			<p class="admin_center_bottom_p"><?php lang_print('IDX.120') ?></p>
		</div>
	</li>
	<li class="admin_bg_div02">
		<div class="admin_center_div">
			<p class="admin_center_p02"><?php lang_print('IDX.130') ?></p>
			<p class="admin_center_btn">
				<a href="#" class="download_btn">
					<?php lang_print('BTN.download') ?>
				</a>
			</p>
			<p class="admin_center_bottom_p"><?php lang_print('IDX.150') ?></p>
		</div>
	</li>
</ul>
<!-- 내용끝 -->
<?
include_once("./footer.php");
?>
<!--div class="fixed_dim"></div>
<div class="admin_index_pop">
	<dl class="admin_pop_dl">
		<dt><?php lang_print('IDX.010', 'Jamie', '14') ?></dt>
		<dd><?php lang_print('IDX.020') ?></dd>
	</dl>
	<ul class="admin_pop_ul">
		<li><a href="/admin/manage/billing/plan_info.php" class="pop_link_a01"><?php lang_print('IDX.030') ?></a></li>
		<li class="last"><a href="/admin/index02.php" class="pop_link_a02"><?php lang_print('IDX.040') ?></a></li>
	</ul>
</div-->