/**
 * manage_trouble.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.trouble_name_input').on('keydown', edit_trouble_name_confirm_enter);
	$('.error_edit_confirm_yes_btn').on('click', edit_trouble_name);
})(jQuery);

function edit_trouble_name_confirm_enter(e){
	if(e.keyCode == 13){//case by 'ENTER'
		e.preventDefault();

		var trbname = $(this).val();//get trbname
		
		var class_arr = $(this).attr('class').split (" ");
		var trbnum = class_arr[2].split ("_")[1];//get trbnum
		
		$('input[name=edit_trbnum]').attr('value', trbnum);
		$('input[name=edit_trbname]').attr('value', trbname);
		$('input[name=edit_flag]').attr('value', "STRING_TRUE");

		document.trouble_edit_form.submit();
	}
}

function edit_trouble_name(e){
	e.preventDefault();

	document.trouble_edit_form.action = "/admin/_func/function.edit_trouble_type.php";
	document.trouble_edit_form.submit();
}

function edit_trouble_name_confirm(e, trbnum){
	e.preventDefault();

	var trbname = $('.trbnum_' + trbnum).val();

	$('input[name=edit_trbnum]').attr('value', trbnum);
	$('input[name=edit_trbname]').attr('value', trbname);
	$('input[name=edit_flag]').attr('value', "STRING_TRUE");

	document.trouble_edit_form.submit();
}