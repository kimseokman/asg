/**
 * reports_monthly.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.report_monthly_search_btn').on('click', search_monthly_report);
	$('.report_monthly_export_btn').on('click', export_monthly_report);
})(jQuery);

function export_monthly_report(e){
	e.preventDefault();

	var year = $('select[name=year]').val();

	var $form = $('<form></form>');
	$form.attr('action', '../../export/export.excel_report_monthly.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_year = $('<input type="hidden" value="'+year+'" name="year" />');
	
	$form.append(input_year);
	
	$form.submit();
}

function search_monthly_report(e){
	e.preventDefault();

	var year = $('select[name=year]').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_monthly.php',
		data: {year: year},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_monthly").html(res);
		}
	});
}