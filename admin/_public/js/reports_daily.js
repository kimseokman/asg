/**
 * reports_daily.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.report_daily_search_btn').on('click', search_daily_report);
	$('.report_daily_export_btn').on('click', export_daily_report);
})(jQuery);

function export_daily_report(e){
	e.preventDefault();

	var year = $('select[name=year]').val();
	var month = $('select[name=month]').val();

	var $form = $('<form></form>');
	$form.attr('action', '../../export/export.excel_report_daily.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_year = $('<input type="hidden" value="'+year+'" name="year" />');
	var input_month = $('<input type="hidden" value="'+month+'" name="month" />');
	
	$form.append(input_year).append(input_month);
	
	$form.submit();
}

function search_daily_report(e){
	e.preventDefault();

	var year = $('select[name=year]').val();
	var month = $('select[name=month]').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_daily.php',
		data: {year: year, month: month},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_daily").html(res);
		}
	});
}