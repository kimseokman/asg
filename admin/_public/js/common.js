/**
 * common.js
 * 2015.05.27 | KSM
 */

(function($){
	//common page
	$('.refresh_btn').on('click', page_refresh);
	$('.logout_btn').on('click', logout);//click logout button in header

	//report page
	$('#startMonth').change({make_type: "0_DAY"}, ajax_selectbox_make_start_days);
	$('#endMonth').change({make_type: "0_DAY"}, ajax_selectbox_make_end_days);
	$('.searchType').change(ajax_selectbox_search_type);

	$('.search_condition_today').on('click', {make_type: "0_DAY"}, make_search_condition);
	$('.search_condition_yesterday').on('click', {make_type: "1_DAY"}, make_search_condition);
	$('.search_condition_3day').on('click', {make_type: "3_DAY"}, make_search_condition);
	$('.search_condition_week').on('click', {make_type: "7_DAY"}, make_search_condition);
	$('.search_condition_month').on('click', {make_type: "1_MONTH"}, make_search_condition);
	$('.search_condition_3month').on('click', {make_type: "3_MONTH"}, make_search_condition);
	$('.search_condition_6month').on('click', {make_type: "6_MONTH"}, make_search_condition);
	$('.search_condition_year').on('click', {make_type: "1_YEAR"}, make_search_condition);

	//if($('input[name=auto_agent_search_flag]').val() == 'TRUE'){
	//	auto_agent_list();
	//}
})(jQuery);

function auto_agent_list(){
	var search_type = $('input[name=search_type]').val();
	var search_val = $('input[name=search_val]').val();
	var start_time = $('input[name=start_time]').val();
	var end_time = $('input[name=end_time]').val();
	var page = $('input[name=page]').val();
	
	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_supporting.php',
		data: {page: page, search_type : search_type, search_val: search_val, start_time : start_time, end_time : end_time},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_supporting").html(res);
		}
	});
}

function ajax_selectbox_search_type(e){
	var search_type = $(this).val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_value.php',
		data: {type: search_type},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_search_type").html(res);
		}
	});
}

function make_search_condition(e){
	e.preventDefault();

	var make_type = e.data.make_type;
	
	ajax_btn_make_start_year(make_type, //step1
		ajax_btn_make_start_month(make_type,  //step2
			ajax_btn_make_start_day(make_type))); //step3
}

function ajax_btn_make_start_day(make_type){
	var search_month = $('#startMonth').val();
	var search_year = $('#startYear').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_days.php',
		data: {type: make_type, year: search_year, month: search_month},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$("#startDay").html(res);
		}
	});
}

function ajax_btn_make_start_month(make_type){
	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_months.php',
		data: {type: make_type},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$("#startMonth").html(res);
		}
	});
}

function ajax_btn_make_start_year(make_type){
	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_years.php',
		data: {type: make_type},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$("#startYear").html(res);
		}
	});
}

function ajax_selectbox_make_end_days(e){
	var search_month = $(this).val();
	var search_year = $('#endYear').val();

	var make_type = e.data.make_type;

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_days.php',
		data: {type: make_type, year: search_year, month: search_month},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$("#endDay").html(res);
		}
	});
}

function ajax_selectbox_make_start_days(e){
	var search_month = $(this).val();
	var search_year = $('#startYear').val();

	var make_type = e.data.make_type;

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.search_condition_days.php',
		data: {type: make_type, year: search_year, month: search_month},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$("#startDay").html(res);
		}
	});
}

function page_refresh(e){
	e.preventDefault();

	location.reload();
}

function logout(){//go to logout
	document.location='/_func/function.logout.php';
}