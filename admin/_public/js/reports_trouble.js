/**
 * reports_trouble.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.report_trouble_search_btn').on('click', search_trouble_report);
	$('.report_trouble_export_btn').on('click', export_trouble_report);
})(jQuery);

function export_trouble_report(e){
	e.preventDefault();

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var $form = $('<form></form>');
	$form.attr('action', '../../export/export.excel_report_trouble.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(input_start_time).append(input_end_time);
	
	$form.submit();
}

function search_trouble_report(e){
	e.preventDefault();

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_trouble.php',
		data: {start_time : start_time, end_time : end_time},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_trouble").html(res);
		}
	});
}