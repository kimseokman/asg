/**
 * manage_agent.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.agent_search_btn').on('click', agent_list_search);

	$('.agent_change_pw').on('keyup', ajax_validate_agent_pw);
	$('.agent_retype_pw').on('keyup', ajax_validate_agent_pw);

	$('.agent_edit_btn').on('click', agent_info_modify);

	$('.agent_create_id').on('keyup', ajax_validate_agent_id);

	$('.agent_create_btn').on('click', agent_create);
})(jQuery);

function agent_list_search(e){
	e.preventDefault();

	document.agent_list_search.submit();
}

function ajax_validate_agent_pw(e){
	var password = $('.agent_change_pw').val();
	var retype_password = $('.agent_retype_pw').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "password", change_pw: password, change_pw_length: password.length, retype_pw: retype_password, retype_pw_length: retype_password.length},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.error.change_pw_flag){
				$(".agent_change_pw").removeClass("common_input");
				$(".agent_change_pw").addClass("common_input_error");
			}else{
				$(".agent_change_pw").removeClass("common_input_error");
				$(".agent_change_pw").addClass("common_input");
			}

			$(".agent_change_pw_res").html(res.error.change_pw_msg);

			if(res.error.retype_pw_flag){
				$(".agent_retype_pw").removeClass("common_input");
				$(".agent_retype_pw").addClass("common_input_error");

				
			}else{
				$(".agent_retype_pw").removeClass("common_input_error");
				$(".agent_retype_pw").addClass("common_input");
			}

			$(".agent_retype_pw_res").html(res.error.retype_pw_msg);
		}
	});
}

function agent_info_modify(e){
	e.preventDefault();

	document.agent_info_from.submit();
}

function agent_create(e){
	e.preventDefault();

	document.agent_create_info_from.submit();
}

function ajax_validate_agent_id(e){
	var agent_id = $('.agent_create_id').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "id", id: agent_id, id_length: agent_id.length},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;
						
			if(res.error.id_flag){
				$(".agent_create_id").removeClass("common_input");
				$(".agent_create_id").addClass("common_input_error");
			}else{
				$(".agent_create_id").removeClass("common_input_error");
				$(".agent_create_id").addClass("common_input");
			}

			$(".agent_create_id_res").html(res.error.id_msg);
		}
	});
}