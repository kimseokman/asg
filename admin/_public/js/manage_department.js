/**
 * manage_department.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.department_add_row_btn').on('click', department_add_row);//department list add row

	if($('input[name=dept_page_status]').val() == 'ADD_ROW'){		//create department input auto focus
		$('input[name=create_dept_name]').focus();
	}

	$('input[name=create_dept_name]').on('keydown', create_department);	//press enter event

	if($('input[name=dept_page_status]').val() == 'EDIT_ROW'){		//edit department input auto focus
		$('.edit_on_input').focus();
	}

	$('.edit_on_input').on('keydown', edit_department);

	$('.department_popup_cancel_btn').on('click', department_popup_cancel);//popup cancel in department
	$('.department_popup_yes_btn').on('click', department_popup_yes);//popup yes in department
})(jQuery);

function department_del_row(e, depnum){//use javascript - del row in department page
	e.preventDefault();

	$('input[name=dept_page_status]').attr('value', 'DEL_ROW');
	$('input[name=depnum]').attr('value', depnum); 

	document.create_department_row_form.action = "";
	document.create_department_row_form.submit();
}



function department_edit_row(e, depnum){//use javascript - edit row in department page
	e.preventDefault();

	$('input[name=dept_page_status]').attr('value', 'EDIT_ROW');
	$('input[name=depnum]').attr('value', depnum); 

	document.create_department_row_form.action = "";
	document.create_department_row_form.submit();
}

function department_add_row(e){
	e.preventDefault();	

	$('input[name=dept_page_status]').attr('value', 'ADD_ROW'); 

	document.create_department_row_form.action = "";
	document.create_department_row_form.submit();
}

function create_department(e){
	if(e.keyCode == 13){//case by 'ENTER'
		e.preventDefault();
		
		document.create_department_row_form.action = "/admin/_func/function.create_department.php";
		document.create_department_row_form.submit();
	}
}

function edit_department(e){
	if(e.keyCode == 13){//case by 'ENTER'
		e.preventDefault();

		$('input[name=edit_dep_name').attr('value', $('.edit_on_input').val());

		document.create_department_row_form.action = "/admin/_func/function.edit_department.php";
		document.create_department_row_form.submit();
	}
}

function department_popup_cancel(e){
	e.preventDefault();

	$('input[name=dept_page_status]').attr('value', 'NORMAL');
	$('input[name=depnum]').attr('value', null); 

	document.create_department_row_form.action = "";
	document.create_department_row_form.submit();
}

function department_popup_yes(e){
	e.preventDefault();

	document.create_department_row_form.action = "/admin/_func/function.delete_department.php";
	document.create_department_row_form.submit();
}