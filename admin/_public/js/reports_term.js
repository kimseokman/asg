/**
 * reports_term.js
 * 2015.05.27 | KSM
 */

(function($){
	$('.report_term_search_btn').on('click', search_term_report);
	$('.report_term_export_btn').on('click', export_term_report);

	$('.memory_agent_list_btn').on('click', memory_agent_list);
})(jQuery);

function memory_agent_list(e){
	e.preventDefault();

	document.search_memory.submit();
}

function detail_agent_info(e, report_num){
	e.preventDefault();

	var page = $('.report_supporting_page_list li.on a').text();
	var search_type_val = $('.searchType').val();
	var search_type = "";
	if(search_type_val==1){
		search_type = "sptid";
	}
	else if(search_type_val==2){
		search_type = "sptname";
	}
	else if(search_type_val==3){
		search_type = "operationlog";
	}
	else if(search_type_val==4){
		search_type = "name";
	}

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var search_val = "";
	if(search_type_val==3){//case by operationlog
		search_val = $(':radio[name="search_val"]:checked').val();
	}
	else{
		search_val = $('.search_val').val();
	}

	var $form = $('<form></form>');
	$form.attr('action', './info.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_search_type = $('<input type="hidden" value="'+search_type+'" name="search_type" />');
	var input_search_val = $('<input type="hidden" value="'+search_val+'" name="search_val" />');
	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	var input_report_num = $('<input type="hidden" value="'+report_num+'" name="report_num" />');
	var input_page = $('<input type="hidden" value="'+page.trim()+'" name="page" />');
	
	$form.append(input_search_type).append(input_search_val).append(input_start_time).append(input_end_time).append(input_report_num).append(input_page);
	
	$form.submit();
}

function search_term_report_pagination(e, page_num){
	e.preventDefault();
	
	var page = page_num;
	var search_type_val = $('.searchType').val();
	var search_type = "";
	if(search_type_val==1){
		search_type = "sptid";
	}else if(search_type_val==2){
		search_type = "sptname";
	}else if(search_type_val==3){
		search_type = "operationlog";
	}else if(search_type_val==4){
		search_type = "name";
	}

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var search_val = "";
	if(search_type_val==3){//case by operationlog
		search_val = $(':radio[name="search_val"]:checked').val();
	}else{
		search_val = $('.search_val').val();
	}
	
	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_supporting.php',
		data: {page: page, search_type : search_type, search_val: search_val, start_time : start_time, end_time : end_time},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_supporting").html(res);
		}
	});
}

function search_term_report(e){
	e.preventDefault();

	var search_type_val = $('.searchType').val();
	var search_type = "";
	if(search_type_val==1){
		search_type = "sptid";
	}else if(search_type_val==2){
		search_type = "sptname";
	}else if(search_type_val==3){
		search_type = "operationlog";
	}else if(search_type_val==4){
		search_type = "name";
	}

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var search_val = "";
	if(search_type_val==3){//case by operationlog
		search_val = $(':radio[name="search_val"]:checked').val();
	}else{
		search_val = $('.search_val').val();
	}
	
	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.report_supporting.php',
		data: {search_type : search_type, search_val: search_val, start_time : start_time, end_time : end_time},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			$(".ajax_report_supporting").html(res);
		}
	});
}

function export_term_report(e){
	e.preventDefault();

	var search_type_val = $('.searchType').val();
	var search_type = "";
	if(search_type_val==1){
		search_type = "sptid";
	}else if(search_type_val==2){
		search_type = "sptname";
	}else if(search_type_val==3){
		search_type = "operationlog";
	}else if(search_type_val==4){
		search_type = "name";
	}

	var startYear = $('#startYear').val();
	var startMonth = $('#startMonth').val();
	var startDay = $('#startDay').val();
	var start_time = startYear +"-"+ startMonth +"-"+ startDay;
	var endYear = $('#endYear').val();
	var endMonth = $('#endMonth').val();
	var endDay = $('#endDay').val();
	var end_time = endYear +"-"+ endMonth +"-"+ endDay;

	var search_val = "";
	if(search_type_val==3){//case by operationlog
		search_val = $(':radio[name="search_val"]:checked').val();
	}else{
		search_val = $('.search_val').val();
	}

	var $form = $('<form></form>');
	$form.attr('action', '../../export/export.excel_report_supporting.php');
	$form.attr('method', 'post');
	$form.appendTo('body');

	var input_search_type = $('<input type="hidden" value="'+search_type+'" name="search_type" />');
	var input_search_val = $('<input type="hidden" value="'+search_val+'" name="search_val" />');
	var input_start_time = $('<input type="hidden" value="'+start_time+'" name="start_time" />');
	var input_end_time = $('<input type="hidden" value="'+end_time+'" name="end_time" />');
	
	$form.append(input_search_type).append(input_search_val).append(input_start_time).append(input_end_time);
	
	$form.submit();
}