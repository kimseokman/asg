/**
 * manage_billing.js
 * 2015.05.27 | KSM
 */

(function($){
	$('#admin_change_pw').on('keyup', ajax_validate_admin_pw);
	$('#admin_retype_pw').on('keyup', ajax_validate_admin_pw);

	$('.admin_info_save_btn').on('click', admin_info_save);
	$('.admin_info_cancel_btn').on('click', {page: "admin_info"}, move_back);
})(jQuery);

function ajax_validate_admin_pw(e){
	var password = $('#admin_change_pw').val();
	var retype_password = $('#admin_retype_pw').val();

	$.ajax({
		type: "POST",
		url:'../../ajax/ajax.validate.php',
		data: {check_type: "password", change_pw: password, change_pw_length: password.length, retype_pw: retype_password, retype_pw_length: retype_password.length},
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			var res = data;

			if(res.error.change_pw_flag){
				$("#admin_change_pw").removeClass("common_input");
				$("#admin_change_pw").addClass("common_input_error");
			}else{
				$("#admin_change_pw").removeClass("common_input_error");
				$("#admin_change_pw").addClass("common_input");
			}

			$(".input_ul_right_area02").html(res.error.change_pw_msg);

			if(res.error.retype_pw_flag){
				$("#admin_retype_pw").removeClass("common_input");
				$("#admin_retype_pw").addClass("common_input_error");

				
			}else{
				$("#admin_retype_pw").removeClass("common_input_error");
				$("#admin_retype_pw").addClass("common_input");
			}

			$(".input_ul_right_area03").html(res.error.retype_pw_msg);
		}
	});
}

function admin_info_save(e){
	e.preventDefault();

	document.admin_info.submit();//submit
}

function move_back(e){
	e.preventDefault();
	if(e.data.page == 'admin_info'){
		document.location = '/admin/manage/billing/billing_index.php';
	}
}