		</div>
	</div>
	<!--내용시작 -->
<?
	$home_url = "";
	if($_SESSION['usertype'] == 'ADMIN'){
		$home_url = ADMIN_INDEX;
	}else if($_SESSION['usertype'] == 'SUPPORTER'){
		$home_url = SUPPORTER_INDEX;
	}else{
		$home_url = GUEST_INDEX;
	}

	$pop_lang = "en";//default english
	if($_SESSION['language'] != NULL){
		$pop_lang = $_SESSION['language'];
	}

	$document_url = "/index/".$pop_lang."/info/manual.php";
	$contact_url = "/index/".$pop_lang."/help/contact_us.php";
?>
		<div id="footer">
			<div class="foot">
				<div class="foot_right_div">
					<ul class="foot_menu">
						<li><a href="<? echo $contact_url; ?>" target="_blank"><?php lang_print('LOB.030') ?></a></li>
						<li><a href="<? echo $document_url; ?>" target="_blank"><?php lang_print('LOB.020') ?></a></li>
						<li class="last"><a href="<? echo $home_url; ?>"><?php lang_print('LOB.010') ?></a></li>
					</ul>
					<dl class="foot_right_dl">
						<dt><?php lang_print('LOB.040') ?></dt>
						<dd><?php lang_print('LOB.050') ?></dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
	<!-- 내용끝 -->
	<script type="text/javascript" src="/_lib/_inc/highcharts/4.1.6/highcharts.js"></script>
	<script type="text/javascript" src="/_lib/_inc/highcharts/4.1.6/modules/exporting.js"></script>
	
	<script type="text/javascript" src="/admin/_public/js/common.js"></script>
	<?
	$inc_file_name = $utils_obj->GetAdminPart($current_url);
	$link_js = "/admin/_public/js/".$inc_file_name.".js";
	
	$exist_flag = file_exists($_SERVER['DOCUMENT_ROOT'].$link_js);
	if($exist_flag){
	?>
	<script type="text/javascript" src="<? echo $link_js; ?>"></script>
	<?
	}//end of : if($exist_flag)
	?>
</body>
</html>